<?php
include_once( 'cabecalho.php' );
include_once( 'model/usuario.php' );

$tipo = $_GET[ 'tipo' ];

if ( $tipo === "administradores" ) {
	$administradores = listarAdministradoresBD( $conexao );	

	if( !empty($_GET[ 'filtro' ]) ){

		if( $_GET[ 'filtro' ] === 'pesquisar' ){
			unset($administradores);
			$nivel = 1;
			$administradores = filtarUsuarioNome($conexao, $nivel, $_POST['Filtro_nome']);
		}

		if( $_GET[ 'filtro' ] === 'nome' ){

			function OrdenarapelidoCrescente($a, $b) {
				return $a['apelido'] > $b['apelido'];
			}
			usort($administradores, 'OrdenarapelidoCrescente');

		}

		if( $_GET[ 'filtro' ] === 'email' ){

			switch ($ultimOrdenamento) {
				default:
					function OrdenarNomeCrescente($a, $b) {
						return $a['email'] > $b['email'];
					}

					OrdenarNomeCrescente($a, $b);
					usort($administradores, 'OrdenarNomeCrescente');
					$ultimOrdenamento = 'Crescente';
					break;
			}

		}
	}
?>

	<div class="container">
		<h3>Internos</h3>		
		
		<div class="row">
			<div class="input-field col s12">
				<form class="col s12" action="listar.php?tipo=administradores&filtro=pesquisar" method="post">
				  <div class="input-field col s6">
					<input type="text" name="Filtro_nome" placeholder="Nome do Interno" value="<?=$_POST['Filtro_nome']?>">
				  </div>
				  <div class="input-field col s1">
					<button class="btn2 waves-effect waves-light" type="submit"> <i class="material-icons">search</i> </button>
				  </div>				  
				</form>
			</div>
		</div>
	
	<div class="row">
	<table>
        <thead>             
          <tr>              
			<th><a href="listar.php?tipo=administradores&filtro=nome" title="Ordenar por Nome" class=" white-text">Nome</a> </th>
			<th><a href="listar.php?tipo=administradores&filtro=email" title="Ordenar por E-mail" class=" white-text">E-mail</a></th>
            <th>Ação</th>
          </tr>
        </thead>
        <tbody>

	<?php
	foreach ( $administradores as $administradores ) {

		if($administradores['ativo'] == 1){
			$color = 'red-text';
		}
		if($administradores['ativo'] == 0){
			$color = 'white-text';
		}
		?>

          <tr>
            <td><a  class="<?= $color?> modal-trigger" href="#modalVisualizar<?=$administradores['id']?>">
            <?=$administradores['nome']?>
            </a></td>
            <td><a class="<?= $color?> modal-trigger" href="#modalVisualizar<?=$administradores['id']?>">
            <?=$administradores['email']?>
            </a></td>
            <td align="center">
            	<a href="usuario.php?acao=alterar&token=<?=$administradores['id']?>" title="Alterar"><i class="material-icons prefix white-text">edit</i></a>
				<?php $id = $administradores['id'];
				if($administradores['ativo']== 0){
					echo '<a href="listar.php?acao=bloq&token='.$id.'" title="Bloquear"><i class="material-icons prefix white-text">block</i></a>';
				}
				if($administradores['ativo']== 1){
					echo '<a href="listar.php?acao=hab&token='.$id.'" title="Habilitar"><i class="material-icons prefix white-text">check_circle</i></a>';
				}?>
         		<a class="modal-trigger Link" href="#modal<?=$administradores['id']?>" title="Excluir"><i class="material-icons prefix amber-text text-darken-1">cancel</i></a>
         		</td>

          </tr>



        <div id="modalVisualizar<?=$administradores['id']?>" class="modal" style=" background-color: #008aae;">
        	<i class="material-icons right modal-close" style="padding: 15px;">close</i>
			<div class="container">	
        		
				<div class="row ">
				<p align="center">Visualizador de Usuario.</p>

				<p>Nome Completo: <?=$administradores['nome']?></p>
				<p>Nome Sistema: <?=$administradores['apelido']?></p>
				<p>CPF: <?=mask($administradores['cpf'],'###.###.###-##')?></p>
				<p>E-mail: <?=$administradores['email']?></p>
				<p>Ativo: <?php switch ($administradores['ativo']) {
										case 0:
											echo " Ativado ";
											break;
										case 1:
											echo " Desativado ";
											break;
									}?></p>
				<p>Tipo: <?php $tipo = listarTiposBD( $conexao );
									if($tipo[0]["id"] === $administradores[ "id_tipo" ]) {
										echo $tipo[0]["nome"];
									} else { echo 'Tipo de usuario não Encontrada';}?></p>
				
				<p>Data de Registro: <?= date('d/m/Y', strtotime($administradores['datadecricao']))?></p>

				<?php $avancado = listarUsuarioAvancadoBD( $conexao, 29 );
				if(!empty($avancado)){?>
				<p>Formação: <?=$avancado[0]['formacao']?></p>
				<p>Qualificação: <?=$avancado[0]['qualificacao']?></p>
				<?php  } ?>
				</div>
			</div>

		  </div>

        <div id="modal<?=$administradores['id']?>" class="modal bottom-sheet">

			<div class="row">

				<div class="col s12 m6">

					<div class="modal-content">
					  <h4 class="grey-text text-darken-2">Deseja excluir <?=$administradores['nome']?>?</h4>
					  <p class="grey-text text-darken-2">Essa ação não poderá ser desfeita.</p>
					</div>

				</div>


				<div class="col s12 m6 Top">

					<div class="modal-footer">
						<a href="listar.php?tipo=administradores" class="btn waves-effect waves-light">Voltar</a>
						<a href="controller/usuario.php?acao=excluir&token=<?=$administradores['id']?>" class="modal-close btn2 waves-effect waves-light">Eu aceito</a>
					</div>

				</div>

			</div>

		  </div>
		<?php }?>
		</tbody>
	</table>
	</div>

	<div class="row">
		<div class="col s6 m3 offset-m1" align="center">
			<a href="dashboard.php" class="btn waves-effect waves-light">Voltar</a>
		</div>
		<div class="col s6 m6">
			<a href="usuario.php?acao=novo&tipo=administradores" class="btn2 waves-effect waves-light right">Novo Cadastro</a> </div>
		</div>
	</div>

	</div>

<?php }

if ( $tipo === "funcionarios" ) {
	$funcionarios = listarFuncionariosBD( $conexao );
	?>

	<div class="container">

		<h3>Funcionários</h3>

	<div class="row">
	<table> 
        <thead>
          <tr>
              <th>Nome</th>
              <th>E-mail</th>
              <th>Ação</th>
          </tr>
        </thead>

	<?php
	foreach ( $funcionarios as $funcionarios ) {
		if($funcionarios['ativo'] == 1){
			$color = 'red-text';
		}
		if($funcionarios['ativo'] == 0){
			$color = 'white-text';
		}
		?>

		<tbody>
          <tr>
            <td><a  class="<?= $color?> modal-trigger" href="#modalVisualizar<?=$funcionarios['id']?>"><?=$funcionarios['nome']?></a></td>
            <td><a  class="<?= $color?> modal-trigger" href="#modalVisualizar<?=$funcionarios['id']?>"><?=$funcionarios['email']?></a></td>
            <td align="center">
            	<a href="usuario.php?acao=alterar&token=<?=$funcionarios['id']?>" title="Alterar"><i class="material-icons prefix white-text">edit</i></a>
				<?php $id = $funcionarios['id'];
				if($funcionarios['ativo']== 0){
					echo '<a href="listar.php?acao=bloq&token='.$id.'" title="Bloquear"><i class="material-icons prefix white-text">block</i></a>';
				}
				if($funcionarios['ativo']== 1){
					echo '<a href="listar.php?acao=hab&token='.$id.'" title="Habilitar"><i class="material-icons prefix white-text">check_circle</i></a>';
				}?>
         		<a class="modal-trigger Link" href="#modal<?=$funcionarios['id']?>" title="Excluir"><i class="material-icons prefix amber-text text-darken-1">cancel</i></a>
         		</td>
          </tr>
        </tbody>
        <div id="modalVisualizar<?=$funcionarios['id']?>" class="modal" style=" background-color: #008aae;">

			<div class="container">
				<div class="row ">
				<p>Visualizador de Usuario.</p>

				<p>Cod.: <?=$funcionarios['id']?></p>
				<p>Nome Completo: <?=$funcionarios['nome']?></p>
				<p>Nome Sistema: <?=$funcionarios['apelido']?></p>
				<p>CPF: <?=mask($funcionarios['cpf_cnpj'],'###.###.###-##')?></p>
				<p>E-mail: <?=$funcionarios['email']?></p>				
				<p>Ativo: <?php switch ($funcionarios['ativo']) {
										case 0:
											echo " Ativado ";
											break;
										case 1:
											echo " Desativado ";
											break;
									}?></p>
				<p>Tipo: <?php $tipo = listarTiposBD( $conexao );
									if($tipo[0]["id"] === $funcionarios[ "id_tipo" ]) {
										echo $tipo[0]["nome"];
									} else { echo 'Tipo de usuario não Encontrada';}?></p>
				
				<p>Data de Registro: <?= date('d/m/Y', strtotime($funcionarios['datadecricao']))?></p>

				</div>
			</div>

		  </div>
        <div id="modal<?=$funcionarios['id']?>" class="modal bottom-sheet">

			<div class="row">

				<div class="col s12 m6">

					<div class="modal-content">
					  <h4 class="grey-text text-darken-2">Deseja excluir <?=$funcionarios['nome']?>?</h4>
					  <p class="grey-text text-darken-2">Essa ação não poderá ser desfeita.</p>
					</div>

				</div>


				<div class="col s12 m6 Top">

					<div class="modal-footer">
						<a href="listar.php?tipo=funcionarios" class="btn waves-effect waves-light">Voltar</a>
						<a href="controller/usuario.php?acao=excluir&token=<?=$funcionarios['id']?>" class="modal-close btn2 waves-effect waves-light">Eu aceito</a>
					</div>

				</div>

			</div>

		  </div>
		<?php } ?>
	</table>
	</div>

	<div class="row">
		<div class="col s6 m3 offset-m1" align="center">
			<a href="dashboard.php" class="btn waves-effect waves-light">Voltar</a>
		</div>
	</div>

	</div>
<?php }

if ( $tipo === "empresas" ) {
	//$empresas = listarEmpresasBD( $conexao );
	$empresas = listarSedeEmpresasBD( $conexao );


	if( !empty($_GET[ 'filtro' ]) ){

		if( $_GET[ 'filtro' ] === 'pesquisar' ){
			unset($empresas);
			$nivel = 3;
			$empresas = filtarUsuario($conexao, $nivel, $_POST['Filtro_nome']);
		}

		if( $_GET[ 'filtro' ] === 'nome' ){

			function OrdenarapelidoCrescente($a, $b) {
				return $a['apelido'] > $b['apelido'];
			}

			usort($empresas, 'OrdenarapelidoCrescente');

		}

		if( $_GET[ 'filtro' ] === 'email' ){

			switch ($ultimOrdenamento) {

				default:
					function OrdenarNomeCrescente($a, $b) {
						return $a['email'] > $b['email'];
					}

					OrdenarNomeCrescente($a, $b);

					usort($empresas, 'OrdenarNomeCrescente');

					$ultimOrdenamento = 'Crescente';
					break;

			}

		}
	}
	?>
	<div class="container">

		<h3>Empresas</h3>

	<div class="row">
        <div class="input-field col s12">
        	<form class="col s12" action="listar.php?tipo=empresas&filtro=pesquisar" method="post">
      		  <div class="input-field col s6">
       		  	<input type="text" name="Filtro_nome" placeholder="Nome da Empresa" value="<?=$_POST['Filtro_nome']?>">
       		  </div>
       		  <div class="input-field col s1">
       		  	<button class="btn2 waves-effect waves-light" type="submit"> <i class="material-icons">search</i> </button>
       		  </div>				  
			</form>
        </div>
	</div>

	<div class="row">
	<table>
        <thead>
          <tr>              
            <?php if( $_GET[ 'filtro' ] === 'pesquisar' ){?>
				<th><a href="listar.php?tipo=empresas&filtro=nome" title="Ordenar por Nome" class=" white-text">Empresa</a> </th>
			<?php } else { ?>
				<th><a href="listar.php?tipo=empresas&filtro=nome" title="Ordenar por Nome" class=" white-text">Sede</a> </th>
			<?php } ?>
              <th>Ação</th>
          </tr>
        </thead>

		<?php foreach ( $empresas as $empresas ) {

			if($empresas['ativo'] == 1){
				$color = 'amber-text text-darken-1';
			}
			if($empresas['ativo'] == 0){
				$color = 'white-text';
			}
		?>

		<tbody>
          <tr>
            
            <?php if( $_GET[ 'filtro' ] === 'pesquisar' ){?>
            <td><a  class="<?= $color?> modal-trigger" href="#modalVisualizar<?=$empresas['id']?>"><?=$empresas['nome']?></a></td>
            <?php } else { ?>
            <td><a  class="<?= $color?> modal-trigger" href="#modalVisualizar<?=$empresas['id']?>">  <?=$empresas['sede']?> </a></td>
            <?php } ?>
            <td align="center">
            	<a href="usuario.php?acao=alterar&token=<?=$empresas['id_empresa']?>" title="Alterar"><i class="material-icons prefix white-text <?= $color?>">edit</i></a>
				<?php /*$id = $empresas['id_empresa'];
				if($empresas['ativo']== 0){
					echo ' <a href="listar.php?acao=bloq&token='.$id.'"  title="Bloquear"><i class="material-icons prefix white-text '. $color .'">block</i></a>';
				}
				if($empresas['ativo']== 1){
					echo ' <a href="listar.php?acao=hab&token='.$id.'" title="Habilitar"><i class="material-icons prefix white-text '. $color .'">check_circle</i></a>';
				}*/?>
         		<!--<a class="modal-trigger Link" href="#modal<?=$empresas['id_empresa']?>" title="Excluir"><i class="material-icons prefix amber-text text-darken-1 <?= $color?>">cancel</i></a>-->
         		</td>
          </tr>
        </tbody>


        <div id="modalVisualizar<?=$empresas['id']?>" class="modal" style=" background-color: #008aae;">
        	<i class="material-icons right modal-close" style="padding: 15px;">close</i>

			<div class="container">

				<div class="row ">

				<p align="center">Visualizador de Empresa.</p>

				<p>Cod.: <?=$empresas['id']?></p>
				<p>Razão Social: <?=$empresas['nome']?></p>
				<?php if( !empty($empresas['sede'])){
					echo "<p>Sede: ".$empresas['sede']."</p>";
				} ?>
				<p>Nome Fantasia: <?=$empresas['apelido']?></p>	
				<?php if( !empty($empresas['contato'])){
					echo "<p>Nome de Contato: ".$empresas['contato']."</p>";
				} ?>
				<p>CNPJ: <?=mask($empresas['cpf_cnpj'],'###.###.###-##')?></p>
				<p>Telefone: <?= mask($empresas['telefone'],'(##) ###-###-###')?></p>
				<?php if( !empty($empresas['ramal'])){
					echo "<p>Nome de Contato: ".$empresas['ramal']."</p>";
				} ?>
				<p>E-mail: <?=$empresas['email']?></p>
				<p>Endereco: <?php echo $empresas['endereco'].', '.$empresas['numero'].' '.$empresas['complemento'].' - '.$empresas['estado'].', '. $empresas['cidade'].' ('. $empresas['cep'].')';?></p>
				<p>Situação : <?php switch ($empresas['ativo']) {
										case 0:
											echo " Ativado ";
											break;
										case 1:
											echo " Desativado ";
											break;
									}?></p>
				<p>NFe: <?= nl2br ($empresas['nfe']);?></p>
				<p>Setor: <br>
				<?php
				include_once( 'model/setor.php' );
				$setor = listarSetorEmpresaBD($conexao, $empresas['id_empresa']); 
				//var_dump($setor);
				if( empty($setor) ){ echo "Não há setor cadastrado";}
				else{
					foreach($setor as $setor){
						echo $setor['setor'] . " / ";
						echo $setor['grupo'] . " /  ";
						echo $setor['andar'] ;
						echo "<br>";
					}
				}
				?></p>
				<p><a href="setor.php?acao=novo&empresa=<?=$empresas['id_empresa']?>">Novo setor aki</a></p>
				<p>Observação:<br>
					 <?= nl2br ($empresas['obs']);?></p>
				<p>Data de Criação: <?= date('d/m/Y', strtotime($empresas['datadecricao']))?></p>

				</div>
			</div>

		  </div>

		<div id="modal<?=$empresas['id_empresa']?>" class="modal bottom-sheet">

			<div class="row">

				<div class="col s12 m6">

					<div class="modal-content">
					  <h4 class="grey-text text-darken-2">Deseja excluir <?=$empresas['nome']?>?</h4>
					  <p class="grey-text text-darken-2">Essa ação não poderá ser desfeita.</p>
					</div>

				</div>


				<div class="col s12 m6 Top">

					<div class="modal-footer">
						<a href="listar.php?tipo=empresas" class="btn waves-effect waves-light">Voltar</a>
						<a href="controller/usuario.php?acao=excluir&token=<?=$empresas['id_empresa']?>" class="modal-close btn2 waves-effect waves-light">Eu aceito</a>
					</div>

				</div>

			</div>

		  </div>
		  
		<?php } ?>
	</table>
	</div>

	<!--<div class="row">
		<div class="col s6 m3 offset-m1" align="center">
			<a href="dashboard.php" class="btn2 waves-effect waves-light">Voltar</a>
		</div>
	</div>-->

		<div class="row">
			<div class="col s6 m3 offset-m1" align="center">
				<a href="dashboard.php" class="btn waves-effect waves-light">Voltar</a> </div>
			<div class="col s6 m6">
				<a href="usuario.php?acao=novo&tipo=empresa" class="btn2 waves-effect waves-light right">Novo Cadastro</a> </div>
			</div>
		</div>

	</div>
<?php }

if ( $tipo === "clientes" ) {
	$clientes = listarClientesBD( $conexao );	
	
	if( !empty($_GET[ 'filtro' ]) ){

		if( $_GET[ 'filtro' ] === 'pesquisar' ){
			unset($clientes);
			$nivel = 4;
			$clientes = filtarUsuarioEmail($conexao, $nivel, $_POST['filtro_email']);
		}

		if( $_GET[ 'filtro' ] === 'nome' ){

			function OrdenarapelidoCrescente($a, $b) {
				return $a['nome'] > $b['nome'];
			}

			usort($clientes, 'OrdenarapelidoCrescente');

		}

		if( $_GET[ 'filtro' ] === 'email' ){

			switch ($ultimOrdenamento) {

				default:
					function OrdenarNomeCrescente($a, $b) {
						return $a['email'] > $b['email'];
					}

					OrdenarNomeCrescente($a, $b);

					usort($clientes, 'OrdenarNomeCrescente');

					$ultimOrdenamento = 'Crescente';
					break;

			}

		}
	}	?>

	<div class="container">

		<h3>Clientes</h3>		
		
		<div class="row">
			<div class="input-field col s12">
				<form class="col s12" action="listar.php?tipo=clientes&filtro=pesquisar" method="post">
				  <div class="input-field col s6">
					<input type="text" name="filtro_email" placeholder="E-mail do Cliente" value="<?=$_POST['filtro_email']?>" autocomplete="off">
				  </div>
				  <div class="input-field col s1">
					<button class="btn2 waves-effect waves-light" type="submit"> <i class="material-icons">search</i> </button>
				  </div>       		  
				  <div class="input-field col s5">
					<a href="listar.php?tipo=clientes" class="btn waves-effect waves-light">Limpar pesquisa</a>
				  </div>
				</form>
			</div>
		</div>

		<div class="row">
			<table>
				<thead>
				  <tr>
					  <th><a href="listar.php?tipo=clientes&filtro=nome" title="Ordenar por Nome" class=" white-text">Nome</a> </th>
					  <th><a href="listar.php?tipo=clientes&filtro=email" title="Ordenar por E-mail" class=" white-text">E-mail</a></th>
					  <th>Ação</th>
				  </tr>
				</thead>

				<?php foreach ( $clientes as $clientes ) {

					if($clientes['ativo'] == 1){
						$color = 'red-text';
					}
					if($clientes['ativo'] == 0){
						$color = 'white-text';
					}
					?>

					<tbody>
					  <tr>
						<td><a  class="<?= $color?> modal-trigger" href="#modalVisualizar<?=$clientes['id']?>"><?=$clientes['nome']?></a></td>
						<td><a  class="<?= $color?> modal-trigger" href="#modalVisualizar<?=$clientes['id']?>"><?=$clientes['email']?></a></td>
						<td align="center">
							<a href="usuario.php?acao=alterar&token=<?=$clientes['id']?>" title="Alterar"><i class="material-icons prefix white-text">edit</i></a>
							<?php $id = $clientes['id'];
							if($clientes['ativo']== 0){
								echo '<a href="listar.php?acao=bloq&token='.$id.'" title="Bloquear"><i class="material-icons prefix white-text">block</i>';
							}
							if($clientes['ativo']== 1){
								echo '<a href="listar.php?acao=hab&token='.$id.'" title="Habilitar"><i class="material-icons prefix white-text">check_circle</i></a>';
							}?>
							<a class="modal-trigger Link" href="#modal<?=$clientes['id']?>" title="Excluir"><i class="material-icons prefix amber-text text-darken-1">cancel</i></a>
							</td>
					  </tr>
					</tbody>

					<div id="modalVisualizar<?=$clientes['id']?>" class="modal" style=" background-color: #008aae;">

						<div class="container">
						<i class="material-icons right modal-close">close</i>
							<div class="row ">
							<p align="center">Visualizador de Usuario</p>
							
							<p>Nome Completo: <?=$clientes['nome']?></p>
							<p>Nome Sistema: <?=$clientes['apelido']?></p>
							<p>E-mail: <?=$clientes['email']?></p>
							<p>Sexo: <?php	switch ($clientes['sexo']) {
											   case 0:
													 echo "Não Informado";
													 break;	
											   case 1:
													 echo "Masculino";
													 break;	
											   case 2:
													 echo "Feminino";
													 break;
											   case 3:
													 echo "Outro";
													 break;	 
											}?></p>
											
							<p>Empresa: <?=$clientes['email']?></p>

							<p>Ativo: <?php switch ($clientes['ativo']) {
													case 0:
														echo " Ativado ";
														break;
													case 1:
														echo " Desativado ";
														break;
												}?></p>
							<p>Tipo: <?php $tipo = listarTiposBD( $conexao );
											foreach ( $tipo as $tipo ) {
												if($tipo["id"] === $clientes["id_tipo"]) {$desc = $tipo["nome"];} 
											} 
											if(!empty($desc)){ echo $desc; } 
											if(empty($desc)){ echo "Tipo Não Encontrado"; }?></p>
							
							<p>Data de Nascimento: <?php if($clientes['nascimento'] === '1111-11-11' || is_null($clientes['nascimento'])){ echo "Data não informada."; } else{ echo date('d/m/Y', strtotime($clientes['nascimento']));} ?></p>
							<p>Data de Registro: <?= date('d/m/Y', strtotime($clientes['datadecricao']))?></p>

							</div>
						</div>

					  </div>

					<div id="modal<?=$clientes['id']?>" class="modal bottom-sheet">

						<div class="row">

							<div class="col s12 m6">

								<div class="modal-content">
								  <h4 class="grey-text text-darken-2">Deseja excluir <?=$clientes['nome']?>?</h4>
								  <p class="grey-text text-darken-2">Essa ação não poderá ser desfeita.</p>
								</div>

							</div>


							<div class="col s12 m6 Top">

								<div class="modal-footer">
									<a href="listar.php?tipo=clientes" class="btn waves-effect waves-light">Voltar</a>
									<a href="controller/usuario.php?acao=excluir&token=<?=$clientes['id']?>" class="modal-close btn2 waves-effect waves-light">Eu aceito</a>
								</div>

							</div>

						</div>

					  </div>
					<?php }?>

			</table>
		</div>

		<div class="row">
			<div class="col s6 m3 offset-m1" align="center">
				<a href="dashboard.php" class="btn waves-effect waves-light">Voltar</a>
			</div>
			<div class="col s6 m7" align="center">
				<a href="usuario.php?acao=novo&tipo=cliente" class="btn3 waves-effect waves-light right">Cadastrar</a>
			</div>
		</div>

	</div>
<?php }

if ( !empty( $_GET[ 'acao' ] ) ) {

	$acao = $_GET[ 'acao' ];

	if ( $acao === "bloq" ) {
		bloquearUsuarioBD( $conexao, $_GET[ 'token' ] );
		echo '	<script>		window.location.replace("dashboard.php");		</script>';
	}

	if ( $acao === "hab" ) {
		habilitarUsuarioBD( $conexao, $_GET[ 'token' ] );
		echo '	<script>		window.location.replace("dashboard.php");		</script>';
	}
}

unset($_SESSION[ "acao" ]);

include_once('rodape.php');