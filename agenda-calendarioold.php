<?php include_once( 'cabecalho.php' );

session_start();

if ( $_SESSION["valor"]["atual"] === '1' ){
?>

		<br>
		<div class="container row">

			<h2>Calendário de Agendamento </h2>

			<div class="row">
				<div class="col s12 m12 l12 center-align PassoAtiv">

					<span style="padding-right: 8px;"><i class="material-icons medium">looks_one</i></span>

				</div>
			</div>

			<div class="row">

				<div class="row">
					<div class="col s12 m12">

						<!--CALENDÁRIO-->
						<div class="calendar" align="center"></div>
						<!--FIM CALENDÁRIO-->

					</div>
				</div>
				<br><br>


					<div class="row">
						<div class="col s6 m3 offset-m1" align="center">
							<a href="dashboard.php" class="btn waves-effect waves-light">Voltar</a> </div>
						<div class="col s6 m3 offset-m4">
							<a class="btn2 waves-effect waves-light rigth avancar" onclick="projectos()">Avançar</a>
						</div>
					</div>
			</div>
		</div>



		<script>
			function projectos () {
					var date = $('.pignose-calendar-unit-first-active').data('date');
					window.location.replace("agenda-calendario.php?voltar=2&data="+date);
			}
		</script>

<?php }

if ( $_SESSION["valor"]["atual"] === '2' ){

	include_once( 'model/atividade.php' );

	$atividade = listarAtividadesBD( $conexao );
?>
	<br>
	<div class="container row">
		<h2>Calendário de Agendamento </h2>
		<div class="row">
				<div class="col s12 m12 l12 center-align PassoAtiv">
					<span style="padding-right: 8px;"><i class="material-icons medium">looks_one</i></span>
					<span style="padding:0 8px 0 8px;"><i class="material-icons medium" >looks_two</i></span>
				</div>
		</div>
		<form action="agenda-calendario.php?voltar=3" method="post">
			<div class="row">
				<div class="input-field col s12 m6 offset-m3">
				  <input id="data" type="text" class="validate" disabled value="<?= date('d/m/Y', strtotime($_SESSION["data"]))?>"  name="data">
				  <label for="data"><p>Data escolhida:  </p></label>
				</div>
		 	</div>
		  <div class="row">
				<div class="input-field col s12 m6 offset-m3">
					<select name="atividade">
					<?php foreach ( $atividade as $atividade ) {?>
					  <option value="<?=$atividade['id']?>" ><?= $atividade[ "nome" ]?></option>
					  <?php } ?>
					</select>
					<label>Escolha uma Atividade: </label>
			  	</div>
		  </div>

		  <div class="row">
			<div class="col s6 m3 offset-m1" align="center">
				<a href="agenda-calendario.php?voltar=1" class="btn waves-effect waves-light">Voltar</a> </div>
			<div class="col s6 m3 offset-m4">
				<button class="btn2 waves-effect waves-light right" type="submit">
					Avançar
				</button>
			</div>
		  </div>
		</form>
  </div>

<?php }

if ( $_SESSION["valor"]["atual"] === '3' ){


	include_once( 'model/atividade.php' );
	$atividade = listarAtividadeBD( $conexao, $_SESSION[ 'atividade' ] );


	//listarAgendamentos do dia.
	include_once( 'model/agenda.php' );
	$dia = $_SESSION["data"];
	$agenda = listarAgendadiaBD($conexao, $dia, $_SESSION[ 'atividade' ]);


	include_once( 'model/usuario.php' );

?>
		<br>
		<div class="container row">

			<h2>Calendário de Agendamento </h2>

			<div class="row">
				<div class="col s12 m12 l12 center-align PassoAtiv">

					<span style="padding-right: 8px;"><i class="material-icons medium">looks_one</i></span>
					<span style="padding:0 8px 0 8px;"><i class="material-icons medium" >looks_two</i></span>
					<span style="padding-left: 8px;"><i class="material-icons medium">looks_3</i></span>

				</div>
			</div>

			<div class="row">
			<p>Data escolhida: <?= date('d/m/Y', strtotime( $_SESSION['data']))?> </p>
			<p>Atividade Escolhida: <?= $atividade[0]['nome'];?> </p>

			<?php foreach ( $agenda as $agenda ) { ?>
				 <div class="col s12 m6 center-align blue">
					<p>Cod.: <?=$agenda['id']?></p>
					<p>Nome: <?= $agenda[ "nome" ]?></p>
					<p>Data: <?= gmdate('H:i', strtotime( $agenda['hora'] ))?></p>
					<p>Empresa:	<?php $empresas = listarUserEmpresasBD($conexao, $agenda['empresa']); echo $empresas[0]['nome']; ?></p>
					<hr>
					<p><a href="#modal<?=$agenda[ "id" ]?>" class="modal-trigger"><i class="material-icons white-text">event_available</i></a></p>
				</div>

				<!-- Modal Structure -->
					<div id="modal<?=$agenda[ "id" ]?>" class="modal grey-text text-darken-3">
						<div class="modal-content">

							<h4><?= $agenda[ "nome" ]?></h4>

							<p>Deseja agendar esse horário?</p>

						</div>

						<div class="modal-footer">
							<a href='agenda.php?acao=agendar&token=<?= $agenda["id"]?>' class="modal-close waves-effect waves-green btn">Agendar</a>
						</div>

					</div>
			<?php }?>

			</div>

			<div class="row">
				<div class="col s6 m3 offset-m1" align="center">
					<a href="agenda-calendario.php?voltar=2" class="btn waves-effect waves-light">Voltar</a> </div>

			<?php if ( empty($agenda)){
				echo '<p>Nenhum agendamento pra hoje</p>';
			} else {?>
				<div class="col s6 m3 offset-m4">
					<a href="agenda-calendario.php" class="btn2 waves-effect waves-light rigth avancar">Avançar</a>
				</div>
			<?php } ?>
			</div>

		</div>



<?php }


if ( !empty( $_GET['voltar'] )){

	unset($_SESSION[ "valor" ]);

	$_SESSION[ "valor" ][ 'atual' ] = $_GET['voltar'];

	if ( !empty($_GET['data'])){
		$_SESSION[ 'data' ] = $_GET['data'];
	}

	if ( !empty($_POST['atividade'])){
		$_SESSION[ 'atividade' ] = $_POST['atividade'];
}

	echo '<script>window.location.replace("agenda-calendario.php");</script>';
}

include_once( 'rodape.php' );
