<?php require_once('../loading.php');
include_once( '../model/questionario.php' );
error_reporting( E_ALL ^ E_NOTICE );
session_start();

//var_dump($_SESSION[ "acao" ]);

/*Tratando */
$caracterEspecial = array( "/", "'", ".", "@", "-", "(", ")", " ", ":" );
$caracterEspecialnbackspace = array( "/", "'", ".", "@", "-", "(", ")" );


if ( $_SESSION[ "acao" ][ "acao" ] === 'inserir' ) {

	if ( $_SESSION[ "acao" ][ 'quest' ] === "ginlab" ) {
		$id_questionario = 1;

		for ( $i = 1; $i <= 27; $i++ ) {

			if ( $_POST[ "body$i" ] === "on" ) {
				$_POST[ "body$i" ] = "1";
			} else {
				$_POST[ "body$i" ] = "0";
			}
		}

		for ( $i = 1; $i <= 6; $i++ ) {

			if ( $_POST[ "a$i" ] === "on" ) {
				$_POST[ "a$i" ] = "1";
			} else {
				$_POST[ "a$i" ] = "0";
			}
		}

		for ( $i = 1; $i <= 6; $i++ ) {

			if ( $_POST[ "gosta$i" ] === "on" ) {
				$_POST[ "gosta$i" ] = "1";
			} else {
				$_POST[ "gosta$i" ] = "0";
			}
		}

		for ( $i = 1; $i <= 6; $i++ ) {

			if ( $_POST[ "b$i" ] === "on" ) {
				$_POST[ "b$i" ] = "1";
			} else {
				$_POST[ "b$i" ] = "0";
			}
		}

		for ( $i = 1; $i <= 9; $i++ ) {

			if ( $_POST[ "c$i" ] === "on" ) {
				$_POST[ "c$i" ] = "1";
			} else {
				$_POST[ "c$i" ] = "0";
			}
		}

		$_POST[ "outro" ] = str_replace( $caracterEspecialnbackspace, "", $_POST[ "outro" ] );

		for ( $i = 1; $i <= 3; $i++ ) {

			if ( $_POST[ "d$i" ] === "on" ) {
				$_POST[ "d$i" ] = "1";
			} else {
				$_POST[ "d$i" ] = "0";
			}
		}

		for ( $i = 1; $i <= 7; $i++ ) {

			if ( $_POST[ "e$i" ] === "on" ) {
				$_POST[ "e$i" ] = "1";
			} else {
				$_POST[ "e$i" ] = "0";
			}
		}

		for ( $i = 1; $i <= 4; $i++ ) {

			if ( $_POST[ "f$i" ] === "on" ) {
				$_POST[ "f$i" ] = "1";
			} else {
				$_POST[ "f$i" ] = "0";
			}
		}

		for ( $i = 1; $i <= 4; $i++ ) {

			if ( $_POST[ "g$i" ] === "on" ) {
				$_POST[ "g$i" ] = "1";
			} else {
				$_POST[ "g$i" ] = "0";
			}
		}

		for ( $i = 1; $i <= 11; $i++ ) {

			if ( $_POST[ "h$i" ] === "on" ) {
				$_POST[ "h$i" ] = "1";
			} else {
				$_POST[ "h$i" ] = "0";
			}
		}

		$array = array(

			$_POST[ "sexo" ], //1
			$_POST[ "idade" ], //2
			$_POST[ "peso" ], //3
			$_POST[ "altura" ], //4
			$_POST[ "lado" ], //5
			$_POST[ "tempoempresa" ], //6
			$_POST[ "departamento" ], //7

			$_POST[ "body1" ], //8
			$_POST[ "body2" ],
			$_POST[ "body3" ],
			$_POST[ "body4" ],
			$_POST[ "body5" ],
			$_POST[ "body6" ],
			$_POST[ "body7" ],
			$_POST[ "body8" ],
			$_POST[ "body9" ],
			$_POST[ "body10" ],
			$_POST[ "body11" ],
			$_POST[ "body12" ],
			$_POST[ "body13" ],
			$_POST[ "body14" ],
			$_POST[ "body15" ],
			$_POST[ "body16" ],
			$_POST[ "body17" ],
			$_POST[ "body18" ],
			$_POST[ "body19" ],
			$_POST[ "body20" ],
			$_POST[ "body21" ],
			$_POST[ "body22" ],
			$_POST[ "body23" ],
			$_POST[ "body24" ],
			$_POST[ "body25" ],
			$_POST[ "body26" ],
			$_POST[ "body27" ],

			$_POST[ "a1" ], //35
			$_POST[ "a2" ],
			$_POST[ "a3" ],
			$_POST[ "a4" ],
			$_POST[ "a5" ],
			$_POST[ "a6" ],
			//41
			$_POST[ "gosta1" ],
			$_POST[ "gosta2" ],
			$_POST[ "gosta3" ],
			$_POST[ "gosta4" ],
			$_POST[ "gosta5" ],
			$_POST[ "gosta6" ],

			$_POST[ "group1" ], //46
			$_POST[ "group2" ], //47
			//48
			$_POST[ "b1" ],
			$_POST[ "b2" ],
			$_POST[ "b3" ],
			$_POST[ "b4" ],
			$_POST[ "b5" ],
			$_POST[ "b6" ],
			//54
			$_POST[ "c1" ],
			$_POST[ "c2" ],
			$_POST[ "c3" ],
			$_POST[ "c4" ],
			$_POST[ "c5" ],
			$_POST[ "c6" ],
			$_POST[ "c7" ],
			$_POST[ "c8" ],
			$_POST[ "c9" ],

			$_POST[ "outro" ], //63
			//64
			$_POST[ "d1" ],
			$_POST[ "d2" ],
			$_POST[ "d3" ],

			$_POST[ "group3" ], //66

			$_POST[ "e1" ],
			$_POST[ "e2" ],
			$_POST[ "e3" ],
			$_POST[ "e4" ],
			$_POST[ "e5" ],
			$_POST[ "e6" ],
			$_POST[ "e7" ],
			//??
			$_POST[ "f1" ],
			$_POST[ "f2" ],
			$_POST[ "f3" ],
			$_POST[ "f4" ],
			//??
			$_POST[ "g1" ],
			$_POST[ "g2" ],
			$_POST[ "g3" ],
			$_POST[ "g4" ],
			//??
			$_POST[ "h1" ],
			$_POST[ "h2" ],
			$_POST[ "h3" ],
			$_POST[ "h4" ],
			$_POST[ "h5" ],
			$_POST[ "h6" ],
			$_POST[ "h7" ],
			$_POST[ "h8" ],
			$_POST[ "h9" ],
			$_POST[ "h10" ],
			$_POST[ "h11" ],
		);

		//var_dump($array);

		$usuario[ 0 ] = $_SESSION[ 'usuario' ];

		if ( $_SESSION[ "acao" ][ 'quest' ] === "ginlab" ) {
			$id_questionario = 1;
		}

		if ( $_SESSION[ "acao" ][ 'quest' ] === "shiatsu" ) {
			$id_questionario = 2;
		}

		if ( $_SESSION[ "acao" ][ 'quest' ] === "ergonomia" ) {
			$id_questionario = 3;
		}

		if ( $_SESSION[ "acao" ][ 'quest' ] === "qualidadevida" ) {
			$id_questionario = 4;
		}


		if ( inserirQuestionarioBD( $conexao, $usuario[ 0 ][ 'id' ], $id_questionario ) ) {
			$_SESSION[ "msg" ] = 'Fase 1 Inserida';

			$ultimoId = buscarUltimoId( $conexao );

			$leng = count( $array );

			for ( $i = 0; $i <= $leng; $i++ ) {

				if ( inserirRespostaBD( $conexao, $ultimoId[ 'id' ], $array[ $i ] ) ) {
					$_SESSION[ "msg" ] = 'Fase 2 Inserida';


				} else {
					$msg = mysqli_error( $conexao );
					$_SESSION[ "msg" ] = 'Fase 2 Não inserida .<br>	Erro:' . $msg . '	</div>';
				}

			}
			
			//redirecionar 
			unset($_SESSION[ "acao" ]);
			
			$_SESSION[ "acao" ][ 'quest' ] = "finalizar" ;
			echo '	<script>		window.location.replace("../questionario.php");		</script>';

		} else {
			$msg = mysqli_error( $conexao );
			$_SESSION[ "msg" ] = 'Fase 1 Não inserida .<br>	Erro:' . $msg . '	</div>';
		}
	}

	if ( $_SESSION[ "acao" ][ 'quest' ] === "shiatsu" ) {
		$id_questionario = 2;

		for ( $i = 1; $i <= 27; $i++ ) {

			if ( $_POST[ "body$i" ] === "on" ) {
				$_POST[ "body$i" ] = "1";
			} else {
				$_POST[ "body$i" ] = "0";
			}
		}

		for ( $i = 1; $i <= 8; $i++ ) {

			if ( $_POST[ "a$i" ] === "on" ) {
				$_POST[ "a$i" ] = "1";
			} else {
				$_POST[ "a$i" ] = "0";
			}
		}

		for ( $i = 1; $i <= 6; $i++ ) {

			if ( $_POST[ "gosta$i" ] === "on" ) {
				$_POST[ "gosta$i" ] = "1";
			} else {
				$_POST[ "gosta$i" ] = "0";
			}
		}
		
		if ( $_POST[ "group1" ] === "on" ) {
				$_POST[ "group1" ] = "1";
		} else {
			$_POST[ "group1" ] = "0";
		}
	
		if ( $_POST[ "group2" ] === "on" ) {
				$_POST[ "group2" ] = "1";
		} else {
			$_POST[ "group2" ] = "0";
		}

		for ( $i = 1; $i <= 5; $i++ ) {

			if ( $_POST[ "b$i" ] === "on" ) {
				$_POST[ "b$i" ] = "1";
			} else {
				$_POST[ "b$i" ] = "0";
			}
		}

		for ( $i = 1; $i <= 6; $i++ ) {

			if ( $_POST[ "c$i" ] === "on" ) {
				$_POST[ "c$i" ] = "1";
			} else {
				$_POST[ "c$i" ] = "0";
			}
		}


		for ( $i = 1; $i <= 9; $i++ ) {

			if ( $_POST[ "d$i" ] === "on" ) {
				$_POST[ "d$i" ] = "1";
			} else {
				$_POST[ "d$i" ] = "0";
			}
		}
		

		for ( $i = 1; $i <= 3; $i++ ) {

			if ( $_POST[ "e$i" ] === "on" ) {
				$_POST[ "e$i" ] = "1";
			} else {
				$_POST[ "e$i" ] = "0";
			}
		}

		for ( $i = 1; $i <= 28; $i++ ) {

			if ( $_POST[ "f$i" ] === "on" ) {
				$_POST[ "f$i" ] = "1";
			} else {
				$_POST[ "f$i" ] = "0";
			}
		}
		
		$array = array(

			$_POST[ "sexo" ], //1
			$_POST[ "idade" ], //2
			$_POST[ "peso" ], //3
			$_POST[ "altura" ], //4
			$_POST[ "lado" ], //5
			$_POST[ "tempoempresa" ], //6
			$_POST[ "departamento" ], //7

			$_POST[ "body1" ], //8
			$_POST[ "body2" ],
			$_POST[ "body3" ],
			$_POST[ "body4" ],
			$_POST[ "body5" ],
			$_POST[ "body6" ],
			$_POST[ "body7" ],
			$_POST[ "body8" ],
			$_POST[ "body9" ],
			$_POST[ "body10" ],
			$_POST[ "body11" ],
			$_POST[ "body12" ],
			$_POST[ "body13" ],
			$_POST[ "body14" ],
			$_POST[ "body15" ],
			$_POST[ "body16" ],
			$_POST[ "body17" ],
			$_POST[ "body18" ],
			$_POST[ "body19" ],
			$_POST[ "body20" ],
			$_POST[ "body21" ],
			$_POST[ "body22" ],
			$_POST[ "body23" ],
			$_POST[ "body24" ],
			$_POST[ "body25" ],
			$_POST[ "body26" ],
			$_POST[ "body27" ],

			$_POST[ "massage" ], 
			
			$_POST[ "a1" ], 
			$_POST[ "a2" ],
			$_POST[ "a3" ],
			$_POST[ "a4" ],
			$_POST[ "a5" ],
			$_POST[ "a6" ],
			$_POST[ "a7" ],
			$_POST[ "a8" ],
			
			$_POST[ "gosta1" ],
			$_POST[ "gosta2" ],
			$_POST[ "gosta3" ],
			$_POST[ "gosta4" ],
			$_POST[ "gosta5" ],
			$_POST[ "gosta6" ],

			$_POST[ "group1" ],
			$_POST[ "group2" ],
			$_POST[ "justifique" ],		
			
			$_POST[ "b1" ],
			$_POST[ "b2" ],
			$_POST[ "b3" ],
			$_POST[ "b4" ],
			$_POST[ "b5" ], 			

			$_POST[ "group3" ],
			
			$_POST[ "c1" ],
			$_POST[ "c2" ],
			$_POST[ "c3" ],
			$_POST[ "c4" ],
			$_POST[ "c5" ],
			$_POST[ "c6" ],			

			$_POST[ "group4" ],		
			$_POST[ "justifique2" ],		

			$_POST[ "group5" ],			
			$_POST[ "justifique3" ],	

			$_POST[ "group6" ],			
			$_POST[ "justifique3" ],

			$_POST[ "group7" ],			
			$_POST[ "justifique4" ],	
			
			$_POST[ "profissional" ],
			$_POST[ "nota" ],	
			$_POST[ "comentario" ], 
			
			
			$_POST[ "d1" ],
			$_POST[ "d2" ],
			$_POST[ "d3" ],
			$_POST[ "d4" ],
			$_POST[ "d5" ],
			$_POST[ "d6" ],
			$_POST[ "d7" ],
			$_POST[ "d8" ],
			$_POST[ "d9" ],
			$_POST[ "doutro" ],

			$_POST[ "e1" ],
			$_POST[ "e2" ],
			$_POST[ "e3" ],

			$_POST[ "group8" ],	
			
			$_POST[ "f1" ],
			$_POST[ "f2" ],
			$_POST[ "f3" ],
			$_POST[ "f4" ],
			$_POST[ "f5" ],
			$_POST[ "f6" ],
			$_POST[ "f7" ],
			$_POST[ "f8" ],
			$_POST[ "f9" ],
			$_POST[ "f10" ],
			$_POST[ "f11" ],
			$_POST[ "f12" ],
			$_POST[ "f13" ],
			$_POST[ "f14" ],
			$_POST[ "f15" ],
			$_POST[ "f16" ],
			$_POST[ "f17" ],
			$_POST[ "f18" ],
			$_POST[ "f19" ],
			$_POST[ "f20" ],
			$_POST[ "f21" ],
			$_POST[ "f22" ],
			$_POST[ "f23" ],
			$_POST[ "f24" ],
			$_POST[ "f25" ],
			$_POST[ "f26" ],
			$_POST[ "f27" ],
			$_POST[ "f28" ],
		);

		var_dump($array);

		$usuario[ 0 ] = $_SESSION[ 'usuario' ];

		if ( inserirQuestionarioBD( $conexao, $usuario[ 0 ][ 'id' ], $id_questionario ) ) {
			$_SESSION[ "msg" ] = 'Fase 1 Inserida';

			$ultimoId = buscarUltimoId( $conexao );

			$leng = count( $array );

			for ( $i = 0; $i <= $leng; $i++ ) {

				if ( inserirRespostaBD( $conexao, $ultimoId[ 'id' ], $array[ $i ] ) ) {
					$_SESSION[ "msg" ] = 'Fase 2 Inserida';


				} else {
					$msg = mysqli_error( $conexao );
					$_SESSION[ "msg" ] = 'Fase 2 Não inserida .<br>	Erro:' . $msg . '	</div>';
				}

			}
			
			//redirecionar 
			unset($_SESSION[ "acao" ]);
			
			$_SESSION[ "acao" ][ 'quest' ] = "finalizar" ;
			echo '	<script>		window.location.replace("../questionario.php");		</script>';

		} else {
			$msg = mysqli_error( $conexao );
			$_SESSION[ "msg" ] = 'Fase 1 Não inserida .<br>	Erro:' . $msg . '	</div>';
		}
	}

	if ( $_SESSION[ "acao" ][ 'quest' ] === "ergonomia" ) {
		$id_questionario = 3;
		
		$array = array(

			$_POST[ "funcao" ], 
			$_POST[ "nivel" ], 
			$_POST[ "idade" ], 
			$_POST[ "pne" ],
			$_POST[ "media" ],
			
			$_POST[ "resposta1" ],
			$_POST[ "resposta2" ],
			$_POST[ "resposta3" ],
			$_POST[ "resposta4" ],
			$_POST[ "resposta5" ],
			$_POST[ "resposta6" ],
			$_POST[ "resposta7" ],
			$_POST[ "resposta8" ],
			$_POST[ "resposta9" ],
			$_POST[ "resposta10" ],
			$_POST[ "resposta11" ],
			$_POST[ "resposta12" ],
			$_POST[ "resposta13" ],
			$_POST[ "resposta14" ],
			$_POST[ "resposta15" ],
			$_POST[ "resposta16" ],
			$_POST[ "resposta17" ],
			$_POST[ "resposta18" ],
			$_POST[ "resposta19" ],
			$_POST[ "resposta20" ],
			$_POST[ "resposta21" ],
			$_POST[ "resposta22" ],
			$_POST[ "resposta23" ],
			$_POST[ "resposta24" ],
			$_POST[ "resposta25" ],
			$_POST[ "resposta26" ],
			$_POST[ "resposta27" ],
			$_POST[ "resposta28" ],
			$_POST[ "resposta29" ],
			$_POST[ "resposta30" ],
			$_POST[ "resposta31" ],
			$_POST[ "resposta32" ],
			
			$_POST[ "textarea1" ],
			
			$_POST[ "resposta33" ],
			$_POST[ "resposta34" ],
			$_POST[ "resposta35" ],
			$_POST[ "resposta36" ],
			$_POST[ "resposta37" ],
			$_POST[ "resposta38" ],
			$_POST[ "resposta39" ],
			$_POST[ "resposta40" ],
			$_POST[ "resposta41" ],
			$_POST[ "resposta42" ],
			$_POST[ "resposta43" ],
			$_POST[ "resposta44" ],
			$_POST[ "resposta45" ],
			$_POST[ "resposta46" ],
			$_POST[ "resposta47" ],
			$_POST[ "resposta48" ],
			$_POST[ "resposta49" ],
			$_POST[ "resposta50" ],
			$_POST[ "resposta51" ],
			$_POST[ "resposta52" ],
			$_POST[ "resposta53" ],
			$_POST[ "resposta54" ],
			$_POST[ "resposta55" ],
			$_POST[ "resposta56" ],
			$_POST[ "resposta57" ],
			$_POST[ "resposta58" ],
			$_POST[ "resposta59" ],
			
			$_POST[ "textarea2" ],
		);

		$usuario[ 0 ] = $_SESSION[ 'usuario' ];
		
		//var_dump($usuario[ 0 ]);

		if ( inserirQuestionarioBD( $conexao, $usuario[ 0 ][ 'id' ], $id_questionario ) ) {
			$_SESSION[ "msg" ] = 'Fase 1 Inserida';

			$ultimoId = buscarUltimoId( $conexao );

			$leng = count( $array );

			for ( $i = 0; $i <= $leng; $i++ ) {

				if ( inserirRespostaBD( $conexao, $ultimoId[ 'id' ], $array[ $i ] ) ) {
					$_SESSION[ "msg" ] = 'Fase 2 Inserida';


				} else {
					$msg = mysqli_error( $conexao );
					$_SESSION[ "msg" ] = 'Fase 2 Não inserida .<br>	Erro:' . $msg . '	</div>';
				}

			}
			
			//redirecionar 
			unset($_SESSION[ "acao" ]);
			
			$_SESSION[ "acao" ][ 'quest' ] = "finalizar" ;
			echo '	<script>		window.location.replace("../questionario.php");		</script>';

		} else {
			$msg = mysqli_error( $conexao );
			$_SESSION[ "msg" ] = 'Fase 1 Não inserida .<br>	Erro:' . $msg . '	</div>';
		}
	}

	if ( $_SESSION[ "acao" ][ 'quest' ] === "qualidadevida" ) {
		$id_questionario = 4;
		
		for ( $i = 1; $i <= 27; $i++ ) {

			if ( $_POST[ "body$i" ] === "on" ) {
				$_POST[ "body$i" ] = "1";
			} else {
				$_POST[ "body$i" ] = "0";
			}
		}
		
		for ( $i = 1; $i <= 9; $i++ ) {

			if ( $_POST[ "c$i" ] === "on" ) {
				$_POST[ "c$i" ] = "1";
			} else {
				$_POST[ "c$i" ] = "0";
			}
		}
		
		for ( $i = 1; $i <= 4; $i++ ) {

			if ( $_POST[ "d$i" ] === "on" ) {
				$_POST[ "d$i" ] = "1";
			} else {
				$_POST[ "d$i" ] = "0";
			}
		}
		
		for ( $i = 1; $i <= 4; $i++ ) {

			if ( $_POST[ "e$i" ] === "on" ) {
				$_POST[ "e$i" ] = "1";
			} else {
				$_POST[ "e$i" ] = "0";
			}
		}
		
		for ( $i = 1; $i <= 11; $i++ ) {

			if ( $_POST[ "f$i" ] === "on" ) {
				$_POST[ "f$i" ] = "1";
			} else {
				$_POST[ "f$i" ] = "0";
			}
		}
		
		$array = array(

			$_POST[ "sexo" ], //1
			$_POST[ "idade" ], //2
			$_POST[ "peso" ], //3
			$_POST[ "altura" ], //4
			$_POST[ "lado" ], //5
			$_POST[ "tempoempresa" ], //6
			$_POST[ "departamento" ], //7

			$_POST[ "body1" ], //8
			$_POST[ "body2" ],
			$_POST[ "body3" ],
			$_POST[ "body4" ],
			$_POST[ "body5" ],
			$_POST[ "body6" ],
			$_POST[ "body7" ],
			$_POST[ "body8" ],
			$_POST[ "body9" ],
			$_POST[ "body10" ],
			$_POST[ "body11" ],
			$_POST[ "body12" ],
			$_POST[ "body13" ],
			$_POST[ "body14" ],
			$_POST[ "body15" ],
			$_POST[ "body16" ],
			$_POST[ "body17" ],
			$_POST[ "body18" ],
			$_POST[ "body19" ],
			$_POST[ "body20" ],
			$_POST[ "body21" ],
			$_POST[ "body22" ],
			$_POST[ "body23" ],
			$_POST[ "body24" ],
			$_POST[ "body25" ],
			$_POST[ "body26" ],
			$_POST[ "body27" ],

			$_POST[ "a1" ], 	//35
			$_POST[ "a2" ],
			$_POST[ "a3" ],
			$_POST[ "a4" ],
			$_POST[ "a5" ],
			$_POST[ "a6" ],	
			$_POST[ "a7" ],	
			$_POST[ "a8" ],	
			$_POST[ "a9" ],	
			$_POST[ "outro" ],	
			
			$_POST[ "b1" ],
			$_POST[ "b2" ],
			$_POST[ "b3" ],
			
			$_POST[ "group1" ],  
			$_POST[ "outro" ],
			
			$_POST[ "c1" ], 	
			$_POST[ "c2" ],
			$_POST[ "c3" ],
			$_POST[ "c4" ],
			$_POST[ "c5" ],
			$_POST[ "c6" ],	
			$_POST[ "c7" ],	
			$_POST[ "c8" ],	
			$_POST[ "c9" ],	
			
			$_POST[ "d1" ], 	
			$_POST[ "d2" ],
			$_POST[ "d3" ],
			$_POST[ "d4" ],
			
			$_POST[ "e1" ], 	
			$_POST[ "e2" ],
			$_POST[ "e3" ],
			$_POST[ "e4" ],
			
			$_POST[ "f1" ], 	
			$_POST[ "f2" ],
			$_POST[ "f3" ],
			$_POST[ "f4" ],
			$_POST[ "f5" ],
			$_POST[ "f6" ],	
			$_POST[ "f7" ],	
			$_POST[ "f8" ],	
			$_POST[ "f9" ],	
			$_POST[ "f10" ],	
			$_POST[ "f11" ],	
			
		);

		//var_dump($array);

		$usuario[ 0 ] = $_SESSION[ 'usuario' ];
		
		//var_dump($usuario[ 0 ]);

		if ( inserirQuestionarioBD( $conexao, $usuario[ 0 ][ 'id' ], $id_questionario ) ) {
			$_SESSION[ "msg" ] = 'Fase 1 Inserida';

			$ultimoId = buscarUltimoId( $conexao );

			$leng = count( $array );

			for ( $i = 0; $i <= $leng; $i++ ) {

				if ( inserirRespostaBD( $conexao, $ultimoId[ 'id' ], $array[ $i ] ) ) {
					$_SESSION[ "msg" ] = 'Fase 2 Inserida';

				} else {
					$msg = mysqli_error( $conexao );
					$_SESSION[ "msg" ] = 'Fase 2 Não inserida .<br>	Erro:' . $msg . '	</div>';
				}

			}
			
			//redirecionar 
			unset($_SESSION[ "acao" ]);
			
			$_SESSION[ "acao" ][ 'quest' ] = "finalizar" ;
			echo '	<script>		window.location.replace("../questionario.php");		</script>';


		} else { 
			$msg = mysqli_error( $conexao );
			$_SESSION[ "msg" ] = 'Fase 1 Não inserida .<br>	Erro:' . $msg . '	</div>';
		}
	}
	
}