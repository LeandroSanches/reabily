<?php require_once('../loading.php');

error_reporting( E_ALL ^ E_NOTICE );
error_reporting( E_WARNING );
error_reporting( 0 );

session_start();
include_once( '../model/contrato.php' );

/*Tratando */
$caracterEspecial = array("/", "'", ".", ",", "@", "-", "(", ")", " ");
$caracterEspecialnbackspace = array("/", "'", ".", "@", "-", "(", ")");

$_POST[ 'nome' ] = str_replace( $caracterEspecialnbackspace, "", $_POST[ "nome" ] );
$_POST[ 'valor' ] = str_replace( $caracterEspecial, "", $_POST[ "valor" ] );
$_POST[ 'date_inicio' ] = str_replace( $caracterEspecial, "", $_POST[ "date_inicio" ] );
$_POST[ 'date_final' ] = str_replace( $caracterEspecial, "", $_POST[ "date_final" ] );

if ( empty( $_POST[ 'id' ] ) ) {
	
	if( empty($_POST[ "id_renovacao" ])){ $_POST[ "id_renovacao" ] = 0;}
	
	$array = array(
		intval($_POST[ "id_empresa" ]),		//0
		$_POST[ "nome" ],					//1
		intval($_POST[ "tipo" ]),			//2
		intval($_POST[ "valor" ]),			//3
		$_POST[ "exigencia" ],				//4
		intval($_POST[ "atividade" ]),		//5
		$_POST[ "date_inicio" ],			//6
		$_POST[ "date_final" ],				//7
		$_POST[ "obs" ],					//8
		$_POST[ "id_renovacao" ]			//9
	);

	if ( gravarContratoBD( $conexao, $array ) ) {
		$msg = mysqli_error( $conexao );
		echo $_SESSION[ "msg" ] = 'Contrato com sucesso';
		
		if( $_POST[ "id_renovacao" ] != 0){
			echo "Renovação: ".$_POST[ "id_renovacao" ];			
			
			$array = array(
				$_POST[ "id_renovacao" ],//0
				1 //1
			);	
			
			if ( bloquearContratoBD( $conexao, $array ) ) {
				echo $_SESSION[ "msg" ] = 'Renovado com sucesso';
				echo '<script>window.location.replace("../contrato.php?acao=listar");</script>';
			} else {				
				$msg = mysqli_error( $conexao );
				echo $_SESSION[ "msg" ] = 'Erro, não Renovado .<br>	Erro:' . $msg ;
				echo '<script>window.location.replace("../contrato.php?acao=listar");</script>';
			}
			
		}

		echo '<script>window.location.replace("../contrato.php?acao=listar");</script>';

	} else {
		echo $_SESSION[ "msg" ] = 'Contrato não inserido .<br>	Erro:' . $msg;

		echo '<script>window.location.replace("../contrato.php?acao=novo");	</script>';
	}
}

if ( !empty( $_POST[ 'id' ] ) ) {
	$array = array(
		intval($_POST[ "id_empresa" ]),		//0
		$_POST[ "nome" ],					//1
		intval($_POST[ "tipo" ]),			//2
		intval($_POST[ "valor" ]),			//3
		$_POST[ "exigencia" ],				//4
		intval($_POST[ "atividade" ]),		//5
		$_POST[ "date_inicio" ],			//6
		$_POST[ "date_final" ],				//7
		$_POST[ "obs" ],					//8
		$_POST[ "id" ]						//9
	);

		if ( $_SESSION[ "acao" ][ 'acao' ] === "editar" ) {
			if ( alterarContratoBD( $conexao, $array ) ) {
				echo $_SESSION[ "msg" ] = 'Alterado com sucesso';

				echo '	<script>		window.location.replace("../contrato.php?acao=listar");		</script>';

			} else {				
				$msg = mysqli_error( $conexao );
				echo $_SESSION[ "msg" ] = 'Erro, não inserido .<br>	Erro:' . $msg ;

				echo '	<script>		window.location.replace("../contrato.php?acao=listar");		</script>';
			}
		}
}