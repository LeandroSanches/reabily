<?php require_once('../loading.php');
include_once('../model/usuario.php');
include_once('../model/endereco.php');
include_once('../model/telefone.php');


error_reporting( E_ERROR | E_PARSE );
echo '<style>body{color: aliceblue;}</style>';

/*Tratando */
$caracterEspecial = array("/", "'", ".", "@", "-", "(", ")", " ");
$caracterEspecialnbackspace = array("/", "'", ".", "@", "-", "(", ")");

//Upload 
$tamanhoMaximoMB = 5;
$_UP['caminho'] = 'D:/VertrigoServ/www/reabily/usuarios';											// Teste
//$_UP['caminho'] = '/home/vlannetw/vlansolution.com.br/projetos/homologacao_reabily/usuarios';		// Homologação
//$_UP['caminho'] = '/home/vlannetw/vemsermovimento.com.br/sistema/usuarios';						// Produção

$upVazio = 'D:/VertrigoServ/www/reabily/usuarios/Usuario_Padrao.png';											// Teste
//upVazio = '/home/vlannetw/vlansolution.com.br/projetos/homologacao_reabily/usuarios/Usuario_Padrao.png';		// Homologação
//upVazio = '/home/vlannetw/vemsermovimento.com.br/sistema/usuarios/Usuario_Padrao.png';						// Produção


session_start();

if ( empty( $_SESSION[ 'usuario' ] ) && empty($_SESSION[ "acao" ]) && $_SESSION[ "acao" ][ 'auto' ] === 'false') {
	$_SESSION[ "msg" ] = 'Não ha Usuário Logado';
	echo " <script>	window.location.replace('../index.php');	</script>";
}
//////////////////////Usuario///////////////////////////

if ( $_SESSION[ "acao" ][ 'acao' ] ===  'cadastrar'){
	
	$proximoPasso = "../dashboard.php";

	if($_SESSION[ "acao" ][ 'auto' ] === 'true'){	
		
		$_SESSION[ "acao" ][ 'auto' ] = 'false';

		$_POST["ativo"] = '0';
		$_POST["nivel"] = '4';	
		
		//Verificação de Dados vazios	
		if(empty($_POST["nome"])){
			echo 'Nome Vazio';
			$_SESSION[ "log" ][ "msg" ] = 'Nome Obrigatório';
			unset($_SESSION[ "acao" ]);
			echo '<script>window.location.replace("../autocadastro.php");</script>';
			exit;
		}
		if(empty($_POST["apelido"])){
			echo 'Sobrenome Vazio';
			$_SESSION[ "log" ][ "msg" ] = 'Sobrenome Obrigatório';
			unset($_SESSION[ "acao" ]);
			echo '<script>window.location.replace("../autocadastro.php");</script>';
			exit;
		}
		if(empty($_POST["email"])){
			echo 'E-mail Vazio';
			$_SESSION[ "log" ][ "msg" ] = 'E-mail Obrigatório';
			unset($_SESSION[ "acao" ]);
			echo '<script>window.location.replace("../autocadastro.php");</script>';
			exit;
		}
		if(empty($_POST["senha"])){
			echo 'Senha Vazio';
			$_SESSION[ "log" ][ "msg" ] = 'Senha Obrigatório';
			unset($_SESSION[ "acao" ]);
			echo '<script>window.location.replace("../autocadastro.php");</script>';
			exit;
		}
		if(empty($_POST["nascimento"])){
			echo 'Nascimento Vazio';
			$_SESSION[ "log" ][ "msg" ] = 'Nascimento Obrigatório';
			unset($_SESSION[ "acao" ]);
			echo '<script>window.location.replace("../autocadastro.php");</script>';
			exit;
		}
		
		//Verificar se Email ja foi usado
		$row = varificarUsuarioBD( $conexao, $_POST["email"] );
		if($row != 0){
			echo 'E-mail Já Cadastrado';
			$_SESSION[ "log" ][ "msg" ] = 'E-mail Já Cadastrado';
			unset($_SESSION[ "acao" ]);
			echo '<script>window.location.replace("../autocadastro.php");</script>';
			exit;
		}
		

		unset($_SESSION[ "acao" ][ 'auto' ]);
		$proximoPasso = '../index.php';

		$_POST["nome"] = str_replace($caracterEspecialnbackspace, "", $_POST["nome"]);
		$_POST["apelido"] = str_replace($caracterEspecialnbackspace, "", $_POST["apelido"]);
		$_POST["telefone"] = str_replace($caracterEspecial, "", $_POST["telefone"]);
		$_POST["nascimento"] = str_replace($caracterEspecial, "", $_POST["nascimento"]);
		$_POST["senha"] = md5($_POST["senha"]);

		// Validação de vazio
		$_POST["cpf"] = str_replace($caracterEspecial, "", $_POST["cpf"]);
		
		if(empty($_POST["cpf"]) ){$_POST["cpf"]  =  buscarUltimoId($conexao); $_POST["cpf"]++;}
		
		//Validação do array
		$emptyVazio = array('nome', 'apelido',
							'email', 'senha', 'nivel', 'empresa');	

		$array = array(					
				$_POST["nome"],				
				$_POST["apelido"],	
				
				$_POST["email"],			
				$_POST["senha"],		
				$_POST["nivel"],		
				$_POST["empresa"],		
			);	
		

		if( count($emptyVazio) == count($array) ){ echo 'Mesma Quantidades no Array <br>'; $gravar[$qtdGravar++] = 'Ok';}
		else {echo 'Quantidades Erradas <br>'.count($emptyVazio).'<br>'.count($array).'<br>';}
		
		//Upload aki
		if(!empty($_FILES['arquivo']['tmp_name'])){
			echo 'Subindo Arquivo ....<br>';
			$tamanhoMB = number_format((($_FILES['arquivo']["size"]/1024)/1024),2, '.', '');
			if($tamanhoMB > $tamanhoMaximoMB){ $gravar[$qtdGravar++] = 'Grande'; }

			mkdir($_UP['caminho'].'/'.$_POST["cpf"]);
			$extencao = strrchr($_FILES['arquivo']["name"],'.');
			$nomeArquivo = 'logo'.$extencao;	
			$caminho = $_UP['caminho'].'/'.$_POST["cpf"].'/'.$nomeArquivo;			
			move_uploaded_file($_FILES['arquivo']['tmp_name'], $caminho);
		} 
		else{ $caminho = $upVazio;	echo 'Sem arquivo ....';}	

		$gravar = array_unique ( $gravar );
		if(count($gravar) === 1){
			echo 'Gravando ...<br>';
			
			// Inserir usuario
			$usuario = array(
				intval($_POST["cpf"]),
				
				$_POST["nome"],				
				$_POST["apelido"],
				$_POST["sexo"], 
				$_POST["nascimento"], 				
				$caminho,			
					
				$_POST["email"],			
				$_POST["senha"], 						
				$_POST["nivel"], 						
				$_POST["empresa"]				
			);			
			//var_dump($usuario); Funionando
			if ( inserirUsuarioBD($conexao, $usuario)  ) {
				echo $_SESSION[ "msg" ] = 'Usuario Inserido<br>';				

			}else {
				$msg = mysqli_error( $conexao );
				echo $_SESSION[ "msg" ] = '------<br> Erro_Usuario: <br>'.$msg.'<br> ---------';				
			}
			
			$ultimoId['id'] = buscarUltimoId($conexao);
			// Inserir endereco
			$endereco = array( 
				intval($ultimoId['id']),
				intval($_POST["cep"]),		
				$_POST["estado"],			
				$_POST["cidade"],			
				$_POST["bairro"],			
				$_POST["endereco"],			
				intval($_POST["numero"]),	
				$_POST["complemento"], 
				$_POST["nivel"]	
			);	
			//var_dump($endereco); Funionando
			if ( inserirEndereco($conexao, $endereco)  ) { 
				echo $_SESSION[ "msg" ] = 'Endereco Inserido<br>';				

			}else {
				$msg = mysqli_error( $conexao );
				echo $_SESSION[ "msg" ] = '------<br> Erro_Endereco: <br>'.$msg.'<br> ---------';				
			}
			
			// Inserir Telefone			
			$qdtTelefone = count($_POST["telefone"]);
			echo '<br>Foram encontrados '. $qdtTelefone . " Telefones. <br> ";
			for($i=0; $i < $qdtTelefone; $i++){
				echo "<br> Gravando Telefone " . $i . "<br>";
				echo 'Contato: '.$_POST["contato"][$i] . "<br>";
				echo 'Telefone: '.$_POST["telefone"][$i] . "<br>";
				echo 'Ramal: '.$_POST["ramal"][$i] . "<br>";
				$telefone = array( 
					intval($ultimoId['id']),
					$_POST["contato"][$i],			
					intval($_POST["telefone"][$i]),			
					intval($_POST["ramal"][$i]),	
					$_POST["nivel"]	
				);	
				
				if ( inserirTelefone($conexao, $telefone)  ) {
				echo $_SESSION[ "msg" ] = 'Telefone Inserido <br>';				

				}else {
					$msg = mysqli_error( $conexao );
					echo $_SESSION[ "msg" ] = '------<br> Erro_Telefone: <br>'.$msg.'<br> ---------';				
				}		
			}			
			
			//echo '	<script>window.location.replace("../usuario.php?acao=novo");</script>';
		} else {
			echo 'Gravação não permitida ...<br>'; 
			//echo '	<script>window.location.replace("../usuario.php?acao=novo&tipo=sede");</script>';
		}

	} 
	else {
		
		if ($_SESSION[ "acao" ][ 'tipo' ] === 'administrador'){
			$_POST["nivel"] = '1';
		}
		if ($_SESSION[ "acao" ][ 'tipo' ] === 'funcionarios'){
			$_POST["nivel"] = '2';
		}
		if ($_SESSION[ "acao" ][ 'tipo' ] === 'cliente'){
			$_POST["nivel"] = '4';
		}
		switch ($_POST["nivel"]) {
				case 0:
					$chamar = "Error ";
					$retornoError = '../dashboard.php';				
					break;
				case 1:
					$chamar = "Administrador ";
					$proximoPasso = '../usuarioavan.php';
					$retornoError = '../usuario.php?acao=novo&tipo=administradores';
					break;
				case 2:
					$chamar = "Funcionário ";
					$proximoPasso = '../usuarioavan.php';
					$retornoError = '../usuario.php?acao=novo&tipo=funcionarios';
					break;
				case 4:
					$chamar = "Cliente ";
					if(!empty($_SESSION[ "acao" ][ 'acao' ])){
						$proximoPasso = '../listar.php?tipo=clientes';
						$retornoError = '../usuario.php?acao=novo&tipo=cliente';
					} else {
						$proximoPasso = '../index.php';
					}
					break;
			}
		
		
		//Verificação de Dados vazios	
		if(empty($_POST["nome"])){
			echo 'Nome Vazio';
			$_SESSION[ "log" ][ "msg" ] = 'Nome Obrigatório';
			unset($_SESSION[ "acao" ]);
			echo '<script>window.location.replace("'.$retornoError.'");</script>';
			exit;
		}
		if(empty($_POST["apelido"])){
			echo 'Sobrenome Vazio';
			$_SESSION[ "log" ][ "msg" ] = 'Sobrenome Obrigatório';
			unset($_SESSION[ "acao" ]);
			echo '<script>window.location.replace("'.$retornoError.'");</script>';
			exit;
		}
		if(empty($_POST["email"])){
			echo 'E-mail Vazio';
			$_SESSION[ "log" ][ "msg" ] = 'E-mail Obrigatório';
			unset($_SESSION[ "acao" ]);
			echo '<script>window.location.replace("'.$retornoError.'");</script>';
			exit;
		}
		if(empty($_POST["senha"])){
			echo 'Senha Vazio';
			$_SESSION[ "log" ][ "msg" ] = 'Senha Obrigatório';
			unset($_SESSION[ "acao" ]);
			echo '<script>window.location.replace("'.$retornoError.'");</script>';
			exit;
		}

		unset($_SESSION[ "acao" ][ 'auto' ]);
		
		
		
		//Verificar se Email ja foi usado
		$row = varificarUsuarioBD( $conexao, $_POST["email"] );
		if($row != 0){
			echo 'E-mail Já Cadastrado';
			$_SESSION[ "log" ][ "msg" ] = 'E-mail Já Cadastrado';
			unset($_SESSION[ "acao" ]);
			if($_SESSION[ "acao" ][ 'auto' ] === 'true'){
				echo '<script>window.location.replace("../autocadastro.php");</script>';
			} else{
				echo '<script>window.location.replace("../usuario.php?acao=novo&tipo='.$_SESSION[ "acao" ][ 'tipo' ].'");</script>';
			}
			exit;
		}
		
		
		// Cadastro de usuario de terceiros com user ja logado
		$_POST["nome"] = str_replace($caracterEspecialnbackspace, "", $_POST["nome"]);
		$_POST["apelido"] = str_replace($caracterEspecialnbackspace, "", $_POST["apelido"]);
		$_POST["nascimento"] = str_replace($caracterEspecial, "", $_POST["nascimento"]);		
		$_POST["cpf"] = str_replace($caracterEspecial, "", $_POST["cpf"]);
		$_POST["telefone"] = str_replace($caracterEspecial, "", $_POST["telefone"]);
		$_POST["senha"] = md5($_POST["senha"]);
		
		
		if(empty($_POST["nascimento"])){$_POST["nascimento"] = 11111111;}
		if(empty($_POST["cpf"]) ){$_POST["cpf"]  =  buscarUltimoId($conexao); $_POST["cpf"]++;}
		
		//Validação do array
		$emptyVazio = array('nome', 'apelido',
							'email', 'senha', 'nivel', 'empresa');	

		$array = array(					
				$_POST["nome"],				
				$_POST["apelido"],	
				
				$_POST["email"],			
				$_POST["senha"],		
				$_POST["nivel"],		
				$_POST["empresa"],		
			);	
		

		if( count($emptyVazio) == count($array) ){ echo 'Mesma Quantidades no Array <br>'; $gravar[$qtdGravar++] = 'Ok';}
		else {echo 'Quantidades Erradas <br>'.count($emptyVazio).'<br>'.count($array).'<br>';}
		
		//Upload aki
		//var_dump($_FILES['arquivo']);
		if(!empty($_FILES['arquivo']['tmp_name'])){
			echo 'Subindo Arquivo ....<br>';
			$tamanhoMB = number_format((($_FILES['arquivo']["size"]/1024)/1024),2, '.', '');
			if($tamanhoMB > $tamanhoMaximoMB){ $gravar[$qtdGravar++] = 'Grande'; }

			mkdir($_UP['caminho'].'/'.$_POST["cpf"]);
			$extencao = strrchr($_FILES['arquivo']["name"],'.');
			$nomeArquivo = 'logo'.$extencao;	
			$caminho = $_UP['caminho'].'/'.$_POST["cpf"].'/'.$nomeArquivo;			
			move_uploaded_file($_FILES['arquivo']['tmp_name'], $caminho);
		} 
		else{ $caminho = $upVazio;	echo 'Sem arquivo ....';}		
		
		$gravar = array_unique ( $gravar );
		if(count($gravar) === 1){
			echo 'Gravando ...<br>';
			
			// Inserir usuario
			$usuario = array(
				intval($_POST["cpf"]),
				
				$_POST["nome"],				
				$_POST["apelido"],
				$_POST["sexo"], 
				$_POST["nascimento"], 				
				$caminho,			
					
				$_POST["email"],			
				$_POST["senha"], 						
				$_POST["nivel"], 						
				$_POST["empresa"]				
			);			
			//var_dump($usuario); Funionando
			if ( inserirUsuarioBD($conexao, $usuario)  ) {
				echo $_SESSION[ "msg" ] = 'Usuario Inserido<br>';				

			}else {
				$msg = mysqli_error( $conexao );
				echo $_SESSION[ "msg" ] = '------<br> Erro_Usuario: <br>'.$msg.'<br> ---------';				
			}
			
			$ultimoId['id'] = buscarUltimoId($conexao);
			// Inserir endereco
			$endereco = array( 
				intval($ultimoId['id']),
				intval($_POST["cep"]),		
				$_POST["estado"],			
				$_POST["cidade"],			
				$_POST["bairro"],			
				$_POST["endereco"],			
				intval($_POST["numero"]),	
				$_POST["complemento"], 
				$_POST["nivel"]	
			);	
			//var_dump($endereco); Funionando
			if ( inserirEndereco($conexao, $endereco)  ) { 
				echo $_SESSION[ "msg" ] = 'Endereco Inserido<br>';				

			}else {
				$msg = mysqli_error( $conexao );
				echo $_SESSION[ "msg" ] = '------<br> Erro_Endereco: <br>'.$msg.'<br> ---------';				
			}
			
			// Inserir Telefone			
			$qdtTelefone = count($_POST["telefone"]);
			echo '<br>Foram encontrados '. $qdtTelefone . " Telefones. <br> ";
			for($i=0; $i < $qdtTelefone; $i++){
				echo "<br> Gravando Telefone " . $i . "<br>";
				echo 'Contato: '.$_POST["contato"][$i] . "<br>";
				echo 'Telefone: '.$_POST["telefone"][$i] . "<br>";
				echo 'Ramal: '.$_POST["ramal"][$i] . "<br>";
				$telefone = array( 
					intval($ultimoId['id']),
					$_POST["contato"][$i],			
					intval($_POST["telefone"][$i]),			
					intval($_POST["ramal"][$i]),	
					$_POST["nivel"]	
				);	
				
				if ( inserirTelefone($conexao, $telefone)  ) {
				echo $_SESSION[ "msg" ] = 'Telefone Inserido <br>';				

				}else {
					$msg = mysqli_error( $conexao );
					echo $_SESSION[ "msg" ] = '------<br> Erro_Telefone: <br>'.$msg.'<br> ---------';				
				}		
			}			
			
			echo '	<script>window.location.replace("../usuario.php?acao=novo");</script>';
		} else {
			echo 'Gravação não permitida ...<br>'; 
			echo '	<script>window.location.replace("../usuario.php?acao=novo&tipo=sede");</script>';
		}
		
	}
}

if ( $_SESSION[ "acao" ][ 'acao' ] ===  'alterar') {
	
		echo "Alterando Usuario  ".$_POST['id']." <br><br>";
	
		// Cadastro de usuario de terceiros com user ja logado
		$_POST["nome"] = str_replace($caracterEspecialnbackspace, "", $_POST["nome"]);
		$_POST["apelido"] = str_replace($caracterEspecialnbackspace, "", $_POST["apelido"]);
		$_POST["nascimento"] = str_replace($caracterEspecial, "", $_POST["nascimento"]);		
		$_POST["cpf"] = str_replace($caracterEspecial, "", $_POST["cpf"]);
	
		$_POST["telefone"] = str_replace($caracterEspecial, "", $_POST["telefone"]);
		$_POST["ramal"] = str_replace($caracterEspecial, "", $_POST["ramal"]);
	
		$_POST["cep"] = str_replace($caracterEspecial, "", $_POST["cep"]);
		
		
		
		if(empty($_POST["nascimento"])){$_POST["nascimento"] = 11111111;}
		if(empty($_POST["cpf"]) ){$_POST["cpf"]  =  buscarUltimoId($conexao); $_POST["cpf"]++;}
		
		//Validação do array
		$emptyVazio = array('nome', 'apelido',
							'email', 'nivel', 'empresa');	

		$array = array(					
				$_POST["nome"],				
				$_POST["apelido"],	
				
				$_POST["email"],		
				$_POST["nivel"],		
				$_POST["empresa"],		
			);	
		

		if( count($emptyVazio) == count($array) ){ echo 'Mesma Quantidades no Array <br>'; $gravar[$qtdGravar++] = 'Ok';}
		else {echo 'Quantidades Erradas <br>'.count($emptyVazio).'<br>'.count($array).'<br>';}
		
		//Upload aki
		//var_dump($_FILES['arquivo']);
		if(!empty($_FILES['arquivo']['tmp_name'])){
			echo 'Subindo Arquivo ....<br>';
			$tamanhoMB = number_format((($_FILES['arquivo']["size"]/1024)/1024),2, '.', '');
			if($tamanhoMB > $tamanhoMaximoMB){ $gravar[$qtdGravar++] = 'Grande'; }

			mkdir($_UP['caminho'].'/'.$_POST["cpf"]);
			$extencao = strrchr($_FILES['arquivo']["name"],'.');
			$nomeArquivo = 'logo'.$extencao;	
			$caminho = $_UP['caminho'].'/'.$_POST["cpf"].'/'.$nomeArquivo;			
			move_uploaded_file($_FILES['arquivo']['tmp_name'], $caminho);
		} 
		else{ $caminho = $upVazio;	echo 'Sem arquivo ....';}		
		
		$gravar = array_unique ( $gravar );
		if(count($gravar) === 1){
			echo 'Gravando ...<br>';
			
			// Alterar usuario //Funciona
			$usuario = array(
				intval($_POST["id"]),
				
				$_POST["nome"],				
				$_POST["apelido"],
				intval($_POST["cpf"]),
				intval($_POST["sexo"]), 
				intval($_POST["nascimento"]), 				
				$caminho,			
					
				$_POST["email"],						
				intval($_POST["nivel"]), 						
				intval($_POST["empresa"])				
			);		
			//var_dump($usuario); 
			if ( alterarUsuarioBD($conexao, $usuario)  ) {
				echo $_SESSION[ "msg" ] = 'Usuario Alterado<br>';				

			}else {
				$msg = mysqli_error( $conexao );
				echo $_SESSION[ "msg" ] = '------<br> Erro_Usuario: <br>'.$msg.'<br> ---------';				
			}
			
			//Alteração de senha //Funciona
			if( !empty($_POST["senha"]) ){
				$_POST["senha"] = md5($_POST["senha"]);
				
				if ( alterarSenhaBD( $conexao, $_POST["senha"], $_POST["id"] )  ) {
					echo $_SESSION[ "msg" ] = 'Senha Alterada<br>';				

				}else {
					$msg = mysqli_error( $conexao );
					echo $_SESSION[ "msg" ] = '------<br> Erro_Usuario: <br>'.$msg.'<br> ---------';				
				}
				
			}
			
			// Inserir endereco //Funcionando
			$endereco = array( 
				intval($_POST['id']),
				intval($_POST["cep"]),		
				$_POST["estado"],			
				$_POST["cidade"],			
				$_POST["bairro"],			
				$_POST["endereco"],			
				intval($_POST["numero"]),	
				$_POST["complemento"], 
				$_POST["nivel"]	
			);	
			//var_dump($endereco); 
			if ( alterarEndereco($conexao, $endereco)  ) { 
				echo $_SESSION[ "msg" ] = 'Endereco Alterado<br>';				

			}else {
				$msg = mysqli_error( $conexao );
				echo $_SESSION[ "msg" ] = '------<br> Erro_Endereco: <br>'.$msg.'<br> ---------';				
			}
			
			// Inserir Telefone	 //Funcionando		
			$qdtTelefone = count($_POST["telefone"]);
			echo '<br>Foram encontrados '. $qdtTelefone . " Telefones. <br> ";
			for($i=0; $i < $qdtTelefone; $i++){
				echo "<br> Gravando Telefone " . $i . "<br>";
				echo 'ID: '.$_POST["idTel"][$i] . "<br>";
				echo 'Contato: '.$_POST["contato"][$i] . "<br>";
				echo 'Telefone: '.$_POST["telefone"][$i] . "<br>";
				echo 'Ramal: '.$_POST["ramal"][$i] . "<br>";
				$telefone = array( 
					intval($_POST['idTel'][$i]),
					$_POST["contato"][$i],			
					intval($_POST["telefone"][$i]),			
					intval($_POST["ramal"][$i])
				);	
				
				if ( alterarTelefone($conexao, $telefone)  ) {
				echo $_SESSION[ "msg" ] = 'Telefone Inserido <br>';				

				}else {
					$msg = mysqli_error( $conexao );
					echo $_SESSION[ "msg" ] = '------<br> Erro_Telefone: <br>'.$msg.'<br> ---------';				
				}	
			}	
	}
	
	echo '<script> window.location.replace("../usuario.php"); </script>';
} 

/////////////////////////////////////////////////////////////////////////////////

if ( $_SESSION[ "acao" ][ 'acao' ] ===  'inseriravan') {

	$_POST["nascimeto"] = str_replace($caracterEspecial, "", $_POST["nascimeto"]);
	$_POST["expedicao"] = str_replace($caracterEspecial, "", $_POST["expedicao"]);
	$_POST["desligamento"] = str_replace($caracterEspecial, "", $_POST["desligamento"]);
	$_POST["admissao"] = str_replace($caracterEspecial, "", $_POST["admissao"]);

	$array = array(
		$_POST["id_usuario"],					//0		ok

		intval($_POST["salarioContratado"]),	//1		ok
		intval($_POST["cargaHoraria"]),			//2		ok
		$_POST["nascimeto"],					//3		ok
		$_POST["naturalidade"],					//4		ok
		$_POST["nacionalidade"],				//5		ok
		intval($_POST["estadocivil"]),			//6		ok

		//Contato
		intval($_POST["celular"]),				//7		ok
		intval($_POST["tRecado"]),				//8		ok
		$_POST["nRecado"],						//9		ok

		//Parentesco
		$_POST["pai"],							//10	ok
		$_POST["mae"],							//11	ok
		$_POST["conjege"],						//12	ok
		$_POST["filiacao"],						//13	ok

		intval($_POST["identidade"]),			//14	ok
		$_POST["identidadeOrgao"],				//15	ok
		$_POST["expedicao"],					//16	ok
		intval($_POST["pis"]),					//17	ok
		intval($_POST["ctps"]),					//18	ok
		intval($_POST["eleitor"]),				//19	ok
		intval($_POST["sessao"]),				//20	ok
		intval($_POST["zona"]),					//21	ok
		intval($_POST["passaporte"]),			//22	ok
		intval($_POST["reservista"]),			//23	ok
		intval($_POST["registroprofissional"]),	//24	ok
		$_POST["observacoesdocumento"],			//25	ok

		$_POST["formacao"],						//26	ok
		$_POST["qualificacao"],					//27	ok
		intval($_POST["contrato"]),				//28	ok
		$_POST["funcao"],						//29	ok
		$_POST["admissao"],						//30	ok
		$_POST["desligamento"],					//31	ok
		$_POST["motivo"],						//32	ok
		$_POST["observacoes_profissionais"],	//33	ok

		intval($_POST["transporte"]),			//34	ok
		intval($_POST["alimentacao"]),			//35	ok
		intval($_POST["planodeSaude"]),			//36	ok
		intval($_POST["planoOdontologico"])		//37	ok
												// + 2 (id + datadeCriacao)

	);

	//var_dump($array);

	if ( inserirusuarioAvancadoBD( $conexao, $array )  ) {

		$_SESSION[ "msg" ] = 'Usuario inserido.';

		echo '	<script> window.location.replace("../infbancaria.php");	</script>';

	}else {
		$msg = mysqli_error( $conexao );
		echo $_SESSION[ "msg" ] = '<br> Usuario não inserido . <br>	Erro: ' . $msg;

		//echo '	<script>		window.location.replace("../usuarioavan.php");		</script>';
	}
}

if ( $_SESSION[ "acao" ][ 'acao' ] ===  'atualizaravan') {

	$_POST["nascimeto"] = str_replace($caracterEspecial, "", $_POST["nascimeto"]);
	$_POST["expedicao"] = str_replace($caracterEspecial, "", $_POST["expedicao"]);
	$_POST["desligamento"] = str_replace($caracterEspecial, "", $_POST["desligamento"]);
	$_POST["admissao"] = str_replace($caracterEspecial, "", $_POST["admissao"]);


	$array = array(
		$_POST["id_usuario"],					//0		ok

		intval($_POST["salarioContratado"]),	//1		ok
		intval($_POST["cargaHoraria"]),			//2		ok
		$_POST["nascimeto"],					//3		ok
		$_POST["naturalidade"],					//4		ok
		$_POST["nacionalidade"],				//5		ok
		intval($_POST["estadocivil"]),			//6		ok

		//Contato
		intval($_POST["celular"]),				//7		ok
		intval($_POST["tRecado"]),				//8		ok
		$_POST["nRecado"],						//9		ok

		//Parentesco
		$_POST["pai"],							//10	ok
		$_POST["mae"],							//11	ok
		$_POST["conjege"],						//12	ok
		$_POST["filiacao"],						//13	ok

		intval($_POST["identidade"]),			//14	ok
		$_POST["identidadeOrgao"],				//15	ok
		$_POST["expedicao"],					//16	ok
		intval($_POST["pis"]),					//17	ok
		intval($_POST["ctps"]),					//18	ok
		intval($_POST["eleitor"]),				//19	ok
		intval($_POST["sessao"]),				//20	ok
		intval($_POST["zona"]),					//21	ok
		intval($_POST["passaporte"]),			//22	ok
		intval($_POST["reservista"]),			//23	ok
		intval($_POST["registroprofissional"]),	//24	ok
		$_POST["observacoesdocumento"],			//25	ok

		$_POST["formacao"],						//26	ok
		$_POST["qualificacao"],					//27	ok
		intval($_POST["contrato"]),				//28	ok
		$_POST["funcao"],						//29	ok
		$_POST["admissao"],						//30	ok
		$_POST["desligamento"],					//31	ok
		$_POST["motivo"],						//32	ok
		$_POST["observacoes_profissionais"],	//33	ok

		intval($_POST["transporte"]),			//34	ok
		intval($_POST["alimentacao"]),			//35	ok
		intval($_POST["planodeSaude"]),			//36	ok
		intval($_POST["planoOdontologico"])		//37	ok
												// + 2 (id + datadeCriacao)

	);

	if ( atualizarusuarioAvancadoBD( $conexao, $array )  ) {

		$_SESSION[ "msg" ] = '<br>Usuario Alterado. <br>';

		echo '<script> window.location.replace("../usuarioavan.php");	</script>';

	}else {
		$msg = mysqli_error( $conexao );
		echo $_SESSION[ "msg" ] = '<br> Usuario não Alterado. <br>	Erro:' . $msg ;

		echo '<script> window.location.replace("../usuarioavan.php");	</script>';
	}
}

if ( $_SESSION[ "acao" ][ 'acao' ] ===  'inserirbanco') {	
	
	if( is_null($_POST["tipodeconta"]) && 
	   is_null($_POST["banco"]) && 
	   empty($_POST["agencia"]) && 
	   empty($_POST["conta"])){
		echo '<script>window.location.replace("../infbancaria.php");</script>';
	} else {
	
		$_POST["tipodeconta"] = str_replace($caracterEspecial, "", $_POST["tipodeconta"]);
		$_POST["banco"] = str_replace($caracterEspecial, "", $_POST["banco"]);
		$_POST["agencia"] = str_replace($caracterEspecial, "", $_POST["agencia"]);
		$_POST["conta"] = str_replace($caracterEspecial, "", $_POST["conta"]);

		$array = array(
			$_SESSION[ "acao" ]['id'],
			intval($_POST["tipodeconta"]),
			intval($_POST["banco"]),
			intval($_POST["agencia"]),
			intval($_POST["conta"])
		);

		switch ($_POST["nivel"]) {
			case 0:
				$chamar = "Error ";
				break;
			case 1:
				$chamar = "Administrador ";
				$proximoPasso = '../usuarioavan.php';
				break;
			case 2:
				$chamar = "Funcionário ";
				$proximoPasso = '../usuarioavan.php';
				break;
			case 3:
				$chamar = "Empresa ";
				$proximoPasso = '../infbancaria.php';
				break;
			case 4:
				$chamar = "Clientes ";
				$proximoPasso = '../listar.php?tipo=clientes';
				break;
		}

		if ( inserirdadosbancoBD( $conexao, $array )  ) {
			$_SESSION[ "msg" ] = 'Inserido';
			echo '	<script>		window.location.replace("'.$proximoPasso.'");		</script>';

		}else {
			$msg = mysqli_error( $conexao );
			$_SESSION[ "msg" ] = 'Não inserido .<br>	Erro:' . $msg . '	</div>';
			echo '	<script>		window.location.replace("../usuarioavan.php");		</script>';
			}
	}
}

if ( $_SESSION[ "acao" ][ 'acao' ] ===  'atualizarbanco') {

		$_POST["tipodeconta"] = str_replace($caracterEspecial, "", $_POST["tipodeconta"]);
		$_POST["banco"] = str_replace($caracterEspecial, "", $_POST["banco"]);
		$_POST["agencia"] = str_replace($caracterEspecial, "", $_POST["agencia"]);
		$_POST["conta"] = str_replace($caracterEspecial, "", $_POST["conta"]);

	$array = array(
		$_SESSION[ "acao" ]['id'],
		intval($_POST["tipodeconta"]),
		intval($_POST["banco"]),
		intval($_POST["agencia"]),
		intval($_POST["conta"])
	);

	if ( atualizardadosbancoBD( $conexao, $array )  ) {

		$_SESSION[ "msg" ] = 'Usuario Alterado.';
		echo '<script>window.location.replace("../infbancaria.php");</script>';

	}else {
		$msg = mysqli_error( $conexao );
		$_SESSION[ "msg" ] = 'Usuario Não Alterado .<br>	Erro:' . $msg . '	</div>';
		echo '<script>window.location.replace("../infbancaria.php");</script>';
		}
}

if ( $_SESSION[ "acao" ][ 'acao' ] ===  'excluir') {

	if ( excluirUsuarioBD( $conexao, $_SESSION[ "acao" ][ 'id' ] )  ) {
		$_SESSION[ "msg" ] = ' Excluido.';
		unset($_SESSION[ "acao" ]);
		echo '<script> window.location.replace("../dashboard.php"); </script>';
		exit;
	}
	else {
		$msg = mysqli_error( $conexao );
		$_SESSION[ "msg" ] = 'Não Excluido.<br>	Erro:' . $msg . '	</div>';
		unset($_SESSION[ "acao" ]);
		echo '<script> window.location.replace("../dashboard.php"); </script>';		
		exit;
	}
}

if ( $_SESSION[ "acao" ][ 'acao' ] ===  'senha') {
	$_POST["senhaold"];
	$_POST["senhaold"] = md5($_POST["senhaold"]);

	$_POST["senhanew"];
	$_POST["senhanew2"];

	$_SESSION['usuario']['senha'];

	if($_POST["senhaold"] === $_SESSION['usuario']['senha']){

		if($_POST["senhanew"] === $_POST["senhanew2"]){

			$_POST["senhanew"] = md5($_POST["senhanew"]);

			if ( alterarSenhaBD( $conexao, $_POST["senhanew"], $_SESSION['usuario']['id'] )  ) {
				$msg = mysqli_error( $conexao );

				$_SESSION[ "msg" ] = 'Senha Alterada';
				unset($_SESSION[ "acao" ]);

				echo '<script> window.location.replace("logout.php"); </script>';

			}else {
				$msg = mysqli_error( $conexao );
				echo $_SESSION[ "msg" ] = 'Erro ao alterar Senha.<br>	Erro:' . $msg . '	</div>';
				unset($_SESSION[ "acao" ]);

				echo '<script> window.location.replace("../mudasenha.php"); </script>';
				}

			} else {
				echo "A senha não é a msm" ;

				unset($_SESSION[ "msg" ]);
				$_SESSION[ "msg" ] = 'Repetição incorreta  ';
				echo $_SESSION[ "msg" ];

				echo '<script> window.location.replace("../mudasenha.php"); </script>';
			}

	} else {
		echo "A senha não é a msm" ;

		unset($_SESSION[ "msg" ]);
		$_SESSION[ "msg" ] = 'Senha antiga incorreta  ';
		echo $_SESSION[ "msg" ];

		echo '<script> window.location.replace("../mudasenha.php"); </script>';
	}

}

if ( $_SESSION[ "acao" ][ 'acao' ] ===  'alterarmeuperfil') {

	echo $_SESSION[ "acao" ][ 'acao' ];

	$_POST["apelido"] = str_replace($caracterEspecialnbackspace, "", $_POST["apelido"]);
	$_POST["telefone"] = str_replace($caracterEspecial, "", $_POST["telefone"]);
	$_POST["cep"] = str_replace($caracterEspecial, "", $_POST["cep"]);


	$array = array(
	$_POST["apelido"],			//0
	$_POST["email"],			//1
	intval($_POST["telefone"]),	//2
	intval($_POST["cep"]),		//3
	$_POST["estado"],			//4
	$_POST["cidade"],			//5
	$_POST["endereco"],			//6
	intval($_POST["numero"]),	//7
	$_POST["complemento"]		//8
	);

	var_dump($array);


	if ( alterarmeuperfilBD($conexao, $array)  ) {
		$_SESSION[ "msg" ] = 'Alteração realizada com sucesso';

		unset($_SESSION[ "acao" ]);

		echo '<script>window.location.replace("../meuperfil.php");</script>';

	}else {
		$msg = mysqli_error( $conexao );
		$_SESSION[ "msg" ] = 'Erro ao alterar .<br>	Erro:' . $msg . '	</div>';

		echo '<script>window.location.replace("../meuperfil.php");</script>';
}

}

if ( $_SESSION[ "acao" ][ 'acao' ] ===  'inserirredessociais') {	

	$_POST["id"];
	$_POST["facebook"];
	$_POST["linkedin"];
	$_POST["instagram"];

	$array = array( $_POST["id"], $_POST["facebook"], $_POST["linkedin"], $_POST["instagram"]);

	if ( inserirRedesSociaisBD( $conexao, $array) ) {

			$_SESSION[ "msg" ] = 'Rede inserida.';
			echo '	<script> window.location.replace("../redessociais.php");	</script>';

		} else {
			$msg = mysqli_error( $conexao );
			echo $_SESSION[ "msg" ] = '<br> Rede não inserida. <br>	Erro: ' . $msg;
			echo '	<script> window.location.replace("../redessociais.php");	</script>';
		}

}

if ( $_SESSION[ "acao" ][ 'acao' ] ===  'alterarredessociais') {
	$_POST["facebook"];
	$_POST["linkedin"];
	$_POST["id_redessociais"];
	$_POST["instagram"];

	$array = array( $_POST["id_redessociais"], $_POST["facebook"], $_POST["linkedin"], $_POST["instagram"]);

	if ( alterarRedesSociaisBD( $conexao, $array) ) {

			$_SESSION[ "msg" ] = 'Rede Alterada.';
			echo '	<script> window.location.replace("../redessociais.php");	</script>';

		} else {
			$msg = mysqli_error( $conexao );
			echo $_SESSION[ "msg" ] = '<br> Rede não Alterada. <br>	Erro: ' . $msg;
			echo '	<script> window.location.replace("../redessociais.php");	</script>';
		}

}

if ( !empty( $_GET['acao'] )){

	unset($_SESSION[ "acao" ]);

	echo $_SESSION[ "acao" ][ 'acao' ] = $_GET['acao'];
	echo $_SESSION[ "acao" ][ 'id' ] = $_GET['token'];

	$infAvan = "false";
	$infBancaria = "false";

	echo '<script>window.location.replace("usuario.php");</script>';

} 
