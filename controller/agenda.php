<?php require_once('../loading.php'); include_once('../model/agenda.php');

error_reporting( E_ALL ^ E_NOTICE );

session_start();

//var_dump($_SESSION["acao"]["acao"]);

/*Tratando */
$caracterEspecial = array("/", "'", ".", "@", "-", "(", ")", " ", ":");
$caracterEspecialnbackspace = array("/", "'", ".", "@", "-", "(", ")");

if ( $_SESSION["acao"]["acao"] ===  'inserir') {
	
	$_SESSION["acao"]["acao"] ===  'inserir';
	
	//Verificação de Dados vazios		
	if ( empty( $_POST[ "atividade" ] ) ) {
		echo $_SESSION[ "log" ][ "msg" ] = $_SESSION[ "log" ][ "msg" ] . '<br> ' . 'Insira a Atividade';
		echo '	<script>window.location.replace("../agenda.php?acao=novo");</script>';
	}
	if ( empty( $_POST[ "data" ] ) ) {
		echo $_SESSION[ "log" ][ "msg" ] = $_SESSION[ "log" ][ "msg" ] . '<br> ' . 'Insira a Data';
		echo '	<script>window.location.replace("../agenda.php?acao=novo");</script>';
	}
	if ( empty( $_POST[ "dataf" ] ) ) {
		echo $_SESSION[ "log" ][ "msg" ] = $_SESSION[ "log" ][ "msg" ] . '<br> ' . 'Insira a Data Final';
		echo '	<script>window.location.replace("../agenda.php?acao=novo");</script>';
	}
	if ( empty( $_POST[ "abertura" ] ) ) {
		echo $_SESSION[ "log" ][ "msg" ] = $_SESSION[ "log" ][ "msg" ] . '<br> ' . 'Insira a Data de Abertura';
		echo '	<script>window.location.replace("../agenda.php?acao=novo");</script>';
	}
	if ( empty( $_POST[ "hora" ] ) ) {
		echo $_SESSION[ "log" ][ "msg" ] = $_SESSION[ "log" ][ "msg" ] . '<br> ' . 'Insira a Hora';
		echo '	<script>window.location.replace("../agenda.php?acao=novo");</script>';
	}
	if ( empty( $_POST[ "horaf" ] ) ) {
		echo $_SESSION[ "log" ][ "msg" ] = $_SESSION[ "log" ][ "msg" ] . '<br> ' . 'Insira a Hora Final';
		echo '	<script>window.location.replace("../agenda.php?acao=novo");</script>';
	}
	
	//Tratamento de Caracteres
	$_POST["nome"] = str_replace($caracterEspecialnbackspace, "", $_POST["nome"]);
	$_POST["descricao"] = str_replace($caracterEspecialnbackspace, "", $_POST["descricao"]);
	$_POST["data"] = str_replace($caracterEspecial, "", $_POST["data"]);
	$_POST["dataf"] = str_replace($caracterEspecial, "", $_POST["dataf"]);
	$_POST["abertura"] = str_replace($caracterEspecial, "", $_POST["abertura"]);
	$_POST["hora"] = str_replace($caracterEspecial, "", $_POST["hora"]);
	$_POST["horaf"] = str_replace($caracterEspecial, "", $_POST["horaf"]);
	$_POST["duracao"] = str_replace($caracterEspecial, "", $_POST["duracao"]);
	$_POST["ativo"] = 0;

	if( empty($_POST["recadastrados"]) ){ $_POST["recadastrados"] = 5 ;}

	//Dadoss de teste
	/*$_POST["data"] = '2018/11/05';
	$_POST["dataf"] = '2018/11/12';
	$_POST["frequencia"] = 1;*/
	/*echo $_POST["dia0"];	//Domingo
	echo $_POST["dia1"];	//Segunda
	echo $_POST["dia2"];	//Terça
	echo $_POST["dia3"];	//Quarta
	echo $_POST["dia4"] = 'on';	//Quinta
	echo $_POST["dia5"];	//Sexta
	echo $_POST["dia6"];	//Sábado*/
	//Fim Dados de teste

	include_once('../model/atividade.php');
	$atividade =  listarAtividadeBD($conexao, $_POST["atividade"]);

	switch ($_POST["frequencia"]) {
		case 1:
			echo "Diaria";
			echo '<br>';
			$espaco = 'day';
			break;
		case 2:
			echo "Semanal";
			echo '<br>';
			$espaco = 'week';
			break;
		case 3:
			echo "Mensal";
			echo '<br>';
			$espaco = 'month';
			break;
		case 4:
			echo "Anual";
			echo '<br>';
			$espaco = 'year';
			break;
	}

	echo '<br>';

	//Conversao em data
	echo $_POST["data"] = date('Y-m-d',strtotime($_POST["data"])); echo '<br>';
	echo $_POST["dataf"] = date('Y-m-d',strtotime($_POST["dataf"])); echo '<br>';echo '<br>';

	$data = date('Y-m-d',strtotime($_POST["data"]));

	$xml = simplexml_load_file('https://api.calendario.com.br/?ano=2018&estado=RJ&token=bGVhbmRyb3NhbmNoZXNsaW1hQGdtYWlsLmNvbSZoYXNoPTE4NzAxNjA1MA');
	$feriado = array();

	$i = 0;
	foreach($xml as $xml){
		$feriado[$i] = str_replace('', "", $xml->date);
		$i++;
	}

	$diasemana = array('Domingo', 'Segunda', 'Terça', 'Quarta', 'Quinta', 'Sexta', 'Sabado');
	$diaSelecionado =  array(	 	$_POST["dia0"], 	//0
									$_POST["dia1"],  	//1
									$_POST["dia2"],  	//2
									$_POST["dia3"],  	//3
									$_POST["dia4"],  	//4
									$_POST["dia5"],  	//5
									$_POST["dia6"]); 	//6	

	while($data < $_POST["dataf"]){

		$diasemana_numero = date('w', strtotime($data));
		$diasemana[$diasemana_numero];

		$DiaPermitido = $diaSelecionado[$diasemana_numero];

			if($DiaPermitido === 'on' ){

				echo 'Dia da semana escolhido';echo '<br>';
				echo $diasemana_numero; echo '<br>';
				echo $diasemana[$diasemana_numero]; echo '<br>';

				$diaAtual = date('d/m/Y',strtotime($data));
				$Eferiado = array_search($diaAtual, $feriado);

				if(!empty($Eferiado) ){
					$data = date('Y-m-d',strtotime('+1 '.$espaco, strtotime($data))); echo '<br>';
					echo "Feriado";echo '<br>';
					echo 'Dia Não Permitido';echo '<br>';
				}
				if(empty($Eferiado) ){
					echo "Não Feriado";echo '<br>';
					echo 'Dia Permitido';echo '<br>';
					echo $data;	echo '<br>';

					$tempoDisponivel = gmdate('H:i', abs( strtotime($_POST["horaf"]) - strtotime($_POST["hora"]) ) );
					$div = abs($atividade[0]['duracao'] / 60);
					$qtdHorarios = floor($tempoDisponivel / $div);

					//Horas para acadastrar
					$i=1;
					$hora = str_replace($caracterEspecial, "", $_POST["hora"]);


					while($i <= $qtdHorarios){
						$i++;
						echo '<br>';
						echo $hora = str_replace($caracterEspecial, "", $hora);

						$array = array(
							$_POST["nome"],			//0
							$_POST["descricao"],	//1
							$_POST["atividade"],	//2
							
							$data,					//3
							$hora,					//4
							
							$_POST["frequencia"],	//5
							
							$_POST["abertura"],		//6
							$_POST["quantidades"],	//7
							$_POST["empresa"],		//8
							$_POST["funcionario"],	//9
							
							intval($_POST["ativo"]),	//10
							intval($_POST["recadastrados"])	//11

						);
						
						var_dump($array);


						if ( inseriragendaBD($conexao, $array)  ) {
							echo $_SESSION[ "msg" ] = 'Agendamento Realizado';
							echo '	<script>window.location.replace("../agenda.php?acao=listar");</script>';

						}else {
							$msg = mysqli_error( $conexao );
							echo $_SESSION[ "msg" ] = 'Erro:' . $msg;
							//echo '	<script>window.location.replace("../agenda.php?acao=novo");</script>';
						}

						$hora = gmdate('H:i', abs( strtotime($hora) + strtotime(gmdate('H:s',$atividade[0]['duracao'])) ) );
					}

					echo '<br>';

					$data = date('Y-m-d',strtotime('+1 '.$espaco, strtotime($data))); echo '<br>';
				}

			} 
			else {
				$data = date('Y-m-d',strtotime('+1 '.$espaco, strtotime($data))); echo '<br>';
				echo $_SESSION[ "log" ][ "msg" ] = 'Escolha os dias para marcação';
				//echo '	<script>window.location.replace("../agenda.php?acao=novo");</script>';
			}

	}

	for($i = 0; $i <= 6; $i++){
		unset($_POST["dia$i"]);
		unset($diaSelecionado);
	}

/* Essa função quando habilitaa não permite o agendamento fds
if($diasemana[$diasemana_numero] === 'Sabado' || $diasemana[$diasemana_numero] === 'Domingo'){
	$data = date('Y-m-d',strtotime('+1 '.$espaco, strtotime($data))); echo '<br>';
} else { }	*/

}

if ( $_SESSION["acao"]["acao"] ===  'alterar') {


	$_POST["nome"] = str_replace($caracterEspecialnbackspace, "", $_POST["nome"]);
	$_POST["descricao"] = str_replace($caracterEspecialnbackspace, "", $_POST["descricao"]);
	$_POST["data"] = str_replace($caracterEspecial, "", $_POST["data"]);
	$_POST["abertura"] = str_replace($caracterEspecial, "", $_POST["abertura"]);
	$_POST["hora"] = str_replace($caracterEspecial, "", $_POST["hora"]);
	$_POST["duracao"] = str_replace($caracterEspecial, "", $_POST["duracao"]);

	if( empty($_POST["recadastrados"]) ){ $_POST["recadastrados"] = 5 ;}

	if($_POST["ativo"] === "on"){
		echo $_POST["ativo"] = "0";
	}else{
		echo $_POST["ativo"] = "1";
	}

	//var_dump($array);

	echo "<br>";
	echo "<br>";
	echo "<br>";
	echo "<br>";
	echo "<br>";

	$i=0;
	while ( $i <= $_POST["agendas_qtds"]  ) {
			if ( !empty($_POST[ "outros$i" ])  ) {
				$array = array(
					$_POST["nome"],					//0
					$_POST["descricao"],			//1
					intval($_POST["atividade"]),	//2
					$_POST["frequencia"],			//3
					$_POST["abertura"],				//4
					$_POST["quantidades"],			//5
					intval($_POST["empresa"]),		//6
					$_POST["funcionario"],			//7
					intval($_POST["ativo"]),		//8
					$_POST["outroid$i"],			//9
					intval($_POST["recadastrados"])	//10
				);
				if ( alterarAgenda2BD($conexao, $array)  ) {

					echo $_SESSION[ "msg" ] = 'Alteração realizada com sucesso';
					echo "<br>";

				}else {
					$msg = mysqli_error( $conexao );
					echo $_SESSION[ "msg" ] = '<br>	Erro:' . $msg . '	</div>';

				}

			}
			//Marcar todos
		
			if ( !empty($_POST[ marcarTodos]) ) {
				$array = array(
					$_POST["nome"],					//0
					$_POST["descricao"],			//1
					intval($_POST["atividade"]),	//2
					$_POST["frequencia"],			//3
					$_POST["abertura"],				//4
					$_POST["quantidades"],			//5
					intval($_POST["empresa"]),		//6
					$_POST["funcionario"],			//7
					intval($_POST["ativo"]),		//8
					$_POST["outroid$i"],			//9
					intval($_POST["recadastrados"])	//10
				);
				if ( alterarAgenda2BD($conexao, $array)  ) {

					echo $_SESSION[ "msg" ] = 'Alteração realizada com sucesso';
					echo "<br>";

				}else {
					$msg = mysqli_error( $conexao );
					echo $_SESSION[ "msg" ] = '<br>	Erro:' . $msg . '	</div>';
				}
				
			}
			$i++;
		}

	$array = array(
		$_POST["nome"],					//0
		$_POST["descricao"],			//1
		intval($_POST["atividade"]),	//2
		$_POST["data"],					//3
		$_POST["hora"],					//4
		$_POST["frequencia"],			//5
		$_POST["abertura"],				//6
		$_POST["quantidades"],			//7
		intval($_POST["empresa"]),		//8
		$_POST["funcionario"],			//9
		intval($_POST["ativo"]),		//10
		$_SESSION["acao"]["id"], 		//11
		intval($_POST["recadastrados"])	//12
	);

	if ( alterarAgendaBD($conexao, $array)  ) {

		echo $_SESSION[ "msg" ] = 'Alteração realizada com sucesso';

		echo '	<script>		window.location.replace("../agenda.php?acao=listar");		</script>';

	}else {
		$msg = mysqli_error( $conexao );
		echo $_SESSION[ "msg" ] = '<br>	Erro:' . $msg . '	</div>';

		//echo '	<script>		window.location.replace("../agenda.php?acao=listar");		</script>';
	}

}

if ( $_SESSION["acao"]["acao"]  === 'agendar') {
		//Agendar o clientes com o item
		$array = array(	$_SESSION[ 'usuario' ]['id'], $_SESSION[ "acao" ][ 'id' ]);

	if ( agendarHorarioBD($conexao, $array)  ) {

		unset($_SESSION["acao"]);
		$_SESSION[ "msg" ] = 'Agendado com Sucesso';
		echo '	<script>window.location.replace("../agenda.php?acao=agendados");</script>';

	}else {
		$msg = mysqli_error( $conexao );
		$_SESSION[ "msg" ] = '<br>	Erro:' . $msg . '	</div>';
		echo '	<script>window.location.replace("../agenda.php?acao=disponiveis");</script>';
	}
}

if ( $_SESSION["acao"]["acao"]  === 'cancelar') {
	if ( cancelarHorarioBD($conexao, $_SESSION[ "acao" ][ 'id' ])) {
		unset($_SESSION["acao"]);
		$_SESSION[ "msg" ] = 'Agendado com Sucesso';
		echo '	<script>window.location.replace("../agenda.php?acao=agendados");</script>';

	}else {
		$msg = mysqli_error( $conexao );
		$_SESSION[ "msg" ] = '<br>	Erro:' . $msg . '	</div>';
		echo '	<script>window.location.replace("../agenda.php?acao=agendados");</script>';
	}
}

if ( $_SESSION["acao"]["acao"]  === 'presenca') {

	include_once( 'usuario.php' );

	$vagas = listarVagas($conexao, $_SESSION[ "acao" ][ 'id' ]);

	foreach ( $vagas as $vagas ) {

		$PessoasAgendadas =  listarUsuarioBD( $conexao, $vagas['id_usuario'] );

		$pessoaId = $PessoasAgendadas[0]['id'];

		$array = array($_POST['id'.$pessoaId], $_POST['apelido'.$pessoaId], $_POST['presenca'.$pessoaId]);

		var_dump($array);

		if ( alterarPresencaBD($conexao, $array)) {
			echo $_SESSION[ "msg" ] = 'Presenca alterada com Sucesso';


		} else {
			$msg = mysqli_error( $conexao );
			echo $_SESSION[ "msg" ] = 'Erro: '.$msg.'</div>';

		}
	}

	echo '	<script>window.location.replace("../agenda.php?acao=visualizar&token='.$_SESSION[ "acao" ][ 'id' ].'");</script>';

}

if ( $_SESSION[ "acao" ][ 'acao' ] ===  'excluir') {
	echo $_SESSION[ "acao" ][ 'id' ] ;

	if ( excluirAgendaBD( $conexao, $_SESSION[ "acao" ][ 'id' ] )  ) {
		echo $_SESSION[ "msg" ] = 'Agendamento excluido';
		unset($_SESSION[ "acao" ]);

		echo '<script> window.location.replace("../agenda.php?acao=listar"); </script>';

	}else {
		$msg = mysqli_error( $conexao );
		echo $_SESSION[ "msg" ] = 'Agendamento não excluido .<br>	Erro:' . $msg . '	</div>';
		unset($_SESSION[ "acao" ]);

		echo '<script> window.location.replace("../agenda.php?acao=listar"); </script>';
		}
}

if ( $_SESSION[ "acao" ][ 'acao' ] ===  'adicionar') {

	$_SESSION[ "acao" ][ 'id' ] ;
	$_POST['empresa'];

	include_once( '../model/usuario.php' );

	$clientes = listarusuarioEmpresaBD($conexao, $_POST['empresa']);
	$qtds = count ($clientes);

	for( $i=0; $i < $qtds; $i++){

		if( $_POST['check'.$i] === 'on'){

			//Agendar o clientes com o item
			$array = array(	$clientes[$i]['id'], $_SESSION[ "acao" ][ 'id' ]);

			if ( agendarHorarioBD($conexao, $array)  ) {
				echo $_SESSION[ "msg" ] = 'Agendado com Sucesso';

			}else {
				$msg = mysqli_error( $conexao );
				echo $_SESSION[ "msg" ] = '<br>	Erro:' . $msg . '	</div>';
			}

		}

	}

	echo '	<script>window.location.replace("../agenda.php?acao=listar");</script>';
}



if ( !empty( $_GET['acao'] )){

	unset($_SESSION[ "acao" ]);

	echo $_SESSION[ "acao" ][ 'acao' ] = $_GET['acao'];
	echo $_SESSION[ "acao" ][ 'id' ] = $_GET['token'];


	echo '<script>window.location.replace("agenda.php");</script>';
}
