<?php require_once('../loading.php');

error_reporting( E_ALL ^ E_NOTICE );
error_reporting( E_WARNING );
error_reporting( 0 );

session_start();
include_once( '../model/aula.php' );



$tamanhoMaximoMB = 5;
$caminho = 'D:/VertrigoServ/www/reabily/aula/';	// Teste
//$caminho = '/home/vlannetw/vlansolution.com.br/projetos/homologacao_reabily/aula/';	//Homologação
//$caminho = '/home/vlannetw/vemsermovimento.com.br/sistema/aula/';	//Produção

if( $_POST['acao'] === 'novo' ){
	
	$ultimoID = buscarUltimaAula( $conexao );
	echo 'COD: '.++$ultimoID['id'] . "<br>";
	
	$_UP['caminho'] = $caminho.$_SESSION[ "usuario" ]["id"].'_'.$ultimoID['id'].'/';	
	
	//Verifica se ha arquivos
	$existeArquivo = strrchr($_FILES['arquivo']["name"][0],'.');
	
	if( is_string($existeArquivo) ){ 
		
		echo "Realizando Upload"; 
	
		for($i=0; $i < count($_FILES['arquivo']['name']); ++$i){

			echo "<br>-----Arquivo ".$i."------<br>";	
			/*echo 'Nome: '.$_FILES['arquivo']["name"][$i];
			echo "<br>";
			echo 'Tamanho: '.$_FILES['arquivo']["size"][$i]. "K";
			echo "<br>";
			echo 'Tamanho: '.($_FILES['arquivo']["size"][$i]/1024). "KB";
			echo "<br>";
			echo 'Tamanho: '.(($_FILES['arquivo']["size"][$i]/1024)/1024). "MB";
			echo "<br>";
			echo 'Tipo: '.$_FILES['arquivo']["type"][$i];	
			echo "<br>";
			echo 'Temporario: '.$_FILES['arquivo']["tmp_name"][$i];	
			echo "<br>";
			echo 'Permanente: '.$_UP['caminho'];
			echo "<br>";
			echo 'Erro: '.$_FILES['arquivo']['error'][$i] ;	
			echo "<br>";	
			echo $_SERVER['REMOTE_ADDR'];
			echo "<br>";	
			echo "Nome do Usuario: ".$_SESSION[ 'usuario' ]['nome'][$i];
			echo "<br>-----------<br>";*/	

			$tamanhoMB = number_format((($_FILES['arquivo']["size"][$i]/1024)/1024),2, '.', '');
			if($tamanhoMB > $tamanhoMaximoMB){ 
				echo 'Arquivo Muito Grande'; 		 
				echo '<script>window.location.replace("../aula.php?acao=novo");</script>';
				exit();
			}

			mkdir($_UP['caminho']);
			$extencao = strrchr($_FILES['arquivo']["name"][$i],'.');
			$nomeArquivo = $_SESSION[ "usuario" ]["id"].'_'.$ultimoID['id'].'_'.date("YmdHis")."_".$i.$extencao;		
			move_uploaded_file($_FILES['arquivo']['tmp_name'][$i], $_UP['caminho'].$nomeArquivo);

			echo $localArquivo = $localArquivo.$_UP['caminho'].$nomeArquivo." ; ";
			}
		echo '<br><br> Total de '.$i.' arquivos.<br>';
	} 
	else{
		echo " Sem arquivos para Upload. Continuando ....";
	}
	
	$array = array( 
		$_POST[ 'nome' ],		//0
		$_POST[ 'objetivo' ],	//1
		$_POST[ 'descricao' ],	//2
		$_POST[ 'id_tipo' ],	//3
		$_POST[ 'links' ],		//4
		$localArquivo			//5
	);	

	if( inserirAulaBD($conexao, $array)  ) {		
		$qtd = count($_POST[ 'id_material' ]);

		$ultimoID = buscarUltimaAula( $conexao );

		$i = 0;
		while ($i < $qtd) {
			$aula_material = array( $_POST[ 'id_material' ][$i], $ultimoID['id'] );
			inserirAulaMaterial($conexao, $aula_material);
			$i++;
		}
		unset($i);
		
		$_SESSION[ "msg" ] = 'Aula Cadastrada.';
		echo '<script>window.location.replace("../aula.php?acao=listar");</script>';

	} else {
		$msg = mysqli_error( $conexao );
		echo $_SESSION[ "msg" ] = 'Aula Não Inserida .<br> Erro:' . $msg ;
		//echo '<script>window.location.replace("../aula.php?acao=novo");</script>';
	}
	
}

if( $_POST['acao'] === 'editar' ){	
	
	echo 'COD: '.$_POST[ 'id' ] . "<br>";
	
	$_UP['caminho'] = $caminho.$_SESSION[ "usuario" ]["id"].'_'.$_POST[ 'id' ].'/';	
	
	//Verifica se ha arquivos
	var_dump($_FILES['arquivo']);
	$existeArquivo = strrchr($_FILES['arquivo']["name"][0],'.');
	
	
	if( is_string($existeArquivo) ){ 
		
		echo "Realizando Upload"; 
	
		for($i=0; $i < count($_FILES['arquivo']['name']); ++$i){

			echo "<br>-----Arquivo ".$i."------<br>";

			$tamanhoMB = number_format((($_FILES['arquivo']["size"][$i]/1024)/1024),2, '.', '');
			if($tamanhoMB > $tamanhoMaximoMB){ 
				echo 'Arquivo Muito Grande'; 		 
				echo '<script>window.location.replace("../aula.php?acao=novo");</script>';
				exit();
			}

			mkdir($_UP['caminho']);
			$extencao = strrchr($_FILES['arquivo']["name"][$i],'.');
			$nomeArquivo = $_SESSION[ "usuario" ]["id"].'_'.$_POST[ 'id' ].'_'.date("YmdHis")."_".$i.$extencao;		
			move_uploaded_file($_FILES['arquivo']['tmp_name'][$i], $_UP['caminho'].$nomeArquivo);

			echo $localArquivo = $localArquivo.$_UP['caminho'].$nomeArquivo." ; ";
			}
		echo '<br><br> Total de '.$i.' arquivos.<br>';
	} 
	else{
		echo " Sem arquivos para Upload. Continuando ....";
	}
	
	$array = array( $_POST[ 'id' ],			//0
				   	$_POST[ 'nome' ],		//1
				    $_POST[ 'objetivo' ],	//2
					$_POST[ 'descricao' ],	//3
					$_POST[ 'id_tipo' ],	//4
					$_POST[ 'links' ],		//5
					$_POST[ 'caminho' ].$localArquivo			//6
				  );
	

	if ( alterarAulaBD($conexao, $array)  ) {
		
		deletarAulaMaterial( $conexao, $_POST[ 'id' ] );
		
		$qtd = count($_POST[ 'id_material' ]);
		$i = 0;
		while ($i < $qtd) {
			$aula_material = array( $_POST[ 'id_material' ][$i], $_POST[ 'id' ] );
			inserirAulaMaterial($conexao, $aula_material);
			$i++;
		}
		unset($i);
		$_SESSION[ "msg" ] = 'Aula Cadastrada.';
		echo '<script>window.location.replace("../aula.php?acao=listar");</script>';

	} else {
		$msg = mysqli_error( $conexao );
		echo $_SESSION[ "msg" ] = 'Aula Não Inserida.<br> Erro:' . $msg ;
		//echo '<script>window.location.replace("../aula.php?acao=novo");</script>';
	}
}

if ($_GET[ 'acao' ] ===  'excluir') {	

	if ( excluirAulaBD( $conexao, $_GET[ "token"] )  ) {
		echo $_SESSION[ "msg" ] = ' Excluido.';
		unset($_SESSION[ "acao" ]);
		echo '<script> window.location.replace("../aula.php?acao=listar"); </script>';
		exit;
	}
	else {
		$msg = mysqli_error( $conexao );
		echo $_SESSION[ "msg" ] = 'Não Excluido.<br>	Erro:' . $msg . '	</div>';
		unset($_SESSION[ "acao" ]);
		//echo '<script> window.location.replace("../dashboard.php"); </script>';		
		exit;
	}
}

if ($_GET[ 'acao' ] ===  'excluirarquivo') {
	
	echo '<br> 1 - '.$_GET[ 'acao' ].' ok... ';
	
	echo '<br>######################<br>';
	
	echo '<br> 2 - '.$_GET[ 'token' ].' ok ... <br>';
	$aula = listarAulaIdBD($conexao, $_GET[ 'token' ]);
	
	
	echo '<br>######################<br>';
	
	echo '<br> 3 - '.$_GET[ 'arquivotipo' ].' ok ... ';
	switch ($_GET[ 'arquivotipo' ]){										
		case 1:
			echo "Todos";
			$tipo = 'link';
			break;												
		case 2:
			echo "Img";
			$tipo = 'caminho';
			break;												
		case 3:
			echo "PDF";
			$tipo = 'caminho';
			break;	
	}
	echo '<br>######################<br>';
			
	echo '<br> 4 - '.$_GET[ 'arquivo' ].' exluindo ...';	
	echo "<br>";
	
	$array = array();
	$array = explode(' ; ',$aula[0]['caminho']);
	$qtdaula = count($array);
	
	foreach ( $array as $array ) {	
		$nome = strrchr($array,'/');		
		
		$bin = strcmp($nome,$_GET[ 'arquivo' ]);
		
		if( $bin == 0 ){			
			echo '<br>Pulando<br>';
		}
		else{
			if( empty($novo)){
				$novo = $array;			
			}
			else{
				$novo = $novo.' ; '.$array;			
			}
		}
	}
	
	$arraynovo = explode(' ; ',$novo);
	$qtdnovo = count($arraynovo);
	
	if($qtdaula != $qtdnovo){
		if($qtdaula < $qtdnovo){ echo 'Algo deu errado, os numeros aumentaram'; exit();}
		if($qtdaula > $qtdnovo){ 
			echo "<br> Antes tinham ". $qtdaula;
			echo "<br> Agora tem ". $qtdnovo;
			$qdttotal = $qtdaula - $qtdnovo;
			echo '<br> Foram excluidas '.  $qdttotal;
		}
	}
	else { echo "Algo deu errado, os numeros continuam os msm;"; exit();}
	
	echo '<br>######################<br>';
	
	/*
	Aula = $_GET[ 'token' ]
	Campo = $tipo
	Alterar para = $novo
	*/
	
	if ( excluirAulaArquivoBD( $conexao, $_GET[ 'token' ],  $tipo, $novo)  ) {
		echo $_SESSION[ "msg" ] = ' Excluido.';
		unset($_SESSION[ "acao" ]);
		echo '<script> window.location.replace("../aula.php?acao=listar"); </script>';
		exit;
	}
	else {
		$msg = mysqli_error( $conexao );
		echo $_SESSION[ "msg" ] = 'Não Excluido.<br>	Erro:' . $msg . '	</div>';
		unset($_SESSION[ "acao" ]);
		//echo '<script> window.location.replace("../dashboard.php"); </script>';		
		exit;
	}
}