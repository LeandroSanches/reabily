<?php require_once('../loading.php'); 
require_once('../model/empresa.php'); 
require_once('../model/telefone.php'); 
require_once('../model/endereco.php'); 

error_reporting( E_ERROR | E_PARSE );

echo '<style>body{color: aliceblue;}</style>';

//Upload 
$tamanhoMaximoMB = 5;
$_UP['caminho']= 'D:/VertrigoServ/www/reabily/usuarios';											// Teste
//$_UP['caminho'] = '/home/vlannetw/vlansolution.com.br/projetos/homologacao_reabily/usuarios';		// Homologação
//$_UP['caminho'] = '/home/vlannetw/vemsermovimento.com.br/sistema/usuarios';						// Produção

$upVazio = 'D:/VertrigoServ/www/reabily/usuarios/Usuario_Padrao.png';											// Teste
//upVazio = '/home/vlannetw/vlansolution.com.br/projetos/homologacao_reabily/usuarios/Usuario_Padrao.png';		// Homologação
//upVazio = '/home/vlannetw/vemsermovimento.com.br/sistema/usuarios/Usuario_Padrao.png';						// Produção

/*Tratando */
$caracterEspecial = array("/", "'", ".", "@", "-", "(", ")", " ");
$caracterEspecialnbackspace = array("/", "'", ".", "@", "-", "(", ")");

session_start();

if ( $_SESSION[ "acao" ][ 'acao' ] ===  'novo'){
	
	if ( $_SESSION[ "acao" ][ 'tipo' ] ===  'sede'){
		
		echo 'Cadastro de '.$_SESSION[ "acao" ][ 'tipo' ]."<br>";
	
		$qtdGravar = 0;

		//Verificar se Email ja foi usado
		$row = verificarUsuarioBD( $conexao, $_POST["email"] );
		if($row != 0){
			echo 'E-mail Já Cadastrado';
			echo '<script>window.location.replace("../empresa.php?acao=novo&tipo=sede");</script>';
			exit;
		} else { 
			echo 'Usuario não existe ---------------- Continuando... <br>';
			$gravar[$qtdGravar++] = 'Ok';
		}

		//Dados padrões
		$_POST["ativo"] = '0';
		$_POST["nivel"] = '3'; //Empresa Sede

		
		// Tratamento
		$_POST["cnpj"] = str_replace($caracterEspecialnbackspace, "", $_POST["cnpj"]);
		$_POST["nome"] = str_replace($caracterEspecialnbackspace, "", $_POST["nome"]);
		$_POST["apelido"] = str_replace($caracterEspecialnbackspace, "", $_POST["apelido"]);
		$_POST["cep"] = str_replace($caracterEspecialnbackspace, "", $_POST["cep"]);
		
		$_POST["telefone"] = str_replace($caracterEspecial, "", $_POST["telefone"]);
		$_POST["nascimento"] = str_replace($caracterEspecial, "", $_POST["nascimento"]);
		$_POST["senha"] = md5($_POST["senha"]);		

		
		$proximoPasso = "../lista.php?tipo=empresa";
		/////////////////////////////////////////////////////////	

		//Validação do array
		$emptyVazio = array('cnpj', 'nome', 'apelido', 'foto', 
							'sede', 'email', 'senha'
							//'contato', 'telefone', 'ramal',
							//'municipal', 'estadual', 'nfe', 'obs',
							//'cep', 'estado', 'cidade', 'bairro', 'endereco', 'numero', //'complemento'
						   );	

		$array = array(
				intval($_POST["cnpj"]),		
				$_POST["nome"],				
				$_POST["apelido"],			
				$_UP['caminho'],			

				$_POST["sede"],						
				$_POST["email"],			
				$_POST["senha"],			

				/*$_POST["contato"],			
				$_POST["telefone"],			
				$_POST["ramal"],			

				intval($_POST["municipal"]),	
				intval($_POST["estadual"]),		
				intval($_POST["nfe"]),			
				intval($_POST["obs"]), 			

				intval($_POST["cep"]),		
				$_POST["estado"],			
				$_POST["cidade"],			
				$_POST["bairro"],			
				$_POST["endereco"],			
				intval($_POST["numero"]),	
				//$_POST["complemento"]	*/	
			);	
		

		if( count($emptyVazio) == count($array) ){ echo 'Mesma Quantidades no Array <br>'; $gravar[$qtdGravar++] = 'Ok';}
		else {echo 'Quantidades Erradas <br>'.count($emptyVazio).'<br>'.count($array).'<br>';}

		echo 'Validando informações do Usuario ... <br>';
		$msg = '-----<br>';
		for($i=0; $i < count($array); $i++){
			if(empty($array[$i])){
				if( array_key_exists($array[$i], $emptyVazio) === false){
					$msg = $msg.$emptyVazio[$i].'<br>';
					$gravar[$qtdGravar++] = 'Vazio';
				} else { $gravar[$qtdGravar++] = 'Ok'; }			
			}
		}	
		//Fim da Validação do array	
		
		//Upload aki
		if(!empty($_FILES['arquivo'])){
			echo 'Subindo Arquivo ....<br>';
			$tamanhoMB = number_format((($_FILES['arquivo']["size"]/1024)/1024),2, '.', '');
			if($tamanhoMB > $tamanhoMaximoMB){ $gravar[$qtdGravar++] = 'Grande'; }

			mkdir($_UP['caminho'].'/'.$_POST["cnpj"]);
			$extencao = strrchr($_FILES['arquivo']["name"],'.');
			$nomeArquivo = 'logo'.$extencao;	
			$caminho = $_UP['caminho'].'/'.$_POST["cnpj"].'/'.$nomeArquivo;			
			move_uploaded_file($_FILES['arquivo']['tmp_name'], $caminho);
		} else{ $caminho = $upVazio;	echo 'Sem arquivo ....';}
		
	 	$gravar = array_unique ( $gravar );
		if(count($gravar) === 1){
			echo 'Gravando ...<br>';
			
			// Inserir usuario_Empresa_Sede
			$usuario_empresa_sede = array(
				intval($_POST["cnpj"]),		
				$_POST["nome"],				
				$_POST["apelido"],			
				$caminho,			

				$_POST["sede"],						
				$_POST["email"],			
				$_POST["senha"]				
			);			
			if ( inserirEmpresaSede($conexao, $usuario_empresa_sede)  ) {
				echo $_SESSION[ "msg" ] = 'Sede Inserida<br>';				

			}else {
				$msg = mysqli_error( $conexao );
				echo $_SESSION[ "msg" ] = '------<br> Erro_Sede: <br>'.$msg.'<br> ---------';				
			}
			
			$ultimoId = buscarUltimoIdEmpresa( $conexao, 'usuario_empresa_sede');	
			// Inserir endereco
			$endereco = array( 
				$ultimoId['id'],
				intval($_POST["cep"]),		
				$_POST["estado"],			
				$_POST["cidade"],			
				$_POST["bairro"],			
				$_POST["endereco"],			
				intval($_POST["numero"]),	
				$_POST["complemento"], 
				$_POST["nivel"]
			);	
			
			if ( inserirEndereco($conexao, $endereco)  ) {
				echo $_SESSION[ "msg" ] = 'Endereco Inserido<br>';				

			}else {
				$msg = mysqli_error( $conexao );
				echo $_SESSION[ "msg" ] = '------<br> Erro_Endereco: <br>'.$msg.'<br> ---------';				
			}
			
			$ultimoIdEndereco = buscarUltimoIdEmpresa( $conexao, 'usuario_endereco');
			// Inserir empresa					   
			$empresa = array( 
				$ultimoId['id'],
				$ultimoIdEndereco['id'],
				$_POST["sede"],	
				intval($_POST["municipal"]),	
				intval($_POST["estadual"]),		
				intval($_POST["nfe"]),			
				intval($_POST["obs"]),
				$_POST["nivel"]
			);			
							
			if ( inserirEmpresa($conexao, $empresa)  ) {
				echo $_SESSION[ "msg" ] = 'Empresa Inserida<br>';				

			}else {
				$msg = mysqli_error( $conexao );
				echo $_SESSION[ "msg" ] = '------<br> Erro_Empresa: <br>'.$msg.'<br> ---------';				
			}
			
			// Inserir Telefone			
			$qdtTelefone = count($_POST["telefone"]);
			echo '<br>Foram emcontrados '. $qdtTelefone . " Telefones. <br> ";
			for($i=0; $i < $qdtTelefone; $i++){
				echo "<br> Gravando Telefone " . $i . "<br>";
				echo 'Contato: '.$_POST["contato"][$i] . "<br>";
				echo 'Telefone: '.$_POST["telefone"][$i] . "<br>";
				echo 'Ramal: '.$_POST["ramal"][$i] . "<br>";
				$telefone = array( 
					intval($ultimoId['id']),
					$_POST["contato"][$i],			
					intval($_POST["telefone"][$i]),			
					intval($_POST["ramal"][$i]),	
					$_POST["nivel"]	
				);	
				
				if ( inserirTelefone($conexao, $telefone)  ) {
				echo $_SESSION[ "msg" ] = 'Telefone Inserido <br>';				

				}else {
					$msg = mysqli_error( $conexao );
					echo $_SESSION[ "msg" ] = '------<br> Erro_Telefone: <br>'.$msg.'<br> ---------';				
				}
				
				echo "Telefone Gravado <br>";
			}
			
			
			echo '	<script>window.location.replace("../empresa.php?acao=novo");</script>';
		} else {
			echo 'Gravação não permitida ...<br>'; 
			//echo '	<script>window.location.replace("../empresa.php?acao=novo&tipo=sede");</script>';
		}		
	
	}
	
	if ( $_SESSION[ "acao" ][ 'tipo' ] ===  'filial'){
		
		echo 'Cadastro de '.$_SESSION[ "acao" ][ 'tipo' ]."<br>";
	
		$qtdGravar = 0;

		//Verificar se Email ja foi usado na tabela usuario(Alterar pra todas as tabelas) 
		$row = verificarUsuarioBD( $conexao, $_POST["email"] );
		if($row != 0){
			echo 'E-mail Já Cadastrado';
			echo '<script>window.location.replace("../empresa.php?acao=novo&tipo=sede");</script>';
			exit;
		} else { 
			echo 'Usuario não existe ---------------- Continuando... <br>';
			$gravar[$qtdGravar++] = 'Ok';
		}

		//Dados padrões 
		$_POST["ativo"] = '0';
		$_POST["nivel"] = '5'; //Empresa Filial
		
		// Tratamento 
		$_POST["cnpj"] = str_replace($caracterEspecialnbackspace, "", $_POST["cnpj"]);
		$_POST["nome"] = str_replace($caracterEspecialnbackspace, "", $_POST["nome"]);
		$_POST["apelido"] = str_replace($caracterEspecialnbackspace, "", $_POST["apelido"]);
		$_POST["cep"] = str_replace($caracterEspecialnbackspace, "", $_POST["cep"]);
		
		$_POST["telefone"] = str_replace($caracterEspecial, "", $_POST["telefone"]);
		$_POST["nascimento"] = str_replace($caracterEspecial, "", $_POST["nascimento"]);
		$_POST["senha"] = md5($_POST["senha"]);
		$_POST['arquivo'] = strchr($_POST['arquivo'],'/');
		$caminho = $_UP['caminho'].$_POST['arquivo'];

		
		$proximoPasso = "../lista.php?tipo=empresa";
		/////////////////////////////////////////////////////////	

		//Validação do array
		$emptyVazio = array('cnpj', 'nome', 'apelido', 'foto', 
							'sede', 'email', 'senha'
						   );	

		$array = array(
				intval($_POST["cnpj"]),		
				$_POST["nome"],				
				$_POST["apelido"],			
				$caminho,			

				$_POST["sede"],						
				$_POST["email"],			
				$_POST["senha"],			
			);
		
		//var_dump($array);

		if( count($emptyVazio) == count($array) ){ echo 'Mesma Quantidades no Array <br>'; $gravar[$qtdGravar++] = 'Ok';}
		else {echo 'Quantidades Erradas <br>'.count($emptyVazio).'<br>'.count($array).'<br>';}

		echo 'Validando informações do Usuario ... <br>';
		$msg = '-----<br>';
		for($i=0; $i < count($array); $i++){
			if(empty($array[$i])){
				if( array_key_exists($array[$i], $emptyVazio) === false){
					$msg = $msg.$emptyVazio[$i].'<br>';
					$gravar[$qtdGravar++] = 'Vazio';
				} else { $gravar[$qtdGravar++] = 'Ok'; }			
			}
		}	
		//Fim da Validação do array	
		
		//var_dump($gravar);
	 	$gravar = array_unique ( $gravar );
		if(count($gravar) === 1){
			echo 'Gravando ...<br>';
			
			// Inserir usuario_Empresa_Filial
			$usuario_empresa_filial = array(				
				intval($_POST["id_sede"]),
				intval($_POST["cnpj"]),		
				$_POST["nome"],				
				$_POST["apelido"],			
				$caminho,			

				$_POST["sede"],						
				$_POST["email"],			
				$_POST["senha"],
				$_POST["ativo"]
			);			
			if ( inserirEmpresaFilial($conexao, $usuario_empresa_filial)  ) {
				echo $_SESSION[ "msg" ] = 'Filial Inserida<br>';				

			}else {
				$msg = mysqli_error( $conexao );
				echo $_SESSION[ "msg" ] = '------<br> Erro_Filial: <br>'.$msg.'<br> ---------';				
			}
			
			// Inserir endereco
			$ultimoId = buscarUltimoIdEmpresa( $conexao, 'usuario_empresa_filial');	
			$endereco = array( 
				$ultimoId['id'],
				intval($_POST["cep"]),		
				$_POST["estado"],			
				$_POST["cidade"],			
				$_POST["bairro"],			
				$_POST["endereco"],			
				intval($_POST["numero"]),	
				$_POST["complemento"],
				$_POST["nivel"]
			);	
			
			if ( inserirEndereco($conexao, $endereco)  ) {
				echo $_SESSION[ "msg" ] = 'Endereco Inserido<br>';				

			}else {
				$msg = mysqli_error( $conexao );
				echo $_SESSION[ "msg" ] = '------<br> Erro_Endereco: <br>'.$msg.'<br> ---------';				
			}
			
			// Inserir empresa
			$ultimoIdEndereco = buscarUltimoIdEmpresa( $conexao, 'usuario_endereco');								   
			$empresa = array( 
				$ultimoId['id'],
				$ultimoIdEndereco['id'],
				'',	
				intval($_POST["municipal"]),	
				intval($_POST["estadual"]),		
				intval($_POST["nfe"]),			
				intval($_POST["obs"]),
				$_POST["nivel"]
			);			
							
			if ( inserirEmpresa($conexao, $empresa)  ) {
				echo $_SESSION[ "msg" ] = 'Empresa Inserida<br>';				

			}else {
				$msg = mysqli_error( $conexao );
				echo $_SESSION[ "msg" ] = '------<br> Erro_Empresa: <br>'.$msg.'<br> ---------';				
			}
			
			// Inserir Telefone			
			$qdtTelefone = count($_POST["telefone"]);
			echo '<br>Foram emcontrados '. $qdtTelefone . " Telefones. <br> ";
			for($i=0; $i < $qdtTelefone; $i++){
				echo "<br> Gravando Telefone " . $i . "<br>";
				echo 'Contato: '.$_POST["contato"][$i] . "<br>";
				echo 'Telefone: '.$_POST["telefone"][$i] . "<br>";
				echo 'Ramal: '.$_POST["ramal"][$i] . "<br>";
				$telefone = array( 
					intval($ultimoId['id']),
					$_POST["contato"][$i],			
					intval($_POST["telefone"][$i]),			
					intval($_POST["ramal"][$i]),	
					$_POST["nivel"]	//Tipo Filial
				);	
				
				if ( inserirTelefone($conexao, $telefone)  ) {
				echo $_SESSION[ "msg" ] = 'Telefone Inserido <br>';				

				}else {
					$msg = mysqli_error( $conexao );
					echo $_SESSION[ "msg" ] = '------<br> Erro_Telefone: <br>'.$msg.'<br> ---------';				
				}
				
				echo "Telefone Gravado <br>";
			}
			
			
			echo '	<script>window.location.replace("../empresa.php?acao=novo");</script>';
		} else {
			echo 'Gravação não permitida ...<br>'; 
			//echo '	<script>window.location.replace("../empresa.php?acao=novo&tipo=filial");</script>';
		}		
	
	}
}

if ( $_SESSION[ "acao" ][ 'acao' ] ===  'editar'){
	
	if ( $_SESSION[ "acao" ][ 'tipo' ] ===  'sede'){
		
		echo "<br>";
		echo 'Alterarção de '.$_SESSION[ "acao" ][ 'tipo' ]."<br><br>";
	
		$qtdGravar = 0;

		//Dados padrões
		$_POST["ativo"] = '0';
		$_POST["nivel"] = '3'; //Empresa Sede

		
		// Tratamento
		$_POST["cnpj"] = str_replace($caracterEspecialnbackspace, "", $_POST["cnpj"]);
		$_POST["nome"] = str_replace($caracterEspecialnbackspace, "", $_POST["nome"]);
		$_POST["apelido"] = str_replace($caracterEspecialnbackspace, "", $_POST["apelido"]);
		$_POST["cep"] = str_replace($caracterEspecialnbackspace, "", $_POST["cep"]);
		
		$_POST["telefone"] = str_replace($caracterEspecial, "", $_POST["telefone"]);
		$_POST["senha"] = md5($_POST["senha"]);		

		
		$proximoPasso = "../listar.php?tipo=sede";
		/////////////////////////////////////////////////////////	

		//Validação do array
		$emptyVazio = array('id', 'cnpj', 'nome', 'apelido',
							'sede', 'email' );	

		$array = array(
				intval($_POST["id"]),		//0
				intval($_POST["cnpj"]),		//1
				$_POST["nome"],				//2
				$_POST["apelido"],			//3		

				$_POST["sede"],				//4	
				$_POST["email"]				//5
			);

		if( count($emptyVazio) == count($array) ){ echo 'Mesma Quantidades no Array <br><br>'; $gravar[$qtdGravar++] = 'Ok';}
		else {echo 'Quantidades Erradas <br>'.count($emptyVazio).'<br>'.count($array).'<br>';}

		echo 'Validando informações do Usuario ... <br><br>';
		$msg = '----- Itens Vazios ----- <br>';
		for($i=0; $i < count($array); $i++){
			if(empty($array[$i])){
				if( array_key_exists($array[$i], $emptyVazio) === false){
					$msg = $msg.$emptyVazio[$i].'<br>';
					$gravar[$qtdGravar++] = 'Vazio';
				} else { $gravar[$qtdGravar++] = 'Ok'; }			
			}
		}
		
		echo $msg = $msg.'----- Itens Vazios ----- <br>';		
		//Fim da Validação do array	
		
		//Upload aki
		if(!empty($_FILES['arquivo'])){
			echo 'Subindo Arquivo ....<br><br>';
			$tamanhoMB = number_format((($_FILES['arquivo']["size"]/1024)/1024),2, '.', '');
			if($tamanhoMB > $tamanhoMaximoMB){ $gravar[$qtdGravar++] = 'Grande'; }

			mkdir($_UP['caminho'].'/'.$_POST["cnpj"]);
			$extencao = strrchr($_FILES['arquivo']["name"],'.');
			$nomeArquivo = 'logo'.$extencao;	
			$caminho = $_UP['caminho'].'/'.$_POST["cnpj"].'/'.$nomeArquivo;			
			move_uploaded_file($_FILES['arquivo']['tmp_name'], $caminho);
		} else{ $caminho = $upVazio;	echo 'Sem arquivo ....<br><br>';}
		
		$gravar = array_unique ( $gravar );
		
		if(count($gravar) == '1'){
			echo 'Gravando ...<br><br>';
			
			// Alterar usuario_Empresa_Sede
			$usuario_empresa_sede = array(
				intval($_POST["cnpj"]),		
				$_POST["nome"],				
				$_POST["apelido"],	

				$_POST["sede"],						
				$_POST["email"]			
			);			
			if ( alterarEmpresaSede($conexao, $usuario_empresa_sede)  ) {
				echo $_SESSION[ "msg" ] = 'Sede Alterada<br>';				

			}else {
				$msg = mysqli_error( $conexao );
				echo $_SESSION[ "msg" ] = '------<br> Erro_Sede: <br>'.$msg.'<br> ---------';				
			}
			
			// Alterar endereco
			$endereco = array( 
				intval($_SESSION[ "acao" ][ 'id' ]),	//0
				intval($_POST["cep"]),					//1
				$_POST["estado"],						//2
				$_POST["cidade"],						//3
				$_POST["bairro"],						//4
				$_POST["endereco"],						//5
				intval($_POST["numero"]),				//6
				$_POST["complemento"], 					//7
				$_POST["nivel"]							//8
			);	
			
			if ( alterarEndereco($conexao, $endereco)  ) {
				echo $_SESSION[ "msg" ] = 'Endereco Atualizado<br>';				

			}else {
				$msg = mysqli_error( $conexao );
				echo $_SESSION[ "msg" ] = '------<br> Erro_Endereco: <br>'.$msg.'<br> ---------';				
			}			
			
			// Alterar empresa					   
			$empresa = array( 
				intval($_SESSION[ "acao" ][ 'id' ]),
				intval($_POST["municipal"]),	
				intval($_POST["estadual"]),		
				intval($_POST["nfe"]),			
				$_POST["obs"],
				$_POST["nivel"]
			);			
							
			if ( alterarEmpresa($conexao, $empresa)  ) {
				echo $_SESSION[ "msg" ] = 'Empresa Inserida<br>';				

			}else {
				$msg = mysqli_error( $conexao );
				echo $_SESSION[ "msg" ] = '------<br> Erro_Empresa: <br>'.$msg.'<br> ---------';				
			}
			
			// Inserir Telefone			
			$qdtTelefone = count($_POST["telefone"]);
			echo '<br>Foram emcontrados '. $qdtTelefone . " Telefones. <br> ";
			for($i=0; $i < $qdtTelefone; $i++){
				echo "<br> Gravando Telefone " . $i . "<br>";
				echo 'ID: '.$_POST["telid"][$i] . "<br>";
				echo 'Contato: '.$_POST["contato"][$i] . "<br>";
				echo 'Telefone: '.$_POST["telefone"][$i] . "<br>";
				echo 'Ramal: '.$_POST["ramal"][$i] . "<br>";
				$telefone = array( 
					intval($_POST["id"][$i]),			
					$_POST["contato"][$i],			
					intval($_POST["telefone"][$i]),			
					intval($_POST["ramal"][$i])
				);	
				
				if ( alterarTelefone($conexao, $telefone)  ) {
				echo $_SESSION[ "msg" ] = 'Telefone Alterado <br>';				

				}else {
					$msg = mysqli_error( $conexao );
					echo $_SESSION[ "msg" ] = '------<br> Erro_Telefone: <br>'.$msg.'<br> ---------';				
				}
				
				echo "Telefone Gravado <br>";
			}
			
			
			echo '	<script>window.location.replace("../empresa.php?acao=listar&tipo=sede");</script>';
		} 
		else {
			echo 'Gravação não permitida ...<br><br>'; 
			echo '	<script>window.location.replace("../empresa.php?acao=editar&tipo=sede&token='.$_POST["id"].'");</script>';
		}		
	
	}
	
	if ( $_SESSION[ "acao" ][ 'tipo' ] ===  'filial'){
		
		echo "<br>";
		echo 'Alterarção de '.$_SESSION[ "acao" ][ 'tipo' ]."<br><br>";
	
		$qtdGravar = 0;

		//Dados padrões
		$_POST["ativo"] = '0';
		$_POST["nivel"] = '5'; //Empresa Filial

		
		// Tratamento
		$_POST["cnpj"] = str_replace($caracterEspecialnbackspace, "", $_POST["cnpj"]);
		$_POST["nome"] = str_replace($caracterEspecialnbackspace, "", $_POST["nome"]);
		$_POST["apelido"] = str_replace($caracterEspecialnbackspace, "", $_POST["apelido"]);
		$_POST["cep"] = str_replace($caracterEspecialnbackspace, "", $_POST["cep"]);
		
		$_POST["telefone"] = str_replace($caracterEspecial, "", $_POST["telefone"]);
		$_POST["senha"] = md5($_POST["senha"]);		

		
		$proximoPasso = "../listar.php?tipo=sede";
		/////////////////////////////////////////////////////////	

		//Validação do array
		$emptyVazio = array('id', 'cnpj', 'nome', 'apelido',
							'sede', 'email' );	

		$array = array(
				intval($_POST["id"]),		//0
				intval($_POST["cnpj"]),		//1
				$_POST["nome"],				//2
				$_POST["apelido"],			//3		

				$_POST["sede"],				//4	
				$_POST["email"]				//5
			);

		if( count($emptyVazio) == count($array) ){ echo 'Mesma Quantidades no Array <br><br>'; $gravar[$qtdGravar++] = 'Ok';}
		else {echo 'Quantidades Erradas <br>'.count($emptyVazio).'<br>'.count($array).'<br>';}

		echo 'Validando informações do Usuario ... <br><br>';
		$msg = '----- Itens Vazios ----- <br>';
		for($i=0; $i < count($array); $i++){
			if(empty($array[$i])){
				if( array_key_exists($array[$i], $emptyVazio) === false){
					$msg = $msg.$emptyVazio[$i].'<br>';
					$gravar[$qtdGravar++] = 'Vazio';
				} else { $gravar[$qtdGravar++] = 'Ok'; }			
			}
		}
		
		echo $msg = $msg.'----- Itens Vazios ----- <br>';		
		//Fim da Validação do array	
		
		//Upload aki
		if(!empty($_FILES['arquivo'])){
			echo 'Subindo Arquivo ....<br><br>';
			$tamanhoMB = number_format((($_FILES['arquivo']["size"]/1024)/1024),2, '.', '');
			if($tamanhoMB > $tamanhoMaximoMB){ $gravar[$qtdGravar++] = 'Grande'; }

			mkdir($_UP['caminho'].'/'.$_POST["cnpj"]);
			$extencao = strrchr($_FILES['arquivo']["name"],'.');
			$nomeArquivo = 'logo'.$extencao;	
			$caminho = $_UP['caminho'].'/'.$_POST["cnpj"].'/'.$nomeArquivo;			
			move_uploaded_file($_FILES['arquivo']['tmp_name'], $caminho);
		} else{ $caminho = $upVazio;	echo 'Sem arquivo ....<br><br>';}
		
		$gravar = array_unique ( $gravar );
		
		if(count($gravar) == '1'){
			echo 'Gravando ...<br><br>';
			
			// Alterar usuario_Empresa_Sede
			$usuario_empresa_sede = array(
				intval($_POST["cnpj"]),		
				$_POST["nome"],				
				$_POST["apelido"],	

				$_POST["sede"],						
				$_POST["email"]			
			);			
			if ( alterarEmpresaSede($conexao, $usuario_empresa_sede)  ) {
				echo $_SESSION[ "msg" ] = 'Sede Alterada<br>';				

			}else {
				$msg = mysqli_error( $conexao );
				echo $_SESSION[ "msg" ] = '------<br> Erro_Sede: <br>'.$msg.'<br> ---------';				
			}
			
			// Alterar endereco
			$endereco = array( 
				intval($_SESSION[ "acao" ][ 'id' ]),	//0
				intval($_POST["cep"]),					//1
				$_POST["estado"],						//2
				$_POST["cidade"],						//3
				$_POST["bairro"],						//4
				$_POST["endereco"],						//5
				intval($_POST["numero"]),				//6
				$_POST["complemento"], 					//7
				$_POST["nivel"]							//8
			);	
			
			if ( alterarEndereco($conexao, $endereco)  ) {
				echo $_SESSION[ "msg" ] = 'Endereco Atualizado<br>';				

			}else {
				$msg = mysqli_error( $conexao );
				echo $_SESSION[ "msg" ] = '------<br> Erro_Endereco: <br>'.$msg.'<br> ---------';				
			}			
			
			// Alterar empresa					   
			$empresa = array( 
				intval($_SESSION[ "acao" ][ 'id' ]),
				intval($_POST["municipal"]),	
				intval($_POST["estadual"]),		
				intval($_POST["nfe"]),			
				$_POST["obs"],
				$_POST["nivel"]
			);			
							
			if ( alterarEmpresa($conexao, $empresa)  ) {
				echo $_SESSION[ "msg" ] = 'Empresa Inserida<br>';				

			}else {
				$msg = mysqli_error( $conexao );
				echo $_SESSION[ "msg" ] = '------<br> Erro_Empresa: <br>'.$msg.'<br> ---------';				
			}
			
			// Inserir Telefone			
			$qdtTelefone = count($_POST["telefone"]);
			echo '<br>Foram emcontrados '. $qdtTelefone . " Telefones. <br> ";
			for($i=0; $i < $qdtTelefone; $i++){
				echo "<br> Gravando Telefone " . $i . "<br>";
				echo 'ID: '.$_POST["telid"][$i] . "<br>";
				echo 'Contato: '.$_POST["contato"][$i] . "<br>";
				echo 'Telefone: '.$_POST["telefone"][$i] . "<br>";
				echo 'Ramal: '.$_POST["ramal"][$i] . "<br>";
				$telefone = array( 
					intval($_POST["telid"][$i]),			
					$_POST["contato"][$i],			
					intval($_POST["telefone"][$i]),			
					intval($_POST["ramal"][$i])
				);	
				
				if ( alterarTelefone($conexao, $telefone)  ) {
				echo $_SESSION[ "msg" ] = 'Telefone Alterado <br>';				

				}else {
					$msg = mysqli_error( $conexao );
					echo $_SESSION[ "msg" ] = '------<br> Erro_Telefone: <br>'.$msg.'<br> ---------';				
				}
				
				echo "Telefone Gravado <br>";
			}
			
			
			echo '	<script>window.location.replace("../empresa.php?acao=listar&tipo=sede");</script>';
		} 
		else {
			echo 'Gravação não permitida ...<br><br>'; 
			echo '	<script>window.location.replace("../empresa.php?acao=editar&tipo=sede&token='.$_POST["id"].'");</script>';
		}		
	
	}
	
}

if ( !empty( $_GET['acao'] )){

	$_SESSION[ "acao" ][ 'acao' ] 		= $_GET['acao'];
	$_SESSION[ "acao" ][ 'id' ] 		= $_GET['token'];
	$_SESSION[ "acao" ][ 'tipo' ] 		= $_GET['tipo'];

	echo '<script>window.location.replace("empresa.php");</script>';

}
