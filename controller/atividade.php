<?php require_once('../loading.php');
include_once('../model/atividade.php');
session_start();

//var_dump($_SESSION["acao"]);

/*Tratando */
$caracterEspecial = array("/", "'", ".", "@", "-", "(", ")", " ", ":");
$caracterEspecialnbackspace = array("/", "'", ".", "@", "-", "(", ")");

if ( $_SESSION["acao"]["acao"] ===  'inserir') { 
	
	$_POST["nome"] = str_replace($caracterEspecialnbackspace, "", $_POST["nome"]);
	$_POST["duracao"] = str_replace($caracterEspecial, "", $_POST["duracao"]);
	
	if($_POST["ativo"] === "on"){
		$_POST["ativo"] = "0";
	}else{
		$_POST["ativo"] = "1";
	}

$array = array(
	$_POST["nome"],
	$_POST["duracao"],
	$_POST["ativo"],
	$_POST["descricao"]
);
	var_dump($array);
	
	if ( inseriratividadeBD($conexao, $array)  ) {
		$_SESSION[ "msg" ] = 'Atividade Inserida';
		
		echo '	<script>		window.location.replace("../atividade.php?acao=novo");		</script>';
		
	}else {
		$msg = mysqli_error( $conexao );
		$_SESSION[ "msg" ] = 'Atividade Não inserida .<br>	Erro:' . $msg . '	</div>';
		
		echo '	<script>		window.location.replace("../atividade.php?acao=listar");		</script>';
}

}

if ( $_SESSION["acao"]["acao"] ===  'alterar') { 
	
	$_POST["nome"] = str_replace($caracterEspecialnbackspace, "", $_POST["nome"]);
	$_POST["duracao"] = str_replace($caracterEspecial, "", $_POST["duracao"]);
	
	if($_POST["ativo"] === "on"){
		$_POST["ativo"] = "0";
	}else{
		$_POST["ativo"] = "1";
	}

$array = array(
	$_SESSION["acao"]["id"],
	$_POST["nome"],
	$_POST["duracao"],
	$_POST["ativo"],
	$_POST["descricao"]
);
	//var_dump($array);
	
	if ( alteraratividadeBD($conexao, $array)  ) {
		$_SESSION[ "msg" ] = 'Atividade Alterada';
		
		echo '	<script>		window.location.replace("../atividade.php?acao=listar");		</script>';
		
	}else {
		$msg = mysqli_error( $conexao );
		$_SESSION[ "msg" ] = 'Erro, Atividade  Não Inserido .<br>	Erro:' . $msg . '	</div>';
		
		echo '	<script>		window.location.replace("../atividade.php?acao=listar");		</script>';
}

}

if ( $_GET['acao'] ===  'excluir') {

	if ( excluirAtividadeBD( $conexao, $_GET[ 'token' ] )  ) {
		echo $_SESSION[ "msg" ] = 'Atividade excluido';	
		unset($_SESSION[ "acao" ]);
		echo '<script> window.location.replace("../atividade.php?acao=listar"); </script>';
		}
	else {
		$msg = mysqli_error( $conexao );
		echo $_SESSION[ "msg" ] = 'Atividade não excluido .<br>	Erro:' . $msg . '</div>';		
		unset($_SESSION[ "acao" ]);
		echo '<script> window.location.replace("../atividade.php?acao=listar"); </script>';
		}
}