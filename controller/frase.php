<?php require_once('../loading.php'); 
require_once("../model/frase.php");
error_reporting( E_ALL ^ E_NOTICE );

session_start();

if ( $_SESSION["acao"]["acao"] ===  'novo') {
	echo $_POST['frase'];
	
	$proximoPasso = '../frase.php?acao=novo';
	
	if(empty($_POST['frase'])){
			echo 'Frase Vazia';
			$_SESSION[ "msg" ] = 'Frase Obrigatoria';
			unset($_SESSION[ "acao" ]);
			echo '<script>window.location.replace("../frase.php?acao=novo");</script>';
	} else{
		
		if ( inserirFraseBD($conexao, $_POST['frase'])  ) {
			$_SESSION[ "msg" ] = 'Frase Cadastrado.';
			echo '<script>window.location.replace("'.$proximoPasso.'");</script>';

		} else {
			$msg = mysqli_error( $conexao );
			echo $_SESSION[ "msg" ] = 'Frase Não Inserido .<br> Erro:' . $msg ;

			//echo '<script>window.location.replace("../usuario.php");</script>';
		}
		
	}
	
}

if ( $_SESSION[ "acao" ][ 'acao' ] ===  'excluir') {
	
	if ( excluirFraseBD( $conexao, $_SESSION[ "acao" ][ 'id' ] )  ) {
		$_SESSION[ "msg" ] = 'Usuario excluido';
		unset($_SESSION[ "acao" ]);
		
		echo '<script> window.location.replace("../frase.php?acao=listar"); </script>';
		
	}else {
		$msg = mysqli_error( $conexao );
		echo $_SESSION[ "msg" ] = 'Usuario não excluido .<br>	Erro:' . $msg . '	</div>';
		unset($_SESSION[ "acao" ]);
		
		echo '<script> window.location.replace("../frase.php?acao=listar"); </script>';
		}
}


if ( $_SESSION["acao"]["acao"] ===  'alterar') {
	echo $_POST['id'];
	echo $_POST['frase'];
	
	$array = array ($_POST['id'], $_POST['frase']);
	
	$proximoPasso = '../frase.php?acao=listar';
	
	if(empty($_POST['frase'])){
			echo 'Frase Vazia';
			$_SESSION[ "msg" ] = 'Frase Obrigatoria';
			unset($_SESSION[ "acao" ]);
			echo '<script>window.location.replace("../frase.php?acao=novo");</script>';
	} else{
		
		if ( AlterarFraseBD($conexao, $array)  ) {
			$_SESSION[ "msg" ] = 'Frase Cadastrado.';
			echo '<script>window.location.replace("'.$proximoPasso.'");</script>';

		} else {
			$msg = mysqli_error( $conexao );
			echo $_SESSION[ "msg" ] = 'Frase Não Inserido .<br> Erro:' . $msg ;

			echo '<script>window.location.replace("'.$proximoPasso.'");</script>';
		}
		
	}
	
}

if ( !empty( $_GET['acao'] )){	
	
	unset($_SESSION[ "acao" ]);
	
	echo $_SESSION[ "acao" ][ 'acao' ] = $_GET['acao'];
	echo $_SESSION[ "acao" ][ 'id' ] = $_GET['token'];
	
	
	echo '<script>window.location.replace("frase.php");</script>';
}