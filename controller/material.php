<?php require_once('../loading.php');

session_start();

include_once( '../model/material.php' );

/*Tratando */
$caracterEspecial = array(  "'", '"', "@" );

$_POST[ 'nome' ] = str_replace( $caracterEspecial, "", $_POST[ "nome" ] );
$_POST[ 'descricao' ] = str_replace( $caracterEspecial, "", $_POST[ "descricao" ] );
echo $_POST['acao'];

if( $_POST['acao'] === 'novo' ){

	if ( inserirMaterialBD( $conexao, $_POST[ "nome" ], $_POST[ "descricao" ] ) ) {
		$msg = mysqli_error( $conexao );
		echo $_SESSION[ "msg" ] = 'Material Inserido';
		echo '<script>window.location.replace("../material.php?acao=listar");</script>';
	} else {				
		$msg = mysqli_error( $conexao );
		echo $_SESSION[ "msg" ] = 'Erro, não Inserido .<br>	Erro:' . $msg ;
		echo '<script>window.location.replace("../material.php?acao=novo");</script>';
	}
}

if( $_POST['acao'] === 'editar' ){

	if ( alterarMaterialBD( $conexao, $_POST[ "id" ], $_POST[ "nome" ], $_POST[ "descricao" ] ) ) {
		$msg = mysqli_error( $conexao );
		echo $_SESSION[ "msg" ] = 'Material Alterado';
		echo '<script>window.location.replace("../material.php?acao=listar");</script>';
	} else {				
		$msg = mysqli_error( $conexao );
		echo $_SESSION[ "msg" ] = 'Erro, não Alterado .<br>	Erro:' . $msg ;
		echo '<script>window.location.replace("../material.php?acao=novo");</script>';
	}
}

if ( $_GET['acao'] ===  'excluir') {

		if ( excluirMaterialBD( $conexao, $_GET['token'] )  ) {
			$_SESSION[ "msg" ] = 'Material excluido';
			unset($_SESSION[ "acao" ]);

			echo '<script> window.location.replace("../material.php?acao=listar"); </script>';

		}else {
			$msg = mysqli_error( $conexao );
			echo $_SESSION[ "msg" ] = 'Material não excluido .<br>	Erro:' . $msg . '	</div>';
			unset($_SESSION[ "acao" ]);

			//echo '<script> window.location.replace("../frase.php?acao=listar"); </script>';
			}
	}