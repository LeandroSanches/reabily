<?php require_once('../loading.php'); 
require_once("../model/fornecedor.php");
error_reporting( E_ALL ^ E_NOTICE );

session_start();

if ( $_SESSION["acao"]["acao"] ===  'novo') {
	
	//$proximoPasso = '../fornecedor.php?acao=listar';
	$proximoPasso = '../dashboard.php';
	
	echo $_POST['nome'];
	echo $_POST['email'];
	echo $_POST['telefone'];
	echo $_POST['material'];
	echo $_POST['valorunitario'];
	echo $_POST['quantidadepedido'];
	echo $_POST['obs'];
	
	/*Tratando */
	$caracterEspecial = array("/", "'", ".", ",", "@", "-", "(", ")", " ");
	$caracterEspecialnbackspace = array("/", "'", ".", "@", "-", "(", ")");
	
	$_POST["telefone"] = str_replace($caracterEspecial, "", $_POST["telefone"]);
	$_POST["valorunitario"] = str_replace($caracterEspecial, "", $_POST["valorunitario"]);
	
	$array = array(
		$_POST['nome'],					//0	ok
		$_POST['email'],				//1	ok
		$_POST['telefone'],				//2	ok
		$_POST['material'],				//3	ok
		$_POST['valorunitario'],		//4	ok
		$_POST['quantidadepedido'],		//5	ok
		$_POST['obs']					//6	ok
		);

		
		if ( inserirFornecedorBD($conexao, $array)  ) {
			$_SESSION[ "msg" ] = 'Fornecedor Cadastrado.';
			echo '<script>window.location.replace("'.$proximoPasso.'");</script>';

		} else {
			$msg = mysqli_error( $conexao );
			echo $_SESSION[ "msg" ] = 'Fornecedor Não Inserido .<br> Erro:' . $msg ;

			echo '<script>window.location.replace("'.$proximoPasso.'");</script>';
		}

	
}

if ( $_SESSION[ "acao" ][ 'acao' ] ===  'excluir') {
	
	if ( excluirFornecedorBD( $conexao, $_SESSION[ "acao" ][ 'id' ] )  ) {
		$_SESSION[ "msg" ] = 'Fornecedor excluido';
		unset($_SESSION[ "acao" ]);
		
		echo '<script> window.location.replace("'.$proximoPasso.'"); </script>';
		
	}else {
		$msg = mysqli_error( $conexao );
		echo $_SESSION[ "msg" ] = 'Fornecedor não excluido .<br>	Erro:' . $msg . '	</div>';
		unset($_SESSION[ "acao" ]);
		
		echo '<script> window.location.replace("'.$proximoPasso.'"); </script>';
		}
}

if ( $_SESSION["acao"]["acao"] ===  'alterar') {
	
	/*Tratando */
	$caracterEspecial = array("/", "'", ".", ",", "@", "-", "(", ")", " ");
	$caracterEspecialnbackspace = array("/", "'", ".", "@", "-", "(", ")");
	
	$_POST["telefone"] = str_replace($caracterEspecial, "", $_POST["telefone"]);
	$_POST["valorunitario"] = str_replace($caracterEspecial, "", $_POST["valorunitario"]);
	
	$array = array(
		$_POST['id'],					//0	ok
		$_POST['nome'],					//1	ok
		$_POST['email'],				//2	ok
		$_POST['telefone'],				//3	ok
		$_POST['material'],				//4	ok
		$_POST['valorunitario'],		//5	ok
		$_POST['quantidadepedido'],		//6	ok
		$_POST['obs']					//7	ok
		);
	
	var_dump($array);
	
	$proximoPasso = '../fornecedor.php?acao=listar';
		
		if ( alterarFornecedorBD($conexao, $array)  ) {
			$_SESSION[ "msg" ] = 'Fornecedor Cadastrado.';
			echo '<script>window.location.replace("'.$proximoPasso.'");</script>';

		} else {
			$msg = mysqli_error( $conexao );
			echo $_SESSION[ "msg" ] = 'Fornecedor Não Inserido .<br> Erro:' . $msg ;

			echo '<script>window.location.replace("'.$proximoPasso.'");</script>';
		}
	
}

if ( !empty( $_GET['acao'] )){	
	
	unset($_SESSION[ "acao" ]);
	
	echo $_SESSION[ "acao" ][ 'acao' ] = $_GET['acao'];
	echo $_SESSION[ "acao" ][ 'id' ] = $_GET['token'];
	
	
	echo '<script>window.location.replace("fornecedor.php");</script>';
}