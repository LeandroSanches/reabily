<?php 
/*error_reporting( E_ALL ^ E_NOTICE );
error_reporting( E_WARNING );
error_reporting( 0 );*/

session_start();

include_once( 'cabecalho.php' );
include_once( 'model/usuario.php' );
include_once( 'model/bancos.php' );

//var_dump($_SESSION[ "acao" ]);

$tipoUsuario = $_SESSION[ "acao" ][ 'id_tipo' ];
$msgBotao = $_SESSION[ "acao" ][ 'msgBotao' ];
$id = $_SESSION[ "acao" ]['id'];


//var_dump($_SESSION[ "acao" ]);

if ( $tipoUsuario == 1 || $tipoUsuario == 2 || $tipoUsuario == 3 ) {

	$usuario = listarUsuarioBD( $conexao, $id );
	$dadosbancarios = listardadosbancariosBD( $conexao, $id );
	$banco = listarBancosBD( $conexao ) ;
	
	
switch ($usuario[0]['id_tipo']) {
    case 0:
        $chamar = "Error ";
		$passo2 = "false";
		$passo3 = "false";
        break;
    case 1:
        $chamar = "Administrador ";
		$passo2 = "true";
		$passo3 = "true";
        break;
    case 2:
        $chamar = "Funcionário ";
		$passo2 = "true";
		$passo3 = "true";
        break;
    case 3:
        $chamar = "Empresa ";
		$passo2 = "false";
		$passo3 = "true";
        break;
    case 4:
        $chamar = "Clientes ";
		$passo2 = "false";
		$passo3 = "false";
        break;
}
	
	if(empty($dadosbancarios) ){
		//usuario não tem ddos cadastrados vamos cadastrar
		$msgdadosbancarios = $chamar.$usuario[0]["apelido"].' não tem dados bancários cadastrados';
		$msgBotao = 'Finalizar';
		$_SESSION[ "acao" ][ 'acao' ] = 'inserirbanco';
		$_SESSION[ "acao" ][ 'id' ] = $id;
	}
	if(!empty($dadosbancarios) ){
		$msgdadosbancarios = $chamar.$usuario[0]["apelido"].' tem os dados bancários cadastrados';
		$msgBotao = 'Atualizar';
		$_SESSION[ "acao" ][ 'acao' ] = 'atualizarbanco';
		$_SESSION[ "acao" ][ 'id' ] = $id;
	}

	
?>

<div class="row container">
		<form action="controller/usuario.php" method="post" enctype="multipart/form-data">
		
			<div class="row">
				<div class="col s12 m12 l12">					
					<h5><?= $msgdadosbancarios?></h5>
				</div>
			</div>				
			
			<div class="row">
				<div class="col s12 m12 l12 center-align PassoAtiv">

					<span style="padding-right: 8px;" class="PassoDes"><i class="material-icons medium">looks_one</i></span> 
					<?php if($passo2 === 'true'){?>
					<span style="padding:0 8px 0 8px;" class="PassoDes"><i class="material-icons medium">looks_two</i></span> 
					<?php } ?>
					<?php if($passo3 === 'true'){?>
					<span style="padding-left: 8px;"><i class="material-icons medium">looks_3</i></span>
					<?php } ?>
				</div>					
			</div>
			<div class="row">				
				<div class="input-field col s12 m3">
					<select name="tipodeconta">
						<?php if($dadosbancarios[0]['tipodeconta'] == '1'){	$valorsim = 'selected';	}if($dadosbancarios[0]['tipodeconta'] == '2'){$valornao = 'selected';}?>
						<option disabled selected>Escolha uma opção</option>										
						<option value="1" <?=$valorsim?> > C/C </option>
						<option value="2" <?=$valornao?> > C/P </option>
					</select>
					<label>Tipo de Conta:</label>
				</div>
				
				<div class="input-field col s12 m3">
					<select name="banco">						
						<option disabled selected>Escolha uma opção</option>
						<?php foreach($banco as $banco){
								if($banco["cod"] ===  $dadosbancarios[0]['banco']){
									$check = 'selected';
								} else {
									$check = '';
								}	
								echo '<option value="'.$banco["cod"].'" '.$check.'> '.$banco["cod"]. ' - '.$banco["nome"].'</option>';
								} ?>					
					</select>
					<label>Código do Banco</label>
				</div>

				<div class="input-field col s12 m3">
					<input id="agencia" type="text" class="validate" name="agencia" value="<?=$dadosbancarios[0]['agencia']?>"  maxlength="4" data-mask="0000">
					<label for="agencia">Agência</label>
				</div>

				<div class="input-field col s12 m3">
					<input id="conta" type="text" class="validate" name="conta" value="<?=$dadosbancarios[0]['conta']?>" maxlength="11" data-mask="0000000000-0">
					<label for="conta">Conta</label>
				</div>
					
			</div>
				
			<div class="row">
				<div class="col s6 m3 offset-m1" align="center">
					<a href="usuario.php?acao=alterar&token=<?=$usuario[0]["id"]?>" class="btn waves-effect waves-light">Voltar</a> </div>
				<div class="col s6 m6">
					<button class="btn2 waves-effect waves-light right" type="submit">
						<?= $msgBotao?>
					</button>
				</div>
			</div>
		</form>
	</div>

<?php
} else {
	echo "<h5 align='center' class='white-text'>Não disponível para o perfil de usuário</h5>";
}

include_once('rodape.php'); ?>