<?php
include_once( 'cabecalho.php' );
include_once( 'model/usuario.php' );

//$_SESSION[ "acao" ][ 'acao' ]  = 'novo'; //apagar
//$_SESSION[ "acao" ][ 'tipo' ]  = 'cliente'; //apagar

if ( isset($_SESSION[ "acao" ][ 'acao' ])) {
	
	$btnvoltar = 'dashboard.php';
	
	if ( $_SESSION[ "acao" ][ 'acao' ]  === 'novo') {
		
		//Retornar MSG
		$_SESSION[ "log" ][ "on" ] = "true";	
		if($_SESSION[ "log" ][ "on" ] === "true" ){
			if( !empty($_SESSION[ "log" ][ "msg" ])){
				echo '<div class="msg">' . $_SESSION[ "log" ][ "msg" ] . '</div>';
			}			
			unset($_SESSION[ "log" ][ "msg" ]);
		}		
		$_SESSION[ "log" ][ "on" ] = "false";
		//Fim de Retornar MSG
		
		
		$infAvan = "false";
		$infBancaria = "false";
		$cpf = 'true';
		$cnpj = 'false';
		
		if ($_SESSION[ "acao" ][ 'tipo' ] === 'administrador'){
			$verifica_nivel = 'administrador';
			$passo2 = 'true';
			$passo3 = 'true';
		}
		if ($_SESSION[ "acao" ][ 'tipo' ] === 'funcionarios'){
			$verifica_nivel = 'funcionarios';
			$passo2 = 'true';
			$passo3 = 'true';
		}
		if ($_SESSION[ "acao" ][ 'tipo' ] === 'cliente'){
			$verifica_nivel = 'cliente';
			$contato = 'false';
			$passo2 = 'false';
			$passo3 = 'false';
			$_SESSION[ "acao" ][ 'auto' ] = 'false';
		}
		
		$msgBotao = 'Cadastrar';
		$formulario = 'true';
		$_SESSION[ "acao" ][ 'acao' ] =  'cadastrar';
		}

	if ( $_SESSION[ "acao" ][ 'acao' ]  === 'alterar') {
		$usuario = listarUsuarioBD( $conexao, $_SESSION[ "acao" ][ 'id' ] );
		$usuario[0]['foto'] = strstr($usuario[0]['foto'],'usuarios/');
			
		$msgBotao = 'Alterar';
		
		$cpf_cnpj = 'false';
		
		$formulario = 'true';
		
		switch ($usuario[0]['id_tipo']) {
			case 0:
				$chamar = "Error ";
				$passo2 = "false";
				$passo3 = "false";
				$infAvan = "false";
				$infBancaria = "false";
				$redessociais = "false";
				$formulario = 'false';
				$btnvoltar = 'index.php';
				$proximoPasso = 'index.php';
				break;
			case 1:
				$chamar = "Administrador ";
				$passo2 = "true";
				$passo3 = "true";
				$infAvan = "true";
				$infBancaria = "true";
				$redessociais = "true";
				$btnvoltar = 'listar.php?tipo=administradores';
				$proximoPasso = 'listar.php?tipo=administradores';
				break;
			case 2:
				$chamar = "Funcionário ";
				$passo2 = "true";
				$passo3 = "true";
				$infAvan = "true";
				$infBancaria = "true";
				$redessociais = "true";
				$btnvoltar = 'listar.php?tipo=funcionarios';
				$proximoPasso = 'listar.php?tipo=funcionarios';
				break;			
			case 4:
				$chamar = "Clientes ";
				$passo2 = "false";
				$passo3 = "false";
				$infAvan = "false";
				$infBancaria = "false";
				$redessociais = "true";
				$btnvoltar = 'listar.php?tipo=clientes';
				$proximoPasso = 'listar.php?tipo=clientes';
				$verifica_nivel = 'cliente';
				break;
		}
		
		
		$_SESSION[ "acao" ][ 'acao' ] =  'alterar';
		$_SESSION[ "acao" ][ 'tipo' ] =  $usuario[0]['id_tipo'];		
		
		
		include_once( 'model/endereco.php' );		
		$endereco = listarEndereco($conexao, $_SESSION[ "acao" ][ 'id' ] , $usuario[0]['id_tipo']);
		
		include_once( 'model/telefone.php' );
		$telefone = listarTelefone($conexao, $_SESSION[ "acao" ][ 'id' ] , $usuario[0]['id_tipo']); 
		
	}
	
	if($formulario == 'true'){	
		include_once( 'model/empresa.php' );

	$nivel = listarTiposBD( $conexao );
	$empresa = listarSedeEmpresasBD( $conexao );?>
	
		<br>
		<div class="container">
			<div class="row">
				<form class="col s12" action="controller/usuario.php" method="post" enctype="multipart/form-data">
				
					<div class="row">				
						<h2>Usuarios</h2>	
					</div>

					<div class="row">
						<div class="col s12 m12 l12 center-align PassoAtiv">

							<span style="padding-right: 8px;"><i class="material-icons medium">looks_one</i></span> 
							<?php if($passo2 === 'true'){?>
							<span style="padding:0 8px 0 8px;" class="PassoDes"><i class="material-icons medium">looks_two</i></span> 
							<?php } ?>
							<?php if($passo3 === 'true'){?>
							<span style="padding-left: 8px;" class="PassoDes"><i class="material-icons medium">looks_3</i></span>
							<?php } ?>
						</div>					
					</div>

					<div class="row">					
						<div class="input-field col s12">
							<h4>Dados do Usuario</h4>	
						</div> 
					
						<?php if ( isset($usuario[0]['id'] )) {
							echo '<input class="hide" type="text" name="id" value="'.$usuario[0]["id"].'">';
						} ?>		
									
						<div class="input-field col s12 m6">
							<i class="material-icons prefix">assignment_ind</i>
							<input id="Nome" type="text" class="validate " name="nome" value="<?= $usuario[0]["nome"]?>" autocomplete="off">
							<label for="Nome">Nome</label>

						</div>
						<div class="input-field col s12 m6">
							<input id="Apelido" type="text" class="validate " name="apelido" value="<?= $usuario[0]["apelido"]?>" autocomplete="off">
							<label for="Apelido">Sobrenome</label>
						</div>	
						
						<div class="input-field col s12 m4">
							<i class="material-icons prefix">credit_card</i>
							<input id="cpf" type="text" class="validate" name="cpf" data-mask="000.000.000-00" value="<?= $usuario[0]["cpf"]?>">
							<label for="cpf">CPF</label>
						</div>	
						
						<div class="input-field col s12 m4"> 
							<select name="sexo" >
								<?php $sexo1 = ""; $sexo2 = ""; $sexo3 = "";
									switch($usuario[0]["sexo"]){
										case 1:
											$sexo1 = "selected";
											break;
										case 2:
											$sexo2 = "selected";
											break;
										case 3:
											$sexo3 = "selected";
											break;
											
									} ?>
								<option value="0" disabled selected>Escolha uma opção</option>
								<option value="1" <?=$sexo1?> >Masculino</option>
								<option value="2" <?=$sexo2?> >Feminino</option>
								<option value="3" <?=$sexo3?> >Outro</option>

							</select>
							<label>Gênero:</label>
						</div>											
				
						<div class="input-field col s12 m4">
							<input id="nascimento" type="date" class="validate" name="nascimento"  value="<?= $usuario[0]["nascimento"]?>">
							<label for="nascimento">Data de Nascimento:</label>
						</div>
						
						<div class="input-field col s12" align="center" >
							<p>Anexar Foto:</p>
							<input type="file" name="arquivo"><br><br>
							<img src=" <?=$usuario[0]['foto']?>" width="20%" />
						</div>
					</div>
						
					<div class="row">												
						<div class="input-field col s12">
							<h4>Acesso ao Sistema</h4>	
						</div>

						<div class="input-field col s12 m8">
							<i class="material-icons prefix">email</i>
							<input id="email" type="email" class="validate" name="email" value="<?= $usuario[0]["email"]?>">
							<label for="email">Email</label>
						</div>	

						<?php if(!isset($_GET[ 'acao' ])){?>
						<div class="input-field col s12 m4">
							<i class="material-icons prefix">fingerprint</i>
							<input id="password" type="password" class="validate" name="senha">
							<label for="password">Senha</label>
						</div>
						<?php } ?>

						<div class="input-field col s12 m6">						
							<select name="nivel">
								<option value="" disabled selected><span class="white-text">Escolha uma opção</span></option>
								<?php foreach ( $nivel as $nivel ){
		
										if ($usuario[0]["id_tipo"] == $nivel["id"]){ $selected = 'selected';} 
										else {$selected = '';}
										if ( $verifica_nivel === 'administrador'){
											if ($nivel["id"] === '1' ){ $selected = 'selected';}
											else {$selected = '';}
										}
										if ( $verifica_nivel === 'funcionarios'){
											if ($nivel["id"] === '2' ){ $selected = 'selected';}
											else {$selected = '';}
										}	
										if ( $verifica_nivel === 'cliente'){
											if ($nivel["id"] === '4' ){ $selected = 'selected';}
											else {$selected = '';}
										}
										
										if($nivel["id"] != 3 && $nivel["id"] != 5 ){ ?>
								<option value="<?=$nivel["id"];?>" <?=$selected;?>><?=$nivel["nome"];?></option>						
									<?php } 
								}  ?>
							</select>
							<label>Nivel:</label>
						</div>

						<div class="input-field col s12 m6">
							<select name="empresa">
								<option value="" disabled selected></option>
								<?php foreach ( $empresa as $empresa ){	?>						
								<optgroup value="<?=$empresa["id"];?>" label="<?=$empresa["sede"];?>" data-icon="<?php echo $empresa['logo'] = strstr($empresa['logo'],'usuarios/');?>">		
								<?php $filial = listarEmpresaSedeFilial( $conexao, $empresa["id"] );
									foreach ( $filial as $filial ){
									if ($usuario[0]["id_empresa"] === $filial["id"]){ $selected = 'selected';} 
									else {$selected = '';} ?>						
									<option value="<?=$filial["id"];?>" data-icon="<?php echo $filial['logo'] = strstr($filial['logo'],'usuarios/');?>" <?=$selected;?> ><?=$filial["sede"];?></option>
									<?php } ?>
								</optgroup>
								<?php } ?>
							</select>
							<label>Empresa:</label>
						</div>
					</div>	
					
					<div class="row">					
						<div class="input-field col s12">
							<h4>Telefones</h4>	
						</div>
						
						<div class="clone">						
						
						<?php
						if(is_array($telefone)){
							foreach($telefone as $telefone){
							?>						
							<div class="input-field col s12 m6 hide">
								<input  type="number" name="idTel[]" value="<?=$telefone["id"]?>" >
							</div>
							
							<div class="input-field col s12 m6">
								<input id="contato" type="text" class="validate" name="contato[]" value="<?=$telefone["contato"]?>" >
								<label for="contato">Nome do Contato</label>
							</div>	

							<div class="input-field col s12 m3">							
								<input id="icon_telephone" type="tel" class="validate telefone sp_celphones" name="telefone[]" data-mask="(00) 000-000-000" maxlength="15" value="<?=$telefone["numero"]?>" >
								<label for="icon_telephone">Número</label>
							</div>

							<div class="input-field col s12 m3">
								 <input id="icon_telephone" type="tel" class="validate Ramal" name="ramal[]" data-mask="-0000" maxlength="15" value="<?=$telefone["ramal"]?>" >
								 <label for="ramal">Ramal</label>
							</div>
						<?php }  }
						else { 	?>
							<div class="input-field col s12 m6">
								<input id="contato" type="text" class="validate" name="contato[]" value="<?=$telefone[0]["contato"]?>" >
								<label for="contato">Nome do Contato</label>
							</div>	

							<div class="input-field col s12 m3">							
								<input id="icon_telephone" type="tel" class="validate telefone sp_celphones" name="telefone[]" data-mask="(00) 000-000-000" maxlength="15" value="<?=$telefone[0]["telefone"]?>" >
								<label for="icon_telephone">Número</label>
							</div>

							<div class="input-field col s12 m3">
								 <input id="icon_telephone" type="tel" class="validate Ramal" name="ramal[]" data-mask="-0000" maxlength="15" value="<?=$telefone[0]["ramal"]?>" >
								 <label for="ramal">Ramal</label>
							</div>		
							<?php } ?>
							
						</div>
						
						<div class="add"></div>						
						
						
						<p align="center" class="addNumero"> <i class="material-icons prefix">add</i> Adicionar Contato</p>
						
						<script>
							$(document).ready(function() {
								  $(".addNumero").click(function() {
								  var novoItem = $(".clone").clone().removeClass('clone'); 
								  $(".add").append(novoItem);
								});
							  });
						</script>
					</div>
						
					<div class="row">
					
						<div class="col s12">
							<h4>Endereço</h4>	
						</div>
							<div class="col s12 m4 hide">
								<input type="number"  name="idEndereco" value="<?= $endereco["id"]?>" >								
							</div>
							
							<div class="col s12 m4">
								<label for="cep"> CEP </label>
								<input id="cep" type="text" class="validate" name="cep" data-mask="00000-000" value="<?= $endereco["cep"]?>" >								
							</div>

							<div class="col s12 m1">
								<label for="uf"> Estado </label>
								<input id="uf" type="text" class="validate blue-text text-lighten-3" name="estado" value="<?= $endereco["estado"]?>" maxlength="3" >

							</div>

							<div class="col s12 m3">
								<label for="cidade"> Cidade </label>
								<input id="cidade" type="text" class="validate blue-text text-lighten-3" name="cidade" value="<?= $endereco["cidade"]?>" >

							</div>	

							<div class="col s12 m4">
								<label for="bairro"> Bairro </label>
								<input id="bairro" type="text" class="validate blue-text text-lighten-3" name="bairro" value="<?= $endereco["bairro"]?>" >

							</div>			

							<div class="col m10">
								<label for="endereco" style="margin-bottom: 3px;"> Endereco </label>
								<input id="endereco" type="text" class="validate blue-text text-lighten-3" name="endereco" value="<?= $endereco["endereco"]?>" >

							</div>

							<div class="input-field col m2">
								<input id="numero" type="number" class="validate" name="numero" value="<?= $endereco["numero"]?>">
								<label for="numero"> Número </label>
							</div>

							<div class="input-field col m12">
								<input id="complemento" type="text" class="validate" name="complemento" value="<?= $endereco["complemento"]?>">
								<label for="complemento"> Complemento </label>
							</div>	


						</div>
			
					<div class="row">
						<?php if($infAvan === "true"){	

							$_SESSION[ "acao" ]['infAvan'] = 'true';
							$_SESSION[ "acao" ]['id'] = $usuario[0]['id'];
							$_SESSION[ "acao" ]['id_tipo'] = $usuario[0]['id_tipo']; 
							$_SESSION[ "acao" ]['msgBotao'] = 'Alteração Avançada'; ?>

							<div class="col s12 m4">
								<p align="center"><a href="usuarioavan.php" class="Link">Informações Avançadas</a></p>
							</div>

						 <?php }	

							if($infBancaria === "true"){	
								$_SESSION[ "acao" ]['infBancaria'] = 'true';
								$_SESSION[ "acao" ]['id'] = $usuario[0]['id'];
								$_SESSION[ "acao" ]['id_tipo'] = $usuario[0]['id_tipo']; 
								$_SESSION[ "acao" ]['msgBotao'] = 'Alteração Avançada';	?>

							<div class="col s12 m4">
								<p align="center"><a href="infbancaria.php" class="Link">Informações Bancária</a></p>
							</div>

							<?php }	

							if($redessociais === "true"){	
								$_SESSION[ "acao" ]['infBancaria'] = 'true';
								$_SESSION[ "acao" ]['id'] = $usuario[0]['id'];
								$_SESSION[ "acao" ]['id_tipo'] = $usuario[0]['id_tipo']; 
								$_SESSION[ "acao" ]['msgBotao'] = 'Alteração Avançada';	?>
							<div class="col s12 m4">
								<p align="center"><a href="redessociais.php" class="Link">Redes Sociais</a></p>
							</div>					

							<?php }	?>

						</div>	

					<div class="row">
						<div class="col s6 m3 offset-m1" align="center">
								<a href="<?= $btnvoltar?>" class="btn waves-effect waves-light">Voltar</a> </div>
							<div class="col s6 m6">
							<button class="btn2 waves-effect waves-light right" type="submit"> <?= $msgBotao?> </button>
						</div>
					</div>		

				</form>
			</div>
		</div>

	<?php } }

if ( !empty( $_GET['acao'] )){	
	
	echo $_SESSION[ "acao" ][ 'acao' ] = $_GET['acao'];
	echo $_SESSION[ "acao" ][ 'id' ] = $_GET['token'];
	echo $_SESSION[ "acao" ][ 'tipo' ] = $_GET['tipo'];	
	
	$infAvan = "false";
	$infBancaria = "false";
	
	echo '<script>window.location.replace("usuario.php");</script>';
}

include_once('rodape.php'); ?>