<?php include_once('cabecalho.php');

session_start();
unset($_SESSION['usuario']);
?>

<br>
  <div class="row">
    <div class="col s12 m4 offset-m4">
      <div class="card ">
        <div class="card-content text-white">
          <span class="card-title white-text"><i class="medium material-icons left">lock_open</i> LOGIN</span>
          <form action="controller/login.php" method="post">
				<div class="row">
					<div class="input-field col s12">
					  <input id="email" type="email" class="validate white-text" name="email">
					  <label for="email" class="white-text">E-mail</label>
					</div>
					<div class="input-field col s12 ">
					  <input id="password" type="password" class="validate white-text" name="senha">
					  <label for="password" class="white-text">Senha</label>
					</div>
					<div class="col s12">
						<button class="btn waves-effect waves-light right" type="submit">Entrar</button>
					</div>
				</div>
				<div class="row">
					<div class="col s12">
						<p align="center"><a href="autocadastro.php" class="amber-text text-darken-1">Cadastre-se</a></p>
						 <p align="center"><a href="trabalhe_conosco.php" class="amber-text text-darken-1">Trabalhe  Conosco</a></p> 
					</div>
				</div>
			</form>
        </div>
      </div>
    </div>
  </div>

 <?php include_once('rodape.php'); ?>
