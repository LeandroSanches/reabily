<?php include_once( 'cabecalho.php' );

if ( isset( $_SESSION[ "acao" ][ 'acao' ] ) ) {

	if ( $_SESSION[ "acao" ][ 'acao' ] === 'listar' ) {	
		
		include_once( 'model/material.php' );
		
		$material = listarMaterialBD($conexao);
		?>

		<div class="row container">
			<h2>Materiais:</h2>
			<table>
				<thead>
					<tr>
						<th>N. de Patrimõnio:</th>
						<th>Nome:</th>
						<th>Ação:</th>
					</tr>
				</thead>

				<tbody>

					<?php foreach ( $material as $material ) { ?>

						<tr>
							<td>
								<?php switch (count($material[ "id" ])) {
    									case 1: 
											echo '<a  class="modal-trigger white-text" href="#modalVisualizar'.$material['id'].'"> 0000'.$material[ "id" ].'</a>';
											break;
										case 2: 
											echo '<a  class="modal-trigger white-text" href="#modalVisualizar'.$material['id'].'"> 000'.$material[ "id" ].'</a>';
											break;
										case 3: 
											echo '<a  class="modal-trigger white-text" href="#modalVisualizar'.$material['id'].'"> 00'.$material[ "id" ].'</a>';
											break;
										case 4: 
											echo '<a  class="modal-trigger white-text" href="#modalVisualizar'.$material['id'].'"> 0'.$material[ "id" ].'</a>';
											break;
										case 5: 
											echo '<a  class="modal-trigger white-text" href="#modalVisualizar'.$material['id'].'"> 0'.$material[ "id" ].'</a>';
											break;
								}?>
							</td>
							<td><a  class="modal-trigger white-text" href="#modalVisualizar<?=$material['id']?>">
								<?= $material[ "material" ]?>
								</a></td>
							<td><a href="material.php?acao=editar&token=<?=$material['id']?>"><i class="material-icons white-text" title="Editar">edit</i></a>
							<a class="modal-trigger Link" href="#modal<?=$material['id']?>" title="Excluir"><i class="material-icons prefix amber-text text-darken-1">cancel</i></a>
							</td>
						</tr>	
						
						<div id="modal<?=$material['id']?>" class="modal bottom-sheet">
							<div class="row">
								<div class="col s12 m6">
									<div class="modal-content">
									  <h4 class="grey-text text-darken-2">Deseja excluir o Material?</h4>
									  <p class="grey-text text-darken-2">Essa ação não poderá ser desfeita.</p>
									</div>
								</div>
								<div class="col s12 m6 Top">
									<div class="modal-footer">
										<a href="setor.php?acao=" class="btn waves-effect waves-light">Voltar</a>
										<a href="controller/material.php?acao=excluir&token=<?=$material['id']?>" class="modal-close btn2 waves-effect waves-light">Eu aceito</a>
									</div>
								</div>
							</div>
						  </div>
						  
						 <div id="modalVisualizar<?=$material['id']?>" class="modal" style=" background-color: #008aae;">
							<i class="material-icons right modal-close" style="padding: 15px;">close</i>
							<div class="container">	

								<div class="row ">
									<p align="center">Visualizador de Material.</p>

									<p>N. de Patrimõnio: <?php switch (count($material[ "id" ])) {
    									case 1: 
											echo '<a  class="modal-trigger white-text" href="#modalVisualizar'.$material['id'].'"> 0000'.$material[ "id" ].'</a>';
											break;
										case 2: 
											echo '<a  class="modal-trigger white-text" href="#modalVisualizar'.$material['id'].'"> 000'.$material[ "id" ].'</a>';
											break;
										case 3: 
											echo '<a  class="modal-trigger white-text" href="#modalVisualizar'.$material['id'].'"> 00'.$material[ "id" ].'</a>';
											break;
										case 4: 
											echo '<a  class="modal-trigger white-text" href="#modalVisualizar'.$material['id'].'"> 0'.$material[ "id" ].'</a>';
											break;
										case 5: 
											echo '<a  class="modal-trigger white-text" href="#modalVisualizar'.$material['id'].'"> 0'.$material[ "id" ].'</a>';
											break;
								}?></p>
									<p>Nome: <?=$material['material']?></p>
									<p>Descrição: <?=$material['descricao']?></p>

								</div>
							</div>

						  </div>
						
						<?php } ?>
					
				</tbody>
			</table>
		</div>
		
		<div class="row">
			<div class="col s6 m3 offset-m1" align="center">
				<a href="dashboard.php" class="btn waves-effect waves-light">Voltar</a> 
			</div>			

			<div class="col s6 m3 offset-m3">
				<a href="material.php?acao=novo" class="btn3 waves-effect waves-light">Novo Cadastro</a>
			</div>
		</div>
		
<?php  }
	
	if ( $_SESSION[ "acao" ][ 'acao' ] === 'novo' ) {
?>
		<br>
		<div class="row container">
			<h2>Novo Material:</h2>
			<div class="row">
				<form class="col s12" action="controller/material.php?acao=novo" method="post" accept-charset="UTF-8">
					<div class="row">
							
						<div class="input-field col s12 hide">
							<input id="acao" type="text" class="validate" name="acao" value="novo">
							
						</div>
							
						<div class="input-field col s12">
							<input id="nome" type="text" class="validate" name="nome">
							<label for="nome">Nome</label>
						</div>
						
						<div class="input-field col s12">
							<input id="descricao" type="text" class="validate" name="descricao">
							<label for="descricao">Descrição</label>
						</div>
						
						
					</div>

					<div class="row">
						<div class="col s6 m3 offset-m1" align="center">
							<a href="dashboard.php" class="btn waves-effect waves-light">Voltar</a> 
						</div>
						<div class="col s6 m6">
							<button class="btn2 waves-effect waves-light right" type="submit">Cadastrar</button>
						</div>
					</div>
				</form>
			</div>			
		</div>	
<?php }
	
	if ( $_SESSION[ "acao" ][ 'acao' ] === 'editar' ) {	
		
		include_once( 'model/material.php' );
		
		$material = listarMaterialIdBD($conexao, $_SESSION[ "acao" ][ 'id' ]);?>
		<br>
		<div class="row container">
			<h2>Editar Material:</h2>
			<div class="row">
				<form class="col s12" action="controller/material.php" method="post" accept-charset="UTF-8">
					<div class="row">
							
						<div class="input-field col s12 hide">
							<input id="acao" type="text" class="validate" name="acao" value="editar">							
						</div>
							
						<div class="input-field col s12 hide">
							<input id="id" type="text" class="validate" name="id" value="<?=$material[0]['id']?>">							
						</div>
							
						<div class="input-field col s12">
							<input id="nome" type="text" class="validate" name="nome" value="<?=$material[0]['material']?>">
							<label for="nome">Nome</label>
						</div>
						
						<div class="input-field col s12">
							<input id="descricao" type="text" class="validate" name="descricao" value="<?=$material[0]['descricao']?>">
							<label for="descricao">Descrição</label>
						</div>
						
						
					</div>

					<div class="row">
						<div class="col s6 m3 offset-m1" align="center">
							<a href="material.php?acao=listar" class="btn waves-effect waves-light">Voltar</a> 
						</div>
						<div class="col s6 m6">
							<button class="btn2 waves-effect waves-light right" type="submit">Alterar</button>
						</div>
					</div>
				</form>
			</div>			
		</div>	
<?php }

}

if ( !empty( $_GET[ 'acao' ] ) ) {

	echo $_SESSION[ "acao" ][ 'acao' ] = $_GET[ 'acao' ];
	echo $_SESSION[ "acao" ][ 'id' ] = $_GET[ 'token' ];
	echo $_SESSION[ "acao" ][ 'tipo' ] = $_GET[ 'tipo' ];

	echo '<script>window.location.replace("material.php");</script>';
	
	include_once( 'rodape.php' );
}

include_once( 'rodape.php' );?>