<?php
include_once( 'cabecalho.php' );
include_once( 'model/usuario.php' );
include_once( 'model/empresa.php' );

if ( $_GET[ 'acao' ] === "escolher" ) {
	$tipos = listarTiposBD( $conexao );
	?>
	<br><br>
	<div class="container row">
		<form action="mala.php?acao=2" method="get">
			<div class="row">
				<div class="input-field col s12 m6 offset-m3">
					<select name="acao">
						<option disabled selected></option>
						<?php foreach ( $tipos as $tipos ) {?>
						<option value="<?=$tipos['id']?>"><?= $tipos['nome'] ?></option>
						<?php } ?>
					</select>
					<label>Escolha um Nivel: </label>
				</div>
			</div>

			<div class="row">
				<div class="col s6 m3 offset-m1" align="center">
					<a href="dashboard.php" class="btn waves-effect waves-light">Voltar</a>
				</div>
				<div class="col s6 m3 offset-m4">
					<button class="btn2 waves-effect waves-light right" type="submit">Avançar</button>
				</div>
			</div>
		</form>
	</div>
	<?php
}

if ( $_GET[ 'acao' ] === "1" ) {		
	$sedes = listarSedeEmpresasBD( $conexao );
	
	if( !empty($_GET[ 'filtro' ]) ){
		
		if( $_GET[ 'ativo' ] === on ){
			$checked = 'checked';
			$_GET[ 'ativo' ] = ' <=2';
		}
		
		$array = array($_GET[ 'acao' ], $_GET[ 'empresa' ], $_GET[ 'ativo' ]);
		
		$clientes = listarEmailTodosUsuarioFiltradoBD( $conexao, $array );			
		
	} else {
		$clientes = listarEmailTodosUsuarioBD( $conexao, $_GET[ 'acao' ] );		
	} ?>
	<br><br><br><br>
	<div class="container row">
		<div class="row">
			<form action="mala.php" method="get">
				<h4>Filtros:</h4>
				<div class="input-field col s12 m6 hide">
						<input name="acao" value="<?=$_GET[ 'acao' ]?>"/>
						<input name="filtro" value="sim"/>
				</div>
				<div class="input-field col s12 m6">
						<select name="empresa">
							<option disabled selected></option>
							<?php foreach ( $sedes as $sedes ) {
							if ($_GET['empresa'] === $sedes["id_empresa"]){ $selected = 'selected';} 
							else {$selected = '';}?>
							<option value="<?=$sedes['id_empresa']?>" <?=$selected?>><?= $sedes['sede'] ?></option>
							<?php } ?>
						</select>
						<label>Filtrar por Empresa: </label>
				</div>
				<div class="input-field col s12 m3">
					<label>
						<input type="checkbox" name="ativo" <?=$checked?>/>
						<span>Somente Ativos</span>
					</label>    		
				</div>
				<div class="input-field col s12 m3">
					<button class="btn">Filtar</button>		
				</div>
     		</form>
      	</div>
      	
		<h2>Lista E-Mail.:</h2>
		<div class="row">
			<div class="btn col s6 offset-s3" onclick="copiarHTML()">Copiar Tudo</div>
		</div>		
		<div class="row">
			<textarea id="copiar"class="materialize-textarea"><?php if(!empty($clientes)){ foreach ( $clientes as $clientes ) {echo $clientes['email'].' ; ' ;} } else { echo 'Nenhum Dado Encontrado.';}?></textarea>
		</div>		
		<div class="row">
			<div class="btn col s6 offset-s3" onclick="copiarHTML()">Copiar Tudo</div>
		</div>
	</div>

	<?php
}

if ( $_GET[ 'acao' ] === "2" ) {		
	$sedes = listarSedeEmpresasBD( $conexao );
	
	if( !empty($_GET[ 'filtro' ]) ){
		
		if( $_GET[ 'ativo' ] === on ){
			$checked = 'checked';
			$_GET[ 'ativo' ] = ' <=2';
		}
		
		$array = array($_GET[ 'acao' ], $_GET[ 'empresa' ], $_GET[ 'ativo' ]);
		
		$clientes = listarEmailTodosUsuarioFiltradoBD( $conexao, $array );			
		
	} else {
		$clientes = listarEmailTodosUsuarioBD( $conexao, $_GET[ 'acao' ] );		
	} ?>
	<br><br><br><br>
	<div class="container row">
		<div class="row">
			<form action="mala.php" method="get">
				<h4>Filtros:</h4>
				<div class="input-field col s12 m6 hide">
						<input name="acao" value="<?=$_GET[ 'acao' ]?>"/>
						<input name="filtro" value="sim"/>
				</div>
				<div class="input-field col s12 m6">
						<select name="empresa">
							<option disabled selected></option>
							<?php foreach ( $sedes as $sedes ) {
							if ($_GET['empresa'] === $sedes["id_empresa"]){ $selected = 'selected';} 
							else {$selected = '';}?>
							<option value="<?=$sedes['id_empresa']?>" <?=$selected?>><?= $sedes['sede'] ?></option>
							<?php } ?>
						</select>
						<label>Filtrar por Empresa: </label>
				</div>
				<div class="input-field col s12 m3">
					<label>
						<input type="checkbox" name="ativo" <?=$checked?>/>
						<span>Somente Ativos</span>
					</label>    		
				</div>
				<div class="input-field col s12 m3">
					<button class="btn">Filtar</button>		
				</div>
     		</form>
      	</div>
      	
		<h2>Lista E-Mail.:</h2>
		<div class="row">
			<div class="btn col s6 offset-s3" onclick="copiarHTML()">Copiar Tudo</div>
		</div>		
		<div class="row">
			<textarea id="copiar"class="materialize-textarea"><?php if(!empty($clientes)){ foreach ( $clientes as $clientes ) {echo $clientes['email'].' ; ' ;} } else { echo 'Nenhum Dado Encontrado.';}?></textarea>
		</div>		
		<div class="row">
			<div class="btn col s6 offset-s3" onclick="copiarHTML()">Copiar Tudo</div>
		</div>
	</div>
	<?php
}

if ( $_GET[ 'acao' ] === "3" ) {		
	$sedes = listarSedeEmpresasBD( $conexao );
	
	if( !empty($_GET[ 'filtro' ]) ){
		
		if( $_GET[ 'ativo' ] === on ){
			$checked = 'checked';
			$_GET[ 'ativo' ] = ' <=2';
		}
		
		$array = array($_GET[ 'acao' ], $_GET[ 'empresa' ], $_GET[ 'ativo' ]);
		
		$clientes = listarEmailTodosUsuarioFiltradoBD( $conexao, $array );			
		
	} else {
		$clientes = listarEmailTodosUsuarioBD( $conexao, $_GET[ 'acao' ] );		
	} ?>
	<br><br><br><br>
	<div class="container row">
		<div class="row">
			<form action="mala.php" method="get">
				<h4>Filtros:</h4>
				<div class="input-field col s12 m6 hide">
						<input name="acao" value="<?=$_GET[ 'acao' ]?>"/>
						<input name="filtro" value="sim"/>
				</div>
				<div class="input-field col s12 m6">
						<select name="empresa">
							<option disabled selected></option>
							<?php foreach ( $sedes as $sedes ) {
							if ($_GET['empresa'] === $sedes["id_empresa"]){ $selected = 'selected';} 
							else {$selected = '';}?>
							<option value="<?=$sedes['id_empresa']?>" <?=$selected?>><?= $sedes['sede'] ?></option>
							<?php } ?>
						</select>
						<label>Filtrar por Empresa: </label>
				</div>
				<div class="input-field col s12 m3">
					<label>
						<input type="checkbox" name="ativo" <?=$checked?>/>
						<span>Somente Ativos</span>
					</label>    		
				</div>
				<div class="input-field col s12 m3">
					<button class="btn">Filtar</button>		
				</div>
     		</form>
      	</div>
      	
		<h2>Lista E-Mail.:</h2>
		<div class="row">
			<div class="btn col s6 offset-s3" onclick="copiarHTML()">Copiar Tudo</div>
		</div>		
		<div class="row">
			<textarea id="copiar"class="materialize-textarea"><?php if(!empty($clientes)){ foreach ( $clientes as $clientes ) {echo $clientes['email'].' ; ' ;} } else { echo 'Nenhum Dado Encontrado.';}?></textarea>
		</div>		
		<div class="row">
			<div class="btn col s6 offset-s3" onclick="copiarHTML()">Copiar Tudo</div>
		</div>
	</div>

	<?php
}

if ( $_GET[ 'acao' ] === "4" ) {		
	$sedes = listarSedeEmpresasBD( $conexao );
	
	if( !empty($_GET[ 'filtro' ]) ){
		
		if( $_GET[ 'ativo' ] === on ){
			$checked = 'checked';
			$_GET[ 'ativo' ] = ' <=2';
		}
		
		$array = array($_GET[ 'acao' ], $_GET[ 'empresa' ], $_GET[ 'ativo' ]);
		
		$clientes = listarEmailTodosUsuarioFiltradoBD( $conexao, $array );			
		
	} else {
		$clientes = listarEmailTodosUsuarioBD( $conexao, $_GET[ 'acao' ] );		
	} ?>
	<br><br><br><br>
	<div class="container row">
		<div class="row">
			<form action="mala.php" method="get">
				<h4>Filtros:</h4>
				<div class="input-field col s12 m6 hide">
						<input name="acao" value="<?=$_GET[ 'acao' ]?>"/>
						<input name="filtro" value="sim"/>
				</div>
				<div class="input-field col s12 m6">
						<select name="empresa">
							<option disabled selected></option>
							<?php foreach ( $sedes as $sedes ) {
							if ($_GET['empresa'] === $sedes["id_empresa"]){ $selected = 'selected';} 
							else {$selected = '';}?>
							<option value="<?=$sedes['id_empresa']?>" <?=$selected?>><?= $sedes['sede'] ?></option>
							<?php } ?>
						</select>
						<label>Filtrar por Empresa: </label>
				</div>
				<div class="input-field col s12 m3">
					<label>
						<input type="checkbox" name="ativo" <?=$checked?>/>
						<span>Somente Ativos</span>
					</label>    		
				</div>
				<div class="input-field col s12 m3">
					<button class="btn">Filtar</button>		
				</div>
     		</form>
      	</div>
      	
		<h2>Lista E-Mail.:</h2>
		<div class="row">
			<div class="btn col s6 offset-s3" onclick="copiarHTML()">Copiar Tudo</div>
		</div>		
		<div class="row">
			<textarea id="copiar"class="materialize-textarea"><?php if(!empty($clientes)){ foreach ( $clientes as $clientes ) {echo $clientes['email'].' ; ' ;} } else { echo 'Nenhum Dado Encontrado.';}?></textarea>
		</div>		
		<div class="row">
			<div class="btn col s6 offset-s3" onclick="copiarHTML()">Copiar Tudo</div>
		</div>
	</div>
	
	<?php
} ?>

<script>
		function copiarHTML() {
			// Seleciona o conteúdo do input
			$('#copiar').select();
			// Copia o conteudo selecionado
			var copiar = document.execCommand('copy');
			// Verifica se foi copia e retona mensagem
			if(copiar){
				alert('Copiado');
			}else {
				alert('Erro ao copiar, seu navegador pode não ter suporte a essa função.');
			}
			// Cancela a execução do formulário
			return false;
        }
	</script>
<?php
//if ( empty($_GET[ 'acao' ]) ){	echo '<script>window.location.replace("dashboard.php");</script>';}
include_once( 'rodape.php' ); ?>