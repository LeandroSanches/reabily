<html>

<head>
	<meta charset="UTF-8">
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
	<!--Import materialize.css-->
	<link type="text/css" rel="stylesheet" href="../css/materialize.min.css" media="screen,projection"/>
	<link type="text/css" rel="stylesheet" href="../css/reabity.css" media="screen,projection"/>
</head>

<body>
	<div class="container">
		<div class="row">
			<div class="col s2 offset-m5 m2">
				<div class="preloader-wrapper  big active">
					<div class="spinner-layer spinner-white-only">
						<div class="circle-clipper left">
							<div class="circle"></div>
						</div>
						<div class="gap-patch">
							<div class="circle"></div>
						</div>
						<div class="circle-clipper right">
							<div class="circle"></div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</body>
<script src="https://code.jquery.com/jquery-3.3.1.js" integrity="sha256-2Kok7MbOyxpgUVvAk/HJ2jigOSYS2auK4Pfzbm7uH60=" crossorigin="anonymous"></script>
<script type="text/javascript" src="../js/materialize.min.js"></script>

</html>

<?php $time = microtime(true) - $_SERVER["REQUEST_TIME"];	$time = number_format($time, 2, ',', ' ');	echo "<br> <small> $time s </small> <br>"; 