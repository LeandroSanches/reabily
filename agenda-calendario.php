<?php include_once( 'cabecalho.php' );

if ( $_SESSION["valor"]["atual"] === '1' ){

	include_once( 'model/atividade.php' );
	$atividadeEmpresa = listarAtividadesDaEmpresaBD($conexao, $_SESSION['usuario']['id_empresa']);?>

	<br>
	<div class="container row">
		<h2>Calendário de Agendamento </h2>
		<div class="row">
				<div class="col s12 m12 l12 center-align PassoAtiv">
					<span style="padding-right: 8px;"><i class="material-icons medium">looks_one</i></span>
				</div>
		</div>
		<?php if(empty($atividadeEmpresa)){
			echo "<p align='center'><strong>Nenhuma Agenda Disponivel</strong></p>";
		} else { ?>
		<form action="agenda-calendario.php?voltar=2" method="post">
		  <div class="row">
				<div class="input-field col s12 m6 offset-m3">
					<select name="atividade">
						<option disabled selected></option>
						<?php foreach ( $atividadeEmpresa as $atividadeEmpresa ) {?>					  		
						  <option value="<?=$atividadeEmpresa['atividade']?>" ><?= $atividadeEmpresa['nome'] ?></option>
					  <?php } ?>
					</select>
					<label>Escolha uma Atividade: </label>
			  	</div>
		  </div>

		  <div class="row">
				<div class="col s6 m3 offset-m1" align="center">
					<!-- <a href="agenda-calendario.php" class="btn waves-effect waves-light">Voltar</a>  -->
					</div>
				<div class="col s6 m3 offset-m4">
					<button class="btn2 waves-effect waves-light right" type="submit">Avançar</button>
				</div>
		  </div>
		</form>
		<?php } ?>
  </div>
	<?php }

if ( $_SESSION["valor"]["atual"] === '2' ){

	if( empty($_SESSION[ 'atividade' ])){ echo '	<script> window.location.replace("agenda-calendario.php?voltar=1");	</script>'; }

	include_once( 'model/atividade.php' );
	$atividade = listarAtividadeBD( $conexao, $_SESSION[ 'atividade' ] );

	include_once( 'model/agenda.php' );
	$array = array(	$_SESSION['atividade'], $_SESSION['usuario']['id_empresa'] );
	$dia = listarAgendaComAtividadeBD( $conexao, $array );

	?>
		<br>
		<div class="container row">
			<h2>Calendário de Agendamento </h2>
			<div class="row">
					<div class="col s12 m12 l12 center-align PassoAtiv">
						<span style="padding-right: 8px;"><i class="material-icons medium">looks_one</i></span>
						<span style="padding:0 8px 0 8px;"><i class="material-icons medium" >looks_two</i></span>
					</div>
			</div>
			<form action="agenda-calendario.php?voltar=3" method="post">
				<div class="row">
					<p align="center">Atividade Escolhida: <?= $atividade[0]['nome'];?> </p>
				</div>
				<div class="row">
					<?php if ( !empty($dia)){ ?>
						<div class="input-field col s12 m6 offset-m3">
							<select name="data">
								<?php foreach ( $dia as $dia ) {
								if( $ultimoDia != $dia['data'] ){
									$ultimoDia = $dia['data'];

									$diasemana = array('Domingo', 'Segunda', 'Terça', 'Quarta', 'Quinta', 'Sexta', 'Sabado');
									$diasemana_numero = date('w', strtotime($dia['data']));

									?>
									<option value="<?=$dia['data']?>" > <?= date('d/m/Y', strtotime($dia["data"]))?>, <?= $diasemana[$diasemana_numero];?> </option>
								<?php	}
								} ?>
							</select>
							<label>Escolha uma Data: </label>
					  	</div>
						<?php } else {
							echo "<p align='center'> <b> Nenhuma Data Disponivel </b> </p>";
						}?>

			  </div>
			  <div class="row">
					<div class="col s6 m3 offset-m1" align="center">
						<a href="agenda-calendario.php?voltar=1" class="btn waves-effect waves-light">Voltar</a> </div>
					<?php if ( !empty($dia)){ ?>
					<div class="col s6 m3 offset-m4">
						<button class="btn2 waves-effect waves-light right" type="submit">Avançar</button>
					</div>
				<?php } ?>
			  </div>
			</form>
	  </div>
	<?php }

if ( $_SESSION["valor"]["atual"] === '3' ){

	//listarAtividade do sistema
	include_once( 'model/atividade.php' );
	$atividade = listarAtividadeBD( $conexao, $_SESSION[ 'atividade' ] );


	//listarAgendamentos do dia.
	include_once( 'model/agenda.php' );
	$dia = $_SESSION["data"];
	$agenda = listarAgendadiaBD($conexao, $dia, $_SESSION[ 'atividade' ], $_SESSION['usuario']['id_empresa']);

	include_once( 'model/usuario.php' );

	date_default_timezone_set('America/Sao_Paulo');
	$date = date('Y-m-d');
	$dataMinima = date('Y-m-d',strtotime('-1 week', strtotime($date)));
	$dataMaxima = date('Y-m-d',strtotime('+1 day', strtotime($date)));

	// listar entre as datas
	$intervalo = listarAgendaNoIntervaloBD($conexao, $dataMaxima, $dataMinima);
	//var_dump($intervalo);
	?>
		<br>
		<div class="container row">

			<h2>Calendário de Agendamento </h2>

			<div class="row">
				<div class="col s12 m12 l12 center-align PassoAtiv">

					<span style="padding-right: 8px;"><i class="material-icons medium">looks_one</i></span>
					<span style="padding:0 8px 0 8px;"><i class="material-icons medium" >looks_two</i></span>
					<span style="padding-left: 8px;"><i class="material-icons medium">looks_3</i></span>

				</div>
			</div>
			

			<div class="row">
				<div class="col s6 m3 offset-m1" align="center">
					<a href="agenda-calendario.php?voltar=2" class="btn waves-effect waves-light">Voltar</a> 
				</div>			
			</div>

			<div class="row">
				<div class="col s12 m6" align="center">Data escolhida: <?= date('d/m/Y', strtotime( $_SESSION['data']))?> </div>
				<div class="col s12 m6" align="center">Atividade Escolhida: <?= $atividade[0]['nome'];?> </div>
			</div>
			
			<div class="row">
			<?php foreach ( $agenda as $agenda ) { ?>
				 <div class="col s12 m6 center-align blue">
					<p>Cod.: <?= $agenda[ 'id' ]?></p>
					<p>Nome: <?= $agenda[ "nome" ]?></p>
					<p>Data: <?= gmdate('H:i', strtotime( $agenda['hora'] ))?></p>
					<p>Empresa:	<?php $empresas = listarUserEmpresasBD($conexao, $agenda['empresa']); echo $empresas[0]['nome']; ?></p>
					<hr>
					<!--Verificar a lista de espera-->
					<?php //qtd de agendas ja realizadas 
						  $qtd = listarVagas($conexao, $agenda[ "id" ]);
						$total = $agenda[ "quantidades" ] - count($qtd);
					 if($total > 0){
						 echo '<p><a href="#modal'.$agenda[ "id" ].'" class="modal-trigger"><i class="material-icons white-text" title="Agendar">event_available</i></a></p>';
					 } 
					 if($total <= 0){
						 echo '<p><a href="#modal'.$agenda[ "id" ].'" class="modal-trigger"><i class="material-icons white-text" title="Lista de Espera">art_track</i></p>';
					 }?>					
				</div>

				<!-- Modal Structure -->
					<div id="modal<?=$agenda[ "id" ]?>" class="modal grey-text text-darken-3">
						<div class="modal-content">

							<h4><?= $agenda[ "nome" ]?></h4>
							<hr>
							<?php
								$intervaloQTD = count ($intervalo);
								if($intervaloQTD > $agenda[ "recadastrados" ]){
										echo "<p> <b> Desculpa voçê chegou no limite de ".$agenda[ "recadastrados" ]." agendamentos por semana. <b> </p>";
										$botao = null;
								} else{
									
									if($total > 0){ 
										echo "<p> Deseja agendar esse horário? </p>"; 
										$botao = "<a href='agenda.php?acao=agendar&token=".$agenda["id"]."' class='modal-close waves-effect waves-green btn'>Agendar</a>";
									} 
									if($total <= 0){ 
										echo "<p> Deseja entrar na lista de espera desse horário? </p>"; 
										$botao = "<a href='agenda.php?acao=agendar&token=".$agenda["id"]."' class='modal-close waves-effect waves-green btn'>Lista de Espera</a>";
									} 
									
								}
							?>
						</div>

						<div class="modal-footer">
							<?= $botao ?>
						</div>

					</div>
			<?php }?>

			</div>

			<div class="row">
				<div class="col s6 m3 offset-m1" align="center">
					<a href="agenda-calendario.php?voltar=2" class="btn waves-effect waves-light">Voltar</a> </div>

			<?php if ( empty($agenda)){
				echo '<p>Nenhum agendamento pra hoje</p>';
			} ?>
			</div>

		</div>

<?php }

if ( !empty( $_GET['voltar'] )){

	$_SESSION["valor"]["atual"] = $_GET['voltar'];

	if ( !empty($_POST['data'])){
		$_SESSION[ 'data' ] = $_POST['data'];
	}

	if ( !empty($_POST['atividade'])){
		$_SESSION[ 'atividade' ] = $_POST['atividade'];
}
	
	echo '<script>window.location.replace("agenda-calendario.php")</script>';
}

include_once( 'rodape.php' );
