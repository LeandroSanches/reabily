<?php
include_once( 'cabecalho.php' );

$msgBotao = "Alterar Senha";

unset($_SESSION[ "acao" ]);
$_SESSION[ "acao" ][ 'acao' ] =  'senha';

?>

<div class="container">
	
	<h2>Trocar Senha</h2>
	
	<p>Tem Certeza que deseja alterar a senha?</p>
	
	<p>Essa ação não poderá ser desfeita</p>
	
	<form class="col s12" action="controller/usuario.php" method="post" enctype="multipart/form-data">
		<div class="row">
			<div class="input-field col s12">
				<i class="material-icons prefix">fingerprint</i>
				<input id="passwordold" type="password" class="validate" name="senhaold">
				<label for="passwordold">Senha Antiga</label>
			</div>
			<div class="input-field col s12">
				<i class="material-icons prefix">fingerprint</i>
				<input id="passwordnew" type="password" class="validate" name="senhanew">
				<label for="passwordnew">Nova senha</label>
			</div>
			<div class="input-field col s12">
				<i class="material-icons prefix">fingerprint</i>
				<input id="passwordnew1" type="password" class="validate" name="senhanew2">
				<label for="passwordnew1">Repitir a Nova senha</label>
			</div>
		</div>
		<div class="row">
		<div class="col s6 m3 offset-m1" align="center">
			<a href="meuperfil.php" class="btn waves-effect waves-light">Voltar</a> </div>
		<div class="col s6 m6">
			<button class="btn2 waves-effect waves-light right" type="submit"><?= $msgBotao?></button>
		</div>
	</div>
	</form>

</div>

<?php include_once('rodape.php'); ?>