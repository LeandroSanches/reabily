<?php
include_once( 'cabecalho.php' );
include_once( 'model/usuario.php' );
include_once( 'model/setor.php' );
include_once( 'model/agenda.php' );

//$_SESSION[ "acao" ][ 'acao' ] = 'tipo';//apagar

if ( isset( $_SESSION[ "acao" ][ 'acao' ] ) ) {

	$btnvoltar = 'dashboard.php';

	if ( $_SESSION[ "acao" ][ 'acao' ] === 'escolher' ) {
		if( !empty($_SESSION[ "acao" ][ 'postemp' ])){
			$empresa = $_SESSION[ "acao" ][ 'postemp' ];	
			
			$array = listarEmpresaBD( $conexao, $empresa );
			$_SESSION[ "acao" ][ 'empresa' ] = $array[0]['nome'];			
			
		} else {
			//empresa
			$empresa = $_SESSION['usuario'];
			$array = listarEmpresaBD( $conexao, $empresa['id'] );
			$_SESSION[ "acao" ][ 'empresa' ] = $array[0]['nome'];				
		}
		?>
		
		<br>
		<div class="container">
			<div class="row">
					<div class="row">
						<div class="col s12 m12 l6">
							<h2>Relatório </h2>
						</div>
						<div class="col s12 m12 l6">
							<form method="post" action="relatorio.php?acao=escolher">							
								<div class="input-field col s12 hide noprint">									
							  		<input type="number" class="validate" name="empresa" value="<?=$empresa?>">	
								</div>
								<div class="input-field col s5">
									<div class="col s2 noprint">
									 <i class="material-icons">update</i> 									 
									</div>
									<div class="col s10">
									  <input type="date" class="validate" name="data1" value="<?=$_SESSION[ 'filtro' ][ 'data1' ]?>">								  
									</div>
								</div>
								<div class="input-field col s5">
									<div class="col s2 noprint">
									 <i class="material-icons">date_range</i> 									 
									</div>
									<div class="col s10">
									  <input type="date" class="validate" name="data2" value="<?=$_SESSION[ 'filtro' ][ 'data2' ]?>">								  
									</div>
								</div>
								<div class="input-field col s2 noprint">
									<button class="waves-effect waves-light right white-text" type="submit" style="background-color: transparent; border: 0px"><i class="material-icons">search</i>  </button>
								</div>
								
							</form>						
							
						</div>
					</div>
										
					<div class="row noprint">
						<div class="col s12">
							<p align="center"><a href="dashboard.php" class="waves-effect waves-light btn">Voltar</a>
							</p>
						</div>
					</div>

					<div class="row noprint">
						<div class="col s12">
							<p align="right"  onClick="window.print();"> <i class="material-icons">local_printshop</i> Imprimir	</p>
						</div>
					</div>

					<div class="row">

						<h4>Empresa:</h4>
						<p><?= $array[ 0 ][ 'id' ] ?> - <?= $_SESSION[ "acao" ][ 'empresa' ] ?> </p>
						
						
						<div class="row PrintQuebraProx">

						<div class="col s12 m12 l12">

							<h4>Filiais: </h4>

								<?php 
								//filiais
								$filiais = listarFilaisEmpresa( $conexao, $array[0]['id'] );
								$i=1;
								$ids = array();

								if ( empty($filiais)){
									echo 'Empresa sem Filiais';								
								} else{?>
							<table width="100%" align="center">
								<tbody>
									<tr>
										<td>
											<h6>COD</h6>
										</td>
										<td>
											<h6>Razão Social </h6>
										</td>
										<td>
											<h6>Endereco</h6>
										</td>
									</tr>

									<?php foreach($filiais as $filiais){ 
												$ids[$i] = $filiais['id']; 
												$i++; ?>
									<tr>
										<td><?= $filiais['id'] ?></td>									
										<td><?= $filiais['nome'] ?></td>									
										<td><?= $filiais['endereco'] ?></td>
									</tr>
									<?php } ?>

								</tbody>
							</table>							
								<?php } $qtds = count($ids); ?>
								<p align="right">Quantidade de Filiais Cadastradas: <?= $qtds?></p>

						</div>

						</div>

						<?php 			
						//aulas	
						include_once( 'model/atividade.php' );
						$atividades = listarAtividadesBD($conexao);
						$qtd_atividades  = count($atividades);

						if( !empty($_SESSION[ "filtro" ][ 'data1' ]) ){
							//listar agenda da empresa Entre datas
							include_once( 'model/agenda.php' );
							$agenda = listarAgendaEmpresaEntreDataBD($conexao, $empresa, $_SESSION[ "filtro" ][ 'data1' ], $_SESSION[ "filtro" ][ 'data2' ]);
							$qtd_agenda = count($agenda);
							$periodo = "Dados Referentes ao periodo de: ".date('d/m/Y', strtotime($_SESSION[ "filtro" ][ 'data1' ]))." há ". date('d/m/Y', strtotime($_SESSION[ "filtro" ][ 'data2' ]));

						} else {
							//listar agenda da empresa
							include_once( 'model/agenda.php' );
							$agenda = listarAgendaEmpresaBD($conexao, $empresa);
							$qtd_agenda = count($agenda);	
							

						} ?>
						<!--GRÁFICO AULAS Agendadas-->
						<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
						<script type="text/javascript">
						google.charts.load( "current", {
							packages: [ "corechart" ]
						} );
						google.charts.setOnLoadCallback( drawChart );

						function drawChart() {
							var data = google.visualization.arrayToDataTable( [
								[ 'Aulas', 'Quantidades de Aulas' ],									
								
								<?php foreach($atividades as $atividades){
											$va = 0;
											for($i=0; $i <= $qtd_agenda; $i++){
												if($agenda[$i]['atividade'] === $atividades['id']){
													$va++;
												}
											}
											echo  "[ '".$atividades['nome']."', ". $va ." ],";								
										}?>
							] );

							var options = {
								title: '',
								//width: '100%',
								//height: '50vh',
								pieHole: 0.4,
								backgroundColor: 'transparent',
								titleTextStyle: {
									color: '#FFFFFF',
									fontName: 'Futura Bold Condensed BT',
									fontSize: '24'
								},
								chartArea: {
									width: '100%',
									height: '75%',
									left: 0,
									backgroundColor: '#FFFFFF'
								},
								legend: {
									textStyle: {
										color: '#cacaca',
										fontName: 'Roboto'
									}
								},
								pieSliceTextStyle: {
									fontName: 'Roboto'
								},
								is3D: true,


							};

							var chart = new google.visualization.PieChart( document.getElementById( 'piechart_3d' ) );
							chart.draw( data, options );

							function resize() {
								// change dimensions if necessary
								chart.draw( data, options );
							}
							if ( window.addEventListener ) {
								window.addEventListener( 'resize', resize );
								console.log( "resize!" );
							} else {
								window.attachEvent( 'onresize', resize );
							}


						}
						</script>
						<!--FIM GRÁFICO AULAS Agendadas-->

						<div class="row PrintQuebraProx">

						<div class="col s12 m12 l12">

							<h4>Atividades Agendadas</h4>

							<p><small><?=$periodo?></small></p>
							
							<?php if($qtd_agenda === 0){
								echo ' <h5 align="center"> Empresa sem Agenda </h5> '; 
							} else {
								echo '<div id="piechart_3d" style="width: 100%; height: 50vh;"></div>';
							} ?>
							<p align="right">Totas de Atividades: <?= $qtd_agenda ?></p>

						</div>

						</div>

						<?php 			
						//aulas	Canceladas
						$qtd_aguardando	= 0;
						$qtd_presente	= 0;
						$qtd_faltou		= 0;
						$qtd_cancelou	= 0;

						if( !empty($_SESSION[ "filtro" ][ 'data1' ]) ){
						$aulas = $aulas = listarAgendaCanceladoEntreDataBD($conexao, $empresa, $_SESSION[ "filtro" ][ 'data1' ], $_SESSION[ "filtro" ][ 'data2' ]);
						} else {
						$aulas = listarAgendaCanceladoBD($conexao, $empresa);
						}
						$qtd_aulas = count($aulas);								
						for($i=0; $i <= $qtd_aulas; $i++){
							switch ($aulas[$i]['presenca']) {
								case 1:
									$qtd_presente++;
									break;
								case 2:
									$qtd_faltou++;
									break;
								case 5:
									 $qtd_cancelou++;
									break;
							}	
						}	

						?>

						<!--GRÁFICO AULAS canceladas-->
						<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
						<script type="text/javascript">
						google.charts.load( "current", {
							packages: [ "corechart" ]
						} );
						google.charts.setOnLoadCallback( drawChart );

						function drawChart() {
							var data = google.visualization.arrayToDataTable( [
								[ 'Aulas', 'Quantidades de Aulas' ],
								[ 'Presente', <?= intval($qtd_presente) ?> ],
								[ 'Faltou', <?= intval($qtd_faltou) ?> ],
								[ 'Cancelou', <?= intval($qtd_cancelou) ?> ]
								
							] );

							var options = {
								title: '',
								//width: '100%',
								//height: '50vh',
								pieHole: 0.4,
								backgroundColor: 'transparent',
								titleTextStyle: {
									color: '#FFFFFF',
									fontName: 'Futura Bold Condensed BT',
									fontSize: '24'
								},
								chartArea: {
									width: '100%',
									height: '75%',
									left: 0,
									backgroundColor: '#FFFFFF'
								},
								legend: {
									textStyle: {
										color: '#cacaca',
										fontName: 'Roboto'
									}
								},
								pieSliceTextStyle: {
									fontName: 'Roboto'
								},
								is3D: true,


							};

							var chart = new google.visualization.PieChart( document.getElementById( 'piechartcanceladas_3d' ) );
							chart.draw( data, options );

							function resize() {
								// change dimensions if necessary
								chart.draw( data, options );
							}
							if ( window.addEventListener ) {
								window.addEventListener( 'resize', resize );
								console.log( "resize!" );
							} else {
								window.attachEvent( 'onresize', resize );
							}


						}
						</script>
						<!--FIM GRÁFICO AULAS canceladas-->

						<div class="row PrintQuebraProx">
							<div class="col s12 m12 l12">

								<h4>Aulas Agendadas: </h4>

								<?php if($qtd_aulas === 0){
									echo ' <h5 align="center"> Empresa sem Presença </h5> '; 
								} else {
									echo '<div id="piechartcanceladas_3d" style="width: 100%; height: 50vh;"></div>';
								} ?>

								<p align="right">Totas de Aulas: <?php echo $qtd_aulas?></p>

							</div>
						</div>

						<!--GRÁFICO PARTICIPAÇÃO POR SETORES-->
						<script type="text/javascript">
						google.charts.load( 'current', {
							'packages': [ 'corechart' ]
						} );
						google.charts.setOnLoadCallback( drawChart );

						function drawChart() {
							var data = google.visualization.arrayToDataTable( [
								[ 'Setores', 'Não informado', 'Total' ],
								[ '2015', 1000, 400 ],
								[ '2016', 1170, 460 ],
								[ '2017', 660, 1120 ],
								[ '2018', 1030, 540 ]
							] );

							var options = {
								title: '',
								hAxis: {
									title: 'Setores',
									titleTextStyle: {
										color: '#FFFFFF',
										fontName: 'Roboto',
										italic: 'false'
									},
									textStyle: {
										color: '#FFFFFF',
										fontName: 'Roboto'
									}
								},
								vAxis: {
									minValue: 0,
									textStyle: {
										color: '#FFFFFF',
										fontName: 'Roboto'
									}
								},
								backgroundColor: 'transparent',
								titleTextStyle: {
									color: '#FFFFFF',
									fontName: 'Futura Bold Condensed BT',
									fontSize: '24'
								},
								legend: {
									textStyle: {
										color: '#FFFFFF',
										fontName: 'Roboto'
									}
								}

							};

							var chart = new google.visualization.AreaChart( document.getElementById( 'chart_div' ) );
							chart.draw( data, options );

							function resize() {
								// change dimensions if necessary
								chart.draw( data, options );
							}
							if ( window.addEventListener ) {
								window.addEventListener( 'resize', resize );
								console.log( "resize!" );
							} else {
								window.attachEvent( 'onresize', resize );
							}



						}
						</script>
						<!--FIM PARTICIPAÇÃO POR SETORES-->

						<!--<div class="row">

						<div class="col s12 m12 l12">

							<h4>Participação por Setores/Grupos/Andares</h4>

							<div id="chart_div" style="width: 100%; height: 50vh;"></div>

						</div>

						</div>

						<div class="row">

						<div class="col s12 m12 l10">

							<h4>Enviar este relatório por email</h4>

						</div>

						<div class="col s12 m12 l2">
							<i class="material-icons tooltipped" data-position="top" data-tooltip="Para mais de um destinatário utilize: vírgula (,) ou ponto-e-vírgula (;)">live_help</i>
							</a>
						</div>

						</div>

						<div class="row">

						<div class="col s12 m4">

							<div class="input-field col s12">
								<input id="contato" type="text" class="validate" name="contato" value="">
								<label for="contato">Destinatários</label>
							</div>

							<div class="input-field col s12">
								<input id="contato" type="text" class="validate" name="contato" value="">
								<label for="contato">Mensagem/Observação</label>
							</div>

						</div>

						<div class="input-field col s12 m4 center-align">

							<p>Imprimir este relatório?</p>

							<p><a href="" class="waves-effect waves-light btn">Imprimir</a>
							</p>

						</div>

						<div class="input-field col s12 m4 center-align">

							<p>Buscar em Outra Data</p>

							<input type="date" name="data" id="iddata" value="2008-05-01" placeholder="Buscar em outra data">

							<a href="" class="waves-effect waves-light btn">Buscar</a>

						</div>

						</div>-->
						
						<div class="row">
							<div class="col s12 m12 l12">								

								<h4>Usuários:</h4>

								<?php
								$clientes = listarusuarioEmpresaBD( $conexao, $empresa );
								$qtd_usuarios = count($clientes);
								
								//genero
								$generoMascolino = 0;
								$generoFeminino = 0;
								$generoOutros = 0;
								
								//idade								
								$menos18 = 0;
								$entre1840 = 0;
								$entre4060 = 0;
								$mais60 = 0;
		
							
								if($qtd_aulas === 0){
									echo ' <h5 align="center"> Empresa sem Presença </h5> '; 
								} else { ?>
									<table width="100%" align="center">
										<tbody>
											<tr>
												<td>
													<h6>Matricula:</h6>
												</td>
												<td>
													<h6>Nome:</h6>
												</td>												
											</tr>

											<?php foreach($clientes as $clientes){ 
														$ids[$i] = $clientes['id']; 
														$i++; ?>
											<tr>									
												<td><?= $clientes['id'] ?></td>																				
												<td><?= $clientes['nome'] ?></td>
											</tr>
											<?php
											switch ($clientes['sexo']) {
												case 0:
													$generoOutros++;
													break;
												case 1:
													$generoMascolino++;
													break;
												case 2:
													$generoFeminino++;
													break;
												case 3:
													$generoOutros++;
													break;
											}
									
											//descobrindo a idade
											$partes = explode("-", $clientes['nascimento']);
											$ano = $partes[0];
											$idade = date(Y) - $ano;
											if($idade < 18) {$menos18++;}
											if($idade >= 18 && $idade <= 40) {$entre1840++;}
											if($idade >= 40 && $idade <= 60) {$entre4060++;}
											if($idade > 60) {$mais60++;}
									
											} ?>
										</tbody>
									</table>
									
									<!--GRÁFICO AULAS canceladas-->
									<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
									<script type="text/javascript">
									google.charts.load( "current", {
										packages: [ "corechart" ]
									} );
									google.charts.setOnLoadCallback( drawChart );

									function drawChart() {
										var data = google.visualization.arrayToDataTable( [
											[ 'Gênero', 'Quantidades' ],
											[ 'Masculino', <?= intval($generoMascolino) ?> ],
											[ 'Feminino', <?= intval($generoFeminino) ?> ],
											[ 'Outros', <?= intval($generoOutros) ?> ]

										] );

										var options = {
											title: '',
											//width: '100%',
											//height: '50vh',
											pieHole: 0.4,
											backgroundColor: 'transparent',
											titleTextStyle: {
												color: '#FFFFFF',
												fontName: 'Futura Bold Condensed BT',
												fontSize: '24'
											},
											chartArea: {
												width: '100%',
												height: '75%',
												left: 0,
												backgroundColor: '#FFFFFF'
											},
											legend: {
												textStyle: {
													color: '#cacaca',
													fontName: 'Roboto'
												}
											},
											pieSliceTextStyle: {
												fontName: 'Roboto'
											},
											is3D: true,


										};

										var chart = new google.visualization.PieChart( document.getElementById( 'genero' ) );
										chart.draw( data, options );

										function resize() {
											// change dimensions if necessary
											chart.draw( data, options );
										}
										if ( window.addEventListener ) {
											window.addEventListener( 'resize', resize );
											console.log( "resize!" );
										} else {
											window.attachEvent( 'onresize', resize );
										}


									}
									</script>
									<!--FIM GRÁFICO AULAS canceladas-->
									
									<h4>Gênero</h4>
									<div id="genero" style="width: 100%; height: 50vh;"></div>
									
								<?php } ?>

								<p align="right">Totas de Usuários: <?= $qtd_usuarios?></p>

							</div>
						</div>
						
						<div class="row">
							<div class="col s12 m12 l12">								

								<h4>Idade:</h4>

								<?php
								if($qtd_aulas === 0){
									echo ' <h5 align="center"> Empresa sem Idades Cadastradas </h5> '; 
								} else { ?>
																	
									<!--GRÁFICO AULAS canceladas-->
									<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
									<script type="text/javascript">
									google.charts.load( "current", {
										packages: [ "corechart" ]
									} );
									google.charts.setOnLoadCallback( drawChart );

									function drawChart() {
										var data = google.visualization.arrayToDataTable( [
											[ 'Idade', 'Médias' ],
											[ 'Menos de 18', <?= intval($menos18) ?> ],
											[ 'Entre 18 e 40', <?= intval($entre1840) ?> ],
											[ 'Entre 40 e 60', <?= intval($entre4060) ?> ],
											[ 'Mais de 60', <?= intval($mais60) ?> ]

										] );

										var options = {
											title: '',
											//width: '100%',
											//height: '50vh',
											pieHole: 0.4,
											backgroundColor: 'transparent',
											titleTextStyle: {
												color: '#FFFFFF',
												fontName: 'Futura Bold Condensed BT',
												fontSize: '24'
											},
											chartArea: {
												width: '100%',
												height: '75%',
												left: 0,
												backgroundColor: '#FFFFFF'
											},
											legend: {
												textStyle: {
													color: '#cacaca',
													fontName: 'Roboto'
												}
											},
											pieSliceTextStyle: {
												fontName: 'Roboto'
											},
											is3D: true,


										};

										var chart = new google.visualization.PieChart( document.getElementById( 'idade' ) );
										chart.draw( data, options );

										function resize() {
											// change dimensions if necessary
											chart.draw( data, options );
										}
										if ( window.addEventListener ) {
											window.addEventListener( 'resize', resize );
											console.log( "resize!" );
										} else {
											window.attachEvent( 'onresize', resize );
										}


									}
									</script>
									<!--FIM GRÁFICO AULAS canceladas-->
									
									
									<div id="idade" style="width: 100%; height: 50vh;"></div>
									
								<?php } ?>

								<p align="right">Totas de Idades: <?= $qtd_usuarios?></p>

							</div>
						</div>
					
						<div class="row noprint">
							<div class="col s12">
								<p align="right"  onClick="window.print();"> <i class="material-icons">local_printshop</i> Imprimir	</p>
							</div>
						</div>
						<div class="row noprint">
							<div class="col s12">
								<p align="center"><a href="dashboard.php" class="waves-effect waves-light btn">Voltar</a>
								</p>
							</div>
						</div>
				</div>
			</div>
		</div>

		<?php
	}
	
	if ( $_SESSION[ "acao" ][ 'acao' ] === 'adm' ) { 
		if ( empty($passo) ) { 
			//escolher empresa
			$empresa = listarSedeEmpresasBD( $conexao );
			//var_dump($empresa);
			$passo = 2;			
			$_SESSION[ "acao" ][ 'acao' ] === 'escolher';
			?>
			<br>
			<div class="container">
				<form class="col s12" action="relatorio.php?acao=escolher" method="post" enctype="multipart/form-data">
					<div class="row">
						<div class="input-field col s12">
							<select name="empresa">
							  <?php foreach ($empresa as $empresa) {
										if( !empty($empresa['sede'])){?>
										<option value="<?=$empresa['id_empresa']?>"><?=$empresa['sede']?></option>
								  <?php	} 
									} ?>
							</select>
							<label>Escolha uma empresa Sede:</label>
						</div>
					</div>
					<div class="row">
						<div class="col s6 m3 offset-m1" align="center">
							<a href="<?=$btnvoltar?>" class="btn waves-effect waves-light">Voltar</a> </div>
						<div class="col s6 m6">
							<button class="btn2 waves-effect waves-light right" type="submit"> Continuar </button>
						</div>
					</div>
				</form>
			</div>
	
			<?php				
		}	
	}
	
	if ( $_SESSION[ "acao" ][ 'acao' ] === 'ginatica' ) {
		
		if ( empty( $_SESSION[ "acao" ][ 'id' ]) ) { 
			$empresa = listarSedeEmpresasBD( $conexao );
			?>
			<br>
			<div class="container">
				<h3>Relatório de Ginástica:</h3>
				<form class="col s12" action="relatorio.php?acao=ginatica&token=2" method="post" enctype="multipart/form-data">
					<div class="row">
						<div class="input-field col s12 m6">
							<select name="empresa">
							  <?php foreach ($empresa as $empresa) {
										if( !empty($empresa['sede'])){?>
										<option value="<?=$empresa['id_empresa']?>"><?=$empresa['sede']?></option>
								  <?php	} 
									} ?>
							</select>
							<label>Escolha uma empresa Sede:</label>
						</div>
						<div class="input-field col s12 m6">
							<input  id="dataInicio" type="date" class="validate" name="data1">
          					<label for="dataInicio">Data inicial</label>
						</div>
					</div>
					<div class="row">
						<div class="col s6 m3 offset-m1" align="center">
							<a href="<?=$btnvoltar?>" class="btn waves-effect waves-light">Voltar</a> </div>
						<div class="col s6 m6">
							<button class="btn2 waves-effect waves-light right" type="submit"> Continuar </button>
						</div>
					</div>
				</form>
			</div>
	
			<?php				
		}
		
		if ( $_SESSION[ "acao" ][ 'id' ] === '2' ) { 
			//Cruzar agenda do dia selecionado com a empresa gerando relatório
			//echo $_SESSION[ "acao" ][ 'postemp' ]; //Empesa Apagar
			//echo $_SESSION[ "filtro" ][ 'data1' ]; //Dia Apagar
			
			$btnvoltar = 'dashboard.php';
			
			$empresa = $_SESSION[ "acao" ][ 'postemp' ];	
			
			$array = listarEmpresaBD( $conexao, $empresa );
			$_SESSION[ "acao" ][ 'empresa' ] = $array[0]['nome'];
			?>	
		<br>
		<div class="container">	
					
			<div>
						
			<div class="row">
				<div class="col s12">
					<p align="center"><a href="dashboard.php" class="waves-effect waves-light btn">Voltar</a>
					</p>
				</div>
			</div>

			<div class="row ">
				<div class="col s12">
					<p align="right"  onClick="window.print();"> <i class="material-icons">local_printshop</i> Imprimir	</p>
				</div>
			</div>
			
			<div class="row">
				<div class="col s12 m6">
					<h4>Empresa:</h4>
					<p><?= $array[ 0 ][ 'id' ] ?> - <?= $_SESSION[ "acao" ][ 'empresa' ] ?> </p>
				</div>
				<div class="col s12 m6">
					<h4>Data:</h4>
					<p><?= $data1 = date('d/m/Y', strtotime($_SESSION[ "filtro" ][ 'data1' ]))?> - <?php $data2 = date('Y-m-d', strtotime("+1 month", strtotime($_SESSION[ "filtro" ][ 'data1' ]))); echo date('d/m/Y', strtotime($data2));?> </p>
				</div>			
			</div>
			</div>
			
			<div class="row PrintQuebraProx">

				<div class="col s12 m12 l12">

					<h4>Filiais: </h4>

						<?php 
						//filiais
						$filiais = listarFilaisEmpresa( $conexao, $array[0]['id'] );
						$i=1;
						$ids = array();

						if ( empty($filiais)){
							echo 'Empresa sem Filiais';								
						} else{?>
					<table width="100%" align="center">
						<tbody>
							<tr>
								<td>
									<h6>COD</h6>
								</td>
								<td>
									<h6>Razão Social </h6>
								</td>
								<td>
									<h6>Endereço</h6>
								</td>
							</tr>

							<?php foreach($filiais as $filiais){ 
										$ids[$i] = $filiais['id']; 
										$i++; ?>
							<tr>
								<td><?= $filiais['id'] ?></td>									
								<td><?= $filiais['nome'] ?></td>									
								<td><?php if( empty( $filiais['endereco'] ) ){echo "-";} else{ echo $filiais['endereco'];} ?></td>
							</tr>
							<?php } ?>

						</tbody>
					</table>							
						<?php } $qtds = count($ids); ?>
						<p align="right">Quantidade de Filiais Cadastradas: <?= $qtds?></p>

				</div>

			</div>
			
			<div class="row PrintQuebraProx">

				<div class="col s12 m12 l12">

					<h4>Setores: </h4>

						<?php 
						//setores
						$setores = listarSetorEmpresaBD( $conexao, $array[0]['id'] );
						$i=1;
						$ids = array();

						if ( empty($setores)){
							echo 'Empresa sem Setor';								
						} else{?>
					<table width="100%" align="center">
						<tbody>
							<tr>
								<td>
									<h6>Setor</h6>
								</td>
								<td>
									<h6>Grupo </h6>
								</td>
								<td>
									<h6>Andar</h6>
								</td>
							</tr>

							<?php foreach($setores as $setores){ 
										$ids[$i] = $setores['id']; 
										$i++; ?>
							<tr>
								<td><?php if( empty($setores['setor'] ) ){echo "-";} else{ echo $setores['setor'];} ?></td>									
								<td><?php if( empty($setores['grupo'] ) ){echo "-";} else{ echo $setores['grupo'];} ?></td>									
								<td><?php if( empty($setores['andar'] ) ){echo "-";} else{ echo $setores['andar'];} ?></td>
							</tr>
							<?php } ?>

						</tbody>
					</table>							
						<?php } $qtds = count($ids); ?>
						<p align="right">Quantidade de Setores Cadastrados: <?= $qtds?></p>

				</div>

			</div>

			<div class="row ">
			
			<?php $adesao = listarAdesao($conexao, $_SESSION[ "acao" ][ 'postemp' ], $_SESSION[ "filtro" ][ 'data1' ], $data2); 
			
				$qtd_adesao = count($adesao);			
				
				$aderiu_adesao = 0;
				$nao_adesao = 0;

				for($i=0; $i != $qtd_adesao; $i++){
					if($adesao[$i]['adesao'] === '1' ){ $aderiu_adesao++;}
					else{ $nao_adesao++;}
				} 
				
				?>
			
				<!--GRÁFICO ADESAO -->
				<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
				<script type="text/javascript">
				google.charts.load( "current", {
					packages: [ "corechart" ]
				} );
				google.charts.setOnLoadCallback( drawChart );

				function drawChart() {
					var data = google.visualization.arrayToDataTable( [
						[ 'Aulas', 'Quantidades de Aulas' ],
						[ 'Adesão', <?=$aderiu_adesao?>  ],									
						[ 'Sem Adesão ', <?=$nao_adesao?> ],				
					] );

					var options = {
						title: '',
						//width: '100%',
						//height: '50vh',
						pieHole: 0.4,
						backgroundColor: 'transparent',
						titleTextStyle: {
							color: '#FFFFFF',
							fontName: 'Futura Bold Condensed BT',
							fontSize: '24'
						},
						chartArea: {
							width: '100%',
							height: '75%',
							left: 0,
							backgroundColor: '#FFFFFF'
						},
						legend: {
							textStyle: {
								color: '#cacaca',
								fontName: 'Roboto'
							}
						},
						pieSliceTextStyle: {
							fontName: 'Roboto'
						},
						is3D: true,


					};

					var chart = new google.visualization.PieChart( document.getElementById( 'adesao' ) );
					chart.draw( data, options );

					function resize() {
						// change dimensions if necessary
						chart.draw( data, options );
					}
					if ( window.addEventListener ) {
						window.addEventListener( 'resize', resize );
						console.log( "resize!" );
					} else {
						window.attachEvent( 'onresize', resize );
					}


				}
				</script>
				<!--FIM GRÁFICO ADESAO -->

				<div class="col s12 m12 l12">

					<h4>Taxa de Adesão</h4>					

			<?php if($qtd_adesao == 0){
						echo ' <h5 align="center"> Sem Taxa de Adesão </h5> '; 
					} 
					if($qtd_adesao != 0) {
						echo '<div id="adesao" style="width: 100%; height: 50vh;"></div>';
					} ?>
					<p align="right">Total de Adesão: <?= $qtd_adesao ?></p>

				</div>

			</div>
			
			<div class="row noprint">
				<div class="col s12">
					<p align="right"  onClick="window.print();"> <i class="material-icons">local_printshop</i> Imprimir	</p>
				</div>
			</div>
			
			<div class="row noprint">
				<div class="col s12">
					<p align="center"><a href="dashboard.php" class="waves-effect waves-light btn">Voltar</a>
					</p>
				</div>
			</div>
		</div>
		<?php	
		}
	}
	
	if ( $_SESSION[ "acao" ][ 'acao' ] === 'tipo' ){
		//escolher o relatorio
		?>
		<br>
		<div class="container">
			<div class="row">
				<div class="col s12 m12 l6">
					<h2>Tipos de Relatórios </h2>
				</div>
			</div>
			<div class="row">
				<div class="col s12 m6">
					<p align="center "><a href="relatorio.php?acao=adm" class="waves-effect waves-light  white-text">Basico</a>
					</p>
				</div>
				<div class="col s12 m6">
					<p align="center"><a href="relatorio.php?acao=ginatica" class="waves-effect waves-light  white-text">Adesão</a>
					</p>
				</div>
			</div>
		</div>
	<?php }
}

if ( !empty( $_GET[ 'acao' ] ) ) {
	
	echo $_SESSION[ "acao" ][ 'acao' ] = $_GET[ 'acao' ];
	echo $_SESSION[ "acao" ][ 'id' ] = $_GET[ 'token' ];
	echo $_SESSION[ "acao" ][ 'tipo' ] = $_GET[ 'tipo' ];	
	echo $_SESSION[ "acao" ][ 'postemp' ] = $_POST['empresa'];
	echo $_SESSION[ "filtro" ][ 'data1' ] = $_POST['data1'];
	echo $_SESSION[ "filtro" ][ 'data2' ] = $_POST['data2'];

	echo '<script>window.location.replace("relatorio.php");</script>';
}

include_once( 'rodape.php' );