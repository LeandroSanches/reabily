<?php include_once( 'cabecalho.php' );

session_start();
unset( $_SESSION[ 'usuario' ] );


?>

<br>

<div class="container">
	<form action="controller/trabalhe_conosco.php" method="post" enctype="multipart/form-data">
		
		<?php if($_SESSION[ 'obrigado' ] === 'ok') { ?>
		<div class="row">
			<h2 align="center">Obrigado Pelo Interesse, Estaremos Analizando seu Curriculo</h2>
		</div>
		<?php unset( $_SESSION[ 'obrigado' ] );
		} else{ ?>

		<div class="row">
			<h2>Banco de Currículos</h2>
		</div>

		<div class="row">
			<div class="input-field col s12 m6">
				<i class="material-icons prefix">assignment_ind</i>
				<input id="Nome" type="text" class="validate" name="nome" value="<?= $usuario[0][" nome "]?>">
				<label for="Nome">Nome</label>
			</div>
			<div class="input-field col s12 m6">
				<i class="material-icons prefix">assignment_ind</i>
				<input id="Apelido" type="text" class="validate" name="apelido" value="<?= $usuario[0][" apelido "]?>">
				<label for="Apelido">Sobrenome</label>
			</div>
			<div class="input-field col s12 m6">
				<i class="material-icons prefix">local_phone</i>
				<input id="icon_telephone" type="tel" class="validate telefone sp_celphones" name="telefone" data-mask="(00) 000-000-000" maxlength="15" value="<?= $usuario[0][" telefone "]?>">
				<label for="icon_telephone">Telefone</label>
			</div>
			<div class="input-field col s12 m6">
				<i class="material-icons prefix">email</i>
				<input id="email" type="email" class="validate" name="email" value="<?= $usuario[0][" email "]?>">
				<label for="email">Email</label>
			</div>
		</div>

		<div class="row">
			<div class="input-field col s12 m3">
				<input id="cep" type="text" class="validate" name="cep" data-mask="00000-000" value="<?= $usuario[0][" cep "]?>">
				<label for="cep"> CEP </label>
			</div>

			<div class="input-field  col s12 m3">
				<label for="estado"> Estado </label>
				<input id="estado" type="text" class="validate blue-text text-lighten-3" name="estado" value="<?= $usuario[0][" estado "]?>" maxlength="3">
			</div>

			<div class="input-field col s12 m3">
				<label for="cidade"> Cidade </label>
				<input id="cidade" type="text" class="validate blue-text text-lighten-3" name="cidade" value="<?= $usuario[0][" cidade "]?>">
			</div>

			<div class="input-field col s12 m3">
				<label for="bairro"> Bairro </label>
				<input id="bairro" type="text" class="validate blue-text text-lighten-3" name="bairro" value="<?= $usuario[0][" bairro "]?>">
			</div>

			<div class="input-field col m10">
				<label for="endereco" style="margin-bottom: 3px;"> Endereco </label>
				<input id="endereco" type="text" class="validate blue-text text-lighten-3" name="endereco" value="<?= $usuario[0][" endereco "]?>">
			</div>

			<div class="input-field col m2">
				<input id="numero" type="number" class="validate" name="numero" value="<?= $usuario[0][" numero "]?>">
				<label for="numero"> Número </label>
			</div>

			<div class="input-field col m12">
				<input id="complemento" type="text" class="validate" name="complemento" value="<?= $usuario[0][" complemento "]?>">
				<label for="complemento"> Complemento </label>
			</div>

		</div>

		<div class="row">
			<div class="input-field col s12 m6">
				<textarea id="textarea1" class="materialize-textarea" name="formacao"></textarea>
				<label for="textarea1">Formação</label>
			</div>

			<div class="input-field col s12 m6">
				<textarea id="textarea1" class="materialize-textarea" name="qualificacoes"></textarea>
				<label for="textarea1">Qualificações</label>
			</div>
		</div>

		<div class="row">
			<h4>Deseja atuar nas atividades:</h4>

			<?php include_once( 'model/atividade.php' );
			$atividades = listarAtividadesBD($conexao); 
			$atividades_qtds = count($atividades);
			$i = 1;
			foreach($atividades as $atividades)	{?>
			<div class="col s12 m3">
				<input class="hide"  type="number" name="atividadesid<?=$i?>" value="<?=$atividades['id']?>">
				<label>
					<input type="checkbox" name="atividade<?=$i?>" />
					<span><?= $atividades['nome'] ?></span>
				  </label>
			</div>
			<?php $i++;  } ?>
			
			<input class="hide"  type="number" name="atividades_qtds" value="<?= $atividades_qtds?>">

			<div class="row">
				<div class="col offset-s6 s6">
					<button class="btn waves-effect waves-light right" type="submit">Cadastrar</button>
				</div>
			</div>
		</div>
		<?php } ?>
	</form>
	</div>

	<?php include_once('rodape.php'); ?>