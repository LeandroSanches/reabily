<footer class="page-footer">
	<div class="footer-copyright">
		<div class="container" align="center">
			<?php if(!empty($_SESSION[ 'usuario' ]['apelido'])){
					echo 'Olá '. $_SESSION[ 'usuario' ]['apelido']. " / ";} ?>
			Desenvolvido por: <a class="Rodape" href="http://www.vlannetwork.com/" target="_blank">VLAN</a> 2019 © copyright. <br>
			<?php $time = microtime(true) - $_SERVER["REQUEST_TIME"];	$time = number_format($time, 2, ',', ' ');	echo "<small> $time s </small>"; ?></div>
	</div>
</footer>
</body>
<script src="https://code.jquery.com/jquery-3.3.1.js" integrity="sha256-2Kok7MbOyxpgUVvAk/HJ2jigOSYS2auK4Pfzbm7uH60=" crossorigin="anonymous"></script>
<script type="text/javascript" src="js/materialize.min.js"></script>
<script type="text/javascript" src="js/jquery.mask.min.js"></script>
 <script src="https://igorescobar.github.io/jQuery-Mask-Plugin/js/jquery.mask.min.js"></script>   
  <script>
	$( document ).ready( function () {
		$( '.msg' ).fadeIn( 600 );
		$( '.msg' ).delay( 2000 ).fadeOut( 600 );
		$('select').formSelect();
		$('.modal').modal();
		$('.sidenav').sidenav();
  		$('.dropdown-trigger').dropdown();
		$('.tooltipped').tooltip();
		$('.chips').chips();
		$('.collapsible').collapsible();
		$('.carousel').carousel();
		
		$('.txt').mask('SSSSSSSSSSSSSSSSSSSSSSSSS');
		$(".time").mask("AB:CD", {
				translation: {
				  "A": { pattern: /[0-2]/, optional: false},
				  "B": { pattern: /[0-9]/, optional: false},
				  "C": { pattern: /[0-5]/, optional: false},
				  "D": { pattern: /[0-9]/, optional: false}
				}
			});
		$('.money').mask('000.000.000.000.000,00', {reverse: true});
		
	} );
	  M.textareaAutoResize($('#textarea1'));
</script>
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-139218360-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-139218360-1');
</script>
<!-- Global site tag (gtag.js) - Google Analytics -->

<!--Pesquisa de CEP-->
<script type="text/javascript" >
jQuery(function($){
   $("#cep").change(function(){
      var cep_code = $(this).val();
      if( cep_code.length <= 0 ) return;
      $.get("http://apps.widenet.com.br/busca-cep/api/cep.json", { code: cep_code },
         function(result){
            if( result.status!=1 ){
               alert(result.message || "Houve um erro desconhecido");
               return;
            }
            $("input#cep").val( result.code );
            $("input#uf").val( result.state );
            $("input#cidade").val( result.city );
            $("input#bairro").val( result.district );
            $("input#endereco").val( result.address );
         });
   });
});
</script>
<!--Pesquisa de CEP-->

</html>