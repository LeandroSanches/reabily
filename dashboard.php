<?php
include_once( 'cabecalho.php' );
include_once( 'model/frase.php' );

$frase = buscarfrases( $conexao );
$rand_frase = array_rand( $frase, 1 );

switch ( $_SESSION[ 'usuario' ][ 'id_tipo' ] ) {
	case 1:
		unset( $_SESSION[ "acao" ] );
		//echo "Vc é um Administrador";
		?>
		<div class="container">

			<div class="row">
				<div class="col s12 m12 l12">
					<h2>Dashboard</h2>
					<p align="center">
						<em>
							<?= $frase[$rand_frase]['frase'];?>
						</em>
					</p>
				</div>
			</div>

			<div class="row">

				<!--AGENDA-->
				<div class="col s12 m6 xl4">

					<div class="card grey lighten-5">

						<div class="card-content center-align">

							<span class="card-title white-text"><i class="large material-icons">date_range</i><br> <h4>Agenda</h4></span>

							<p class="white-text">Agenda das Atividades</p>

						</div>

						<div class="card-action center-align">

							<div class="col s12">
								<a href="agenda.php?acao=listarMatriz" class="btn3 waves-effect waves-light">Consultar</a>
								<!--<a href="agenda-calendario.php?voltar=1" class="btn3 waves-effect waves-light">Consultar</a>-->
							</div>

							<div class="col s12">
								<a href="agenda.php?acao=listar" class="btn3 waves-effect waves-light">Cadastrar</a>
							</div>

						</div>

					</div>
				</div>

				<!--ATIVIDADES-->
				<div class="col s12 m6 xl4">

					<div class="card grey lighten-5">

						<div class="card-content center-align">

							<span class="card-title white-text"><i class="large material-icons">directions_run</i><br> <h4>Atividades</h4></span>

							<p class="white-text">Atividades Oferecidas</p>

						</div>

						<div class="card-action center-align">

							<div class="col s12">
								<a href="atividade.php?acao=listar" class="btn3 waves-effect waves-light">Consultar</a>
							</div>

							<div class="col s12">
								<a href="atividade.php?acao=novo" class="btn3 waves-effect waves-light">Cadastrar</a>
							</div>

						</div>

					</div>
				</div>
				
				<!--AULAS-->
				<div class="col s12 m6 xl4">

					<div class="card grey lighten-5">

						<div class="card-content center-align">

							<span class="card-title white-text"><i class="large material-icons">class</i><br> <h4>Aulas</h4></span>

							<p class="white-text">Aulas Oferecidas</p>

						</div>

						<div class="card-action center-align">

							<div class="col s12">
								<a href="aula.php?acao=listar" class="btn3 waves-effect waves-light">Consultar</a>
							</div>

							<div class="col s12">
								<a href="aula.php?acao=novo" class="btn3 waves-effect waves-light">Cadastrar</a>
							</div>

						</div>

					</div>
				</div>

				<!--AVALIAÇÕES--
						<div class="col s12 m6 xl4">

							  <div class="card grey lighten-5">

								<div class="card-content center-align">

								  <span class="card-title white-text"><i class="large material-icons">thumbs_up_down</i><br> <h4>Avaliações</h4></span>

								  <p class="white-text">Avaliações entregues</p>

								</div>

							   <div class="card-action center-align">

										<div class="col s12">
											<a href="" class="btn3 waves-effect waves-light">Consultar</a>
										</div>

										<div class="col s12">
											<a href="" class="btn3 waves-effect waves-light">Cadastrar</a>
										</div>

								</div>

							  </div>
						</div>-->

				<!--CLIENTES-->
				<div class="col s12 m6 xl4">

					<div class="card grey lighten-5">

						<div class="card-content center-align">

							<span class="card-title white-text"><i class="large material-icons">people</i><br> <h4>Clientes</h4></span>

							<p class="white-text">Pacientes</p>

						</div>

						<div class="card-action center-align">

							<div class="col s12">
								<a href="listar.php?tipo=clientes" class="btn3 waves-effect waves-light">Consultar</a>
							</div>

							<div class="col s12">
								<a href="usuario.php?acao=novo&tipo=cliente" class="btn3 waves-effect waves-light">Cadastrar</a>
							</div>

						</div>

					</div>
				</div>

				<!--CONTRATOS-->
				<div class="col s12 m6 xl4">

					<div class="card grey lighten-5">

						<div class="card-content center-align">

							<span class="card-title white-text"><i class="large material-icons">description</i><br> <h4>Contratos</h4></span>

							<p class="white-text">Contratos Firmados</p>

						</div>

						<div class="card-action center-align">

							<div class="col s12">
								<a href="contrato.php?acao=listar" class="btn3 waves-effect waves-light">Consultar</a>
							</div>

							<div class="col s12">
								<a href="contrato.php?acao=novo" class="btn3 waves-effect waves-light">Cadastrar</a>
							</div>

						</div>

					</div>
				</div>

				<!--CRONOGRAMA-->
				<div class="col s12 m6 xl4">

				  <div class="card grey lighten-5">

					<div class="card-content center-align">

					  <span class="card-title white-text"><i class="large material-icons">schedule</i><br> <h4>Cronograma</h4></span>

					  <p class="white-text">Veja sua agenda</p>

						</div>

					<div class="card-action center-align">

							<div class="col s12">
								<a href="cronograma.php?acao=listar" class="btn3 waves-effect waves-light">Consultar</a>
							</div>

							<div class="col s12">
								<a href="cronograma.php?acao=novo" class="btn3 waves-effect waves-light">Cadastrar</a>
							</div>

					</div>

				  </div>
				</div>

				<!--EMPRESAS-->
				<div class="col s12 m6 xl4">

					<div class="card grey lighten-5">

						<div class="card-content center-align">

							<span class="card-title white-text"><i class="large material-icons">business</i><br> <h4>Empresas</h4></span>

							<p class="white-text">Empresas Cadastradas</p>

						</div>

						<div class="card-action center-align">

							<div class="col s12">
								<a href="empresa.php?acao=listar&tipo=sede" class="btn3 waves-effect waves-light">Consultar</a>
							</div>

							<div class="col s12">
								<a href="empresa.php?acao=novo" class="btn3 waves-effect waves-light">Cadastrar</a>
							</div>	

						</div>

					</div>
				</div>

				<!--FRASES-->
				<div class="col s12 m6 xl4">

					<div class="card grey lighten-5">

						<div class="card-content center-align">

							<span class="card-title white-text"><i class="large material-icons">?</i><br> <h4>Frase</h4></span>

							<p class="white-text">Frases motivacionais<br>
							</p>

						</div>

						<div class="card-action center-align">

							<div class="col s12">
								<a href="frase.php?acao=listar" class="btn3 waves-effect waves-light">Consultar</a>
							</div>

							<div class="col s12">
								<a href="frase.php?acao=novo" class="btn3 waves-effect waves-light">Cadastrar</a>
							</div>

						</div>

					</div>
				</div>

				<!--Funcionários-->
				<div class="col s12 m6 xl4">

					<div class="card grey lighten-5">

						<div class="card-content center-align">

							<span class="card-title white-text"><i class="large material-icons">build</i><br> <h4>Internos</h4></span>

							<p class="white-text">Funcionários Internos<br>
							</p>

						</div>

						<div class="card-action center-align">

							<div class="col s12">
								<a href="listar.php?tipo=administradores" class="btn3 waves-effect waves-light">Consultar</a>
							</div>

							<div class="col s12">
								<a href="usuario.php?acao=novo&tipo=administrador" class="btn3 waves-effect waves-light">Cadastrar</a>
							</div>

						</div>

					</div>
				</div>

				<!--FORNECEDORES-->
				<div class="col s12 m6 xl4">

					<div class="card grey lighten-5">

						<div class="card-content center-align">

							<span class="card-title white-text"><i class="large material-icons">work</i><br> <h4>Fornecedores</h4></span>

							<p class="white-text">Fornecedores</p>

						</div>

						<div class="card-action center-align">

							<div class="col s12">
								<a href="fornecedor.php?acao=listar" class="btn3 waves-effect waves-light">Consultar</a>
							</div>

							<div class="col s12">
								<a href="fornecedor.php?acao=novo" class="btn3 waves-effect waves-light">Cadastrar</a>
							</div>

						</div>

					</div>
				</div>				

				<!--Mala Direta-->
				<div class="col s12 m6 xl4">

					<div class="card grey lighten-5">

						<div class="card-content center-align">

							<span class="card-title white-text"><i class="large material-icons">contact_mail</i><br> <h4>Mala Direta</h4></span>

							<p class="white-text">E-mails de Colaboradores</p>

						</div>

						<div class="card-action center-align">

							<div class="col s12">
								<a href="mala.php?acao=escolher" class="btn3 waves-effect waves-light">Consultar</a>
							</div>
							<div class="col s12">
								<a href="" class="btn3 waves-effect waves-light " style="opacity: 0;"> opacity: 0; </a>
							</div>

						</div>

					</div>
				</div>

				<!--Materiais-->
				<div class="col s12 m6 xl4">

					<div class="card grey lighten-5">

						<div class="card-content center-align">

							<span class="card-title white-text"><i class="large material-icons">shopping_cart</i><br> <h4>Materiais</h4></span>

							<p class="white-text">Lista de Materiais Utilizados</p>

						</div>

						<div class="card-action center-align">

							<div class="col s12">
								<a href="material.php?acao=listar" class="btn3 waves-effect waves-light">Consultar</a>
							</div>

							<div class="col s12">
								<a href="material.php?acao=novo" class="btn3 waves-effect waves-light">Cadastrar</a>
							</div>

						</div>

					</div>
				</div>

				<!--PRESENÇA-->
				<div class="col s12 m6 xl4">

					<div class="card grey lighten-5">

						<div class="card-content center-align">

							<span class="card-title white-text"><i class="large material-icons">assignment_turned_in</i><br> <h4>Presença</h4></span>

							<p class="white-text">Alterar Presença</p>

						</div>

						<div class="card-action center-align">

							<div class="col s12">
								<a href="presenca.php?acao=lista" class="btn3 waves-effect waves-light">Consultar</a>
							</div>
							<div class="col s12">
								<a href="" class="btn3 waves-effect waves-light " style="opacity: 0;"> opacity: 0; </a>
							</div>

						</div>

					</div>
				</div>
				
				<!--Profissionais-->
				<div class="col s12 m6 xl4">

					<div class="card grey lighten-5">

						<div class="card-content center-align">

							<span class="card-title white-text"><i class="large material-icons">assignment_ind</i><br> <h4>Profissionais</h4></span>

							<p class="white-text">Funcionários Reabily</p>

						</div>

						<div class="card-action center-align">

							<div class="col s12">
								<a href="listar.php?tipo=funcionarios" class="btn3 waves-effect waves-light">Consultar</a>
							</div>

							<div class="col s12">
								<a href="usuario.php?acao=novo&tipo=funcionarios" class="btn3 waves-effect waves-light">Cadastrar</a>
							</div>

						</div>

					</div>
				</div>

				<!--QUESTIONÁRIO-->
				<div class="col s12 m6 xl4">

					<div class="card grey lighten-5">

						<div class="card-content center-align">

							<span class="card-title white-text white-text"><i class="large material-icons">create</i><br> <h4>Questionário</h4></span>

							<p class="white-text">Paciente e Profisional</p>

						</div>

						<div class="card-action center-align">

							<div class="col s12">
								<a href="questionario.php" class="btn3 waves-effect waves-light">Consultar</a>
							</div>
							<div class="col s12">
								<a href="" class="btn3 waves-effect waves-light " style="opacity: 0;"> opacity: 0; </a>
							</div>

						</div>

					</div>

				</div>

				<!--RELATÓRIOS-->
				<div class="col s12 m6 xl4">

					<div class="card grey lighten-5">

						<div class="card-content center-align">

							<span class="card-title white-text"><i class="large material-icons">library_books</i><br> <h4>Relatórios</h4></span>

							<p class="white-text">Relatórios</p>

						</div>

						<div class="card-action center-align">

							<div class="col s12">
								<a href="relatorio.php?acao=tipo" class="btn3 waves-effect waves-light">Consultar</a>
							</div>

							<div class="col s12">
								<a href="" class="btn3 waves-effect waves-light " style="opacity: 0;"> opacity: 0; </a>
							</div>

						</div>

					</div>

				</div>

				<!--SETOR-->
				<div class="col s12 m6 xl4">

					<div class="card grey lighten-5">

						<div class="card-content center-align">

							<span class="card-title white-text"><i class="large material-icons">tab_unselected</i><br> <h4>Setor</h4></span>

							<p class="white-text">Setores Cadastrados</p>

						</div>

						<div class="card-action center-align">

							<div class="col s12">
								<a href="setor.php?acao=listar" class="btn3 waves-effect waves-light">Consultar</a>
							</div>

							<div class="col s12">
								<a href="setor.php?acao=novo" class="btn3 waves-effect waves-light">Cadastrar</a>
							</div>

						</div>

					</div>

				</div>

			</div>
			
			
		</div>
		<?php break;
	case 2:
		//echo "Vc é um Funcionario";
		?>

		<div class="container">

			<div class="row">
				<div class="col s12 m12 l12">
					<h2>Dashboard</h2>
					<p align="center">
						<em>
							<?= $frase[$rand_frase]['frase'];?>
						</em>
					</p>
				</div>
			</div>


			<div class="row">

				<div class="col s12 m6 xl4 Top">
					<div class="card grey lighten-5">

						<div class="card-content center-align">

							<span class="card-title white-text"><i class="large material-icons">date_range</i><br> <h4>Agenda</h4></span>

							<p class="white-text">Agenda das Atividades<br></p>

						</div>

						<div class="card-action center-align">

							<div class="col s12">
								<a href="agenda.php?acao=agendados" class="btn2 waves-effect waves-light">Agendados</a>
							</div>
						</div>

					</div>
				</div>
				
			</div>
		</div>

		<?php break;

	case 3:
		//echo "Você é uma Empresas";?>

		<div class="container">

			<div class="row">
				<div class="col s12 m12 l12">
					<h2>Dashboard</h2>
					<p align="center">
						<em>
							<?= $frase[$rand_frase]['frase'];?>
						</em>
					</p>
				</div>
			</div>


			<div class="row">
				<!--RELATÓRIOS-->
				<div class="col s12 m6 xl4 Top">

					<div class="card grey lighten-5">

						<div class="card-content center-align">

							<span class="card-title white-text"><i class="large material-icons">library_books</i><br> <h4>Relatórios</h4></span>

							<p class="white-text">Relatórios</p>

						</div>

						<div class="card-action center-align">

							<div class="col s12 offset-l3">
								<a href="relatorio.php?acao=escolher" class="btn3 waves-effect waves-light">Consultar</a>
							</div>
						</div>

					</div>
				</div>
			</div>
		</div>
		<?php break;

	case 4:
		//echo "Você é um Cliente";?>

		<div class="container">

			<div class="row">
				<div class="col s12 m12 l12">
					<h2>Dashboard</h2>
					<p align="center">
						<em>
							<?= $frase[$rand_frase]['frase'];?>
						</em>
					</p>
				</div>
			</div>


			<div class="row">


				<div class="col s12 m6 xl4 Top">

					<a href="" title="Agenda">

						<div class="card grey lighten-5">

							<div class="card-content center-align">

								<span class="card-title white-text"><i class="large material-icons">date_range</i><br> <h4>Agenda</h4></span>

								<p class="white-text">Agenda das Atividades</p>

							</div>

							<div class="card-action center-align">

								<div class="col s12">
									<a href="agenda.php?acao=agendados" class="btn3 waves-effect waves-light">Agendados</a>
								</div>

								<div class="col s12">
									<!--<a href="agenda.php?acao=disponiveis" class="btn3 waves-effect waves-light">Disponiveis</a>-->
									<a href="agenda-calendario.php?voltar=1" class="btn3 waves-effect waves-light">Consultar</a>
								</div>

							</div>

						</div>

					</a>

				</div>
			</div>
		</div>
		<?php
}

if ( empty( $_SESSION[ 'usuario' ] ) ) {
	echo "Sem usuario";
	echo '<script>window.location.replace("controller/logout.php");</script>';
}

include_once( 'rodape.php' );