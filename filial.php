<?php
include_once( 'cabecalho.php' );
include_once( 'model/usuario.php' );

session_start();

if ( isset( $_SESSION[ "acao" ][ 'acao' ] ) ) {

	if ( $_SESSION[ "acao" ][ 'acao' ] === 'escolher' ) {
		
		$empresas = listarSedeEmpresasBD( $conexao );

		$antPasso = 'usuario.php?acao=novo&tipo=empresa';
		$msgBotao = 'Continuar';
		
		$passo2 = 'true';
		?>
		<div class="container">
		
			<div class="row">
				<form class="col s12" action="filial.php?acao=novo" method="post" enctype="multipart/form-data">
					<div class="row">
						<div class="col s12 m12 l12">
							<h2>Cadastro de Filiais</h2>
							<div class="row">
								<div class="col s12 m12 l12 center-align PassoAtiv">

									<span style="padding-right: 8px;"><i class="material-icons medium">looks_one</i></span> 
									<?php if($passo2 === 'true'){?>
									<span style="padding:0 8px 0 8px;" class="PassoDes"><i class="material-icons medium">looks_two</i></span> 
									<?php } ?>
									<?php if($passo3 === 'true'){?>
									<span style="padding-left: 8px;" class="PassoDes"><i class="material-icons medium">looks_3</i></span>
									<?php } ?>
								</div>					
							</div>
							<p>Escolha a empresa sede para a filial.</p>
						</div>
					</div>
					<div class="input-field col s12 ">
						<select name="nomeEmpresa">
							<option value="" disabled selected> </option>
							<?php foreach ( $empresas as $empresas ) { ?>
							<option value="<?= $empresas['id'] ?>"><?= $empresas['sede'] ?> - COD.:<?= $empresas['id'] ?></option>';
							<?php	}?>
						</select>
						<label>Empresas</label>
					</div>
					<div class="row">
						<div class="col s6 m3 offset-m1" align="center">
							<a href="<?=$antPasso?>" class="btn waves-effect waves-light">Voltar</a> </div>
						<div class="col s6 m6">
							<button class="btn2 waves-effect waves-light right" type="submit">
								<?= $msgBotao?>
							</button>
						</div>
					</div>
				</form>
			</div>
		</div>
		<?php
	}

	if ( $_SESSION[ "acao" ][ 'acao' ] === 'novo' ) {
		
		$_SESSION[ "acao" ][ 'acao' ] = 'novo';
		
		$passo2 = 'true';

		$antPasso = 'filial.php?acao=escolher';
		$msgBotao = 'cadastrar';
		$disablednivel = 'disabled';		
		$cnpj = 'true';
			
		//$empresa =  listarUsuarioBD( $conexao, $_SESSION[ "acao" ][ 'id' ] );
		$empresa =  listarSedeEmpresasIdBD( $conexao, $_SESSION[ "acao" ][ 'id' ] );
		$nivel = listarTiposBD( $conexao );
		?>
		<div class="container">
			<form class="col s12" action="controller/filial.php" method="post" enctype="multipart/form-data">
				<div class="row">
					<h2>Cadastro de Filiais</h2>
					<div class="col s12 m12 l12 center-align PassoAtiv">

						<span style="padding-right: 8px;"class="PassoDes"><i class="material-icons medium">looks_one</i></span> 
						<?php if($passo2 === 'true'){?>
						<span style="padding:0 8px 0 8px;" ><i class="material-icons medium">looks_two</i></span> 
						<?php } ?>
						<?php if($passo3 === 'true'){?>
						<span style="padding-left: 8px;" class="PassoDes"><i class="material-icons medium">looks_3</i></span>
						<?php } ?>
					</div>					
				</div>
				<div class="row">
				<h4>Informação da Sede</h4>
				
					<!--<div class="input-field col s12 m6">
						<input id="Nome" type="text" class="" value="<?= $empresa[0]["nome"]?>"  <?=$disablednivel?>>
						<label for="Nome">Razão Social</label>

					</div>
					<div class="input-field col s12 m6">
						<input id="Apelido" type="text" class="" value="<?= $empresa[0]["apelido"]?>"  <?=$disablednivel?>>
						<label for="Apelido">Nome Fantasia</label>
					</div>	

					<div class="input-field col s12 m4">
						<input id="cep" type="text" class="" value="<?= $empresa[0]["cep"]?>" <?=$disablednivel?>>
						<label for="cep">CEP</label>
					</div>	
					
					<div class="input-field col s12 m4">
						<i class="material-icons prefix">credit_card</i>
						<input id="cnpj" type="text" class="" data-mask="00.000.000/0000-00" value="<?= $empresa[0]["cpf_cnpj"]?>" <?=$disablednivel?>>
						<label for="cnpj">CNPJ</label>
					</div>-->
						
					<div class="input-field col s12 m6">
						<input id="Nome" type="text" class="" value="<?= $empresa[0]["sede"]?>" <?=$disablednivel?>>
						<label for="Nome">Sede:</label>
					</div>
					
					<div class="input-field col s12 m1">
						<input id="cod" type="text" value="<?=$empresa[0]["id"]?>" <?=$disablednivel?>>
						<input class="hide" type="number" name="id_empresa" value="<?=$empresa[0]["id"]?>">
						<label for="cod">COD.: </label>
					</div>
						
				</div>
				<div class="row">
					<h4>Informação da Filial</h4>				
				</div>	
			<div class="row">
			<?php if ( isset($filial[0]['id'] )) {
				echo '<input class="hide" type="text" name="id" value="'.$filial[0]["id"].'">';
			} ?>
				<div class="input-field col s12 m6">
					<input id="Nome" type="text" class="" name="nome" value="<?= $filial[0]["nome"]?>">
					<label for="Nome">Razão Social</label>
					
				</div>
				<div class="input-field col s12 m6">
					<input id="Apelido" type="text" class="" name="apelido" value="<?= $filial[0]["apelido"]?>">
					<label for="Apelido">Nome Fantasia</label>
				</div>
				
				
				<div class="input-field col s12 m3">
					<input id="cnpj" type="text" class="" name="cnpj" data-mask="00.000.000/0000-00" value="<?= $filial[0]["cpf_cnpj"]?>">
					<label for="cnpj">CNPJ</label>
				</div>

				<div class="input-field col s12 m6">
					<input id="email" type="email" class="" name="email" value="<?= $filial[0]["email"]?>">
					<label for="email">Email</label>
				</div>
				
				<div class="input-field col s12 m3">
					<input id="password" type="password" class="" name="senha">
					<label for="password">Senha</label>
				</div>
				
				<div class="input-field col s12 m3">
					<input id="contato" type="text" class="" name="contato" value="<?= $filial[0]["contato"]?>" >
					<label for="contato">Nome do Contato</label>
				</div>
				<div class="input-field col s12 m3">
					<input id="icon_telephone" type="tel" class="telefone sp_celphones" name="telefone" data-mask="(00) 000-000-000" maxlength="15" value="<?= $filial[0]["telefone"]?>">
					<label for="icon_telephone">Telefone</label>
				</div>
				
				<div class="input-field col s12 m3 hide">
					<select name="nivel" <?=$disablednivel?>>
						<option value="" disabled selected><span class="white-text">Escolha uma opção</span></option>
						<?php foreach ( $nivel as $nivel ){
								if ($filial[0]["id_tipo"] == $nivel["id"]){ $selected = 'selected';} 
								else {$selected = '';}
					
								if ( $cnpj === 'true'){
										if ($nivel["id"] === '3' ){ $selected = 'selected';} 
										else {$selected = '';}
									}	?>
						<option value="<?=$nivel["id"];?>" <?=$selected;?>><?=$nivel["nome"];?></option>
						<?php } ?>
					</select>
					<label>Nivel:</label>
				</div>				
				
				<div class="input-field col s12 m3">
					<input id="municipal" type="text" class="" name="municipal" value="<?= $filial[0]["municipal"]?>" data-mask="00000-0" maxlength="6">
					<label for="municipal">Inscrição Municipal</label>
				</div>
				<div class="input-field col s12 m3">
					<input id="estadual" type="text" class="" name="estadual" value="<?= $filial[0]["estadual"]?>" data-mask="000.000.000.000" maxlength="15">
					<label for="estadual">Inscrição Estadual</label>
				</div>
				<div class="input-field col s12">
					 <textarea id="obs" class="materialize-textarea" name="obs" value="<?= $filial[0]["obs"]?>"></textarea>
         			 <label for="obs">Observações</label>
				</div>
				
			</div>
			<div class="row">

				<div class="input-field col s12 m4">
					<input id="cep" type="text" class="" name="cep" data-mask="00000-000" value="<?= $usuario[0]["cep"]?>" >
					<label for="cep"> CEP </label>
				</div>
				<div class="input-field col s12 m4">
					<input id="estado" type="text" class="" name="estado" value="<?= $usuario[0]["estado"]?>" >
					<label for="estado"> Estado </label>
				</div>
				<div class="input-field col s12 m4">
					<input id="cidade" type="text" class="" name="cidade" value="<?= $usuario[0]["cidade"]?>" >
					<label for="cidade"> Cidade </label>
				</div>
				
				<div class="input-field col s12 m4">
					<input id="endereco" type="text" class="" name="endereco" value="<?= $usuario[0]["endereco"]?>" >
					<label for="endereco"> Endereco </label>
				</div>
				<div class="input-field col s12 m4">
					<input id="numero" type="number" class="" name="numero" value="<?= $usuario[0]["numero"]?>">
					<label for="numero"> Número </label>
				</div>
				<div class="input-field col s12 m4">
					<input id="complemento" type="text" class="" name="complemento" value="<?= $usuario[0]["complemento"]?>">
					<label for="complemento"> Complemento </label>
				</div>
				
			</div>
					<div class="row">
						<div class="col s6 m3 offset-m1" align="center">
							<a href="<?=$antPasso?>" class="btn waves-effect waves-light">Voltar</a> </div>
						<div class="col s6 m6">
							<button class="btn2 waves-effect waves-light right" type="submit">
								<?= $msgBotao?>
							</button>
						</div>
					</div>
			</form>
		</div>
		<?php
	}

}

if ( !empty( $_GET[ 'acao' ] ) ) {

	echo $_SESSION[ "acao" ][ 'acao' ] = $_GET[ 'acao' ];
	echo $_SESSION[ "acao" ][ 'id' ] = $_POST[ 'nomeEmpresa' ];
	echo $_SESSION[ "acao" ][ 'tipo' ] = $_GET[ 'tipo' ];
	
	echo '<script>window.location.replace("filial.php");</script>';
}

include_once( 'rodape.php' );
?>