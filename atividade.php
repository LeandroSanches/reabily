<?php
include_once( 'cabecalho.php' );
include_once( 'model/usuario.php' );
include_once( 'model/atividade.php' );

if ( empty( $_GET[ 'acao' ] ) ) {
	?>

<br>
	<div class="container row">
		
		<h2>Atividades</h2>
		
		<div class="col s12 m6" align="center">
			<div class="card grey lighten-5">
				<div class="card-content ">
					<span class="card-title"><a href="atividade.php?acao=listar"><i class="material-icons">list</i> Lista</a></span>
				</div>
			</div>
		</div>
		<div class="col s12 m6" align="center">
			<div class="card grey lighten-5">
				<div class="card-content ">
					<span class="card-title"><a href="atividade.php?acao=novo"><i class="material-icons">create_new_folder</i> Novo </a></span>
				</div>
			</div>
		</div>
	</div>
	
<?php }

if ( $_GET[ 'acao' ] == "novo" ) {

	$msgBotao = 'Nova';
	$_SESSION["acao"]["acao"] =  'inserir';
	
	?>
	<br>
	<div class="container row">
		
		<h2>Cadastrar Atividades</h2>
		
		<div class="row">
			<form class="col s12" action="controller/atividade.php" method="post" accept-charset="UTF-8">
				<div class="row">
					<div class="input-field col s12 m9">
						<i class="material-icons prefix">account_circle</i>
						<input id="nome" type="text" class="validate" name="nome">
						<label for="nome">Nome </label>
					</div>
					
					<div class="input-field col s12 m3">
						<i class="material-icons prefix">watch_later</i>
						<input id="duracao" type="text" class="validate" name="duracao" data-mask="00:00">
						<label for="duracao">Duração <small> Ex.: 00:30 </small></label>
					</div>					
					
					<div class="input-field col s12">
						<i class="material-icons prefix">description</i>
						<input id="descricao" type="text" class="validate" name="descricao">
						<label for="descricao">Descrição </label>
					</div>	
					
					<div class="input-field col s12 m3 offset-m5">
						<p> Disponibilidade: &nbsp;&nbsp;
							<label>
								<input type="checkbox" name="ativo" checked />
								<span>Ativo</span>
							  </label>						
						</p>
					</div>
					
				</div>

				<div class="row">
					<div class="col s6 m3 offset-m1" align="center">
						<a href="dashboard.php" class="btn waves-effect waves-light">Voltar</a> </div>
					<div class="col s6 m6">
						<button class="btn2 waves-effect waves-light right" type="submit">
							<?= $msgBotao?>
						</button>
					</div>
				</div>
			</form>
		</div>
	</div>
	<?php } 
	
if ( $_GET[ 'acao' ] == "listar" ) {

	$atividade = listarAtividadesBD( $conexao );	
	?>
	<br>
	<div class="container row">
		
		<h2>Atividades</h2>
		
		<table>
        <thead>
          <tr>
              <th>Nome</th>
              <th>Descrição</th>
              <th>Duração</th>
              <th></th>
          </tr>
        </thead>

        <tbody>
         <?php foreach ( $atividade as $atividade ) {?>
          <tr>
            <td><?= $atividade[ "nome" ]?></td>
            <td><?= $atividade[ "descricao" ]?></td>
            <td><?= mask($atividade[ "duracao" ], '##:##') ?></td>
            <td><a href="atividade.php?acao=editar&token=<?= $atividade['id']?>"><i class="material-icons white-text">edit</i></a>
            <a class="modal-trigger Link" href="#modal<?=$atividade['id']?>" title="Excluir"><i class="material-icons prefix amber-text text-darken-1 <?= $color?>">cancel</i></a></td>
          </tr>

			<div id="modal<?=$atividade['id']?>" class="modal bottom-sheet">
				<div class="row">
					<div class="col s12 m6">
						<div class="modal-content">
							<h4 class="grey-text text-darken-2">Deseja excluir <?=$atividade['nome']?>?</h4>
							<p class="grey-text text-darken-2">Essa ação não poderá ser desfeita.</p>
							<p class="grey-text text-darken-2"><small>Cod.: <?=$atividade['id']?></small>
							</p>
						</div>
					</div>
					<div class="col s12 m6 Top">
						<div class="modal-footer">
							<a href="atividade.php?acao=listar" class="btn waves-effect waves-light">Voltar</a>
							<a href="controller/atividade.php?acao=excluir&token=<?=$atividade['id']?>" class="modal-close btn2 waves-effect waves-light">Eu aceito</a>
						</div>
					</div>
				</div>
			</div>
         
          <?php } ?>
        </tbody>
        
      </table>
	</div>	

	<div class="row">
		<div class="col s6 m3 offset-m1" align="center">
			<a href="dashboard.php" class="btn3 waves-effect waves-light">Voltar</a> </div>
		<div class="col s6 m6">
			<a href="atividade.php?acao=novo" class="btn waves-effect waves-light right">Cadastrar</a>
		</div>
	</div>
	<?php }
	
if ( $_GET[ 'acao' ] == "editar" ) {
	$contrato = listarAtividadeBD( $conexao , $_GET['token']);
	
	$_SESSION["acao"]["acao"] =  'alterar';
	$_SESSION["acao"]['id'] =  $contrato[0]['id'];
	
	
	
		if($contrato[0]["ativo"] === '0'){
			$ativo = "checked";
		}
		if($contrato[0]["ativo"] === '1'){
			$ativo = "";
		}
		
		$msgBotao = 'Alterar';
	
	?>	
	<br>
	<div class="container row">
		
		<h2>Atividade</h2>
		
		<div class="row">
			<form class="col s12" action="controller/atividade.php" method="post" accept-charset="UTF-8">
				<div class="row">
					<div class="input-field col s12 m9">
						<i class="material-icons prefix">account_circle</i>
						<input id="nome" type="text" class="validate" name="nome"  value="<?= $contrato[0]['nome']?>">
						<label for="nome">Nome </label>
					</div>
					
					<div class="input-field col s12 m3">
						<i class="material-icons prefix">watch_later</i>
						<input id="duracao" type="text" class="validate" name="duracao" data-mask="00:00"  value="<?= $contrato[0]['duracao']?>">
						<label for="duracao">Duração <small> Ex.: 00:30 </small> </label>
					</div>					
					
					<div class="input-field col s12">
						<i class="material-icons prefix">description</i>
						<input id="descricao" type="text" class="validate" name="descricao"  value="<?= $contrato[0]['descricao']?>">
						<label for="descricao">Descrição </label>
					</div>	
					
					<div class="input-field col s12 m3 offset-m5">
						<p> Disponibilidade: &nbsp;&nbsp;
							<label>
								<input type="checkbox" name="ativo" <?=$ativo?> />
								<span>Ativo</span>
							  </label>						
						</p>
					</div>
					
				</div>

				<div class="row">
					<div class="col s6 m3 offset-m1" align="center">
						<a href="dashboard.php" class="btn waves-effect waves-light">Voltar</a> </div>
					<div class="col s6 m6">
						<button class="btn2 waves-effect waves-light right" type="submit">
							<?= $msgBotao?>
						</button>
					</div>
				</div>
			</form>
		</div>
	</div>
	
	<?php
}


include_once('rodape.php'); ?>