<?php
include_once( 'cabecalho.php' );
include_once( 'model/empresa.php' );
include_once( 'model/telefone.php' );
include_once( 'model/endereco.php' );

//$_SESSION[ "acao" ][ 'acao' ] 	= 'editar';	//Apagar
//$_SESSION[ "acao" ][ 'tipo' ] 	= 'sede';	//Apagar
//$_SESSION[ "acao" ][ 'id' ]		= 1; 		//Apagar

	
if ( $_SESSION[ "acao" ][ 'acao' ]  === 'novo') {
	
	if ( empty( $_SESSION[ "acao" ][ 'tipo' ] ) ) { ?>
	<br>
	<div class="container">
		<div class="row">
			<h2 align="center">O que deseja Cadastrar?</h2>
			<a href="empresa.php?acao=novo&tipo=sede"><div class="col s3 offset-s1 btn">Matriz</div></a>
			<a href="empresa.php?acao=novo&tipo=filial"><div class="col s3 offset-s4 btn">Filial</div></a>
		</div>
	</div>	
	<?php
	}
	
	if ($_SESSION[ "acao" ][ 'tipo' ] === 'sede'){ ?>
		
		<br>
		<div class="container">
			<form class="col s12" action="controller/empresa.php?acao=novo&tipo=sede" method="post" enctype="multipart/form-data">
			
				<div class="row">
					<div class="col s12 m12 l12">					
						<h2>Cadastro de Matriz</h2>					
					</div>
					
					<div class="row">
						<div class="col s12 m12 l12 center-align PassoAtiv">
							<span style="padding-right: 8px;"><i class="material-icons medium">looks_one</i></span>	
						</div>					
					</div>
					
					<div class="row">
						
						<div class="input-field col s12">
							<h4>Dados da Matriz</h4>	
						</div>	
					
						<div class="input-field col s12 m6">
							<i class="material-icons prefix">assignment_ind</i>
							<input id="Nome" type="text" class="validate" name="nome" autocomplete="off">
							<label for="Nome">Razão Social</label>
						</div>						
						
						<div class="input-field col s12 m6">
							<i class="material-icons prefix">assignment_ind</i>
							<input id="Apelido" type="text" class="validate" name="apelido" autocomplete="off">
							<label for="Apelido">Nome Fantasia</label>
						</div>	
						
						<div class="input-field col s12 m6">
							<i class="material-icons prefix">credit_card</i>
							<input id="cnpj" type="text" class="validate" name="cnpj" data-mask="00.000.000/0000-00">
							<label for="cnpj">CNPJ</label>
						</div>	
						
						<div class="input-field col s12" align="center" >
							<p>Anexar Logo:</p>
							<input type="file" name="arquivo">
						</div>						
						
					</div>	
					
					<div class="row">						
						<div class="input-field col s12">
							<h4>Acesso ao Sistema</h4>	
						</div>
						
						<div class="input-field col s12">
							<i class="material-icons prefix">business</i>
							<input id="sede" type="text" class="validate" name="sede">
							<label for="sede">Nome de Visualização no Sistema (Sede)</label>
						</div>
						
						<div class="input-field col s12 m8">
							<i class="material-icons prefix">email</i>
							<input id="email" type="email" class="validate" name="email">
							<label for="email">Email</label>
						</div>
						
						<div class="input-field col s12 m4">
							<i class="material-icons prefix">fingerprint</i>
							<input id="password" type="password" class="validate" name="senha">
							<label for="password">Senha</label>
						</div>
						
					</div>	
					
					<div class="row">
					
						<div class="input-field col s12">
							<h4>Telefones</h4>	
						</div>
						
						<div class="clone">
						
						<div class="input-field col s12 m6">
							<input id="contato" type="text" class="validate" name="contato[]" >
							<label for="contato">Nome do Contato</label>
						</div>	
						
						<div class="input-field col s12 m3">							
							<input id="icon_telephone" type="tel" class="validate telefone sp_celphones" name="telefone[]" data-mask="(00) 000-000-000" maxlength="15">
							<label for="icon_telephone">Número</label>
						</div>

						<div class="input-field col s12 m3">
							 <input id="icon_telephone" type="tel" class="validate Ramal" name="ramal[]" data-mask="-0000" maxlength="15">
							 <label for="ramal">Ramal</label>
						</div>
						
						</div>
						
						<div class="add"></div>						
						
						
						<p align="center" class="addNumero"> <i class="material-icons prefix">add</i> Adicionar Contato</p>
						
						<script>
							$(document).ready(function() {
								  $(".addNumero").click(function() {
								  var novoItem = $(".clone").clone().removeClass('clone'); 
								  $(".add").append(novoItem);
								});
							  });
						</script>
					</div>
						
					<div class="row">
					
						<div class="input-field col s12">
							<h4>Outras Informações</h4>	
						</div>
						
						<div class="input-field col s12 m4">
							<input id="municipal" type="text" class="validate" name="municipal" data-mask="00000-0" maxlength="6">
							<label for="municipal">Inscrição Municipal</label>
						</div>
						
						<div class="input-field col s12 m4">
							<input id="estadual" type="text" class="validate" name="estadual" data-mask="000.000.000.000" maxlength="15">
							<label for="estadual">Inscrição Estadual</label>
						</div>
						
						<div class="input-field col s12 m4">
							<input id="nfe" type="text" class="validate" name="nfe">
							<label for="nfe"> NFe </label>
						</div>
											
						<div class="input-field col s12">
							 <textarea id="obs" class="materialize-textarea" name="obs"></textarea>
							 <label for="obs">Observações</label>
						</div>
						
					</div>
					
					<div class="row">
					
						<div class="input-field col s12">
							<h4>Endereço</h4>	
						</div>
						
						<div class="input-field col s12 m4">
							<label for="cep"> CEP </label>
							<input id="cep" type="text" class="validate" name="cep" data-mask="00000-000" onblur="pesquisacep(this.value);" size="10" maxlength="9">
						</div>	

						<div class="input-field col s12 m1">
							<label for="uf"> Estado </label>
							<input id="uf" type="text" class="validate blue-text text-lighten-3" name="estado" size="2" >
						</div>

						<div class="input-field col s12 m3">
							<label for="cidade"> Cidade </label>
							<input id="cidade" type="text" class="validate blue-text text-lighten-3" name="cidade" size="40">
						</div>	

						<div class="input-field col s12 m4">
							<label for="bairro"> Bairro </label>
							<input id="bairro" type="text" class="validate blue-text text-lighten-3" name="bairro" size="40">
						</div>						
						
						<div class="input-field col m10">
							<label for="endereco"> Endereco </label>
							<input id="endereco" type="text" class="validate blue-text text-lighten-3" name="endereco" size="60">
						</div>
						
						<div class="input-field col m2">
							<label for="numero"> Número </label>
							<input id="numero" type="number" class="validate" name="numero">
						</div>			

						<div class="input-field col m12">
							<label for="complemento"> Complemento </label>
							<input id="complemento" type="text" class="validate" name="complemento">
						</div>
						
					</div>
					
					<div class="row">
						<div class="col s6 m3 offset-m1" align="center">
							<a href="empresa.php?acao=novo" class="btn waves-effect waves-light">Voltar</a> 
						</div>	
						<div class="col s6 m6">
							<button class="btn2 waves-effect waves-light right" type="submit"> Cadastrar </button>
						</div>		
					</div>	
					
				</div>	
			</form>
		</div>	
	<?php
	}
	
	if ($_SESSION[ "acao" ][ 'tipo' ] === 'filial'){
		
		if ( empty($_SESSION[ "acao" ][ 'id' ] )){
			$matriz = listarSedeEmpresasBD($conexao);
			?>

			<br>
			<div class="container">
				<h4>Escolha uma Matriz</h4>
				<form class="col s12" action="empresa.php" method="get">
					<div class="input-field col s6">
					  <input class="hide" name="acao" value="novo">					
					  <input class="hide" name="tipo" value="filial">
					</div>
					<div class="row">
						<div class="col s12 m12 l12">	
							 <select name="token">
							  <option disabled selected></option>
							  <?php foreach($matriz as $matriz){ ?>
								<option value="<?=$matriz['id']?>"> <?=$matriz['sede']?> </option>
							  <?php } ?>
							</select>
							<label>Matriz</label>				
						</div>
					</div>
					<div class="row">
						<div class="col s6 m3 offset-m1" align="center">
							<a href="empresa.php?acao=novo" class="btn waves-effect waves-light">Voltar</a> 
						</div>	
						<div class="col s6 m6">
							<button class="btn2 waves-effect waves-light right" type="submit"> Cadastrar </button>
						</div>		
					</div>
				</form>
			</div>
		<?php } 
		
		if ( !empty($_SESSION[ "acao" ][ 'id' ] )){	
			$matriz = listarEmpresaBD($conexao, $_SESSION[ "acao" ][ 'id' ], 3);
			$matriz[0]['logo'] = strstr($matriz[0]['logo'],'usuarios/');
			?>

			<div class="container">
				<form class="col s12" action="controller/empresa.php?acao=novo&tipo=filial" method="post" enctype="multipart/form-data">

					<div class="row">
						<div class="col s12 m12 l12">					
							<h2>Cadastro de Filial</h2>					
						</div>

						<div class="row">
							<div class="col s12 m12 l12 center-align PassoAtiv">
								<span style="padding-right: 8px;"><i class="material-icons medium">looks_one</i></span>	
							</div>					
						</div>

						<div class="row">
							<div class="input-field col s12" align="center" >
								<img src="<?=$matriz[0]['logo']?>" width="30%"/>
								<input class="hide" type="text"  name="arquivo" value="<?=$matriz[0]['logo']?>">
								<input class="hide" type="text"  name="id_sede" value="<?=$_SESSION[ "acao" ][ 'id' ]?>">
							</div>
						</div>
						
						<div class="row">
							<div class="input-field col s12">
								<h4>Dados da Filial</h4>	
							</div>	

							<div class="input-field col s12 m6">
								<i class="material-icons prefix">assignment_ind</i>
								<input id="Nome" type="text" class="validate" name="nome" autocomplete="off" value="<?=$matriz[0]['social']?>">
								<label for="Nome">Razão Social</label>
							</div>						

							<div class="input-field col s12 m6">
								<i class="material-icons prefix">assignment_ind</i> <!--Mesmo nome da Sede-->
								<input id="Apelido" type="text" class="validate" name="apelido" autocomplete="off" value="<?=$matriz[0]['fantasia']?>">
								<label for="Apelido">Nome Fantasia</label>
							</div>	

							<div class="input-field col s12 m6">
								<i class="material-icons prefix">credit_card</i>
								<input id="cnpj" type="text" class="validate" name="cnpj" data-mask="00.000.000/0000-00" value="<?=$matriz[0]['cnpj']?>">
								<label for="cnpj">CNPJ</label>
							</div>	

													

						</div>	

						<div class="row">	

							<div class="input-field col s12">
								<h4>Acesso ao Sistema</h4>	
							</div>

							<div class="input-field col s12">
								<i class="material-icons prefix">business</i>
								<input id="sede" type="text" class="validate" name="sede"  value="<?=$matriz[0]['sede']?>">
								<label for="sede">Nome de Visualização no Sistema (Filial)</label>
							</div>

							<div class="input-field col s12 m8">
								<i class="material-icons prefix">email</i>
								<input id="email" type="email" class="validate" name="email" value="<?=$matriz[0]['email']?>">
								<label for="email">Email</label>
							</div>

							<div class="input-field col s12 m4">
								<i class="material-icons prefix">fingerprint</i>
								<input id="password" type="password" class="validate" name="senha">
								<label for="password">Senha</label>
							</div>

						</div>	

						<div class="row">

							<div class="input-field col s12">
								<h4>Telefones</h4>	
							</div>

							<div class="clone">

							<div class="input-field col s12 m6">
								<input id="contato" type="text" class="validate" name="contato[]" >
								<label for="contato">Nome do Contato</label>
							</div>	

							<div class="input-field col s12 m3">							
								<input id="icon_telephone" type="tel" class="validate telefone sp_celphones" name="telefone[]" data-mask="(00) 000-000-000" maxlength="15">
								<label for="icon_telephone">Número</label>
							</div>

							<div class="input-field col s12 m3">
								 <input id="icon_telephone" type="tel" class="validate Ramal" name="ramal[]" data-mask="-0000" maxlength="15">
								 <label for="ramal">Ramal</label>
							</div>

							</div>

							<div class="add"></div>						


							<p align="center" class="addNumero"> <i class="material-icons prefix">add</i> Adicionar Contato</p>

							<script>
								$(document).ready(function() {
									  $(".addNumero").click(function() {
									  var novoItem = $(".clone").clone().removeClass('clone'); 
									  $(".add").append(novoItem);
									});
								  });
							</script>
						</div>

						<div class="row">

							<div class="input-field col s12">
								<h4>Outras Informações</h4>	
							</div>

							<div class="input-field col s12 m4">
								<input id="municipal" type="text" class="validate" name="municipal" data-mask="00000-0" maxlength="6">
								<label for="municipal">Inscrição Municipal</label>
							</div>

							<div class="input-field col s12 m4">
								<input id="estadual" type="text" class="validate" name="estadual" data-mask="000.000.000.000" maxlength="15">
								<label for="estadual">Inscrição Estadual</label>
							</div>

							<div class="input-field col s12 m4">
								<input id="nfe" type="text" class="validate" name="nfe">
								<label for="nfe"> NFe </label>
							</div>

							<div class="input-field col s12">
								 <textarea id="obs" class="materialize-textarea" name="obs"></textarea>
								 <label for="obs">Observações</label>
							</div>

						</div>

						<div class="row">

							<div class="input-field col s12">
								<h4>Endereço</h4>	
							</div>

							<div class="input-field col s12 m4">
								<label for="cep"> CEP </label>
								<input id="cep" type="text" class="validate" name="cep" data-mask="00000-000" onblur="pesquisacep(this.value);" size="10" maxlength="9">
							</div>	

							<div class="input-field col s12 m1">
								<label for="uf"> Estado </label>
								<input id="uf" type="text" class="validate blue-text text-lighten-3" name="estado" size="2" >
							</div>

							<div class="input-field col s12 m3">
								<label for="cidade"> Cidade </label>
								<input id="cidade" type="text" class="validate blue-text text-lighten-3" name="cidade" size="40">
							</div>	

							<div class="input-field col s12 m4">
								<label for="bairro"> Bairro </label>
								<input id="bairro" type="text" class="validate blue-text text-lighten-3" name="bairro" size="40">
							</div>						

							<div class="input-field col m10">
								<label for="endereco"> Endereco </label>
								<input id="endereco" type="text" class="validate blue-text text-lighten-3" name="endereco" size="60">
							</div>

							<div class="input-field col m2">
								<label for="numero"> Número </label>
								<input id="numero" type="number" class="validate" name="numero">
							</div>			

							<div class="input-field col m12">
								<label for="complemento"> Complemento </label>
								<input id="complemento" type="text" class="validate" name="complemento">
							</div>

						</div>

						<div class="row">
							<div class="col s6 m3 offset-m1" align="center">
								<a href="empresa.php?acao=novo" class="btn waves-effect waves-light">Voltar</a> 
							</div>	
							<div class="col s6 m6">
								<button class="btn2 waves-effect waves-light right" type="submit"> Cadastrar </button>
							</div>		
						</div>	

					</div>	
				</form>
			</div>	

		<?php }		

	}
}

if ( $_SESSION[ "acao" ][ 'acao' ]  === 'listar' ) {
	
	if ($_SESSION[ "acao" ][ 'tipo' ] === 'sede'){	
		$empresas = listarSedeEmpresasBD( $conexao ); ?>
		<div class="container">

		<h3>Lista de Matriz</h3>

		<div class="row">
			<table>

				<tbody>

				<?php 
					foreach ( $empresas as $empresas ) {

					if($empresas['ativo'] == 1){
						$color = 'amber-text text-darken-1';
					}
					if($empresas['ativo'] == 0){
						$color = 'white-text';
					}
				?>


					<tr>
						<td width="120px" ><img src="<?php echo $empresas['logo'] = strstr($empresas['logo'],'usuarios/');?>" width="50px" height="50px" class="circle"/></td>
						<td><a  class="<?= $color?> modal-trigger" href="#modalVisualizar<?=$empresas['id']?>"> <?=$empresas['sede']?> </a></td>
						<td width="100px" align="center"><a  class="<?= $color?> modal-trigger" href="empresa.php?acao=editar&tipo=sede&token=<?=$empresas['id']?>"><i class="material-icons prefix white-text <?= $color?>">edit</i> </a></td>
					</tr>
									  
					<!--Visualizador-->
					<div id="modalVisualizar<?=$empresas['id']?>" class="modal" style=" background-color: #008aae;">
						<i class="material-icons right modal-close" style="padding: 15px;">close</i>

						<div class="container">

							<div class="row ">

							<p align="center">Visualizador da Matriz.</p>

								<div>
									<img src="<?=$empresas['logo']?>" width="50%" class="center"/>

									<p>Nome no Sistema: <?=$empresas['sede']?></p>
									<p>Razão Social: <?=$empresas['social']?></p>						
									<p>Nome Fantasia: <?=$empresas['fantasia']?></p>
									<p>CNPJ: <?=$empresas['cnpj']?></p>	
									<p>E-mail: <?=$empresas['email']?></p>	
									
									
										<?php
											$telefone = listarTelefone( $conexao, $empresas['id'], 3 );
											foreach ($telefone as $telefone){ ?>
											<div class="row">
											<?php
												if( !empty($telefone['contato']) ){?>
												<div class=" col s12 m4">Contato: <?= $telefone['contato'] ?></div>
												<?php }
												if( !empty($telefone['numero']) ){?>
												<div class=" col s12 m4">Número: <?= $telefone['numero'] ?></div>
												<?php }
												if( !empty($telefone['ramal']) ){?>
												<div class=" col s12 m4">Ramal: <?= $telefone['ramal'] ?></div>
											<?php } ?>
											</div>							 
											<?php } ?>
									
									<p>	<?php unset($endereco);
											$endereco = listarEndereco( $conexao, $empresas['id'], 3 );
											
												if(strlen($endereco['endereco']) > 1){
													$enderecoCompleto =  $endereco['endereco'];
												}
												if(strlen($endereco['numero']) > 1){
													$enderecoCompleto = $enderecoCompleto.', '.$endereco['numero'];
												}
												if(strlen($endereco['complemento']) > 1){
													$enderecoCompleto =  $enderecoCompleto.' - '.$endereco['complemento'];
												}
												if(strlen($endereco['bairro']) > 1){
													$enderecoCompleto =  $enderecoCompleto.', '.$endereco['bairro'];
												}
												if(strlen($endereco['cidade']) > 1){
													$enderecoCompleto =  $enderecoCompleto.', '.$endereco['cidade'];
												}
												if(strlen($endereco['estado']) > 1){
													$enderecoCompleto =  $enderecoCompleto.', '.$endereco['estado'];
												}
												if(strlen($endereco['cep']) > 1){
													$enderecoCompleto =  $enderecoCompleto.', ('.$endereco['cep'].')';
												}
												if( !empty($enderecoCompleto) ) { 
													echo 'Endereço: '.$enderecoCompleto; 
												}
												unset($enderecoCompleto);
											?>	</p>									
														
									<p>Situação : <?php switch ($empresas['ativo']) {
															case 0:
																echo " Ativado ";
																break;
															case 1:
																echo " Desativado ";
																break;
														}?></p>											
									<p>Data de Criação: <?= date('d/m/Y', strtotime($empresas['datadecriacao']))?></p>
								</div>
									<hr>
									<p>Filiais: <?php $filiais = listarEmpresaFilial($conexao, $empresas['id']);?>
									<div class="row ">
										<?php foreach($filiais as $filiais){ ?>
											<div class=" col s2 "><a class="white-text" href="empresa.php?acao=listar&tipo=filial&token=<?=$empresas['id']?>" ><img src="<?=$empresas['logo']?>" width="100%"/></a></div>
											<div class=" col s4 "><a class="white-text" href="empresa.php?acao=listar&tipo=filial&token=<?=$empresas['id']?>" ><?=$filiais['social'];?></a></div>
											<div class=" col s4 "><a class="white-text" href="empresa.php?acao=listar&tipo=filial&token=<?=$empresas['id']?>" ><?=$filiais['email'];?></a></div>
											<div class=" col s2 "><a href="empresa.php?acao=editar&tipo=filial&token=<?=$filiais['id'];?>"><i class="material-icons prefix white-text <?= $color?>">edit</i></a></div>
										<?php } ?>
									</div>
								</div>

							</div>
						</div>

					</div>

				<?php 
				} 
					?>

				</tbody>
			</table>
		</div>

			<div class="row">
				<div class="col s6 m3 offset-m1" align="center">
					<a href="dashboard.php" class="btn waves-effect waves-light">Voltar</a> </div>
				<div class="col s6 m6">
					<a href="empresa.php?acao=novo" class="btn2 waves-effect waves-light right">Novo Cadastro</a> </div>
				</div>
			</div>

		</div>
	<?php 
	}
	
	if ($_SESSION[ "acao" ][ 'tipo' ] === 'filial'){
		$matriz = listarSedeEmpresasIdBD( $conexao, $_SESSION[ "acao" ][ 'id' ] );	
		$matriz[0]['logo'] = strstr($matriz[0]['logo'],'usuarios/');
		
		$empresas = listarEmpresaSedeFilial( $conexao, $_SESSION[ "acao" ][ 'id' ] ); ?>
		<div class="container">	

		<h3>Lista de Filial</h3>

		<div class="row">
		
		<div class="col s12 m6 offset-m3">
		  <div class="card">
			<div class="card-content white-text">
			  <div  class="card-image">
			  	<img src="<?=$matriz[0]['logo']?>"/> 
			  	<span class="card-title"><?= $matriz[0]['sede'] ?></span>
			  </div>
			  	<?php unset($endereco);
					$endereco = listarEndereco( $conexao, $empresas['id'], 3 );

						if(strlen($endereco['endereco']) > 1){
							$enderecoCompleto =  $endereco['endereco'];
						}
						if(strlen($endereco['numero']) > 1){
							$enderecoCompleto = $enderecoCompleto.', '.$endereco['numero'];
						}
						if(strlen($endereco['complemento']) > 1){
							$enderecoCompleto =  $enderecoCompleto.' - '.$endereco['complemento'];
						}
						if(strlen($endereco['bairro']) > 1){
							$enderecoCompleto =  $enderecoCompleto.', '.$endereco['bairro'];
						}
						if(strlen($endereco['cidade']) > 1){
							$enderecoCompleto =  $enderecoCompleto.', '.$endereco['cidade'];
						}
						if(strlen($endereco['estado']) > 1){
							$enderecoCompleto =  $enderecoCompleto.', '.$endereco['estado'];
						}
						if(strlen($endereco['cep']) > 1){
							$enderecoCompleto =  $enderecoCompleto.', ('.$endereco['cep'].')';
						}
						if( !empty($enderecoCompleto) ) { 
							echo 'Endereço: '.$enderecoCompleto; 
						}
						unset($enderecoCompleto);
					?>
					
			<?php $telefone = listarTelefone( $conexao, $_SESSION[ "acao" ][ 'id' ], 3 );
				foreach ($telefone as $telefone){ ?>
				<?php
					if( !empty($telefone['contato']) ){?>
					Contato: <?= $telefone['contato'] ?><br>
					<?php }
					if( !empty($telefone['numero']) ){?>
					Número: <?= $telefone['numero'] ?><br>
					<?php }
					if( !empty($telefone['ramal']) ){?>
					Ramal: <?= $telefone['ramal'] ?><br>
				<?php } ?>				 
			<?php } ?>
			</div>
		  </div>
		</div>
		
		</div>
		
		<div class="row">
			<table>

				<tbody>

				<?php 
					foreach ( $empresas as $empresas ) {

					if($empresas['ativo'] == 1){
						$color = 'amber-text text-darken-1';
					}
					if($empresas['ativo'] == 0){
						$color = 'white-text';
					}
				?>


					<tr>
						<td width="120px" ><img src="<?php echo $empresas['logo'] = strstr($empresas['logo'],'usuarios/');?>" width="50px" height="50px" class="circle"/></td>
						<td><a  class="<?= $color?> modal-trigger" href="#modalVisualizar<?=$empresas['id']?>"> <?=$empresas['sede']?> </a></td>
						<td width="100px" align="center"><a  class="<?= $color?> modal-trigger" href="empresa.php?acao=editar&tipo=filial&token=<?=$empresas['id']?>"><i class="material-icons prefix white-text <?= $color?>">edit</i> </a></td>
					</tr>
									  
					<!--Visualizador-->
					<div id="modalVisualizar<?=$empresas['id']?>" class="modal" style=" background-color: #008aae;">
						<i class="material-icons right modal-close" style="padding: 15px;">close</i>

						<div class="container">

							<div class="row ">

							<p align="center">Visualizador da Matriz.</p>

								<div>
									<img src="<?=$empresas['logo']?>" width="50%" class="center"/>

									<p>Nome no Sistema: <?=$empresas['sede']?></p>
									<p>Razão Social: <?=$empresas['social']?></p>						
									<p>Nome Fantasia: <?=$empresas['fantasia']?></p>
									<p>CNPJ: <?=$empresas['cnpj']?></p>	
									<p>E-mail: <?=$empresas['email']?></p>	
									
									
										<?php
											$telefone = listarTelefone( $conexao, $empresas['id'], 5 );
											foreach ($telefone as $telefone){ ?>
											<div class="row">
											<?php
												if( !empty($telefone['contato']) ){?>
												<div class=" col s12 m4">Contato: <?= $telefone['contato'] ?></div>
												<?php }
												if( !empty($telefone['numero']) ){?>
												<div class=" col s12 m4">Número: <?= $telefone['numero'] ?></div>
												<?php }
												if( !empty($telefone['ramal']) ){?>
												<div class=" col s12 m4">Ramal: <?= $telefone['ramal'] ?></div>
											<?php } ?>
											</div>							 
											<?php } ?>
									
									<p>	<?php unset($endereco);
											$endereco = listarEndereco( $conexao, $empresas['id'], 5 );
											
												if(strlen($endereco['endereco']) > 1){
													$enderecoCompleto =  $endereco['endereco'];
												}
												if(strlen($endereco['numero']) > 1){
													$enderecoCompleto = $enderecoCompleto.', '.$endereco['numero'];
												}
												if(strlen($endereco['complemento']) > 1){
													$enderecoCompleto =  $enderecoCompleto.' - '.$endereco['complemento'];
												}
												if(strlen($endereco['bairro']) > 1){
													$enderecoCompleto =  $enderecoCompleto.', '.$endereco['bairro'];
												}
												if(strlen($endereco['cidade']) > 1){
													$enderecoCompleto =  $enderecoCompleto.', '.$endereco['cidade'];
												}
												if(strlen($endereco['estado']) > 1){
													$enderecoCompleto =  $enderecoCompleto.', '.$endereco['estado'];
												}
												if(strlen($endereco['cep']) > 1){
													$enderecoCompleto =  $enderecoCompleto.', ('.$endereco['cep'].')';
												}
												if( !empty($enderecoCompleto) ) { 
													echo 'Endereço: '.$enderecoCompleto; 
												}
												unset($enderecoCompleto);
											?>		
									<p>Situação: <?php switch ($empresas['ativo']) {
															case 0:
																echo " Ativado ";
																break;
															case 1:
																echo " Desativado ";
																break;
														}?></p>											
									<p>Data de Criação: <?= date('d/m/Y', strtotime($empresas['datadecriacao']))?></p>
								</div>

							</div>
						</div>

					</div>

				<?php 
				} 
					?>

				</tbody>
			</table>
		</div>

			<div class="row">
				<div class="col s6 m3 offset-m1" align="center">
					<a href="dashboard.php" class="btn waves-effect waves-light">Voltar</a> </div>
				<div class="col s6 m6">
					<a href="empresa.php?acao=novo" class="btn2 waves-effect waves-light right">Novo Cadastro</a> </div>
				</div>
			</div>

		</div>
	<?php 
	}
	
}

if ( $_SESSION[ "acao" ][ 'acao' ]  === 'editar' ){
	
	if ($_SESSION[ "acao" ][ 'tipo' ] === 'sede'){ 
		
		$empresa = listarSedeEmpresasIdBD($conexao, $_SESSION[ "acao" ][ 'id' ]); 
		$empresa[0]['logo'] = strstr($empresa[0]['logo'],'usuarios/');
		
		$telefone = listarTelefone($conexao, $_SESSION[ "acao" ][ 'id' ] , 3);
		$infoEmpresa = listarEmpresaBD($conexao , $_SESSION[ "acao" ][ 'id' ], 3);
		$endereco = listarEndereco($conexao, $_SESSION[ "acao" ][ 'id' ] , 3);
		
		?>  
		
		<br>
		<div class="container">
			<form class="col s12" action="controller/empresa.php?acao=editar&tipo=sede" method="post" enctype="multipart/form-data">
			
				<div class="row">
					<div class="col s12 m12 l12">					
						<h2>Editar Matriz</h2>					
					</div>
					
					<div class="row">
						<div class="col s12 m12 l12 center-align PassoAtiv">
							<span style="padding-right: 8px;"><i class="material-icons medium">looks_one</i></span>	
						</div>					
					</div>
					
					<div class="row">
						
						<div class="input-field col s12">
							<h4>Dados da Matriz</h4>	
						</div>
						
					<div class="hide">
							<input  name="id" value="<?=$empresa[0]['id']?>">
						</div>						
							
					
						<div class="input-field col s12 m6">
							<i class="material-icons prefix">assignment_ind</i>
							<input id="Nome" type="text" class="validate" name="nome" value="<?= $empresa[0]['social']?>">
							<label for="Nome">Razão Social</label>
						</div>						
						
						<div class="input-field col s12 m6">
							<i class="material-icons prefix">assignment_ind</i>
							<input id="Apelido" type="text" class="validate" name="apelido" value="<?= $empresa[0]['fantasia']?>">
							<label for="Apelido">Nome Fantasia</label>
						</div>	
						
						<div class="input-field col s12 m6">
							<i class="material-icons prefix">credit_card</i>
							<input id="cnpj" type="text" class="validate" name="cnpj" data-mask="00.000.000/0000-00" value="<?= $empresa[0]['cnpj']?>">
							<label for="cnpj">CNPJ</label>
						</div>	
						
						<div class="input-field col s12" align="center" >
							<p>Anexar Logo:</p>
							<input type="file" name="arquivo" ><br><br>
							<img src=" <?=$empresa[0]['logo']?>" width="20%" />
						</div>						
						
					</div>	
					
					<div class="row">						
						<div class="input-field col s12">
							<h4>Acesso ao Sistema</h4>	
						</div>
						
						<div class="input-field col s12">
							<i class="material-icons prefix">business</i>
							<input id="sede" type="text" class="validate" name="sede"  value="<?=$empresa[0]['sede']?>">
							<label for="sede">Nome de Visualização no Sistema (Sede)</label>
						</div>
						
						<div class="input-field col s12 m8">
							<i class="material-icons prefix">email</i>
							<input id="email" type="email" class="validate" name="email" value="<?=$empresa[0]['email']?>">
							<label for="email">Email</label>
						</div>
						
						<div class="input-field col s12 m4">
							<i class="material-icons prefix">fingerprint</i>
							<input id="password" type="password" class="validate" name="senha">
							<label for="password">Senha</label>
						</div>
						
					</div>	
					
					<div class="row">
					
						<div class="input-field col s12">
							<h4>Telefones</h4>	
						</div>	
						
						<div class="clone">
						
						<?php foreach($telefone as $telefone){ ?>
						
						<div class="input-field col s12 m6 hide">
							<input id="id" type="number" class="validate" name="telid[]" value="<?=$telefone['id']?>" >
							<label for="id">ID</label>
						</div>	
						
						<div class="input-field col s12 m6">
							<input id="contato" type="text" class="validate" name="contato[]" value="<?=$telefone['contato']?>" >
							<label for="contato">Nome do Contato</label>
						</div>	
						
						<div class="input-field col s12 m3">							
							<input id="icon_telephone" type="tel" class="validate telefone sp_celphones" name="telefone[]" data-mask="(00) 000-000-000" maxlength="15"  value="<?=$telefone['numero']?>" >
							<label for="icon_telephone">Número</label>
						</div>

						<div class="input-field col s12 m3">
							 <input id="icon_telephone" type="tel" class="validate Ramal" name="ramal[]" data-mask="-0000" maxlength="15" value="<?=$telefone['ramal']?>" >
							 <label for="ramal">Ramal</label>
						</div>
						
						<?php } ?>
						
						</div>
						
						<div class="add"></div>						
						
						
						<p align="center" class="addNumero"> <i class="material-icons prefix">add</i> Adicionar Contato</p>
						
						<script>
							$(document).ready(function() {
								  $(".addNumero").click(function() {
								  var novoItem = $(".clone").clone().removeClass('clone'); 
								  $(".add").append(novoItem);
								});
							  });
						</script>
					</div>
						
					<div class="row">
					
						<div class="input-field col s12">
							<h4>Outras Informações</h4>	
						</div>
						
						<div class="input-field col s12 m4">
							<input id="municipal" type="text" class="validate" name="municipal" data-mask="00000-0" maxlength="6" value="<?=$infoEmpresa['municipal']?>">
							<label for="municipal">Inscrição Municipal</label>
						</div>
						
						<div class="input-field col s12 m4">
							<input id="estadual" type="text" class="validate" name="estadual" data-mask="000.000.000.000" maxlength="15" value="<?=$infoEmpresa['estadual']?>">
							<label for="estadual">Inscrição Estadual</label>
						</div>
						
						<div class="input-field col s12 m4">
							<input id="nfe" type="text" class="validate" name="nfe" value="<?=$infoEmpresa['nfe']?>">
							<label for="nfe"> NFe </label>
						</div>
											
						<div class="input-field col s12">
							 <textarea id="obs" class="materialize-textarea" name="obs"> <?=$infoEmpresa['obs']?> </textarea>
							 <label for="obs">Observações</label>
						</div>
						
					</div>
					
					<div class="row">
					
						<div class="input-field col s12">
							<h4>Endereço</h4>	
						</div>
						
						<div class="input-field col s12 m4">
							<label for="cep"> CEP </label>
							<input id="cep" type="text" class="validate" name="cep" data-mask="00000-000" onblur="pesquisacep(this.value);" size="10" maxlength="9" value="<?=$endereco['cep']?>">
						</div>	

						<div class="input-field col s12 m1">
							<label for="uf"> Estado </label>
							<input id="uf" type="text" class="validate blue-text text-lighten-3" name="estado" size="2"  value="<?=$endereco['estado']?>">
						</div>

						<div class="input-field col s12 m3">
							<label for="cidade"> Cidade </label>
							<input id="cidade" type="text" class="validate blue-text text-lighten-3" name="cidade" size="40"  value="<?=$endereco['cidade']?>">
						</div>	

						<div class="input-field col s12 m4">
							<label for="bairro"> Bairro </label>
							<input id="bairro" type="text" class="validate blue-text text-lighten-3" name="bairro" size="40" value="<?=$endereco['bairro']?>">
						</div>						
						
						<div class="input-field col m10">
							<label for="endereco"> Endereco </label>
							<input id="endereco" type="text" class="validate blue-text text-lighten-3" name="endereco" size="60" value="<?=$endereco['endereco']?>">
						</div>
						
						<div class="input-field col m2">
							<label for="numero"> Número </label>
							<input id="numero" type="number" class="validate" name="numero" value="<?=$endereco['numero']?>">
						</div>			

						<div class="input-field col m12">
							<label for="complemento"> Complemento </label>
							<input id="complemento" type="text" class="validate" name="complemento" value="<?=$endereco['complemento']?>">
						</div>
						
					</div>
					
					<div class="row">
						<div class="col s6 m3 offset-m1" align="center">
							<a href="empresa.php?acao=listar&tipo=sede" class="btn waves-effect waves-light">Voltar</a> 
						</div>	
						<div class="col s6 m6">
							<button class="btn2 waves-effect waves-light right" type="submit"> Alterar </button>
						</div>		
					</div>	
					
				</div>	
			</form>
		</div>	
	<?php
	}	
	
	if ($_SESSION[ "acao" ][ 'tipo' ] === 'filial'){ 
		
		$empresa = listarEmpresaFilial($conexao, $_SESSION[ "acao" ][ 'id' ]); 
		$empresa[0]['logo'] = strstr($empresa[0]['logo'],'usuarios/');
		
		$telefone = listarTelefone($conexao, $_SESSION[ "acao" ][ 'id' ] , 5);
		$infoEmpresa = listarEmpresaBD($conexao , $_SESSION[ "acao" ][ 'id' ], 5);
		$endereco = listarEndereco($conexao, $_SESSION[ "acao" ][ 'id' ] , 5);
		
		?>  
		
		<br>
		<div class="container">
			<form class="col s12" action="controller/empresa.php?acao=editar&tipo=sede" method="post" enctype="multipart/form-data">
			
				<div class="row">
					<div class="col s12 m12 l12">					
						<h2>Editar Filial</h2>					
					</div>
					
					<div class="row">
						<div class="col s12 m12 l12 center-align PassoAtiv">
							<span style="padding-right: 8px;"><i class="material-icons medium">looks_one</i></span>	
						</div>					
					</div>
					
					<div class="row">
						
						<div class="input-field col s12">
							<h4>Dados da Matriz</h4>	
						</div>
						
					<div class="hide">
							<input  name="id" value="<?=$empresa[0]['id']?>">
						</div>						
							
					
						<div class="input-field col s12 m6">
							<i class="material-icons prefix">assignment_ind</i>
							<input id="Nome" type="text" class="validate" name="nome" value="<?= $empresa[0]['social']?>">
							<label for="Nome">Razão Social</label>
						</div>						
						
						<div class="input-field col s12 m6">
							<i class="material-icons prefix">assignment_ind</i>
							<input id="Apelido" type="text" class="validate" name="apelido" value="<?= $empresa[0]['fantasia']?>">
							<label for="Apelido">Nome Fantasia</label>
						</div>	
						
						<div class="input-field col s12 m6">
							<i class="material-icons prefix">credit_card</i>
							<input id="cnpj" type="text" class="validate" name="cnpj" data-mask="00.000.000/0000-00" value="<?= $empresa[0]['cnpj']?>">
							<label for="cnpj">CNPJ</label>
						</div>	
						
						<div class="input-field col s12" align="center" >
							<p>Anexar Logo:</p>
							<input type="file" name="arquivo" ><br><br>
							<img src=" <?=$empresa[0]['logo']?>" width="20%" />
						</div>						
						
					</div>	
					
					<div class="row">						
						<div class="input-field col s12">
							<h4>Acesso ao Sistema</h4>	
						</div>
						
						<div class="input-field col s12">
							<i class="material-icons prefix">business</i>
							<input id="sede" type="text" class="validate" name="sede"  value="<?=$empresa[0]['sede']?>">
							<label for="sede">Nome de Visualização no Sistema (Sede)</label>
						</div>
						
						<div class="input-field col s12 m8">
							<i class="material-icons prefix">email</i>
							<input id="email" type="email" class="validate" name="email" value="<?=$empresa[0]['email']?>">
							<label for="email">Email</label>
						</div>
						
						<div class="input-field col s12 m4">
							<i class="material-icons prefix">fingerprint</i>
							<input id="password" type="password" class="validate" name="senha">
							<label for="password">Senha</label>
						</div>
						
					</div>	
					
					<div class="row">
					
						<div class="input-field col s12">
							<h4>Telefones</h4>	
						</div>	
						
						<div class="clone">
						
						<?php foreach($telefone as $telefone){ ?>
						
						<div class="input-field col s12 m6 hide">
							<input id="id" type="number" class="validate" name="telid[]" value="<?=$telefone['id']?>" >
							<label for="id">ID</label>
						</div>	
						
						<div class="input-field col s12 m6">
							<input id="contato" type="text" class="validate" name="contato[]" value="<?=$telefone['contato']?>" >
							<label for="contato">Nome do Contato</label>
						</div>	
						
						<div class="input-field col s12 m3">							
							<input id="icon_telephone" type="tel" class="validate telefone sp_celphones" name="telefone[]" data-mask="(00) 000-000-000" maxlength="15"  value="<?=$telefone['numero']?>" >
							<label for="icon_telephone">Número</label>
						</div>

						<div class="input-field col s12 m3">
							 <input id="icon_telephone" type="tel" class="validate Ramal" name="ramal[]" data-mask="-0000" maxlength="15" value="<?=$telefone['ramal']?>" >
							 <label for="ramal">Ramal</label>
						</div>
						
						<?php } ?>
						
						</div>
						
						<div class="add"></div>						
						
						
						<p align="center" class="addNumero"> <i class="material-icons prefix">add</i> Adicionar Contato</p>
						
						<script>
							$(document).ready(function() {
								  $(".addNumero").click(function() {
								  var novoItem = $(".clone").clone().removeClass('clone'); 
								  $(".add").append(novoItem);
								});
							  });
						</script>
					</div>
						
					<div class="row">
					
						<div class="input-field col s12">
							<h4>Outras Informações</h4>	
						</div>
						
						<div class="input-field col s12 m4">
							<input id="municipal" type="text" class="validate" name="municipal" data-mask="00000-0" maxlength="6" value="<?=$infoEmpresa['municipal']?>">
							<label for="municipal">Inscrição Municipal</label>
						</div>
						
						<div class="input-field col s12 m4">
							<input id="estadual" type="text" class="validate" name="estadual" data-mask="000.000.000.000" maxlength="15" value="<?=$infoEmpresa['estadual']?>">
							<label for="estadual">Inscrição Estadual</label>
						</div>
						
						<div class="input-field col s12 m4">
							<input id="nfe" type="text" class="validate" name="nfe" value="<?=$infoEmpresa['nfe']?>">
							<label for="nfe"> NFe </label>
						</div>
											
						<div class="input-field col s12">
							 <textarea id="obs" class="materialize-textarea" name="obs"> <?=$infoEmpresa['obs']?> </textarea>
							 <label for="obs">Observações</label>
						</div>
						
					</div>
					
					<div class="row">
					
						<div class="input-field col s12">
							<h4>Endereço</h4>	
						</div>
						
						<div class="input-field col s12 m4">
							<label for="cep"> CEP </label>
							<input id="cep" type="text" class="validate" name="cep" data-mask="00000-000" onblur="pesquisacep(this.value);" size="10" maxlength="9" value="<?=$endereco['cep']?>">
						</div>	

						<div class="input-field col s12 m1">
							<label for="uf"> Estado </label>
							<input id="uf" type="text" class="validate blue-text text-lighten-3" name="estado" size="2"  value="<?=$endereco['estado']?>">
						</div>

						<div class="input-field col s12 m3">
							<label for="cidade"> Cidade </label>
							<input id="cidade" type="text" class="validate blue-text text-lighten-3" name="cidade" size="40"  value="<?=$endereco['cidade']?>">
						</div>	

						<div class="input-field col s12 m4">
							<label for="bairro"> Bairro </label>
							<input id="bairro" type="text" class="validate blue-text text-lighten-3" name="bairro" size="40" value="<?=$endereco['bairro']?>">
						</div>						
						
						<div class="input-field col m10">
							<label for="endereco"> Endereco </label>
							<input id="endereco" type="text" class="validate blue-text text-lighten-3" name="endereco" size="60" value="<?=$endereco['endereco']?>">
						</div>
						
						<div class="input-field col m2">
							<label for="numero"> Número </label>
							<input id="numero" type="number" class="validate" name="numero" value="<?=$endereco['numero']?>">
						</div>			

						<div class="input-field col m12">
							<label for="complemento"> Complemento </label>
							<input id="complemento" type="text" class="validate" name="complemento" value="<?=$endereco['complemento']?>">
						</div>
						
					</div>
					
					<div class="row">
						<div class="col s6 m3 offset-m1" align="center">
							<a href="empresa.php?acao=listar&tipo=sede" class="btn waves-effect waves-light">Voltar</a> 
						</div>	
						<div class="col s6 m6">
							<button class="btn2 waves-effect waves-light right" type="submit"> Alterar </button>
						</div>		
					</div>	
					
				</div>	
			</form>
		</div>	
	<?php
	}	
	
}


if ( !empty( $_GET['acao'] )){	
	
	echo $_SESSION[ "acao" ][ 'acao' ] 	= $_GET['acao'];
	echo $_SESSION[ "acao" ][ 'id' ] 	= $_GET['token'];
	echo $_SESSION[ "acao" ][ 'tipo' ]	= $_GET['tipo'];		
	
	echo '<script>window.location.replace("empresa.php");</script>';
}

include_once('rodape.php'); ?>