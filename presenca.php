<?php include_once( 'cabecalho.php' );
include_once( 'model/usuario.php' );
session_start();

error_reporting( E_ERROR | E_PARSE );
echo '<style>body{color: aliceblue;}</style>';
//echo $_POST['empresa']  ; //Apagar

if ( isset( $_SESSION[ "acao" ][ 'acao' ] ) ) {	
	
	if ( $_SESSION[ "acao" ][ 'acao' ] === "alterar" ){
		echo $_SESSION[ "acao" ][ 'token' ];
		include_once( 'model/agenda.php' );
		$array = array($_SESSION[ "acao" ][ 'vaga' ], 2 => $_SESSION[ "acao" ][ 'token' ]);
		alterarPresencaBD( $conexao, $array );
		echo '<script>window.location.replace("presenca.php?acao=horario");</script>';
	}
	
	if ( $_SESSION[ "acao" ][ 'acao' ] === "adesao" ){
		echo $_SESSION[ "acao" ][ 'token' ];
		include_once( 'model/agenda.php' );
		$array = array($_SESSION[ "acao" ][ 'vaga' ], 2 => $_SESSION[ "acao" ][ 'token' ]);
		alterarAdesaoBD( $conexao, $array );
		echo '<script>window.location.replace("presenca.php?acao=horario");</script>';
	}
		
	if ( $_SESSION[ "acao" ][ 'acao' ] === "horario" ) {

		include_once( 'model/agenda.php' );
		include_once( 'model/atividade.php' );
		
		$parametro = " = '". $_SESSION[ "acao" ][ 'dia' ]." ' ";
		$agenda = listarAgendamentosBD($conexao, $_SESSION[ "acao" ][ 'empresa' ], $parametro );		
		$empresa = listarUsuarioBD($conexao, $_SESSION[ "acao" ][ 'empresa' ]);		
		//$atividade = listarAtividadeBD($conexao, $_POST['atividade']);
		
		if( empty($agenda) ){ ?>
		<div class="container">
			<h3 align="center">Nenhuma agenda para esse dia</h3>
		</div>
		<div class="row">
			<div class="col s6 m3 offset-m1" align="center">
				<a href="presenca.php?acao=lista" class="btn waves-effect waves-light">Voltar</a> </div>
		</div>
		<?php }
		else{ ?>
			<br>
			<div class="container row">

				<h2>Lista De Presença</h2>
				
				<div class="col s12 m6">
					<p>Empresa: <?= $empresa[0]['nome'] ?></p>
				</div>
				<div class="col s12 m3">
					<p>Data: <?=date('d/m/Y', strtotime($_SESSION[ "acao" ][ 'dia' ]))?></p>
				</div>
				<!--<div class="col s12 m3">
					<p>Atividade: <?= $atividade[0]['nome']?></p>
				</div>-->
			</div>
			
			<div class="container row">				
				
				<p>Horários:</p>
				

				<div class="row">											
					<ul class="collapsible">
						<?php foreach($agenda as $agenda){ 
						 $vagas = listarVagas($conexao, $agenda['id']);?>
						<li>
						  <div class="collapsible-header" align="center"><?= $agenda['nome']?> (Horario: <?=mask($agenda['hora'], '##:##')?> / Quantidades: <?= $agenda['quantidades']?> / Recadastros: <?= $agenda['recadastrados']?>)</div>
						  <div class="collapsible-body">
								<?php if( empty( $vagas ) ) { echo "Nao há Cliente agendado"; }
							  		else {?>
									  	<div class="row">												
											  <?php foreach($vagas as $vagas){ 
											  $usuario = listarUsuarioBD($conexao, $vagas['id_usuario'])?>
												<div class="col s12 m6 l3" style="box-shadow: 0 2px 2px 0 rgba(0,0,0,0.14), 0 3px 1px -2px rgba(0,0,0,0.12), 0 1px 5px 0 rgba(0,0,0,0.2);">
												
													<div class="input-field col s12">
														Pacientes: <?=$usuario[0]['nome'] ?>
													</div>
													<div class="input-field col s12">
														Matricula: <?=$usuario[0]['id'] ?>
													</div>
													<?php switch ($vagas['presenca']) {
														case 0:
															$Status = 'Não Informado';															
															break;
														case 1:
															$Status = '<spam class=" green-text "><i class="material-icons tiny">check_circle</i> Presente </spam>';
															break;
														case 2:
															$Status = '<spam class=" red-text "><i class="material-icons tiny">close</i> Faltou </spam>';
															break;
													}  ?>	
													<div class="input-field col s12">
														
														<div>Status: <?=$Status?></div>
														<?php if($vagas['presenca'] === '0' || $vagas['presenca'] === '2'){?>
												  			<a href="presenca.php?acao=alterar&vaga=<?=$vagas['id']?>&token=1" class="white-text"> Alterar para Presente</a>
											  			<?php } ?>	
														<?php if($vagas['presenca'] === '0' || $vagas['presenca'] === '1' ){?>
												  			<a href="presenca.php?acao=alterar&vaga=<?=$vagas['id']?>&token=2" class="white-text"> Alterar para Faltou</a>
											  			<?php } ?>	
											  		
													</div>
													<?php switch ($vagas['adesao']) {
														case 0:
															$adesao = '';															
															break;
														case 1:
															$adesao = 'Aderiu';
															break;
													}  ?>	
													<div class="input-field col s12">
													<div><?php if($vagas['adesao'] === '1'){ echo 'Adesão: <spam class=" green-text "><i class="material-icons tiny">check_circle</i>'. $adesao.' </spam>' ;} ?> </div>
											  			<?php if($vagas['adesao'] != '1'){?>
												  			<a href="presenca.php?acao=adesao&vaga=<?=$vagas['id']?>&token=1" class="white-text"> Adicionar Adesão </a>	
												  		<?php } ?>
											  			<?php if($vagas['adesao'] === '1'){?>
												  			<a href="presenca.php?acao=adesao&vaga=<?=$vagas['id']?>&token=2" class="white-text"> Alterar Adesão </a>	
												  		<?php } ?>
													</div>
												</div>	
										</div>
								<?php } ?>
							</div>
						</li>
						<?php } } ?>
					</ul>	
				</div>	
				
			</div>
			
			<div class="row">
				<div class="col s6 m3 offset-m1" align="center">
					<a href="presenca.php?acao=lista" class="btn waves-effect waves-light">Voltar</a> </div>
			</div>
		<?php } 
	}
	
	if ( $_SESSION[ "acao" ][ 'acao' ] === "lista" ) {

		//Escolha a empresa
		$empresa = listarSedeEmpresasBD($conexao);
		
		include_once( 'model/atividade.php' );
		$atividade = listarAtividadesBD($conexao);?>
		<br>
			<div class="container"><h2>Presença</h2>
			<form class="col s12" action="presenca.php?acao=horario" method="post" accept-charset="UTF-8">
				<div class="row">
					<div class="input-field col s12 m6">
						<i class="material-icons prefix">domain</i>
						<select name="empresa">
							<option disabled selected></option>
							<?php foreach ( $empresa as $empresa ){
									if ($agenda[0]["empresa"] == $empresa["id_empresa"]){ $selected = 'selected';} else {$selected = '';}?>
							<option value="<?=$empresa["id_empresa"];?>" <?=$selected;?>><?=$empresa["sede"];?></option>
							<?php } ?>
						</select>
						<label for="empresa">Escolha uma Sede:</label>
					</div>
					<div class="input-field col s12 m6">
					
							<i class="material-icons prefix">date_range</i>
						<input id="dia" type="date" class="validate" name="dia" value="<?= $_POST['date']?>">
						<label for="dia">Data</label>
					</div>
					<!--<div class="input-field col s12 m6">						
						<i class="material-icons prefix">directions_run</i>
							<select name="atividade">
								<option value="" disabled selected>Escolha uma Atividade</option>
								<?php foreach ( $atividade as $atividade ){
										if ($agenda[0]["atividade"] === $atividade["id"]){ $selected = 'selected';} else {$selected = '';}?>
								<option value="<?=$atividade["id"];?>" <?=$selected;?>><?=$atividade["nome"];?> (<?= mask($atividade["duracao"], "##:##")?>)</option>
								<?php } ?>
							</select>
							<label for="atividade">Atividade</label>
					</div>-->
				</div>
				<div class="row">
					<div class="col s6 m3 offset-m1" align="center">
						<a href="dashboard.php" class="btn waves-effect waves-light">Voltar</a> </div>
					<div class="col s6 m6">
						<button class="btn2 waves-effect waves-light right" type="submit">Avançar</button>
					</div>
				</div>
			</form>
			</div>
	<?php }
	
}


if ( !empty( $_GET[ 'acao' ] ) ) {

	$_SESSION[ "acao" ][ 'acao' ] 		= $_GET[ 'acao' ];	
	$_SESSION[ "acao" ][ 'vaga' ] 		= $_GET[ 'vaga' ];
	
	if( !empty($_POST['empresa']) ){
		echo $_SESSION[ "acao" ][ 'empresa' ] = $_POST['empresa'];
	}
	if( !empty($_POST['dia']) ){
		echo $_SESSION[ "acao" ][ 'dia' ] = $_POST['dia'];
	}
	if( !empty($_GET[ 'token' ]) ){
		echo $_SESSION[ "acao" ][ 'token' ] = $_GET[ 'token' ];
	}

	echo '<script>window.location.replace("presenca.php");</script>';	
} 

include_once( 'rodape.php' );