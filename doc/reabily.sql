-- phpMyAdmin SQL Dump
-- version 4.8.2
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: 15-Ago-2019 às 18:10
-- Versão do servidor: 5.7.22
-- PHP Version: 7.1.19

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `reabily`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `agenda`
--

CREATE TABLE `agenda` (
  `id` int(11) NOT NULL,
  `nome` varchar(255) NOT NULL,
  `descricao` text NOT NULL,
  `atividade` int(11) NOT NULL,
  `data` date NOT NULL,
  `hora` varchar(10) NOT NULL,
  `frequencia` int(11) NOT NULL,
  `abertura` date NOT NULL,
  `quantidades` int(11) NOT NULL,
  `empresa` int(11) NOT NULL,
  `funcionario` int(11) NOT NULL,
  `datadecriacao` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `ativo` int(1) NOT NULL,
  `recadastrados` int(2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `agenda`
--

INSERT INTO `agenda` (`id`, `nome`, `descricao`, `atividade`, `data`, `hora`, `frequencia`, `abertura`, `quantidades`, `empresa`, `funcionario`, `datadecriacao`, `ativo`, `recadastrados`) VALUES
(1, 'Teste 1', 'Descrição da Atividade', 3, '2019-08-12', '1000', 1, '2019-08-02', 1, 1, 3, '2019-08-05 17:38:55', 0, 5),
(2, 'Teste 1', 'Descrição da Atividade', 3, '2019-08-12', '1015', 1, '2019-08-02', 1, 1, 3, '2019-08-05 17:38:55', 2, 5),
(3, 'Teste 1', 'Descrição da Atividade', 3, '2019-08-12', '1030', 1, '2019-08-02', 1, 1, 3, '2019-08-05 17:38:55', 0, 5),
(4, 'Teste 1', 'Descrição da Atividade', 3, '2019-08-12', '1045', 1, '2019-08-02', 1, 1, 3, '2019-08-05 17:38:55', 0, 5),
(5, 'Teste 1', 'Descrição da Atividade', 3, '2019-08-13', '1000', 1, '2019-08-02', 1, 1, 3, '2019-08-05 17:38:55', 0, 5),
(6, 'Teste 1', 'Descrição da Atividade', 3, '2019-08-13', '1015', 1, '2019-08-02', 1, 1, 3, '2019-08-05 17:38:55', 0, 5),
(7, 'Teste 1', 'Descrição da Atividade', 3, '2019-08-13', '1030', 1, '2019-08-02', 1, 1, 3, '2019-08-05 17:38:55', 0, 5),
(8, 'Teste 1', 'Descrição da Atividade', 3, '2019-08-13', '1045', 1, '2019-08-02', 1, 1, 3, '2019-08-05 17:38:55', 0, 5),
(9, 'Teste 1', 'Descrição da Atividade', 3, '2019-08-14', '1000', 1, '2019-08-02', 1, 1, 3, '2019-08-05 17:38:56', 0, 5),
(10, 'Teste 1', 'Descrição da Atividade', 3, '2019-08-14', '1015', 1, '2019-08-02', 1, 1, 3, '2019-08-05 17:38:56', 0, 5),
(11, 'Teste 1', 'Descrição da Atividade', 3, '2019-08-14', '1030', 1, '2019-08-02', 1, 1, 3, '2019-08-05 17:38:56', 0, 5),
(12, 'Teste 1', 'Descrição da Atividade', 3, '2019-08-14', '1045', 1, '2019-08-02', 1, 1, 3, '2019-08-05 17:38:56', 2, 5),
(13, 'Teste 1', 'Descrição da Atividade', 3, '2019-08-15', '1000', 1, '2019-08-02', 1, 1, 3, '2019-08-05 17:38:56', 0, 5),
(14, 'Teste 1', 'Descrição da Atividade', 3, '2019-08-15', '1015', 1, '2019-08-02', 1, 1, 3, '2019-08-05 17:38:56', 0, 5),
(15, 'Teste 1', 'Descrição da Atividade', 3, '2019-08-15', '1030', 1, '2019-08-02', 1, 1, 3, '2019-08-05 17:38:56', 0, 5),
(16, 'Teste 1', 'Descrição da Atividade', 3, '2019-08-15', '1045', 1, '2019-08-02', 1, 1, 3, '2019-08-05 17:38:56', 0, 5);

-- --------------------------------------------------------

--
-- Estrutura da tabela `agendamentos`
--

CREATE TABLE `agendamentos` (
  `id` int(11) NOT NULL,
  `presenca` int(1) NOT NULL DEFAULT '0',
  `adesao` int(1) NOT NULL DEFAULT '0',
  `id_usuario` int(11) NOT NULL,
  `id_agenda` int(11) NOT NULL,
  `datadecriacao` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `agendamentos`
--

INSERT INTO `agendamentos` (`id`, `presenca`, `adesao`, `id_usuario`, `id_agenda`, `datadecriacao`) VALUES
(1, 5, 0, 5, 13, '2019-08-05 20:40:12'),
(2, 0, 0, 5, 12, '2019-08-05 20:42:12'),
(3, 0, 0, 5, 16, '2019-08-15 18:05:14');

-- --------------------------------------------------------

--
-- Estrutura da tabela `atividade`
--

CREATE TABLE `atividade` (
  `id` int(11) NOT NULL,
  `nome` varchar(255) NOT NULL,
  `duracao` varchar(10) NOT NULL,
  `ativo` int(1) NOT NULL,
  `descricao` text NOT NULL,
  `datadecriacao` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `atividade`
--

INSERT INTO `atividade` (`id`, `nome`, `duracao`, `ativo`, `descricao`, `datadecriacao`) VALUES
(1, 'Shiatsu', '0020', 0, 'Não Há', '2018-10-25 16:47:46'),
(2, 'Massagem', '0050', 0, 'Descrição da Atividade', '2018-11-28 21:21:46'),
(3, 'Shiatsu', '0015', 0, 'Shiatsu de 15 min', '2019-03-12 18:09:20'),
(4, 'Massagem', '0030', 0, 'Atividade Adesão', '2019-03-12 20:39:55'),
(5, 'Massagem', '0100', 2, 'Atividade Teste 2', '2019-03-12 20:41:09');

-- --------------------------------------------------------

--
-- Estrutura da tabela `aula`
--

CREATE TABLE `aula` (
  `id` int(11) NOT NULL,
  `nome` varchar(255) NOT NULL,
  `objetivo` varchar(255) NOT NULL,
  `descricao` text NOT NULL,
  `id_tipo` int(11) NOT NULL,
  `links` text NOT NULL,
  `caminho` longtext NOT NULL,
  `ativo` int(11) NOT NULL DEFAULT '0',
  `datadecriacao` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `aula`
--

INSERT INTO `aula` (`id`, `nome`, `objetivo`, `descricao`, `id_tipo`, `links`, `caminho`, `ativo`, `datadecriacao`) VALUES
(1, 'Teste', '', '', 1, '', 'D:/VertrigoServ/www/reabily/aula/2_1/2_1_20190626183306_0.pdf ; ', 1, '2019-06-26 18:33:06'),
(2, 'Imagem', '', '', 1, ' https://www.google.com', 'D:/VertrigoServ/www/reabily/aula/2_2/2_2_20190628151140_0.png ; ', 0, '2019-06-28 15:11:40'),
(3, 'Fotos', '', '', 1, '', '/home/vlannetw/vemsermovimento.com.br/aula/2_133/2_133_20190704182117_0.png ; ', 0, '2019-06-28 15:42:50'),
(4, 'PDF', '', '', 1, '', 'D:/VertrigoServ/www/reabily/aula/2_4/2_4_20190628205745_0.pdf ; ', 1, '2019-06-28 20:57:45'),
(5, 'PDF', '', '', 1, '', 'D:/VertrigoServ/www/reabily/aula/2_5/2_5_20190628205903_0.pdf ; ', 1, '2019-06-28 20:59:03'),
(6, 'PDF', '', '', 1, '', '', 0, '2019-06-28 20:59:33');

-- --------------------------------------------------------

--
-- Estrutura da tabela `aula_material`
--

CREATE TABLE `aula_material` (
  `id_material` int(11) NOT NULL,
  `id_aula` int(11) NOT NULL,
  `datadecriacao` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `aula_material`
--

INSERT INTO `aula_material` (`id_material`, `id_aula`, `datadecriacao`) VALUES
(1, 3, '2019-08-13 17:25:55'),
(3, 3, '2019-08-13 17:25:55');

-- --------------------------------------------------------

--
-- Estrutura da tabela `bancos`
--

CREATE TABLE `bancos` (
  `cod` int(11) NOT NULL,
  `nome` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `bancos`
--

INSERT INTO `bancos` (`cod`, `nome`) VALUES
(1, 'Banco do Brasil S.A.'),
(33, 'Banco Santander (Brasil) S.A.'),
(104, 'Caixa Econômica Federal'),
(237, 'Banco Bradesco S.A.'),
(341, 'Itaú Unibanco S.A.'),
(453, 'Banco Safra S.A.'),
(745, 'Banco Citibank S.A');

-- --------------------------------------------------------

--
-- Estrutura da tabela `contrato`
--

CREATE TABLE `contrato` (
  `id` int(11) NOT NULL,
  `id_empresa` int(11) NOT NULL,
  `apelido` varchar(255) NOT NULL,
  `tipo` int(11) NOT NULL,
  `valor` int(11) NOT NULL,
  `exigencia` varchar(255) NOT NULL,
  `id_atividade` int(11) NOT NULL,
  `data_inicio` date NOT NULL,
  `data_final` date NOT NULL,
  `obs` text NOT NULL,
  `ativo` int(11) NOT NULL DEFAULT '0',
  `id_renovacao` int(11) DEFAULT NULL,
  `datadecriacao` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estrutura da tabela `cronograma`
--

CREATE TABLE `cronograma` (
  `id` int(11) NOT NULL,
  `nome` varchar(255) NOT NULL,
  `descricao` text NOT NULL,
  `data` date NOT NULL,
  `hora` varchar(11) NOT NULL,
  `frequencia` int(11) NOT NULL,
  `quantidades` int(11) NOT NULL,
  `id_aula` int(11) NOT NULL,
  `id_empresa` int(11) NOT NULL,
  `id_funcionario` int(11) NOT NULL,
  `ativo` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `cronograma`
--

INSERT INTO `cronograma` (`id`, `nome`, `descricao`, `data`, `hora`, `frequencia`, `quantidades`, `id_aula`, `id_empresa`, `id_funcionario`, `ativo`) VALUES
(1, 'Teste Cronograma', 'Descrição de Cronograma Teste', '2019-08-12', '1300', 1, 30, 2, 1, 3, 0),
(2, 'Teste Cronograma', 'Descrição de Cronograma Teste', '2019-08-19', '1400', 1, 30, 2, 1, 3, 2),
(3, 'Teste Cronograma', 'Descrição de Cronograma Teste', '2019-08-21', '1300', 1, 30, 2, 1, 3, 2),
(4, 'Teste Cronograma ', 'Descrição de Cronograma Teste  Corrigido', '2019-08-21', '1400', 1, 30, 2, 1, 3, 0),
(5, 'Teste Cronograma', 'Descrição de Cronograma Teste', '2019-08-23', '1300', 1, 30, 2, 1, 3, 0),
(6, 'Teste Cronograma', 'Descrição de Cronograma Teste', '2019-08-23', '1400', 1, 30, 2, 1, 3, 0),
(7, 'Teste Cronograma', 'Descrição de Cronograma Teste', '2019-08-19', '1300', 1, 30, 2, 1, 3, 0),
(8, 'Teste Cronograma', 'Descrição de Cronograma Teste', '2019-08-19', '1400', 1, 30, 2, 1, 3, 0),
(9, 'Teste Cronograma', 'Descrição de Cronograma Teste', '2019-08-21', '1300', 1, 30, 2, 1, 3, 0),
(10, 'Teste Cronograma', 'Descrição de Cronograma Teste', '2019-08-21', '1400', 1, 30, 2, 1, 3, 0),
(11, 'Teste Cronograma', 'Descrição de Cronograma Teste', '2019-08-23', '1300', 1, 30, 2, 1, 3, 0),
(12, 'Teste Cronograma', 'Descrição de Cronograma Teste', '2019-08-23', '1400', 1, 30, 2, 1, 3, 0);

-- --------------------------------------------------------

--
-- Estrutura da tabela `curriculo`
--

CREATE TABLE `curriculo` (
  `id` int(11) NOT NULL,
  `nome` varchar(255) NOT NULL,
  `apelido` varchar(255) NOT NULL,
  `telefone` bigint(20) NOT NULL,
  `email` varchar(255) NOT NULL,
  `cep` int(11) NOT NULL,
  `estado` varchar(255) NOT NULL,
  `cidade` varchar(255) NOT NULL,
  `bairro` varchar(255) NOT NULL,
  `endereco` varchar(255) NOT NULL,
  `numero` int(11) NOT NULL,
  `complemento` varchar(255) NOT NULL,
  `formacao` text NOT NULL,
  `qualificacoes` text NOT NULL,
  `atividade` text NOT NULL,
  `datadeCriacao` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estrutura da tabela `empresa`
--

CREATE TABLE `empresa` (
  `id` int(11) NOT NULL,
  `id_empresa` int(11) NOT NULL,
  `sede` varchar(255) DEFAULT NULL,
  `municipal` bigint(20) DEFAULT NULL,
  `estadual` bigint(20) DEFAULT NULL,
  `nfe` text,
  `obs` text,
  `id_endereco` int(11) DEFAULT NULL,
  `id_tipo` int(1) NOT NULL,
  `datadecriacao` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `empresa`
--

INSERT INTO `empresa` (`id`, `id_empresa`, `sede`, `municipal`, `estadual`, `nfe`, `obs`, `id_endereco`, `id_tipo`, `datadecriacao`) VALUES
(1, 1, 'Matriz', 11111, 111, '9223372036854775807', '  OBs Matriz  ', 1, 3, '2019-07-16 19:57:12'),
(2, 1, '', 0, 0, '0', '   0   ', 2, 5, '2019-07-16 19:57:38');

-- --------------------------------------------------------

--
-- Estrutura da tabela `fornecedor`
--

CREATE TABLE `fornecedor` (
  `id` int(11) NOT NULL,
  `nome` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `telefone` bigint(20) NOT NULL,
  `material` varchar(255) NOT NULL,
  `valorunitario` int(11) NOT NULL,
  `quantidadepedido` int(11) NOT NULL,
  `descricao` text NOT NULL,
  `ativo` int(1) NOT NULL DEFAULT '0',
  `datadecriacao` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estrutura da tabela `frase`
--

CREATE TABLE `frase` (
  `id` int(11) NOT NULL,
  `frase` text NOT NULL,
  `ativo` int(1) NOT NULL,
  `datadecriacao` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `frase`
--

INSERT INTO `frase` (`id`, `frase`, `ativo`, `datadecriacao`) VALUES
(1, 'As pessoas costumam dizer que a motivação não dura sempre. Bem, nem o efeito do banho, por isso recomenda-se diariamente.', 0, '2018-11-12 20:47:06'),
(2, 'Toda ação humana, quer se torne positiva ou negativa, precisa depender de motivação.', 0, '2018-11-12 20:47:06'),
(3, 'A verdadeira motivação vem de realização, desenvolvimento pessoal, satisfação no trabalho e reconhecimento.', 0, '2018-11-12 20:47:06'),
(4, 'Eu faço da dificuldade a minha motivação. A volta por cima, vem na continuação.', 0, '2018-11-12 20:47:06');

-- --------------------------------------------------------

--
-- Estrutura da tabela `funcionario`
--

CREATE TABLE `funcionario` (
  `id` int(11) NOT NULL,
  `nomedoPai` varchar(255) DEFAULT NULL,
  `nomedaMae` varchar(255) DEFAULT NULL,
  `conjege` varchar(255) NOT NULL,
  `filiacao` varchar(255) NOT NULL,
  `nascimento` date DEFAULT NULL,
  `identidade` int(9) DEFAULT NULL,
  `identidadeOrgao` varchar(255) NOT NULL,
  `datadeExpedicao` date DEFAULT NULL,
  `naturalidade` varchar(255) DEFAULT NULL,
  `nacionalidade` varchar(255) DEFAULT NULL,
  `estadoCivil` int(2) DEFAULT NULL,
  `celular` bigint(20) NOT NULL,
  `trecado` int(11) NOT NULL,
  `nrecado` varchar(255) NOT NULL,
  `titulodeEleitor` bigint(12) DEFAULT NULL,
  `sessao` int(11) DEFAULT NULL,
  `zona` int(11) DEFAULT NULL,
  `passaporte` varchar(15) DEFAULT NULL,
  `reservista` bigint(20) NOT NULL,
  `registroprofissional` bigint(20) NOT NULL,
  `observacoesdocumento` text,
  `formacao` varchar(255) DEFAULT NULL,
  `qualificacao` text,
  `id_contratos` int(1) DEFAULT NULL,
  `funcao` varchar(255) DEFAULT NULL,
  `datadeDesligamento` date DEFAULT NULL,
  `motivo` text,
  `datadeAdmissão` date DEFAULT NULL,
  `pis` bigint(20) DEFAULT NULL,
  `ctps` int(11) DEFAULT NULL,
  `observacoes` text,
  `datadeCriacao` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `id_usuario` int(11) DEFAULT NULL,
  `salarioContratado` int(11) DEFAULT NULL,
  `cargaHoraria` int(11) DEFAULT NULL,
  `transporte` int(1) DEFAULT NULL,
  `alimentacao` int(1) DEFAULT NULL,
  `planodeSaude` int(1) DEFAULT NULL,
  `planoOdontologico` int(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estrutura da tabela `infobancario`
--

CREATE TABLE `infobancario` (
  `id` int(11) NOT NULL,
  `id_usuario` int(11) NOT NULL,
  `agencia` int(11) DEFAULT NULL,
  `conta` bigint(20) DEFAULT NULL,
  `tipodeconta` int(11) DEFAULT NULL,
  `banco` int(5) NOT NULL,
  `catadecriacao` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estrutura da tabela `material`
--

CREATE TABLE `material` (
  `id` int(11) NOT NULL,
  `material` varchar(255) NOT NULL,
  `descricao` text NOT NULL,
  `ativo` int(1) NOT NULL DEFAULT '0',
  `datadecriacao` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `material`
--

INSERT INTO `material` (`id`, `material`, `descricao`, `ativo`, `datadecriacao`) VALUES
(1, 'Cadeira', 'Utilizado para alongamento', 0, '2019-04-25 18:33:34'),
(2, 'Caneta', '', 0, '2019-04-25 18:34:03'),
(3, 'Anel', '', 0, '2019-05-13 17:30:39'),
(4, 'Material', '', 0, '2019-06-19 17:46:22');

-- --------------------------------------------------------

--
-- Estrutura da tabela `questionarios`
--

CREATE TABLE `questionarios` (
  `id` int(11) NOT NULL,
  `id_usuario` int(11) NOT NULL,
  `datadecriacao` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estrutura da tabela `redessociais`
--

CREATE TABLE `redessociais` (
  `id` int(11) NOT NULL,
  `facebook` varchar(255) DEFAULT NULL,
  `linkedin` varchar(255) DEFAULT NULL,
  `instagram` varchar(255) DEFAULT NULL,
  `id_usuario` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estrutura da tabela `resposta`
--

CREATE TABLE `resposta` (
  `id` int(11) NOT NULL,
  `id_questionarios` int(11) NOT NULL,
  `resposta` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estrutura da tabela `setor`
--

CREATE TABLE `setor` (
  `id` int(11) NOT NULL,
  `setor` varchar(255) DEFAULT NULL,
  `grupo` varchar(255) DEFAULT NULL,
  `andar` varchar(255) DEFAULT NULL,
  `id_empresa` int(11) NOT NULL,
  `ativo` int(1) NOT NULL DEFAULT '0',
  `datadeCriacao` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `setor`
--

INSERT INTO `setor` (`id`, `setor`, `grupo`, `andar`, `id_empresa`, `ativo`, `datadeCriacao`) VALUES
(1, 'Setor 1', 'Grupo 1', '1º Andar', 41, 1, '2019-06-04 19:56:55'),
(2, '', '', '3º Andar', 41, 1, '2019-06-10 19:15:07'),
(3, 'Setor 2', 'Grupo 1', 'Andar 2', 1, 0, '2019-08-05 18:12:45'),
(4, '', '', '2º ', 1, 0, '2019-08-06 19:05:27');

-- --------------------------------------------------------

--
-- Estrutura da tabela `sugestao`
--

CREATE TABLE `sugestao` (
  `id` int(11) NOT NULL,
  `email` varchar(255) NOT NULL,
  `sugestao` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estrutura da tabela `telefone`
--

CREATE TABLE `telefone` (
  `id` int(11) NOT NULL,
  `id_usuario` int(11) NOT NULL,
  `contato` varchar(255) DEFAULT NULL,
  `numero` bigint(20) DEFAULT NULL,
  `ramal` int(11) DEFAULT NULL,
  `id_tipo` int(1) NOT NULL,
  `datadecriacao` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `telefone`
--

INSERT INTO `telefone` (`id`, `id_usuario`, `contato`, `numero`, `ramal`, `id_tipo`, `datadecriacao`) VALUES
(1, 1, 'EU', 21111111111, 0, 3, '2019-07-16 16:57:12'),
(2, 1, '', 4444444444, 0, 5, '2019-07-16 16:57:38'),
(3, 5, 'EU', 21111111111, 0, 4, '2019-07-25 14:12:03'),
(4, 1, 'tu', 0, 0, 4, '2019-07-25 14:12:03'),
(5, 1, 'EU', 2121212121, 0, 4, '2019-07-25 14:13:25'),
(6, 1, 'tu', 21212222222, 0, 4, '2019-07-25 14:13:25'),
(7, 2, 'EU', 2121212121, 0, 1, '2019-07-25 14:14:23'),
(8, 2, 'tu', 21212222222, 0, 1, '2019-07-25 14:14:23'),
(9, 1, '', 0, 0, 4, '2019-07-25 14:42:58'),
(10, 2, '', 0, 0, 3, '2019-08-06 16:09:59'),
(11, 2, '', 0, 0, 5, '2019-08-06 16:16:19'),
(12, 1, '', 0, 0, 1, '2019-08-06 16:18:39'),
(13, 1, '', 0, 0, 2, '2019-08-06 16:22:01');

-- --------------------------------------------------------

--
-- Estrutura da tabela `tipo`
--

CREATE TABLE `tipo` (
  `id` int(11) NOT NULL,
  `nome` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `tipo`
--

INSERT INTO `tipo` (`id`, `nome`) VALUES
(1, 'Internos'),
(2, 'Profissionais'),
(3, 'Empresa Sede'),
(4, 'Cliente'),
(5, 'Empresa Filial');

-- --------------------------------------------------------

--
-- Estrutura da tabela `usuario`
--

CREATE TABLE `usuario` (
  `id` int(11) NOT NULL,
  `cpf` bigint(20) NOT NULL,
  `nome` varchar(60) NOT NULL,
  `apelido` varchar(30) NOT NULL,
  `email` varchar(255) NOT NULL,
  `nascimento` date DEFAULT NULL,
  `sexo` int(1) DEFAULT NULL,
  `foto` varchar(255) NOT NULL,
  `senha` varchar(255) NOT NULL,
  `ativo` int(1) NOT NULL DEFAULT '0',
  `id_tipo` int(11) NOT NULL,
  `id_empresa` int(3) NOT NULL,
  `datadecricao` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `usuario`
--

INSERT INTO `usuario` (`id`, `cpf`, `nome`, `apelido`, `email`, `nascimento`, `sexo`, `foto`, `senha`, `ativo`, `id_tipo`, `id_empresa`, `datadecricao`) VALUES
(2, 33333333333, 'Contato', 'leandrobaptistacombr', 'contato@leandrobaptista.com.br', '1995-09-25', 1, 'D:/VertrigoServ/www/reabily/usuarios/Usuario_Padrao.png', 'e10adc3949ba59abbe56e057f20f883e', 0, 1, 0, '2018-10-20 00:22:03'),
(3, 22222222222, 'Usuario', 'Teste', 'usuarioteste@teste.com', '1995-09-25', 1, 'D:/VertrigoServ/www/reabily/usuarios/Usuario_Padrao.png', 'e10adc3949ba59abbe56e057f20f883e', 0, 2, 0, '2019-07-25 16:01:03'),
(4, 1, 'usuario', 'test', 'foi@c', '1111-11-11', 1, 'D:/VertrigoServ/www/reabily/usuarios/Usuario_Padrao.png', 'e10adc3949ba59abbe56e057f20f883e', 0, 4, 1, '2019-07-25 17:14:23'),
(5, 33232323232, 'Auto', 'Cadastro', 'autocadastro@email.com', '1995-09-25', 3, 'D:/VertrigoServ/www/reabily/usuarios/Usuario_Padrao.png', 'e10adc3949ba59abbe56e057f20f883e', 0, 4, 1, '2019-07-25 17:42:58'),
(6, 1, 'Interno', 'Colaborador', 'interno@email.com', '1982-03-22', 3, 'D:/VertrigoServ/www/reabily/usuarios/Usuario_Padrao.png', 'e10adc3949ba59abbe56e057f20f883e', 0, 1, 1, '2019-08-06 19:18:39');

-- --------------------------------------------------------

--
-- Estrutura da tabela `usuario_empresa_filial`
--

CREATE TABLE `usuario_empresa_filial` (
  `id` int(11) NOT NULL,
  `id_empresa_sede` int(11) NOT NULL,
  `cnpj` bigint(15) NOT NULL,
  `social` varchar(255) NOT NULL,
  `fantasia` varchar(255) NOT NULL,
  `logo` varchar(255) NOT NULL,
  `sede` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `senha` varchar(255) NOT NULL,
  `ativo` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `usuario_empresa_filial`
--

INSERT INTO `usuario_empresa_filial` (`id`, `id_empresa_sede`, `cnpj`, `social`, `fantasia`, `logo`, `sede`, `email`, `senha`, `ativo`) VALUES
(1, 1, 11111111222222, 'Filial', 'Matriz', 'D:/VertrigoServ/www/reabily/usuarios/11111111111111/logo.png', 'Filial', 'filial@matriz.com.br', 'e10adc3949ba59abbe56e057f20f883e', 0),
(2, 2, 22222222222211, 'Filial 2', 'Filial 2', '/home/vlannetw/vlansolution.com.br/projetos/homologacao_reabily/usuarios', 'Filial 2', 'filial2@matriz.com.br', 'e10adc3949ba59abbe56e057f20f883e', 0);

-- --------------------------------------------------------

--
-- Estrutura da tabela `usuario_empresa_sede`
--

CREATE TABLE `usuario_empresa_sede` (
  `id` int(11) NOT NULL,
  `cnpj` bigint(20) NOT NULL,
  `social` varchar(255) NOT NULL,
  `fantasia` varchar(255) NOT NULL,
  `logo` text NOT NULL,
  `sede` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `senha` varchar(255) NOT NULL,
  `ativo` tinyint(1) NOT NULL DEFAULT '0',
  `datadecriacao` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `usuario_empresa_sede`
--

INSERT INTO `usuario_empresa_sede` (`id`, `cnpj`, `social`, `fantasia`, `logo`, `sede`, `email`, `senha`, `ativo`, `datadecriacao`) VALUES
(1, 11111111111111, 'Matriz 1', 'Matriz ', 'D:/VertrigoServ/www/reabily/usuarios/11111111111111/logo.png', 'Matriz', 'matriz@matriz.com.br', '', 0, '2019-07-16 16:57:12'),
(2, 22222222222222, 'Matriz 2', 'Matriz 2', '/home/vlannetw/vlansolution.com.br/projetos/homologacao_reabily/usuarios/22222222222222/logo.png', 'Matriz 2', 'matriz2@matriz.com.br', 'e10adc3949ba59abbe56e057f20f883e', 0, '2019-08-06 16:09:59');

-- --------------------------------------------------------

--
-- Estrutura da tabela `usuario_endereco`
--

CREATE TABLE `usuario_endereco` (
  `id` int(11) NOT NULL,
  `id_empresa` int(11) NOT NULL,
  `cep` int(11) NOT NULL,
  `estado` varchar(255) NOT NULL,
  `cidade` varchar(255) NOT NULL,
  `bairro` varchar(255) NOT NULL,
  `endereco` varchar(255) NOT NULL,
  `numero` int(11) NOT NULL,
  `complemento` varchar(255) NOT NULL,
  `id_tipo` int(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `usuario_endereco`
--

INSERT INTO `usuario_endereco` (`id`, `id_empresa`, `cep`, `estado`, `cidade`, `bairro`, `endereco`, `numero`, `complemento`, `id_tipo`) VALUES
(1, 1, 25071202, 'RJ', 'Duque de Caxias', 'Jardim Vinte e Cinco de Agosto', 'Avenida Perimetral Professor José de Souza Herdy', 1, 'de 534 ao fim - lado par', 3),
(2, 1, 21221300, 'RJ', 'Rio de Janeiro', 'Vila da Penha', 'Avenida Oliveira Belo', 500, 'loja', 5),
(3, 5, 21221300, 'RJ', 'Rio de Janeiro', 'Vila da Penha', 'Avenida Oliveira Belo', 300, 'loja', 4),
(4, 2, 21221300, 'RJ', 'Rio de Janeiro', 'Vila da Penha', 'Avenida Oliveira Belo', 0, '', 1),
(5, 1, 0, '', '', '', '', 0, '', 4),
(6, 2, 0, '', '', '', '', 0, '', 3),
(7, 2, 0, '', '', '', '', 0, '', 5),
(8, 1, 0, '', '', '', '', 0, '', 1),
(9, 1, 0, '', '', '', '', 0, '', 2);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `agenda`
--
ALTER TABLE `agenda`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `agendamentos`
--
ALTER TABLE `agendamentos`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_usuario` (`id_usuario`),
  ADD KEY `id_agenda` (`id_agenda`);

--
-- Indexes for table `atividade`
--
ALTER TABLE `atividade`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `aula`
--
ALTER TABLE `aula`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `aula_material`
--
ALTER TABLE `aula_material`
  ADD KEY `id_material` (`id_material`),
  ADD KEY `id_aula` (`id_aula`);

--
-- Indexes for table `bancos`
--
ALTER TABLE `bancos`
  ADD PRIMARY KEY (`cod`);

--
-- Indexes for table `contrato`
--
ALTER TABLE `contrato`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cronograma`
--
ALTER TABLE `cronograma`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `curriculo`
--
ALTER TABLE `curriculo`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `empresa`
--
ALTER TABLE `empresa`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `fornecedor`
--
ALTER TABLE `fornecedor`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `frase`
--
ALTER TABLE `frase`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `funcionario`
--
ALTER TABLE `funcionario`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `infobancario`
--
ALTER TABLE `infobancario`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `material`
--
ALTER TABLE `material`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `questionarios`
--
ALTER TABLE `questionarios`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `redessociais`
--
ALTER TABLE `redessociais`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `resposta`
--
ALTER TABLE `resposta`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `setor`
--
ALTER TABLE `setor`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sugestao`
--
ALTER TABLE `sugestao`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `telefone`
--
ALTER TABLE `telefone`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tipo`
--
ALTER TABLE `tipo`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id` (`id`);

--
-- Indexes for table `usuario`
--
ALTER TABLE `usuario`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_tipo` (`id_tipo`),
  ADD KEY `id_empresa` (`id_empresa`);

--
-- Indexes for table `usuario_empresa_filial`
--
ALTER TABLE `usuario_empresa_filial`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `usuario_empresa_sede`
--
ALTER TABLE `usuario_empresa_sede`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `usuario_endereco`
--
ALTER TABLE `usuario_endereco`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `agenda`
--
ALTER TABLE `agenda`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT for table `agendamentos`
--
ALTER TABLE `agendamentos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `atividade`
--
ALTER TABLE `atividade`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `aula`
--
ALTER TABLE `aula`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `contrato`
--
ALTER TABLE `contrato`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `cronograma`
--
ALTER TABLE `cronograma`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `curriculo`
--
ALTER TABLE `curriculo`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `empresa`
--
ALTER TABLE `empresa`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `fornecedor`
--
ALTER TABLE `fornecedor`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `frase`
--
ALTER TABLE `frase`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `funcionario`
--
ALTER TABLE `funcionario`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `infobancario`
--
ALTER TABLE `infobancario`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `material`
--
ALTER TABLE `material`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `questionarios`
--
ALTER TABLE `questionarios`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `redessociais`
--
ALTER TABLE `redessociais`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `resposta`
--
ALTER TABLE `resposta`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `setor`
--
ALTER TABLE `setor`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `sugestao`
--
ALTER TABLE `sugestao`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `telefone`
--
ALTER TABLE `telefone`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `tipo`
--
ALTER TABLE `tipo`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `usuario`
--
ALTER TABLE `usuario`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `usuario_empresa_filial`
--
ALTER TABLE `usuario_empresa_filial`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `usuario_empresa_sede`
--
ALTER TABLE `usuario_empresa_sede`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `usuario_endereco`
--
ALTER TABLE `usuario_endereco`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- Constraints for dumped tables
--

--
-- Limitadores para a tabela `aula_material`
--
ALTER TABLE `aula_material`
  ADD CONSTRAINT `aula_material_ibfk_1` FOREIGN KEY (`id_material`) REFERENCES `material` (`id`),
  ADD CONSTRAINT `aula_material_ibfk_2` FOREIGN KEY (`id_aula`) REFERENCES `aula` (`id`);

--
-- Limitadores para a tabela `usuario`
--
ALTER TABLE `usuario`
  ADD CONSTRAINT `usuario_ibfk_1` FOREIGN KEY (`id_tipo`) REFERENCES `tipo` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
