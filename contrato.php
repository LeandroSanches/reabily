<?php
include_once( 'cabecalho.php' );
include_once( 'model/contrato.php' );
include_once( 'model/usuario.php' );
include_once( 'model/atividade.php' );
include_once( 'model/empresa.php' );


if ( isset( $_SESSION[ "acao" ][ 'acao' ] ) ) {

	if ( $_SESSION[ "acao" ][ 'acao' ] === 'listar' ) {

		date_default_timezone_set( 'America/Sao_Paulo' );
		$date = date( 'Y-m-d' );
		?>


		<div class="row container">
			<h2>Contratos:</h2>
			<h5>Contrato prestes a vencer ou vencidos:</h5>
			<table>
				<thead>
					<tr>
						<th>Nome:</th>
						<th>Empresa:</th>
						<th>Data Final:</th>
						<th></th>
					</tr>
				</thead>

				<tbody>

					<?php
					$contratos = listarContratosBD( $conexao );		
					$i = 0;
						foreach ( $contratos as $contratos ) {
							$contratos[ "data_final" ];
							$dataAviso = date( 'Y-m-d', strtotime( '-3 month', strtotime( $contratos[ "data_final" ] ) ) );

							if ( $dataAviso <= $date ) {
								$cor = "red-text";	$i++; ?>

						<tr class="<?=$cor?>">
							<td>
								<?= $contratos[ "apelido" ]?>
							</td>
							<td>
								<?php if($contratos[ "id_empresa" ] != 0){
									$empresa = listarEmpresaFilial($conexao, $contratos[ "id_empresa" ]);
									echo $empresa[0]['sede'];
								} else {
									echo "Empresa nao Encontrada." ;
							}?>
							</td>
							<td>
								<?= date('d/m/Y', strtotime($contratos[ "data_final" ]))?>
							</td>
							<td><a href="contrato.php?acao=editar&token=<?=$contratos['id']?>"><i class="material-icons white-text" title="Editar">edit</i></a>
								<a href="contrato.php?acao=renovar&token=<?=$contratos['id']?>"><i class="material-icons white-text" title="Renovar">autorenew</i></a>
								<a href="#modal<?=$contratos['id']?>" class="modal-trigger"><i class="material-icons amber-text" title="Cancelar">closer</i></a>
							</td>
						</tr>
						
						<!-- Modal Structure -->
						  <div id="modal<?=$contratos['id']?>" class="modal">
							<div class="modal-content black-text">
							  <h4>Cancelamento de Contrato</h4>
							  <p>Deseja Cancelar o Contrato <strong><?=$contratos['apelido']?></strong>, COD:<?=$contratos['id']?>?</p>
							  <p><small> *Essa ação nao poderar ser desfeita!</small></p>
							</div>
							<div class="modal-footer">
							  <a href="contrato.php?acao=cancelar&token=<?=$contratos['id']?>" class="modal-close waves-effect waves-green btn-flat">Sim</a>
							</div>
						  </div>

						<?php } 
							
						} 
					if ( $i === 0 ) { ?>
					<tr>
						<td> Não ha contratos á vencer :) </td>
						<td>  </td>
						<td>  </td>
					</tr>
						
					<?php }  ?>
					
				</tbody>
			</table>
		</div>
		
		<br>
		<br>
		<br>
		
		<div class="row container">
			<h5>Todos os contratos ativos:</h5>
			<table>
				<thead>
					<tr>
						<th>Nome:</th>
						<th>Empresa:</th>
						<th>Data Final:</th>
						<th></th>
					</tr>
				</thead>

				<tbody>

				<?php	
				$contratos = listarContratos2BD( $conexao );
				foreach ( $contratos as $contratos ) {
					$contratos[ "data_final" ]; 
					$dataAviso = date('Y-m-d',strtotime('-3 month', strtotime($contratos[ "data_final" ])));

					if($dataAviso <= $date){
						$cor = "red-text";
						$renovar = '<a href="contrato.php?acao=renovar&token='.$contratos['id'].'"><i class="material-icons white-text" title="Renovar">autorenew</i></a>';
						$cancelar = '<a href="#modal'.$contratos['id'].'" class="modal-trigger"><i class="material-icons amber-text" title="Cancelar">closer</i></a>';
					} else { $cor = ""; $renovar = ""; $cancelar = ""; }

					?>


					<tr class="<?=$cor?>">
						<td>
							<?= $contratos[ "apelido" ]?>
						</td>
						<td>
							<?php if($contratos[ "id_empresa" ] != 0){
									$empresa = listarEmpresaFilial($conexao, $contratos[ "id_empresa" ]);
									echo $empresa[0]['sede'];
								} else {
									echo "Empresa nao Encontrada." ;
							}?>
						</td>
						<td>
							<?= date('d/m/Y', strtotime($contratos[ "data_final" ]))?>
						</td>
						<td><a href="contrato.php?acao=editar&token=<?=$contratos['id']?>"><i class="material-icons white-text">edit</i></a>
							<?=$renovar?>
							<?=$cancelar?>
						</td>
					</tr>

					<?php } ?>
				</tbody>
			</table>
		</div>

		<div class="row">
			<div class="col s6 m3 offset-m1" align="center">
				<a href="dashboard.php" class="btn waves-effect waves-light">Voltar</a> 
			</div>			

			<div class="col s6 m3 offset-m3">
				<a href="contrato.php?acao=novo" class="btn3 waves-effect waves-light">Novo Cadastro</a>
			</div>
		</div>
		<?php
	}

	if ( $_SESSION[ "acao" ][ 'acao' ] === 'editar' ) {
		$contrato = listarContratoBD( $conexao, $_SESSION[ "acao" ][ 'id' ] );
		$empresa = listarSedeEmpresasBD( $conexao );	
		$atividade = listarAtividadesBD($conexao);		
		?>

		<br>
		<div class="container row">

			<h2>Editar Contrato </h2>

			<div class="row">
				<form class="col s12" action="controller/contrato.php?acao=alterar" method="post" accept-charset="UTF-8">
				
					<div class="row">
					
						<div class="input-field col s12 m6 hide">
							<input type="number" name="id" value="<?=$contrato[0]['id']?>">
						</div>
					
						<div class="input-field col s12 m6">
							<i class="material-icons prefix">business</i>
							<select name="id_empresa">
								<option value="" disabled selected></option>
								<?php foreach ( $empresa as $empresa ){?>						
								<optgroup value="<?=$empresa["id"];?>"label="<?=$empresa["sede"];?>" data-icon="<?php echo $empresa['logo'] = strstr($empresa['logo'],'usuarios/');?>">		
								<?php $filial = listarEmpresaSedeFilial( $conexao, $empresa["id"] );
									foreach ( $filial as $filial ){
									if ($contrato[0]["id_empresa"] === $filial["id"]){ $selected = 'selected';} 
									else {$selected = '';}?>						
									<option value="<?=$filial["id"];?>" data-icon="<?php echo $filial['logo'] = strstr($filial['logo'],'usuarios/');?>"  <?=$selected;?>  ><?=$filial["sede"];?></option>
									<?php } ?>
								</optgroup>
								<?php } ?>
							</select>
							<label>Empresa:</label>
						</div>
							
						<div class="input-field col s12 m6">
							<i class="material-icons prefix">account_circle</i>
							<input id="nome" type="text" class="validate" name="nome" value="<?=$contrato[0]['apelido']?>">
							<label for="nome">Nome</label>
						</div>
											
						<div class="input-field col s12 m6">
							<i class="material-icons prefix">merge_type</i>
							<select name="tipo">
								<option value="" disabled selected>Escolha uma Opção</option>	
								<?php switch ($contrato[0]['tipo']) {
										case 1:
											$tipoEvento1 =  "selected";
											break;
										case 2:
											$tipoEvento2 =  "selected";
											break;
									}?>					
								<option value="1" <?=$tipoEvento1?> >Evento</option>								
								<option value="2" <?=$tipoEvento2?> >Fixo</option>								
							</select>
							<label>Tipo de Contrato</label>
						</div>
						
						<div class="input-field col s12 m6">
							<i class="material-icons prefix">attach_money</i>
							<input id="valor" type="text" class="validate money" name="valor" value="<?=$contrato[0]['valor']?>">
							<label for="valor">Valor</label>
						</div>
						
						<div class="input-field col s12 m6">
							<i class="material-icons prefix">assistant_photo</i>
							<input id="exigencia" type="text" class="validate" name="exigencia" value="<?=$contrato[0]['exigencia']?>">
							<label for="exigencia">Exigência</label>
						</div>
						
						<div class="input-field col s12 m6">
							<i class="material-icons prefix">directions_run</i>
							<select name="atividade">
								<option value="" disabled selected>Escolha uma Atividade</option>
								<?php foreach ( $atividade as $atividade ){
								if ($contrato[0]["id_atividade"] === $atividade["id"]){ $selected = 'selected';} 
								else {$selected = '';}							?>
								
								<option value="<?=$atividade["id"]?>" <?=$selected?> > <?=$atividade["nome"]?> <small>(<?php
								$ativQTD = strlen($atividade["duracao"]);
									switch ($ativQTD) {
										case 1:
											echo mask($atividade["duracao"], '00:0#');
											break;
										case 2:
											echo mask($atividade["duracao"], '00:##');
											break;
										case 3:
											echo mask($atividade["duracao"], '0#:##');
											break;
										case 4:
											echo mask($atividade["duracao"], '##:##');
											break;
									}?>)</small> </option>
								<?php } ?>
							</select>
							<label for="atividade">Atividade</label>
						</div>
						
					</div>
					
					<div class="row">
						<div class="input-field col s12 m6">
							<i class="material-icons prefix">trending_up</i>
							<input id="inicio" type="date" class="validate" name="date_inicio"  value="<?=$contrato[0]['data_inicio']?>">
							<label for="inicio">Inicio</label>
						</div>
						<div class="input-field col s12 m6">
							<i class="material-icons prefix">trending_down</i>
							<input id="final" type="date" class="validate" name="date_final"  value="<?=$contrato[0]['data_final']?>">
							<label for="final">Final</label>
						</div>
					</div>
					
					<div class="row">
						<div class="input-field col s12">
							<i class="material-icons prefix">report_problem</i>
							<textarea id="obs" type="text" class="materialize-textarea" name="obs"><?=$contrato[0]['obs']?></textarea>
							<label for="obs">Observações</label>
						</div>
					</div>
					
					<div class="row">
						<div class="col s6 m3 offset-m1" align="center">
							<a href="contrato.php?acao=listar" class="btn waves-effect waves-light">Voltar</a> </div>
						<div class="col s6 m6">
							<button class="btn2 waves-effect waves-light right" type="submit">Alterar </button>
						</div>
					</div>
					
				</form>
			</div>
		</div>
		<?php
	}

	if ( $_SESSION[ "acao" ][ 'acao' ] === 'renovar' ) {
		$contrato = listarContratoBD( $conexao, $_SESSION[ "acao" ][ 'id' ] );
		$empresa = listarSedeEmpresasBD( $conexao );			
		$atividade = listarAtividadesBD($conexao);	

		?>

		<br>
		<div class="container row">

			<h2>Renovar Contrato</h2>

			<div class="row">
				<form class="col s12" action="controller/contrato.php" method="post" accept-charset="UTF-8">
					<div class="row">
						<input type="text" name="id_empresa" value="<?= $contrato[0]['id']?>" class="hide">
						
					
						<div class="input-field col s12 m6">
							<i class="material-icons prefix">business</i>
							<select name="id_empresa">
								<option value="" disabled selected></option>
								<?php foreach ( $empresa as $empresa ){?>						
								<optgroup value="<?=$empresa["id"];?>"label="<?=$empresa["sede"];?>" data-icon="<?php echo $empresa['logo'] = strstr($empresa['logo'],'usuarios/');?>">		
								<?php $filial = listarEmpresaSedeFilial( $conexao, $empresa["id"] );
									foreach ( $filial as $filial ){
									if ($contrato[0]["id_empresa"] === $filial["id"]){ $selected = 'selected';} 
									else {$selected = '';}?>						
									<option value="<?=$filial["id"];?>" data-icon="<?php echo $filial['logo'] = strstr($filial['logo'],'usuarios/');?>"  <?=$selected;?>  ><?=$filial["sede"];?></option>
									<?php } ?>
								</optgroup>
								<?php } ?>
							</select>
							<label>Empresa:</label>
						</div>
						
						<div class="input-field col s12 m6">
							<i class="material-icons prefix">assignment_ind</i>
							<input id="nome" type="text" class="validate" name="nome" value="<?= $contrato[0]['apelido']?>">
							<label for="nome">Nome</label>
						</div>	
						
						<div class="input-field col s12 m6">
							<i class="material-icons prefix">merge_type</i>
							<select name="tipo">
								<option value="" disabled selected>Escolha uma Opção</option>	
								<?php switch ($contrato[0]['tipo']) {
										case 1:
											$tipoEvento1 =  "selected";
											break;
										case 2:
											$tipoEvento2 =  "selected";
											break;
									}?>					
								<option value="1" <?=$tipoEvento1?> >Evento</option>								
								<option value="2" <?=$tipoEvento2?> >Fixo</option>								
							</select>
							<label>Tipo de Contrato</label>
						</div>				
						
						<div class="input-field col s12 m6">
							<i class="material-icons prefix">attach_money</i>
							<input id="valor" type="text" class="validate money" name="valor" value="<?= $contrato[0]['valor']?>">
							<label for="valor">Valor</label>
						</div>	
						
						<div class="input-field col s12 m6">
							<i class="material-icons prefix">assistant_photo</i>
							<input id="exigencia" type="text" class="validate" name="exigencia" value="<?=$contrato[0]['exigencia']?>">
							<label for="exigencia">Exigência</label>
						</div>
						
						<div class="input-field col s12 m6">
							<i class="material-icons prefix">directions_run</i>
							<select name="atividade">
								<option value="" disabled selected>Escolha uma Atividade</option>
								<?php foreach ( $atividade as $atividade ){
								if ($contrato[0]["id_atividade"] === $atividade["id"]){ $selected = 'selected';} 
								else {$selected = '';}							?>
								
								<option value="<?=$atividade["id"]?>" <?=$selected?> > <?=$atividade["nome"]?> <small>(<?php
								$ativQTD = strlen($atividade["duracao"]);
									switch ($ativQTD) {
										case 1:
											echo mask($atividade["duracao"], '00:0#');
											break;
										case 2:
											echo mask($atividade["duracao"], '00:##');
											break;
										case 3:
											echo mask($atividade["duracao"], '0#:##');
											break;
										case 4:
											echo mask($atividade["duracao"], '##:##');
											break;
									}?>)</small> </option>
								<?php } ?>
							</select>
							<label for="atividade">Atividade</label>
						</div>
						
						<div class="input-field col s12 m3 hide">
							<input id="id_renovacao" type="number" name="id_renovacao" value="<?= $contrato[0]['id']?>">
							<label for="id_renovacao">id_renovacao</label>
						</div>
					</div>
					
					<div class="row">
						<div class="input-field col s12 m6">
							<i class="material-icons prefix">trending_up</i>
							<input id="inicio" type="date" class="validate" name="date_inicio" value="<?= $contrato[0]['data_inicio']?>">
							<label for="inicio"> Inicio</label>
						</div>
						<div class="input-field col s12 m6">
							<i class="material-icons prefix">trending_down</i>
							<input id="final" type="date" class="validate" name="date_final" value="<?= $contrato[0]['data_final']?>">
							<label for="final">Final</label>
						</div>
					</div>
					
					<div class="row">
						<div class="input-field col s12">
							<i class="material-icons prefix">report_problem</i>
							<textarea id="obs" type="text" class="materialize-textarea" name="obs"><?=$contrato[0]['obs']?></textarea>
							<label for="obs">Observações</label>
						</div>
					</div>

					<div class="row">
						<div class="col s6 m3 offset-m1" align="center">
							<a href="contrato.php?acao=listar" class="btn waves-effect waves-light">Voltar</a> </div>
						<div class="col s6 m6">
							<button class="btn2 waves-effect waves-light right" type="submit">Renovar </button>
						</div>
					</div>
				</form>
			</div>
			
			<hr>

			<h5 align="center">Contrato Antigo</h5>
			<div class="row">
			
				<div class="col s12 m6">
					<p>Empresa:
						<?php $empresa = listarEmpresaFilial( $conexao, $contrato[0]["id_empresa"] );
								echo $empresa[0]['sede'];?>
					</p>
				</div>
				
				<div class="col s12 m6">
					<p>Nome:
						<?= $contrato[0]['apelido']?>
					</p>
				</div>	
				
				<div class="col s12 m4">
					<p>Valor: R$
						<spam class='money'><?= $contrato[0]['valor']?></spam>
					</p>
				</div>
				
				<div class="col s12 m4">
					<p>Inicio:
						<?= date('d/m/Y', strtotime($contrato[0]['data_inicio']))?>
					</p>
				</div>
				
				<div class="col s12 m4">
					<p>Final:
						<?= date('d/m/Y', strtotime($contrato[0]['data_final']))?>
					</p>
				</div>
				
			</div>


		</div>
		<?php
	}

	if ( $_SESSION[ "acao" ][ 'acao' ] === 'novo' ) {

		$empresa = listarSedeEmpresasBD($conexao);		
		$atividade = listarAtividadesBD($conexao);
		?>
		<br>
		<div class="container row">

			<h2>Novo Contrato</h2>

			<div class="row">
				<form class="col s12" action="controller/contrato.php" method="post" accept-charset="UTF-8">
					<div class="row">						

						<div class="input-field col s12 m6">
							<select name="id_empresa">
								<option value="" disabled selected></option>
								<?php foreach ( $empresa as $empresa ){
								if ($usuario[0]["id_empresa"] === $empresa["id"]){ $selected = 'selected';} 
								else {$selected = '';}
								?>						
								<optgroup value="<?=$empresa["id"];?>" <?=$selected;?> label="<?=$empresa["sede"];?>" data-icon="<?php echo $empresa['logo'] = strstr($empresa['logo'],'usuarios/');?>">		
								<?php $filial = listarEmpresaSedeFilial( $conexao, $empresa["id"] );
									foreach ( $filial as $filial ){?>						
									<option value="<?=$filial["id"];?>" data-icon="<?php echo $filial['logo'] = strstr($filial['logo'],'usuarios/');?>"><?=$filial["sede"];?></option>
									<?php } ?>
								</optgroup>
								<?php } ?>
							</select>
							<label>Empresa:</label>
						</div>
							
						<div class="input-field col s12 m6">
							<i class="material-icons prefix">account_circle</i>
							<input id="nome" type="text" class="validate" name="nome">
							<label for="nome">Nome</label>
						</div>
											
						<div class="input-field col s12 m6">
							<i class="material-icons prefix">merge_type</i>
							<select name="tipo">
								<option value="" disabled selected>Escolha uma Opção</option>								
								<option value="1">Evento</option>								
								<option value="2">Fixo</option>								
							</select>
							<label>Tipo de Contrato</label>
						</div>
						
						<div class="input-field col s12 m6">
							<i class="material-icons prefix">attach_money</i>
							<input id="valor" type="text" class="validate money" name="valor">
							<label for="valor">Valor</label>
						</div>
						
						<div class="input-field col s12 m6">
							<i class="material-icons prefix">assistant_photo</i>
							<input id="exigencia" type="text" class="validate" name="exigencia">
							<label for="exigencia">Exigência</label>
						</div>
						
						<div class="input-field col s12 m6">
							<i class="material-icons prefix">directions_run</i>
							<select name="atividade">
								<option value="" disabled selected>Escolha uma Atividade</option>
								<?php foreach ( $atividade as $atividade ){?>
								<option value="<?=$atividade["id"]?>"> <?=$atividade["nome"]?> <small>(<?php
								$ativQTD = strlen($atividade["duracao"]);
									switch ($ativQTD) {
										case 1:
											echo mask($atividade["duracao"], '00:0#');
											break;
										case 2:
											echo mask($atividade["duracao"], '00:##');
											break;
										case 3:
											echo mask($atividade["duracao"], '0#:##');
											break;
										case 4:
											echo mask($atividade["duracao"], '##:##');
											break;
									}?>)</small> </option>
								<?php } ?>
							</select>
							<label for="atividade">Atividade</label>
						</div>
						
						<!--<div class="input-field col s12 m6">
							<i class="material-icons prefix">email</i>
							<input id="email" type="email" class="validate" name="email">
							<label for="email">Email</label>
						</div>
						<div class="input-field col s12 m4">
							<i class="material-icons prefix">phone</i>
							<input id="telefone" type="text" class="validate" name="telefone" data-mask="(00) 000-000-000">
							<label for="telefone">Telefone</label>
						</div>-->
						
					</div>
					<div class="row">
						<div class="input-field col s12 m6">
							<i class="material-icons prefix">trending_up</i>
							<input id="inicio" type="date" class="validate" name="date_inicio">
							<label for="inicio">Inicio</label>
						</div>
						<div class="input-field col s12 m6">
							<i class="material-icons prefix">trending_down</i>
							<input id="final" type="date" class="validate" name="date_final">
							<label for="final">Final</label>
						</div>
					</div>
					
					<div class="row">
						<div class="input-field col s12">
							<i class="material-icons prefix">report_problem</i>
							<textarea id="obs" type="text" class="materialize-textarea" name="obs"></textarea>
							<label for="obs">Observações</label>
						</div>
					</div>

					<div class="row">
						<div class="col s6 m3 offset-m1" align="center">
							<a href="dashboard.php" class="btn waves-effect waves-light">Voltar</a> </div>
						<div class="col s6 m6">
							<button class="btn2 waves-effect waves-light right" type="submit">Cadastrar</button>
						</div>
					</div>
				</form>
			</div>
		</div>
		<?php
	}
	
	if ( $_SESSION[ "acao" ][ 'acao' ] ===  'cancelar') {
		
		$array = array( $_SESSION[ "acao" ][ 'id' ], 1 );

		if ( excluirContratoBD( $conexao, $array ) ){
			$_SESSION[ "msg" ] = 'Contrato Cancelado';
			unset($_SESSION[ "acao" ]);

			echo '<script> window.location.replace("contrato.php?acao=listar"); </script>';

		} else {
			$msg = mysqli_error( $conexao );
			echo $_SESSION[ "msg" ] = 'Contrato não Cancelado .<br>	Erro:' . $msg . '	</div>';
			unset($_SESSION[ "acao" ]);

			echo '<script> window.location.replace("contrato.php?acao=listar"); </script>';
			}
	}

}

if ( !empty( $_GET[ 'acao' ] ) ) {

	echo $_SESSION[ "acao" ][ 'acao' ] = $_GET[ 'acao' ];
	echo $_SESSION[ "acao" ][ 'id' ] = $_GET[ 'token' ];
	echo $_SESSION[ "acao" ][ 'tipo' ] = $_GET[ 'tipo' ];

	echo '<script>window.location.replace("contrato.php");</script>';
}

include_once( 'rodape.php' );
?>