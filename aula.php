<?php include_once( 'cabecalho.php' );
echo ' <style> body{color: aliceblue;}</style>';

if ( isset( $_SESSION[ "acao" ][ 'acao' ] ) ) {
	
	//$URL = 'http://127.0.0.1/reabily';				// Teste
	$URL = 'https://sistema.vemsermovimento.com.br';	//Produção

	if ( $_SESSION[ "acao" ][ 'acao' ] === 'listar' ) {	
		
		include_once( 'model/aula.php' );		
		include_once( 'model/material.php' );
		
		$aula = listarAulaBD($conexao);
		?>

		<div class="row container">
			<h2>Aulas:</h2>
			<table>
				<thead>
					<tr>
						<th>Cod.:</th>
						<th>Nome:</th>
						<th></th>
					</tr>
				</thead>

				<tbody>

					<?php foreach ( $aula as $aula ) { ?>

						<tr>
							<td><a  class="white-text modal-trigger" href="#modalVisualizar<?=$aula['id']?>">
								<?= $aula[ "id" ]?>
							</a></td>
							<td><a  class="white-text modal-trigger" href="#modalVisualizar<?=$aula['id']?>">
								<?= $aula[ "nome" ]?>
							</a></td>
							<td><a href="aula.php?acao=editar&token=<?=$aula['id']?>"><i class="material-icons white-text" title="Editar">edit</i></a>
								<a class="modal-trigger Link" href="#modal<?=$aula['id']?>" title="Excluir"><i class="material-icons prefix amber-text text-darken-1">cancel</i></a>
							</td>
						</tr>
						
						<div id="modal<?=$aula['id']?>" class="modal bottom-sheet">
							<div class="row">
								<div class="col s12 m6">
									<div class="modal-content">
									  <h4 class="grey-text text-darken-2">Deseja excluir <?=$aula['nome']?>?</h4>
									  <p class="grey-text text-darken-2">Essa ação não poderá ser desfeita.</p>
									</div>
								</div>
								<div class="col s12 m6 Top">
									<div class="modal-footer">
										<a href="aula.php" class="btn waves-effect waves-light">Voltar</a>
										<a href="controller/aula.php?acao=excluir&token=<?=$aula['id']?>" class="modal-close btn2 waves-effect waves-light">Eu aceito</a>
									</div>
								</div>
							</div>
						  </div>
						  
						<div id="modalVisualizar<?=$aula['id']?>" class="modal" style=" background-color: #008aae;">
							<i class="material-icons right modal-close" style="padding: 15px;">close</i>
							<div class="container">	

								<div class="row ">
								<p align="center">Visualizador de Aula.</p>
								<p>COD.:<?=$aula['id']?></p>
								<p>Nome: <?=$aula['nome']?></p>
								<p>Objetivo: <?=$aula['objetivo']?></p>
								<p>Descrição: <?=$aula['descricao']?></p>
								<p>Tipo: <?php switch ($aula['id_tipo']){										
													case 1:
														echo "Clássico";
														break;												
													case 2:
														echo "Motivacional";
														break;												
													case 3:
														echo "Temático";
														break;												
													case 4:
														echo "Zem";
														break;
												}?></p>
								<p>Material: <?php $AulaMaterial = listarAulaMaterialBD($conexao, $aula['id']);
													foreach($AulaMaterial as $AulaMaterial){
														$material = listarMaterialIdBD($conexao, $AulaMaterial['id_material']);
														echo " - ".$material[0]['material'];
													}?></p>
								<p>Link: <br> <?php $array = explode(';',$aula['links']);
													if( empty( $aula['links'] ) ){
														echo 'Não há link para Visualizar';
													}
													else{
														for($i = 0; $i < count($array); $i++){														
														echo '<a href="'.$array[$i].'" target="_blank" class="white-text" > '.$array[$i].'</a> <br>';
														}  
													}	?></p>								
								</div>	
								
								<div class="row ">
								<hr>
								<p>Arquivos:</p>	
								<ul>							
								<?php $array = explode(' ; ',$aula['caminho']);
										unset($arrayIMG);
										unset($arraydoc);
									if( empty( $aula['caminho'] ) ){
										echo 'Não há arquivos para Visualizar';
									}
									else{
										for($i = 0; $i < count($array); $i++){
											if( !empty( $array[$i] ) ){
												$extencao = strrchr($array[$i],'.');
												$nome = strrchr($array[$i],'/');
												$pasta = strstr($array[$i],'/aula');
												$link = $URL.$pasta;
												if($extencao === '.png' || $extencao === '.jpg' || $extencao === '.jpeg'){ 	$arrayIMG[$i] = array('nome' => $nome, 'link' => $link);}
												elseif($extencao === '.pdf' ){ 	$arraydoc[$i] = array('nome' => $nome, 'link' => $link);}
												else{?>
												<li type="circle">
													<a href="<?=$link?>" class="white-text" download> <?=$nome?></a> 
												</li>
												<?php }												
											}
										} ?>
								  </ul>
										<?php if( !empty($arrayIMG)){
											 foreach($arrayIMG as $arrayIMG){ ?> 
											<div class="col s12 m6 l4"> <a href="<?=$arrayIMG['link']?>" target="_blank"><img src="<?=$arrayIMG['link']?>" width="100%"></a>
											</div>
											<?php } 
											}
										if( !empty($arraydoc)){
											 foreach($arraydoc as $arraydoc){ ?> 
											<div class="col s12"> 
												<p><?=$arraydoc['nome']?></p>
												<iframe src="<?=$arraydoc['link']?>" width="100%" height="500px"></iframe>
											</div>
											<?php } 
											}
									} ?>					
								
								</div>
							</div>

						  </div>

					<?php } ?>
					
				</tbody>
			</table>
		</div>

		
		<div class="row">
			<div class="col s6 m3 offset-m1" align="center">
				<a href="dashboard.php" class="btn waves-effect waves-light">Voltar</a> 
			</div>			

			<div class="col s6 m3 offset-m3">
				<a href="aula.php?acao=novo" class="btn3 waves-effect waves-light">Novo Cadastro</a>
			</div>
		</div>
		
<?php  }
	
	if ( $_SESSION[ "acao" ][ 'acao' ] === 'novo' ) {	
		include_once( 'model/material.php' );
		$material = listarMaterialBD($conexao);
		 //Listagem alfabetica
		function abc($a, $b){ return $a['material'] > $b['material'];}
		usort($material,'abc');
		unset($_FILES['arquivo']);

		?>
		<br>
		<div class="row container">
			<h2>Nova Aula:</h2>
			<div class="row">
				<form class="col s12" action="controller/aula.php?acao=novo" method="post" accept-charset="UTF-8" enctype="multipart/form-data">
					<div class="row">
							
						<div class="input-field col s12 hide">
							<input id="acao" type="text" class="validate" name="acao" value="novo">
							
						</div>
							
						<div class="input-field col s12 m6">
							<input id="nome" type="text" class="validate" name="nome">
							<label for="nome">Nome:</label>
						</div>
						
						<div class="input-field col s12 m6">
							<input id="objetivo" type="text" class="validate" name="objetivo">
							<label for="objetivo">Objetivo:</label>
						</div>
						
						<div class="input-field col s12 m6">
							<select name="id_tipo">
							  <option value="1">Clássico</option>
							  <option value="2">Motivacional</option>
							  <option value="3">Temático</option>
							  <option value="4">Zen</option>
							</select>
							<label>Tipo:</label>
						</div>
						
						<div class="input-field col s12 m6">
							<select multiple  name="id_material[]">
							  <option value="" disabled></option>
							  <?php foreach($material as $material){
									echo  '<option value="'.$material['id'].'">'.$material['material'].'</option>' ;
								}?>
							</select>
							<label>Material:</label>
						</div>						
						
						<div class="input-field col s12">
							 <textarea id="descricao" class="materialize-textarea" name="descricao"></textarea>
							<label for="descricao">Descrição:</label>
						</div>		
						
						<div class="input-field col s12">
							<p>Links Recomedados:<br>
							<small>Para separar os links utilize a tecla " ; ". <br>
							Ex.: https://www.youtube.com/; https://www.google.com</small></p>
							<input id="links" type="text" class="validate" name="links">
						</div>													
						
						<div class="input-field col s12">
							<p>Anexar Arquivos:</p>
							<input type="file" name="arquivo[]" multiple>
						</div>
						
					</div>

					<div class="row">
						<div class="col s6 m3 offset-m1" align="center">
							<a href="dashboard.php" class="btn waves-effect waves-light">Voltar</a> 
						</div>
						<div class="col s6 m6">
							<button class="btn2 waves-effect waves-light right" type="submit">Cadastrar</button>
						</div>
					</div>
				</form>
			</div>			
		</div>	
<?php }
	
	if ( $_SESSION[ "acao" ][ 'acao' ] === 'editar' ) {	
		
		include_once( 'model/material.php' );
		include_once( 'model/aula.php' );
		
		$material = listarMaterialBD($conexao);
		$aula = listarAulaIdBD($conexao, $_SESSION[ "acao" ][ 'id' ]);
		$aula_material = listarAulaMaterialBD($conexao, $_SESSION[ "acao" ][ 'id' ]);?>
		
		<br>
		<div class="row container">
			<h2>Editar Aula:</h2>
			<div class="row">
				<form class="col s12" action="controller/aula.php" method="post" accept-charset="UTF-8" enctype="multipart/form-data">
				
					<div class="row">
							
						<div class="input-field col s12 hide">
							<input id="acao" type="text" class="validate" name="acao" value="editar">					
							<input id="id" type="text" class="validate" name="id" value="<?= $aula[0]['id']?>">					
						</div>
							
						<div class="input-field col s12 m6">
							<input id="nome" type="text" class="validate" name="nome" value="<?= $aula[0]['nome']?>">
							<label for="nome">Nome:</label>
						</div>
						
						<div class="input-field col s12 m6">
							<input id="objetivo" type="text" class="validate" name="objetivo" value="<?= $aula[0]['objetivo']?>">
							<label for="objetivo">Objetivo:</label>
						</div>
						
						<div class="input-field col s12 m6">
							<select name="id_tipo">
						  	 <?php if($aula[0]['id_tipo'] === '1'){ $selected1 = 'selected';} 
								   if($aula[0]['id_tipo'] === '2'){ $selected2 = 'selected';} 
								   if($aula[0]['id_tipo'] === '3'){ $selected3 = 'selected';} 
								   if($aula[0]['id_tipo'] === '4'){ $selected4 = 'selected';} ?>
							  <option value="" disabled></option>
							  <option value="1" <?=$selected1?>>Clássico</option>
							  <option value="2" <?=$selected2?>>Motivacional</option>
							  <option value="3" <?=$selected3?>>Temático</option>
							  <option value="4" <?=$selected4?>>Zem</option>
							</select>
							<label>Tipo:</label>
						</div>
						
						<div class="input-field col s12 m6">
							<select  multiple name="id_material[]">
							  <?php $i=0; 
									foreach($material as $material){
									   
									if($aula_material[$i]['id_material'] === $material['id']){ $selected = 'selected';$i++;} 
									else{ $selected = '';}
									   
									echo  '<option value="'.$material['id'].'"'.$selected.'>'.$material['material'].'</option>' ;
									
									
								} unset($i);?>
							</select>
							<label>Material:</label>
						</div>						
						
						<div class="input-field col s12">
							<input id="descricao" type="text" class="validate" name="descricao" value="<?= $aula[0]['descricao']?>">
							<label for="descricao">Descrição:</label>
						</div>
						
						<div class="input-field col s12">
							<p>Adicionar Links Recomedados:<br>
							<small>Para separar os links utilize a tecla " ; ". <br>
							Ex.: https://www.youtube.com/; https://www.google.com</small></p>
							<input id="links" type="text" class="validate" name="links" value="<?=$aula[0]['links']?>">
						</div>
						
						<!--<div class="input-field col s12">		
							<p>Link: <br> <?php /*$array = explode(';',$aula['links']);
												if( empty( $aula['links'] ) ){
													echo 'Não há link para Visualizar';
												}
												else{
													for($i = 0; $i < count($array); $i++){														
													echo '<a href="'.$array[$i].'" target="_blank" class="white-text" > '.$array[$i].'</a> <br>';
													}  
												}	*/?></p>		
						</div>-->			
						
					</div>
					
					<div class="row ">	
						
						<div class="input-field col s12">							
							<input type="text" name="caminho" class="hide" value="<?=$aula[0]['caminho']?>">
						</div>
					
						<div class="input-field col s12">
							<p>Inserir Arquivos:</p>
							<input type="file" name="arquivo[]" multiple>
						</div>
						
						<div class="input-field col s12">
														
							<?php $array = explode(' ; ',$aula[0]['caminho']);
									unset($arrayIMG);
									unset($arraydoc);

								if( empty( $aula[0]['caminho'] ) ){
									echo 'Não há arquivos para Visualizar';
								}
								else{
									echo '<ul>';
									for($i = 0; $i < count($array); $i++){
										if( !empty( $array[$i] ) ){
											$extencao = strrchr($array[$i],'.');
											$nome = strrchr($array[$i],'/');
											$pasta = strstr($array[$i],'/aula');
											$link = $URL.$pasta;
											if($extencao === '.png' || $extencao === '.jpg' || $extencao === '.jpeg'){ 	$arrayIMG[$i] = array('nome' => $nome, 'link' => $link);}
											elseif($extencao === '.pdf' ){ 	$arraydoc[$i] = array('nome' => $nome, 'link' => $link);}
											else{ 
												$tipoarquivo = 1; // Qualquer outro ?>
												<li type="circle">
													<a href="<?=$link?>" class="white-text" download> <?=$nome?></a> 
													<a class="modal-trigger Link" href="#modal<?=$tipoarquivo?><?=$nome?>" title="Excluir"><i class="material-icons prefix amber-text text-darken-1">cancel</i></a>
												</li>



											<div id="modal<?=$tipoarquivo?><?=$nome?>" class="modal bottom-sheet">
												<div class="row">
													<div class="col s12 m6">
														<div class="modal-content">
														  <h4 class="grey-text text-darken-2">Deseja excluir arquivo?</h4>
														  <p class="grey-text text-darken-2">Essa ação não poderá ser desfeita.</p>
														</div>
													</div>
													<div class="col s12 m6 Top">
														<div class="modal-footer">
															<a href="aula.php" class="btn waves-effect waves-light">Voltar</a>
															<a href="controller/aula.php?acao=excluirarquivo&token=<?=$aula[0]['id']?>&arquivotipo=<?=$tipoarquivo?>&arquivo=<?=$nome?>" class="modal-close btn2 waves-effect waves-light">Eu aceito</a>
														</div>
													</div>
												</div>
											 </div>

											<?php }												
										}
									echo '</ul>';
									} ?>							  
							  <div class="row">
								<?php if( !empty($arrayIMG)){ //Img
									$tipoarquivo = 2; 
									 foreach($arrayIMG as $arrayIMG){ ?>
										<div class="col s12 m6 l4"> 
											<p align="center" class="white-text"><?=$arrayIMG['nome']?>
												<a class="modal-trigger Link" href="#modal<?=$tipoarquivo?><?=$arrayIMG['nome']?>" title="Excluir"><i class="material-icons prefix amber-text text-darken-1">cancel</i></a>
											</p>
											<a href="<?=$arrayIMG['link']?>" target="_blank">
												<img src="<?=$arrayIMG['link']?>" width="100%">	
											</a>
											
										</div>
										
										<div id="modal<?=$tipoarquivo?><?=$arrayIMG['nome']?>" class="modal bottom-sheet">
											<div class="row">
												<div class="col s12 m6">
													<div class="modal-content">
													  <h4 class="grey-text text-darken-2">Deseja excluir arquivo?</h4>
													  <p class="grey-text text-darken-2">Essa ação não poderá ser desfeita.</p>
													</div>
												</div>
												<div class="col s12 m6 Top">
													<div class="modal-footer">
														<a href="aula.php" class="btn waves-effect waves-light">Voltar</a>
														<a href="controller/aula.php?acao=excluirarquivo&token=<?=$aula[0]['id']?>&arquivotipo=<?=$tipoarquivo?>&arquivo=<?=$arrayIMG['nome']?>" class="modal-close btn2 waves-effect waves-light">Eu aceito</a>
													</div>
												</div>
											</div>
										 </div>
										 
									<?php } 
									}
								if( !empty($arraydoc)){	 //pdf
									$tipoarquivo = 3; 
									 foreach($arraydoc as $arraydoc){ ?>
										<div class="col s12"> 
											<p align="center" class="white-text"><?=$arraydoc['nome']?> 
											<a class="modal-trigger Link" href="#modal<?=$tipoarquivo?><?=$arraydoc['nome']?>" title="Excluir"><i class="material-icons prefix amber-text text-darken-1">cancel</i></a></p>
											<iframe src="<?=$arraydoc['link']?>" width="100%" height="500px"></iframe>
										</div>
										
										<div id="modal<?=$tipoarquivo?><?=$arraydoc['nome']?>" class="modal bottom-sheet">
											<div class="row">
												<div class="col s12 m6">
													<div class="modal-content">
													  <h4 class="grey-text text-darken-2">Deseja excluir arquivo?</h4>
													  <p class="grey-text text-darken-2">Essa ação não poderá ser desfeita.</p>
													</div>
												</div>
												<div class="col s12 m6 Top">
													<div class="modal-footer">
														<a href="aula.php" class="btn waves-effect waves-light">Voltar</a>
														<a href="controller/aula.php?acao=excluirarquivo&token=<?=$aula[0]['id']?>&arquivotipo=<?=$tipoarquivo?>&arquivo=<?=$arraydoc['nome']?>" class="modal-close btn2 waves-effect waves-light">Eu aceito</a>
													</div>
												</div>
											</div>
										 </div>
										 
									<?php } 
									}
							} ?>					

							</div>
						</div>
						
					</div>

					<div class="row">
						<div class="col s6 m3 offset-m1" align="center">
							<a href="aula.php?acao=listar" class="btn waves-effect waves-light">Voltar</a> 
						</div>
						<div class="col s6 m6">
							<button class="btn2 waves-effect waves-light right" type="submit">Alterar</button>
						</div>
					</div>
						  
				</form>
			</div>			
		</div>	
<?php }

}

if ( !empty( $_GET[ 'acao' ] ) ) {

	echo $_SESSION[ "acao" ][ 'acao' ] = $_GET[ 'acao' ];
	echo $_SESSION[ "acao" ][ 'id' ] = $_GET[ 'token' ];
	echo $_SESSION[ "acao" ][ 'tipo' ] = $_GET[ 'tipo' ];

	echo '<script>window.location.replace("aula.php");</script>';
}

include_once( 'rodape.php' );?>