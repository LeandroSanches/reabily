<?php
include_once( 'cabecalho.php' );
include_once( 'model/fornecedor.php' );
include_once( 'model/usuario.php' );


if ( isset($_SESSION[ "acao" ][ 'acao' ])) {
	
	if ( $_SESSION[ "acao" ][ 'acao' ]  === 'listar') {
	$fornecedor = listarFornecedorBD( $conexao ); ?>

<div class="row container">
	
	<h2>Fornecedor</h2>
	
	<table>
		<thead>
			<tr>
				<th>Nome:</th>
				<th>E-mail:</th>
				<th>Material:</th>
				<th></th>
			</tr>
		</thead>

		<tbody>
			<?php foreach ( $fornecedor as $fornecedor ) {?>

			<tr>
				<td><a  class="<?= $color?> modal-trigger white-text" href="#modalVisualizar<?=$fornecedor['id']?>"><?= $fornecedor[ "nome" ]?></a></td>
				<td><a  class="<?= $color?> modal-trigger white-text" href="#modalVisualizar<?=$fornecedor['id']?>"><?= $fornecedor[ "email" ]?></a></td>
				<td><a  class="<?= $color?> modal-trigger white-text" href="#modalVisualizar<?=$fornecedor['id']?>"><?= $fornecedor[ "material" ]?></a></td>
				<td><a href="fornecedor.php?acao=alterar&token=<?=$fornecedor['id']?>"><i class="material-icons white-text">edit</i></a></td>
			</tr>
			
			
        <div id="modalVisualizar<?=$fornecedor['id']?>" class="modal" style=" background-color: #008aae;">
        	<i class="material-icons right modal-close" style="padding: 15px;">close</i>
			<div class="container">	
        		
				<div class="row">
					<p align="center">Visualizador de Fornecedor.</p>

					<!--<p>Cod.: <?=$fornecedor['id']?></p>-->
					<p>Nome: <?=$fornecedor['nome']?></p>		
					<p>E-mail: <?=$fornecedor['email']?></p>		
					<p>Telefone: <?=mask($fornecedor['telefone'],'(##) ###-###-###')?></p>		
					<p>Material: <?=$fornecedor['material']?></p>		
					<p>Valor Unitário: <?=$fornecedor['valorunitario']?></p>		
					<p>Quantidade Pedido: <?=$fornecedor['quantidadepedido']?></p>		
					<p>Descrição: <?=$fornecedor['descricao']?></p>	
					<p>Ativo: <?php switch ($fornecedor['ativo']) {
										case 0:
											echo " Ativado ";
											break;
										case 1:
											echo " Desativado ";
											break;
									}?></p>
				</div>
				<div class="row">
					<p align="center"><a href="fornecedor.php?acao=alterar&token=<?=$fornecedor['id']?>" title="Alterar Fornecedor"><i class="material-icons amber-text">edit</i></a></p>
				</div>
			</div>

		  </div>

			<?php } ?>
		</tbody>
	</table>
 </div>
 
	<div class="row">
	
		<div class="col s6 m3 offset-m1" align="center">
			<a href="dashboard.php" class="btn waves-effect waves-light">Voltar</a> 
		</div>
		
		<div class="col s6 m6">
			<a href="fornecedor.php?acao=novo" class="btn2 waves-effect waves-light right">Novo Cadastro</a> </div>
		</div>
		
	</div>
	<?php
}
	
	if ( $_SESSION[ "acao" ][ 'acao' ]  === 'alterar') {
	$fornecedor = buscarFornecedor( $conexao , $_SESSION[ "acao" ][ 'id' ]);
	?>
	<br>
	<div class="container row">
		
		<h2>Alterar Fornecedor</h2>
		
		<div class="row">
			<form class="col s12" action="controller/fornecedor.php" method="post" accept-charset="UTF-8">
				<div class="row">
					<div class="input-field col s12 m6 hide">
						<input type="number" name="id" value="<?= $fornecedor[0]['id']?>">
					</div>
					<div class="input-field col s12 m6">
						<i class="material-icons prefix">account_circle</i>
						<input id="nome" type="text" class="validate" name="nome" value="<?= $fornecedor[0]['nome']?>">
						<label for="nome">Nome</label>
					</div>
					<div class="input-field col s12 m6">
						<i class="material-icons prefix">email</i>
						<input id="email" type="email" class="validate" name="email" value="<?= $fornecedor[0]['email']?>">
						<label for="email">Email</label>
					</div>
					<div class="input-field col s12 m6">
						<i class="material-icons prefix">phone</i>
						<input id="telefone" type="text" class="validate" name="telefone" data-mask="(00) 000-000-000" value="<?= $fornecedor[0]['telefone']?>">
						<label for="telefone">Telefone</label>
					</div>					
					<div class="input-field col s12 m6">
						<input id="material" type="text" class="validate" name="material" value="<?= $fornecedor[0]['material']?>">
						<label for="material">Material</label>
					</div>				
					<div class="input-field col s12 m3">
						<i class="material-icons prefix">attach_money</i>
						<input id="valorunitario" type="text" class="validate money" name="valorunitario" value="<?= $fornecedor[0]['valorunitario']?>">
						<label for="valorunitario">Valor Unitario</label>
					</div>					
					<div class="input-field col s12 m3">
						<input id="quantidadepedido" type="number" class="validate" name="quantidadepedido" value="<?= $fornecedor[0]['quantidadepedido']?>">
						<label for="quantidadepedido">Quantidade do Pedido</label>
					</div>				
					<div class="input-field col s12">
						<textarea id="textarea1" class="materialize-textarea" name="obs"><?= $fornecedor[0]['descricao']?></textarea>
						<label for="obs">Observação</label>
					</div>
				</div>

				<div class="row">
					<div class="col s6 m3 offset-m1" align="center">
						<a href="fornecedor.php?acao=listar" class="btn waves-effect waves-light">Voltar</a> </div>
					<div class="col s6 m6">
						<button class="btn2 waves-effect waves-light right" type="submit">Alterar </button>
					</div>
				</div>
			</form>
		</div>
	</div>
	<?php
}

	if ( $_SESSION[ "acao" ][ 'acao' ]  === 'novo' ) {	?>
	<br>
	<div class="container row">
		
		<h2>Novo Fornecedor</h2>
		
		<div class="row">
			<form class="col s12" action="controller/fornecedor.php" method="post" accept-charset="UTF-8">
				<div class="row">
					<div class="input-field col s12 m6">
						<i class="material-icons prefix">account_circle</i>
						<input id="nome" type="text" class="validate" name="nome">
						<label for="nome">Nome</label>
					</div>
					<div class="input-field col s12 m6">
						<i class="material-icons prefix">email</i>
						<input id="email" type="email" class="validate" name="email">
						<label for="email">Email</label>
					</div>
					<div class="input-field col s12 m6">
						<i class="material-icons prefix">phone</i>
						<input id="telefone" type="text" class="validate" name="telefone" data-mask="(00) 000-000-000">
						<label for="telefone">Telefone</label>
					</div>					
					<div class="input-field col s12 m6">
						<input id="material" type="text" class="validate" name="material">
						<label for="material">Material</label>
					</div>				
					<div class="input-field col s12 m3">
						<i class="material-icons prefix">attach_money</i>
						<input id="valorunitario" type="text" class="validate money" name="valorunitario">
						<label for="valorunitario">Valor Unitario</label>
					</div>					
					<div class="input-field col s12 m3">
						<input id="quantidadepedido" type="number" class="validate" name="quantidadepedido">
						<label for="quantidadepedido">Quantidade do Pedido</label>
					</div>				
					<div class="input-field col s12">
						<textarea id="textarea1" class="materialize-textarea" name="obs"></textarea>
						<label for="obs">Observação</label>
					</div>
				</div>

				<div class="row">
					<div class="col s6 m3 offset-m1" align="center">
						<a href="dashboard.php" class="btn waves-effect waves-light">Voltar</a> </div>
					<div class="col s6 m6">
						<button class="btn2 waves-effect waves-light right" type="submit">Cadastrar </button>
					</div>
				</div>
			</form>
		</div>
	</div>
	<?php
}
	
}

if ( !empty( $_GET['acao'] )){	
	
	echo $_SESSION[ "acao" ][ 'acao' ] = $_GET['acao'];
	echo $_SESSION[ "acao" ][ 'id' ] = $_GET['token'];
	echo $_SESSION[ "acao" ][ 'tipo' ] = $_GET['tipo'];
	
	echo '<script>window.location.replace("fornecedor.php");</script>';
}

include_once( 'rodape.php' );
?>