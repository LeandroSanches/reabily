<?php
include_once( 'cabecalho.php' );
include_once( 'model/setor.php' );
include_once( 'model/usuario.php' );
include_once( 'model/empresa.php' );

if ( isset( $_SESSION[ "acao" ][ 'acao' ] ) ) {

	if ( $_SESSION[ "acao" ][ 'acao' ] === "novo" ) {
		//Escolher Sede
		if( empty($_GET['empresa']) ){
			$empresa = listarSedeEmpresasBD( $conexao );?>
			<br><br>
			<div class="container row">		
				<h2>Novo Setor</h2>
				<form action="setor.php?acao=novo" method="get">
					<div class="row">
					
					<div class="input-field col s12">
							<select name="empresa">
								<option value=""  selected></option>
								<?php foreach ( $empresa as $empresa ){	?>						
								<option value="<?=$empresa["id"];?>" label="<?=$empresa["sede"];?>" data-icon="<?php echo $empresa['logo'] = strstr($empresa['logo'],'usuarios/');?>"><?=$empresa["sede"];?>	
								</option>
								<?php } ?>
							</select>
							<label>Empresa:</label>
						</div>				
					</div>
					<div class="row">
						<div class="col s6 m3 offset-m1" align="center">
							<a href="dashboard.php" class="btn waves-effect waves-light">Voltar</a> </div>
						<div class="col s6 m3 offset-m3">
							<button class="btn2 waves-effect waves-light right" type="submit">Avançar</button>
						</div>
					</div>
				</form>
			</div>
	<?php }
		//Escolher filiais		
		if( !empty($_GET['empresa']) ){
			//echo $_GET['empresa'];
		$empresa = listarEmpresaSedeFilial( $conexao, $_GET['empresa'] );?>
		<br><br>
		<div class="container row">		
			<h2>Novo Setor</h2>
			<form action="controller/setor.php" method="post">
				<div class="row">
				<?php if( empty($empresa)){ echo "<p align='center'><strong>Sem Filial</strong></p>"; }
					else { ?>
					<div class="input-field col s12">
						<select name="empresa">
							<option value="" disabled selected>Escolha sua empresa</option>
						<?php foreach ( $empresa as $empresa ){ ?>
								<option value="<?=$empresa["id"];?>" ><?=$empresa["social"];?></option>
						<?php } ?>
						</select>
						<label>Filial:</label>
					</div>
					<div class="input-field col s12 m4">
						<input id="setor" type="text" class="validate" name="setor">
						<label for="setor">Setor</label>
					</div>
					<div class="input-field col s12 m4">
						<input id="grupo" type="text" class="validate" name="grupo">
						<label for="grupo">Grupo</label>
					</div>
					<div class="input-field col s12 m4">						
						<input id="andar" type="text" class="validate" name="andar">
						<label for="andar">Andar</label>
					</div>
					<?php } ?>
				</div>

				<div class="row">
					<div class="col s6 m3 offset-m1" align="center">
							<a href="setor.php" class="btn waves-effect waves-light">Voltar</a> </div>
					<div class="col s6 m3 offset-m3">
						<button class="btn2 waves-effect waves-light right" type="submit">Cadastrar</button>
					</div>
				</div>
			</form>
		</div>
	<?php
	} }
	
	if ( $_SESSION[ "acao" ][ 'acao' ] === "listar" ) {			
		$setor = listarSetoresBD($conexao);	?>
		<br><br>
		<div class="container row">
		<h2>Lista de Setor</h2>
			<form action="controller/setor.php" method="post">
				<div class="row">	
					<table>
						<thead>
							<tr>
								<th>Empresa:</th>
								<th>Setor:</th>
								<th>Grupo:</th>
								<th>Andar:</th>
								<th></th>
							</tr>
						</thead>

						<tbody>				
						<?php
						if(empty($setor)){echo "<h3  align='center'>Sem Setor Cadatrado</h3>";} 
						else{
							foreach ( $setor as $setor ){  
								$empresa = listarEmpresaFilial( $conexao, $setor['id_empresa'] ) ;?>
								<tr>
									<td>
										<?= $empresa[0]['sede']?>
									</td><td>
										<?= $setor['setor']?>
									</td>
									<td>
										<?= $setor['grupo']?>
									</td>
									<td>
										<?= $setor['andar']?>
									</td>
									<td>
										<a href="setor.php?acao=editar&token=<?=$setor['id']?>"><i class="material-icons white-text" title="Editar">edit</i></a>
										<a class="modal-trigger Link" href="#modal<?=$setor['id']?>" title="Excluir"><i class="material-icons prefix amber-text text-darken-1">cancel</i></a>
									</td>
								</tr>

								<div id="modal<?=$setor['id']?>" class="modal bottom-sheet">
									<div class="row">
										<div class="col s12 m6">
											<div class="modal-content">
											  <h4 class="grey-text text-darken-2">Deseja excluir o Setor?</h4>
											  <p class="grey-text text-darken-2">Essa ação não poderá ser desfeita.</p>
											</div>
										</div>
										<div class="col s12 m6 Top">
											<div class="modal-footer">
												<a href="setor.php?acao=" class="btn waves-effect waves-light">Voltar</a>
												<a href="controller/setor.php?acao=excluir&token=<?=$setor['id']?>" class="modal-close btn2 waves-effect waves-light">Eu aceito</a>
											</div>
										</div>
									</div>
								  </div>

							<?php } 
						} ?>
						</tbody>	
					</table>
				</div>				
				<div class="row">
					<div class="col s6 m3 offset-m1" align="center">
						<a href="dashboard.php" class="btn waves-effect waves-light">Voltar</a> 
					</div>					
				</div>
			</form>
		</div>
	<?php
	}
	
	if ( $_SESSION[ "acao" ][ 'acao' ] === 'editar' ) { 
		$setor = listarSetorBD($conexao, $_SESSION[ "acao" ][ 'id' ]);		
		$empresa = listarEmpresaFilial( $conexao, $setor[0]['id_empresa'] );?>
		<br><br>
		<div class="container row">
		<h2>Alterar Setor</h2>
			<form action="controller/setor.php" method="post">
				<div class="row">				
					<div class="input-field col s12">						
						<p>Empresa: <strong><?= $empresa[0]['sede']?></strong></p>
					</div>
					<div class="input-field col s12 m4 hide">
						<input type="number" name="id" value="<?= $setor[0]['id'];?>">
						
					</div>
					<div class="input-field col s12 m4">
						<input id="setor" type="text" class="validate" name="setor" value="<?= $setor[0]['setor'];?>">
						<label for="setor">Setor</label>
					</div>
					<div class="input-field col s12 m4">
						<input id="grupo" type="text" class="validate" name="grupo" value="<?= $setor[0]['grupo'];?>">
						<label for="grupo">Grupo</label>
					</div>
					<div class="input-field col s12 m4">						
						<input id="andar" type="text" class="validate" name="andar" value="<?= $setor[0]['andar'];?>">
						<label for="andar">Andar</label>
					</div>
				</div>

				<div class="row">
					<div class="col s6 m3 offset-m8">
						<button class="btn2 waves-effect waves-light right" type="submit">Alterar</button>
					</div>
				</div>
			</form>
		</div>
	<?php }
	
}

if ( !empty( $_GET[ 'acao' ] ) ) {

	//unset( $_SESSION[ "acao" ] );

	echo $_SESSION[ "acao" ][ 'acao' ] 		= $_GET[ 'acao' ];
	echo $_SESSION[ "acao" ][ 'id' ] 		= $_GET[ 'token' ];
	echo $_SESSION[ "acao" ][ 'empresa' ] 	= $_GET[ 'empresa' ];

	echo '<script>window.location.replace("setor.php");</script>';	
} 

include_once( 'rodape.php' );