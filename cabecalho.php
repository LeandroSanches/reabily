<?php
error_reporting( E_ALL ^ E_NOTICE );
error_reporting( E_WARNING );
error_reporting( 0 );

session_start();

if ( !isset( $_SESSION[ 'usuario' ] ) ) {
	$actual_link = "$_SERVER[REQUEST_URI]";
	switch ( $actual_link ) {
		case '/index.php':
			break;
		case '/autocadastro.php':
			break;
		case '/trabalhe_conosco.php':
			break;
		case '/reabily/index.php':
			break;
		case '/reabily/autocadastro.php':
			break;
		case '/reabily/trabalhe_conosco.php':
			break;
		case '/projetos/reabily/index.php':
			break;
		case '/projetos/reabily/autocadastro.php':
			break;
		case '/projetos/reabily/trabalhe_conosco.php':
			break;
		case '/projetos/homologacao_reabily/index.php':
			break;
		case '/projetos/homologacao_reabily/autocadastro.php':
			break;
		case '/projetos/homologacao_reabily/trabalhe_conosco.php':
			break;
		case '/sistema/index.php':
			break;
		case '/sistema/autocadastro.php':
			break;
		case '/sistema/trabalhe_conosco.php':
			break;
		default:
			echo " <script>	window.location.replace('/index.php');	</script>";
	}
}

?>
<!DOCTYPE html>
<html>

<head>
	<meta charset="UTF-8">
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
	<link rel="shortcut icon" href="img/icon.png" type="image/x-icon" />
	<!--Import Google Icon Font-->
	<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
	<!--<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.4.1/css/all.css" integrity="sha384-5sAR7xN1Nv6T6+dT2mhtzEpVJvfS3NScPQTrOxhwjIuvcA67KV2R5Jz6kr4abQsz" crossorigin="anonymous">-->
	<!--Import materialize.css-->
	<link type="text/css" rel="stylesheet" href="css/materialize.min.css" media="screen,projection"/>
	<link type="text/css" rel="stylesheet" href="css/animate.css" media="screen,projection"/>
	<link type="text/css" rel="stylesheet" href="css/reabity.css" media="screen,projection"/>
	<link type="text/css" rel="stylesheet" href="css/mobile.css" media="screen,projection"/>

	<!--Let browser know website is optimized for mobile-->
	<meta name="viewport" content="width=device-width, initial-scale=1.0"/>

	<script src="https://code.jquery.com/jquery-3.3.1.js" integrity="sha256-2Kok7MbOyxpgUVvAk/HJ2jigOSYS2auK4Pfzbm7uH60=" crossorigin="anonymous"></script>
	<style>
		.dropdown-content {
			min-width: 155px;
		}
		@media print { 
			.noprint { display:none;}
			input{ border: 0px; }
			.PrintQuebraProx{page-break-after: always}
		}
	</style>
</head>

<body>

	<?php if ( !isset( $_SESSION[ 'usuario' ] ) ) { 
	?>

	<nav class="nav-extended">
		<div class="nav-wrapper">
			<a href="<?=$actual_link?>" class="brand-logo"><img src="img/logo.png" class="responsive-img"/></a>
		</div>
	</nav>

	<?php } 
	else { 	?>
	<nav class="nav-extended noprint">
		<div class="nav-wrapper">
			<a href="<?=$actual_link?>" class="brand-logo"><img src="img/logo.png" class="responsive-img"/></a>
			<a href="#" data-target="mobile-demo" class="sidenav-trigger"><i class="material-icons">menu</i></a>
			<ul id="nav-mobile" class="right hide-on-med-and-down">

				<li><a href="dashboard.php" title="Dashboard"><i class="material-icons">view_module</i> </a></li>
				
				<li><a href="sugestoes.php" title="Sugestões"><i class="material-icons">announcement</i></a></li>

				<li>
					<a class="dropdown-trigger" href="#" data-target="dropdown1" title="Meu perfil">				
					<img class="material-icons" src="<?php echo $_SESSION[ 'usuario' ]['foto'] = strstr($_SESSION[ 'usuario' ][ 'foto' ],'usuarios/');?>" width="20px"/>
					<!--<i class="material-icons">account_box</i>-->
					</a>

					<ul id='dropdown1' class='dropdown-content'>
						<li><a href="meuperfil.php" title="Meu perfil" class="white-text">
						<i class="material-icons white-text ">account_box</i>Meu perfil</a>
						</li>
						<li><a href="controller/logout.php" title="Sair" class="amber-text text-darken-1"><i class="material-icons amber-text text-darken-1">exit_to_app</i> Sair</a></li>
					</ul>

				</li>
			</ul>
		</div>
	</nav>

	<ul class="sidenav  noprint" id="mobile-demo">
		<li><a href="dashboard.php" title="Dashboard" class="light-blue-text text-darken-2"><i class="material-icons light-blue-text text-darken-2">view_module</i> Dashboard</a>
		</li>

		<li><a href="meuperfil.php" title="Meu perfil" class="light-blue-text text-darken-2"><i class="material-icons light-blue-text text-darken-2">person</i> Meu perfil</a>

		</li>

		<li><a href="sugestoes.php" title="Sugestões" class="light-blue-text text-darken-2"><i class="material-icons light-blue-text text-darken-2">announcement</i> Sugestões</a>

		</li>

		<li><a href="controller/logout.php" title="Deslogar" class="amber-text text-darken-1"><i class="material-icons amber-text text-darken-1">exit_to_app</i> Sair</a>

		</li>
	</ul>
	<?php
	}

	//Mensagem para o usuario
	if ( !empty( $_SESSION[ "msg" ] ) ) {
		echo '<div class="msg">' . $_SESSION[ "msg" ] . '</div>';
		unset( $_SESSION[ "msg" ] );
	}

	if ( $_SESSION[ "log" ][ "on" ] === "true" ) {
		echo '<div class="msg">' . $_SESSION[ "log" ][ "msg" ] . '</div>';
	}