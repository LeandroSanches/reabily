<?php
include_once( 'cabecalho.php' );
include_once( 'model/frase.php' );

session_start();

if ( isset( $_SESSION[ "acao" ][ 'acao' ] ) ) {

	$btnvoltar = 'dashboard.php';


	if ( $_SESSION[ "acao" ][ 'acao' ] === 'novo' ) {

		$btnvoltar = 'dashboard.php';
		$msgBotao = 'Cadastrar';
		?>
		<br>
		<div class="container row">

			<h2>Nova Frase</h2>

			<div class="row">
				<form class="col s12" action="controller/frase.php" method="post" accept-charset="UTF-8">
					<div class="row">
						<div class="input-field col s12">
							<input id="Frase" type="text" class="validate" name="frase">
							<label for="Frase">Frase </label>
						</div>

					</div>

					<div class="row">
						<div class="col s6 m3 offset-m1" align="center">
							<a href="<?=$btnvoltar?>" class="btn waves-effect waves-light">Voltar</a> </div>
						<div class="col s6 m6">
							<button class="btn2 waves-effect waves-light right" type="submit">
								<?= $msgBotao?>
							</button>
						</div>
					</div>
				</form>
			</div>
		</div>

		<?php
	}

	if ( $_SESSION[ "acao" ][ 'acao' ] === 'listar' ) {	
		
		unset($_SESSION[ "acao" ]);

		$frase = buscarfrases($conexao);
		
		?>
		<div class="container">
			<div class="row">
				<h3>Frases Motivacionais</h3>
			</div>
			<div class="row">
				<table>
					<thead>
						<tr>
							<th>Frase:</th>
							<th>Ação</th>
						</tr>
					</thead>
					<tbody>
					<?php foreach ( $frase as $frase ){?>
						<tr>
							<td><?= $frase['frase']?></td>
							<td align="center">
								<a href="frase.php?acao=alterar&token=<?=$frase['id']?>" title="Alterar"><i class="material-icons prefix white-text">edit</i></a>
								<a class="modal-trigger Link" href="#modal<?=$frase['id']?>" title="Excluir"><i class="material-icons prefix amber-text text-darken-1">cancel</i></a>
							</td>
						</tr>
						<div id="modal<?=$frase['id']?>" class="modal bottom-sheet">			
							<div class="row">

								<div class="col s12 m6">

									<div class="modal-content">
									  <h4 class="grey-text text-darken-2">Deseja excluir?</h4>
									  <p class="grey-text text-darken-2">Essa ação não poderá ser desfeita.</p>
									</div>

								</div>


								<div class="col s12 m6 Top">

									<div class="modal-footer">
										<a href="controller/frase.php?acao=excluir&token=<?=$frase['id']?>" class="modal-close btn2 waves-effect waves-light">Eu aceito</a>
									</div>

								</div>

							</div>

						  </div>
					<?php }?>
					</tbody>
				</table>
			</div>

			<div class="row">
				<div class="col s6 m3 offset-m1" align="center">
					<a href="dashboard.php" class="btn waves-effect waves-light">Voltar</a>
				</div>
				<div class="col s6 m6">
					<a href="frase.php?acao=novo" class="btn2 waves-effect waves-light right">Novo Cadastro</a>
				</div>
			</div>

		</div>
		<?php
	}

	if ( $_SESSION[ "acao" ][ 'acao' ] === 'alterar' ) {	
		
		$btnvoltar = 'frase.php?acao=listar';
		$msgBotao = 'Alterar';

		$frase =  buscarfrase($conexao, $_SESSION[ "acao" ][ 'id' ]);		
		
	?>
	
		<br>
		<div class="container row">

			<h2>Alterar Frase</h2>

			<div class="row">
				<form class="col s12" action="controller/frase.php" method="post" accept-charset="UTF-8">
					<div class="row">
						<div class="input-field col s12 hide">
							<input type="text" class="validate" name="id" value="<?=$frase[0]['id']?>">
						</div>
						<div class="input-field col s12">
							<input id="Frase" type="text" class="validate" name="frase" value="<?=$frase[0]['frase']?>">
							<label for="Frase">Frase </label>
						</div>

					</div>

					<div class="row">
						<div class="col s6 m3 offset-m1" align="center">
							<a href="<?=$btnvoltar?>" class="btn waves-effect waves-light">Voltar</a> </div>
						<div class="col s6 m6">
							<button class="btn2 waves-effect waves-light right" type="submit">
								<?= $msgBotao?>
							</button>
						</div>
					</div>
				</form>
			</div>
		</div>

	<?php }
}


if ( !empty( $_GET[ 'acao' ] ) ) {

	$_SESSION[ "acao" ][ 'acao' ] = $_GET[ 'acao' ];
	$_SESSION[ "acao" ][ 'id' ] = $_GET[ 'token' ];

	echo '<script>window.location.replace("frase.php");</script>';
}

include_once( 'rodape.php' );
?>