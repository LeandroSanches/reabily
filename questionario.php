<?php
include_once( 'cabecalho.php' );

session_start();

if ( isset( $_SESSION[ "acao" ][ 'quest' ] ) ) {
	
	//unset($_SESSION[ "acao" ]);?>
	
	<br><br>
	<div class="container">
		<div class="row">
			<h3 align="center">Escolha um Questionário</h3>
		</div>
		<div class="row">
			<div class="col s12 m6">
				<div align="center"><a href="questionario.php?quest=ginlab" class="btn waves-effect waves-light" style="width: 200px;">Ginástica Laboral</a>
				</div>
			</div>
			<div class="col s12 m6">
				<div align="center"><a href="questionario.php?quest=shiatsu" class="btn waves-effect waves-light" style="width: 200px;">Shiatsu</a>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col s12 m6">
				<div align="center"><a href="questionario.php?quest=ergonomia" class="btn waves-effect waves-light" style="width: 200px;">Ergonomia</a>
				</div>
			</div>
			<div class="col s12 m6">
				<div align="center"><a href="questionario.php?quest=qualidadevida" class="btn waves-effect waves-light" style="width: 200px;">Qualida de Vida</a>
				</div>
			</div>
		</div>
	</div>

<?php 
	if ( $_SESSION[ "acao" ][ 'quest' ] === "ginlab" ) {
		
		unset($_SESSION[ "acao" ]);

		$_SESSION[ "acao" ][ 'quest' ] = "ginlab";
		$_SESSION[ "acao" ][ "acao" ] = 'inserir';
		?>

		<br>
		<div class="container">
			<div class="row">
				<form class="col s12" action="controller/questionario.php" method="post" enctype="multipart/form-data">
					<div class="row">
						<div class="col s12 m12 l12">
							<h2>Ginástica Laboral</h2>
						</div>
					</div>

					<div class="row">

						<input class="hide" type="text" name="id" value="">

						<div class="input-field col s12 m3">
							<select name="sexo">
								<option value="0" disabled selected>Selecione</option>
								<option value="1">Masculino</option>
								<option value="2">Feminino</option>
								<option value="3">Outro</option>

							</select>
							<label>Sexo:</label>
						</div>

						<div class="input-field col s12 m3">
							<input id="Idade" type="number" class="validate" name="idade" value="">
							<label for="Idade">Idade</label>
						</div>

						<div class="input-field col s12 m3">
							<input id="Peso" type="number" class="validate" name="peso" value="">
							<label for="Peso">Peso: Kg</label>
						</div>

						<div class="input-field col s12 m3">
							<input id="Altura" type="number" class="validate" name="altura" value="">
							<label for="Altura">Altura cm</label>
						</div>

					</div>

					<div class="row">

						<div class="input-field col s12 m4">
							<select name="lado">
								<option value="0" disabled selected>Selecione</option>
								<option value="1">Direito</option>
								<option value="2">Esquerdo</option>

							</select>
							<label>Lado dominante (que escreve):</label>
						</div>

						<div class="input-field col s12 m4">
							<select name="tempoempresa">
								<option value="0" disabled selected>Selecione</option>
								<option value="1">De 0 até 1 ano</option>
								<option value="2">De 1 até 2 anos</option>
								<option value="3">De 2 até 5 anos</option>
								<option value="3">Mais de 5 anos</option>

							</select>
							<label>Há quanto tempo trabalha na empresa?</label>
						</div>

						<div class="input-field col s12 m4">
							<input id="Departamento" type="text" class="validate" name="departamento" value="" placeholder="Unidade de Trabalho">
							<label for="Departamento">Departamento</label>
						</div>

					</div>





					<div class="row">
						<div class="col s12">
							<h5>Você sente frequentemente desconforto em alguma das regiões abaixo? Marque ao lado do número referente a região de desconforto. Observe atentamente a indicação dos lados no desenho.</h5>
						</div>
					</div>

					<div class="row">
						<div class="col s12">

							<div class="container-fluid">
								<div class="row">

									<div class="col-md-8 col-md-offset-2" style="min-height:495px;">

										<div id="#map" class="Corllet">

											<!-- 1 cabeça -->
											<label style="position:absolute; top: 0px; left: 179px;"><input type="checkbox" class="filled-in" name="body1"><span></span></label>
											<!-- 2 pescoço -->
											<label style="position:absolute; top:45px; left: 179px;"><input type="checkbox" class="filled-in" name="body2"><span></span></label>
											<!-- 3 cent -->
											<label style="position:absolute; top: 135px; left: 179px;"><input type="checkbox" class="filled-in" name="body3"><span></span></label>
											<!-- 4 cent -->
											<label style="position:absolute; top: 190px; left: 179px;"><input type="checkbox" class="filled-in" name="body4"><span></span></label>
											<!-- 5 cent -->
											<label style="position:absolute; top: 230px; left: 179px;"><input type="checkbox" class="filled-in" name="body5"><span></span></label>

											<!-- 6 dir -->
											<label style="position:absolute; top: 90px; left: 200px;"><input type="checkbox" class="filled-in" name="body6"><span></span></label>
											<!-- 7 dir -->
											<label style="position:absolute; top: 130px; left: 223px;"><input type="checkbox" class="filled-in" name="body7"><span></span></label>
											<!-- 8 dir -->
											<label style="position:absolute; top: 160px; left: 233px;"><input type="checkbox" class="filled-in" name="body8"><span></span></label>
											<!-- 9 dir -->
											<label style="position:absolute; top: 185px; left: 250px;"><input type="checkbox" class="filled-in" name="body9"><span></span></label>
											<!-- 10 dir -->
											<label style="position:absolute; top: 209px; left: 264px;"><input type="checkbox" class="filled-in" name="body10"><span></span></label>
											<!-- 11 dir -->
											<label style="position:absolute; top: 234px; left: 273px;"><input type="checkbox" class="filled-in" name="body11"><span></span></label>
											<!-- 12 dir -->
											<label style="position:absolute; top: 280px; left: 223px;"><input type="checkbox" class="filled-in" name="body12"><span></span></label>
											<!-- 13 dir -->
											<label style="position:absolute; top: 327px; left: 218px;"><input type="checkbox" class="filled-in" name="body13"><span></span></label>
											<!-- 14 dir -->
											<label style="position:absolute; top: 365px; left: 216px;"><input type="checkbox" class="filled-in" name="body14"><span></span></label>
											<!-- 15 dir -->
											<label style="position:absolute; top: 415px; left: 208px;"><input type="checkbox" class="filled-in" name="body15"><span></span></label>
											<!-- 16 dir -->
											<label style="position:absolute; top: 440px; left: 208px;"><input type="checkbox" class="filled-in" name="body16"><span></span></label>

											<!-- 6 esq -->
											<label style="position:absolute; top: 90px; left: 157px;"><input type="checkbox" class="filled-in" name="body17"><span></span></label>
											<!-- 7 esq -->
											<label style="position:absolute; top: 130px; left: 135px;"><input type="checkbox" class="filled-in" name="body18"><span></span></label>
											<!-- 8 esq -->
											<label style="position:absolute; top: 160px; left: 124px;"><input type="checkbox" class="filled-in" name="body19"><span></span></label>
											<!-- 9 esq -->
											<label style="position:absolute; top: 185px; left: 110px;"><input type="checkbox" class="filled-in" name="body20"><span></span></label>
											<!-- 10 esq -->
											<label style="position:absolute; top: 210px; left: 95px;"><input type="checkbox" class="filled-in" name="body21"><span></span></label>
											<!-- 11 esq -->
											<label style="position:absolute; top: 235px; left: 85px;"><input type="checkbox" class="filled-in" name="body22"><span></span></label>
											<!-- 12 esq -->
											<label style="position:absolute; top: 280px; left: 135px;"><input type="checkbox" class="filled-in" name="body23"><span></span></label>
											<!-- 13 esq -->
											<label style="position:absolute; top: 327px; left: 140px;"><input type="checkbox" class="filled-in" name="body24"><span></span></label>
											<!-- 14 esq -->
											<label style="position:absolute; top: 365px; left: 140px;"><input type="checkbox" class="filled-in" name="body25"><span></span></label>
											<!-- 15 esq -->
											<label style="position:absolute; top: 415px; left: 143px;"><input type="checkbox" class="filled-in" name="body26"><span></span></label>
											<!-- 16 esq -->
											<label style="position:absolute; top: 440px; left: 144px;"><input type="checkbox" class="filled-in" name="body27"><span></span></label>

										</div>

									</div>
								</div>
							</div>



						</div>
					</div>





					<div class="row">
						<div class="input-field col s12 m12">
							<h5>Como percebe a Ginástica Laboral, marque até 2 respostas:</h5>
						</div>
					</div>

					<div class="row">

						<div class="col s12 m3">
							<label><input type="checkbox" class="filled-in" name="a1"/><span>Atividade de integração</span></label>
						</div>

						<div class="col s12 m3">
							<label><input type="checkbox" class="filled-in" name="a2"/><span>Momento de descontração</span></label>
						</div>

						<div class="col s12 m3">
							<label><input type="checkbox" class="filled-in" name="a3"/><span>Momento de alívio mente/corpo</span></label>
						</div>

						<div class="col s12 m3">
							<label><input type="checkbox" class="filled-in" name="a4"/><span>Pausa benéfica</span></label>
						</div>
					</div>

					<div class="row">
						<div class="col s12 m3">
							<label><input type="checkbox" class="filled-in" name="a5"/><span>Pausa desnecessária</span></label>
						</div>

						<div class="col s12 m9">
							<label><input type="checkbox" class="filled-in" name="a6"/><span>É indiferente</span></label>
						</div>
					</div>





					<br>
					<div class="row">
						<div class="input-field col s12 m12">
							<h5>Qual é a sua avalição do Programa de Ginástica Laboral?</h5>
						</div>
					</div>

					<div class="row">
						<div class="col s12 m3">
							<label><input type="radio" name="group1" value="1"/><span>Ruim</span></label>
						</div>

						<div class="col s12 m3">
							<label><input type="radio" name="group1" value="2"/><span>Regular</span></label>
						</div>

						<div class="col s12 m3">
							<label><input type="radio" name="group1" value="3"/><span>Bom</span></label>
						</div>

						<div class="col s12 m3">
							<label><input type="radio" name="group1" value="4"/><span>Ótimo</span></label>
						</div>
					</div>





					<div class="row">
						<div class="input-field col s12 m12">
							<h5>Qual é a sua avaliação do professor de Ginástica Laboral?</h5>
						</div>
					</div>

					<div class="row">
						<div class="col s12 m3">
							<label><input type="radio" name="group2" value="1"/><span>Ruim</span></label>
						</div>

						<div class="col s12 m3">
							<label><input type="radio" name="group2" value="2"/><span>Regular</span></label>
						</div>

						<div class="col s12 m3">
							<label><input type="radio" name="group2" value="3"/><span>Bom</span></label>
						</div>

						<div class="col s12 m3">
							<label><input type="radio" name="group2" value="4"/><span>Ótimo</span></label>
						</div>
					</div>





					<div class="row">
						<div class="input-field col s12 m12">
							<h5>Qual tipo de aula de Ginástica Laboral que mais gosta, marque até 2 opções:</h5>
						</div>
					</div>

					<div class="row">

						<div class="col s12 m4">
							<label><input type="checkbox" class="filled-in" name="gosta1"/><span>Aulas de alongamentos tradicionais</span></label>
						</div>

						<div class="col s12 m4">
							<label><input type="checkbox" class="filled-in" name="gosta2"/><span>Aulas de alongamentos com materiais</span></label>
						</div>

						<div class="col s12 m4">
							<label><input type="checkbox" class="filled-in" name="gosta3"/><span>Aulas temáticas</span></label>
						</div>

					</div>

					<div class="row">

						<div class="col s12 m4">
							<label><input type="checkbox" class="filled-in" name="gosta4"/><span>Aulas dinâmicas / motivacionais</span></label>
						</div>

						<div class="col s12 m4">
							<label><input type="checkbox" class="filled-in" name="gosta5"/><span>Aulas de relaxamento e massagem</span></label>
						</div>

						<div class="col s12 m4">
							<label><input type="checkbox" class="filled-in" name="gosta6"/><span>Gosta de todos os tipos de aula</span></label>
						</div>


					</div>





					<div class="row">
						<div class="input-field col s12 m12">
							<h5>Sobre o Programa de Ginástica Laboral (Caso a resposta seja negativa, não altere a opção):</h5>
						</div>
					</div>

					<div class="row">
						<div class="col s12 m6">Você participa regularmente da ginástica laboral?</div>
						<div class="col s12 m6">
							<div class="switch">
								<label>Não<input type="checkbox" name="b1"><span class="lever"></span> Sim </label>
							</div>
						</div>
					</div>

					<div class="row">
						<div class="col s12 m6">Você percebeu melhora na sua postura corporal?</div>
						<div class="col s12 m6">
							<div class="switch">
								<label>Não<input type="checkbox" name="b2"><span class="lever"></span> Sim </label>
							</div>
						</div>
					</div>

					<div class="row">
						<div class="col s12 m6">Você percebeu melhora no relacionamento com os colegas de trabalho?</div>
						<div class="col s12 m6">
							<div class="switch">
								<label>Não<input type="checkbox" name="b3"><span class="lever"></span> Sim </label>
							</div>
						</div>
					</div>

					<div class="row">
						<div class="col s12 m6">Considera a periodicidade de realização das aulas adequada?</div>
						<div class="col s12 m6">
							<div class="switch">
								<label>Não<input type="checkbox" name="b4"><span class="lever"></span> Sim </label>
							</div>
						</div>
					</div>

					<div class="row">
						<div class="col s12 m6">A prática da ginástica laboral trouxe algum benefício para você?</div>
						<div class="col s12 m6">
							<div class="switch">
								<label>Não<input type="checkbox" name="b5"><span class="lever"></span> Sim </label>
							</div>
						</div>
					</div>

					<div class="row">
						<div class="col s12 m6">Acredita que a continuidade do programa evidenciará as melhoras?</div>
						<div class="col s12 m6">
							<div class="switch">
								<label>Não<input type="checkbox" name="b6"><span class="lever"></span> Sim </label>
							</div>
						</div>
					</div>





					<div class="row">
						<div class="input-field col s12 m12">
							<h5>Área de Interesse (marque até 3):</h5>
						</div>
					</div>

					<div class="row">

						<div class="col s12 m3">
							<label><input type="checkbox" class="filled-in" name="c1"/><span>Nutrição/alimentação</span></label>
						</div>

						<div class="col s12 m3">
							<label><input type="checkbox" class="filled-in" name="c2"//><span>Gastronomia</span></label>
						</div>

						<div class="col s12 m3">
							<label><input type="checkbox" class="filled-in" name="c3"//><span>Finanças pessoais</span></label>
						</div>

						<div class="col s12 m3">
							<label><input type="checkbox" class="filled-in" name="c4"//><span>Cinema/leitura</span></label>
						</div>
					</div>

					<div class="row">
						<div class="col s12 m3">
							<label><input type="checkbox" class="filled-in" name="c5"//><span>Equilíbrio interior/meditação</span></label>
						</div>

						<div class="col s12 m3">
							<label><input type="checkbox" class="filled-in" name="c6"//><span>Distúrbio do sono</span></label>
						</div>

						<div class="col s12 m3">
							<label><input type="checkbox" class="filled-in" name="c7"//><span>Estética com saúde</span></label>
						</div>

						<div class="col s12 m3">
							<label><input type="checkbox" class="filled-in" name="c8"//><span>Terapias de relaxamento</span></label>
						</div>
					</div>

					<div class="row">
						<div class="col s12 m3">
							<label><input type="checkbox" class="filled-in" name="c9"//><span>Música/Dança</span></label>
						</div>
					</div>

					<div class="row">
						<div class="input-field col s12">
							<input id="Outro" type="text" class="validate" name="outro">
							<label for="Outro">Outro</label>
						</div>
					</div>





					<div class="row">
						<div class="col s12 m12">
							<h5>Qual horário que você se sente menos produtivo:</h5>
						</div>
					</div>

					<div class="row">

						<div class="col s12 m5">
							<label><input type="checkbox" name="d1" class="filled-in"/><span>Manhã</span></label>
						</div>

						<div class="col s12 m5">
							<label><input type="checkbox" name="d2" class="filled-in"/><span>Tarde</span></label>
						</div>

						<div class="col s12 m2">
							<label><input type="checkbox" name="d3" class="filled-in"/><span>Tarde/Noite</span></label>
						</div>
					</div>





					<div class="row">
						<div class="input-field col s12 m12">
							<h5>De 1 (mínimo) a 5 ( máximo). Qual é o seu grau de estresse:</h5>
						</div>
					</div>

					<div class="row">
						<div class="col s12 m2">
							<label><input type="radio" name="group3" value="1"/><span>1</span></label>
						</div>

						<div class="col s12 m2">
							<label><input type="radio" name="group3" value="2"/><span>2</span></label>
						</div>

						<div class="col s12 m2">
							<label><input type="radio" name="group3" value="3"/><span>3</span></label>
						</div>

						<div class="col s12 m2">
							<label><input type="radio" name="group3" value="4"/><span>4</span></label>
						</div>

						<div class="col s12 m4">
							<label><input type="radio" name="group3" value="5"/><span>5</span></label>
						</div>
					</div>





					<div class="row">
						<div class="input-field col s12 m12">
							<h5>SINTOMAS - Você tem apresentado regularmente algum dos sintomas abaixo? (Caso a resposta seja negativa, não altere a opção)</h5>
						</div>
					</div>

					<div class="row">
						<div class="col s12 m6">Desconforto físico</div>
						<div class="col s12 m6">
							<div class="switch">
								<label>Não<input type="checkbox" name="e1"><span class="lever"></span> Sim </label>
							</div>
						</div>
					</div>

					<div class="row">
						<div class="col s12 m6">Indisposição para o trabalho</div>
						<div class="col s12 m6">
							<div class="switch">
								<label>Não<input type="checkbox" name="e1"><span class="lever"></span> Sim </label>
							</div>
						</div>
					</div>

					<div class="row">
						<div class="col s12 m6">Desanimo</div>
						<div class="col s12 m6">
							<div class="switch">
								<label>Não<input type="checkbox" name="e1"><span class="lever"></span> Sim </label>
							</div>
						</div>
					</div>

					<div class="row">
						<div class="col s12 m6">Dor de cabeça</div>
						<div class="col s12 m6">
							<div class="switch">
								<label>Não<input type="checkbox" name="e2"><span class="lever"></span> Sim </label>
							</div>
						</div>
					</div>

					<div class="row">
						<div class="col s12 m6">Ansiedade</div>
						<div class="col s12 m6">
							<div class="switch">
								<label>Não<input type="checkbox" name="e3"><span class="lever"></span> Sim </label>
							</div>
						</div>
					</div>

					<div class="row">
						<div class="col s12 m6">Irritabilidade</div>
						<div class="col s12 m6">
							<div class="switch">
								<label>Não<input type="checkbox" name="e4"><span class="lever"></span> Sim </label>
							</div>
						</div>
					</div>

					<div class="row">
						<div class="col s12 m6">Perda de memória</div>
						<div class="col s12 m6">
							<div class="switch">
								<label>Não<input type="checkbox" name="e5"><span class="lever"></span> Sim </label>
							</div>
						</div>
					</div>

					<div class="row">
						<div class="col s12 m6">Sente que tem trabalhado abaixo do seu potencial</div>
						<div class="col s12 m6">
							<div class="switch">
								<label>Não<input type="checkbox" name="e6"><span class="lever"></span> Sim </label>
							</div>
						</div>
					</div>

					<div class="row">
						<div class="col s12 m6">Sente vontade de ficar sozinho</div>
						<div class="col s12 m6">
							<div class="switch">
								<label>Não<input type="checkbox" name="e7"><span class="lever"></span> Sim </label>
							</div>
						</div>
					</div>





					<div class="row">
						<div class="input-field col s12 m12">
							<h5>Você buscou tratamento, nos últimos 6 meses para (Caso a resposta seja negativa, não altere a opção):</h5>
						</div>
					</div>

					<div class="row">
						<div class="col s12 m6">Dores nos membros superiores</div>
						<div class="col s12 m6">
							<div class="switch">
								<label>Não<input type="checkbox" name="f1"><span class="lever"></span> Sim </label>
							</div>
						</div>
					</div>

					<div class="row">
						<div class="col s12 m6">Dores nos membros inferiores</div>
						<div class="col s12 m6">
							<div class="switch">
								<label>Não<input type="checkbox" name="f2"><span class="lever"></span> Sim </label>
							</div>
						</div>
					</div>

					<div class="row">
						<div class="col s12 m6">Dores nas costas</div>
						<div class="col s12 m6">
							<div class="switch">
								<label>Não<input type="checkbox" name="f3"><span class="lever"></span> Sim </label>
							</div>
						</div>
					</div>

					<div class="row">
						<div class="col s12 m6">Sintomas de estresse</div>
						<div class="col s12 m6">
							<div class="switch">
								<label>Não<input type="checkbox" name="f4"><span class="lever"></span> Sim </label>
							</div>
						</div>
					</div>





					<div class="row">
						<div class="input-field col s12 m12">
							<h5>Você se ausentou do trabalho nos últimos 6 meses por causa de (Caso a resposta seja negativa, não altere a opção):</h5>
						</div>
					</div>

					<div class="row">
						<div class="col s12 m6">Dores nos membros superiores</div>
						<div class="col s12 m6">
							<div class="switch">
								<label>Não<input type="checkbox" name="g1"><span class="lever"></span> Sim </label>
							</div>
						</div>
					</div>

					<div class="row">
						<div class="col s12 m6">Dores nos membros inferiores</div>
						<div class="col s12 m6">
							<div class="switch">
								<label>Não<input type="checkbox" name="g2"><span class="lever"></span> Sim </label>
							</div>
						</div>
					</div>

					<div class="row">
						<div class="col s12 m6">Dores nas costas</div>
						<div class="col s12 m6">
							<div class="switch">
								<label>Não<input type="checkbox" name="g3"><span class="lever"></span> Sim </label>
							</div>
						</div>
					</div>

					<div class="row">
						<div class="col s12 m6">Sintomas de estresse</div>
						<div class="col s12 m6">
							<div class="switch">
								<label>Não<input type="checkbox" name="g4"><span class="lever"></span> Sim </label>
							</div>
						</div>
					</div>





					<div class="row">
						<div class="input-field col s12 m12">
							<h5>Perfil de Qualidade de Vida (Caso a resposta seja negativa, não altere a opção):</h5>
						</div>
					</div>

					<div class="row">
						<div class="col s12 m6">É fumante?</div>
						<div class="col s12 m6">
							<div class="switch">
								<label>Não<input type="checkbox" name="h1"><span class="lever"></span> Sim </label>
							</div>
						</div>
					</div>

					<div class="row">
						<div class="col s12 m6">Pratica atividade física?</div>
						<div class="col s12 m6">
							<div class="switch">
								<label>Não<input type="checkbox" name="h2"><span class="lever"></span> Sim </label>
							</div>
						</div>
					</div>

					<div class="row">
						<div class="col s12 m6">Estuda antes ou depois do trabalho?</div>
						<div class="col s12 m6">
							<div class="switch">
								<label>Não<input type="checkbox" name="h3"><span class="lever"></span> Sim </label>
							</div>
						</div>
					</div>

					<div class="row">
						<div class="col s12 m6">Tem outro trabalho?</div>
						<div class="col s12 m6">
							<div class="switch">
								<label>Não<input type="checkbox" name="h4"><span class="lever"></span> Sim </label>
							</div>
						</div>
					</div>

					<div class="row">
						<div class="col s12 m6">Toca instrumento musical?</div>
						<div class="col s12 m6">
							<div class="switch">
								<label>Não<input type="checkbox" name="h5"><span class="lever"></span> Sim </label>
							</div>
						</div>
					</div>

					<div class="row">
						<div class="col s12 m6">Tem algum problema de audição?</div>
						<div class="col s12 m6">
							<div class="switch">
								<label>Não<input type="checkbox" name="h6"><span class="lever"></span> Sim </label>
							</div>
						</div>
					</div>

					<div class="row">
						<div class="col s12 m6">Tem algum problema nas cordas vocais?</div>
						<div class="col s12 m6">
							<div class="switch">
								<label>Não<input type="checkbox" name="h7"><span class="lever"></span> Sim </label>
							</div>
						</div>
					</div>

					<div class="row">
						<div class="col s12 m6">Se alimenta de forma adequada?</div>
						<div class="col s12 m6">
							<div class="switch">
								<label>Não<input type="checkbox" name="h8"><span class="lever"></span> Sim </label>
							</div>
						</div>
					</div>

					<div class="row">
						<div class="col s12 m6">Apresenta pressão alta?</div>
						<div class="col s12 m6">
							<div class="switch">
								<label>Não<input type="checkbox" name="h9"><span class="lever"></span> Sim </label>
							</div>
						</div>
					</div>

					<div class="row">
						<div class="col s12 m6">Tem diabetes?</div>
						<div class="col s12 m6">
							<div class="switch">
								<label>Não<input type="checkbox" name="h10"><span class="lever"></span> Sim </label>
							</div>
						</div>
					</div>

					<div class="row">
						<div class="col s12 m6">Usa óculos ou lente corretora?</div>
						<div class="col s12 m6">
							<div class="switch">
								<label>Não<input type="checkbox" name="h11"><span class="lever"></span> Sim </label>
							</div>
						</div>
					</div>


					<br>
					<div class="row">
						<div class="col s12">
							<button class="btn2 waves-effect waves-light btn2" type="submit">Enviar</button>
						</div>
					</div>


				</form>
			</div>
		</div>

		<?php
	}

	if ( $_SESSION[ "acao" ][ 'quest' ] === "shiatsu" ) {
		
		unset($_SESSION[ "acao" ]);

		$_SESSION[ "acao" ][ 'quest' ] = "shiatsu";
		$_SESSION[ "acao" ][ "acao" ] = 'inserir';
		?>

		<br>
		<div class="container">
			<div class="row">
				<form class="col s12" action="controller/questionario.php" method="post" enctype="multipart/form-data">
					<div class="row">
						<div class="col s12 m12 l12">
							<h2>Shiatsu</h2>
						</div>
					</div>

					<div class="row">

						<input class="hide" type="text" name="id" value="">

						<div class="input-field col s12 m3">
							<select name="sexo">
								<option value="0" disabled selected>Selecione</option>
								<option value="1">Masculino</option>
								<option value="2">Feminino</option>
								<option value="3">Outro</option>

							</select>
							<label>Sexo:</label>
						</div>

						<div class="input-field col s12 m3">
							<input id="Idade" type="text" class="validate" name="idade" value="">
							<label for="Idade">Idade</label>
						</div>

						<div class="input-field col s12 m3">
							<input id="Peso" type="text" class="validate" name="peso" value="">
							<label for="Peso">Peso: Kg</label>
						</div>

						<div class="input-field col s12 m3">
							<input id="Altura" type="text" class="validate" name="altura" value="">
							<label for="Altura">Altura</label>
						</div>

					</div>

					<div class="row">

						<div class="input-field col s12 m4">
							<select name="lado">
								<option value="0" disabled selected>Selecione</option>
								<option value="1">Direito</option>
								<option value="2">Esquerdo</option>

							</select>
							<label>Lado dominante (que escreve):</label>
						</div>

						<div class="input-field col s12 m4">
							<select name="tempoempresa">
								<option value="0" disabled selected>Selecione</option>
								<option value="1">De 0 até 1 ano</option>
								<option value="2">De 1 até 2 anos</option>
								<option value="3">De 2 até 5 anos</option>
								<option value="3">Mais de 5 anos</option>

							</select>
							<label>Há quanto tempo trabalha na empresa?</label>
						</div>

						<div class="input-field col s12 m4">
							<input id="Departamento" type="text" class="validate" name="departamento" value="" placeholder="Unidade de Trabalho">
							<label for="Departamento">Departamento</label>
						</div>

					</div>


					<div class="row">
						<div class="col s12">
							<h5>Você sente frequentemente desconforto em alguma das regiões abaixo? Marque ao lado do número referente a região de desconforto. Observe atentamente a indicação dos lados no desenho.</h5>
						</div>
					</div>

					<div class="row">
						<div class="col s12">

							<div class="container-fluid">
								<div class="row">

									<div class="col-md-8 col-md-offset-2" style="min-height:495px;">

										<div id="#map" class="Corllet">

											<!-- 1 cabeça -->
											<label style="position:absolute; top: 0px; left: 179px;"><input type="checkbox" class="filled-in" name="body1"><span></span></label>
											<!-- 2 pescoço -->
											<label style="position:absolute; top:45px; left: 179px;"><input type="checkbox" class="filled-in" name="body2"><span></span></label>
											<!-- 3 cent -->
											<label style="position:absolute; top: 135px; left: 179px;"><input type="checkbox" class="filled-in" name="body3"><span></span></label>
											<!-- 4 cent -->
											<label style="position:absolute; top: 190px; left: 179px;"><input type="checkbox" class="filled-in" name="body4"><span></span></label>
											<!-- 5 cent -->
											<label style="position:absolute; top: 230px; left: 179px;"><input type="checkbox" class="filled-in" name="body5"><span></span></label>

											<!-- 6 dir -->
											<label style="position:absolute; top: 90px; left: 200px;"><input type="checkbox" class="filled-in" name="body6"><span></span></label>
											<!-- 7 dir -->
											<label style="position:absolute; top: 130px; left: 223px;"><input type="checkbox" class="filled-in" name="body7"><span></span></label>
											<!-- 8 dir -->
											<label style="position:absolute; top: 160px; left: 233px;"><input type="checkbox" class="filled-in" name="body8"><span></span></label>
											<!-- 9 dir -->
											<label style="position:absolute; top: 185px; left: 250px;"><input type="checkbox" class="filled-in" name="body9"><span></span></label>
											<!-- 10 dir -->
											<label style="position:absolute; top: 209px; left: 264px;"><input type="checkbox" class="filled-in" name="body10"><span></span></label>
											<!-- 11 dir -->
											<label style="position:absolute; top: 234px; left: 273px;"><input type="checkbox" class="filled-in" name="body11"><span></span></label>
											<!-- 12 dir -->
											<label style="position:absolute; top: 280px; left: 223px;"><input type="checkbox" class="filled-in" name="body12"><span></span></label>
											<!-- 13 dir -->
											<label style="position:absolute; top: 327px; left: 218px;"><input type="checkbox" class="filled-in" name="body13"><span></span></label>
											<!-- 14 dir -->
											<label style="position:absolute; top: 365px; left: 216px;"><input type="checkbox" class="filled-in" name="body14"><span></span></label>
											<!-- 15 dir -->
											<label style="position:absolute; top: 415px; left: 208px;"><input type="checkbox" class="filled-in" name="body15"><span></span></label>
											<!-- 16 dir -->
											<label style="position:absolute; top: 440px; left: 208px;"><input type="checkbox" class="filled-in" name="body16"><span></span></label>

											<!-- 6 esq -->
											<label style="position:absolute; top: 90px; left: 157px;"><input type="checkbox" class="filled-in" name="body17"><span></span></label>
											<!-- 7 esq -->
											<label style="position:absolute; top: 130px; left: 135px;"><input type="checkbox" class="filled-in" name="body18"><span></span></label>
											<!-- 8 esq -->
											<label style="position:absolute; top: 160px; left: 124px;"><input type="checkbox" class="filled-in" name="body19"><span></span></label>
											<!-- 9 esq -->
											<label style="position:absolute; top: 185px; left: 110px;"><input type="checkbox" class="filled-in" name="body20"><span></span></label>
											<!-- 10 esq -->
											<label style="position:absolute; top: 210px; left: 95px;"><input type="checkbox" class="filled-in" name="body21"><span></span></label>
											<!-- 11 esq -->
											<label style="position:absolute; top: 235px; left: 85px;"><input type="checkbox" class="filled-in" name="body22"><span></span></label>
											<!-- 12 esq -->
											<label style="position:absolute; top: 280px; left: 135px;"><input type="checkbox" class="filled-in" name="body23"><span></span></label>
											<!-- 13 esq -->
											<label style="position:absolute; top: 327px; left: 140px;"><input type="checkbox" class="filled-in" name="body24"><span></span></label>
											<!-- 14 esq -->
											<label style="position:absolute; top: 365px; left: 140px;"><input type="checkbox" class="filled-in" name="body25"><span></span></label>
											<!-- 15 esq -->
											<label style="position:absolute; top: 415px; left: 143px;"><input type="checkbox" class="filled-in" name="body26"><span></span></label>
											<!-- 16 esq -->
											<label style="position:absolute; top: 440px; left: 144px;"><input type="checkbox" class="filled-in" name="body27"><span></span></label>

										</div>

									</div>
								</div>
							</div>



						</div>
					</div>





					<div class="row">

						<div class="input-field col s12 m6">
							<select name="massage">
								<option value="0" disabled selected>Selecione</option>
								<option value="1">1 vez</option>
								<option value="2">2 vezes</option>
								<option value="3">3 a 5 vezes</option>
								<option value="3">+ de 5 vezes</option>

							</select>
							<label>Quantas vezes você realiza a atividade de Quick Massage no mês?</label>
						</div>
					</div>





					<div class="row">
						<div class="input-field col s12 m12">
							<h5>Qual o primeiro item que lhe vem a mente quando ouve as palavras Quick Massage, relativo ao ambiente de trabalho? Marque até 2 alternativas.</h5>
						</div>
					</div>

					<div class="row">

						<div class="col s12 m4">
							<label><input type="checkbox" class="filled-in" name="a1"/><span>Momento de carinho da empresa consigo próprio</span></label>
						</div>

						<div class="col s12 m4">
							<label><input type="checkbox" class="filled-in" name="a2"/><span>Momento de recuperar as energias</span></label>
						</div>

						<div class="col s12 m4">
							<label><input type="checkbox" class="filled-in" name="a3"/><span>Pausa fundamental para o dia de trabalho</span></label>
						</div>

					</div>

					<div class="row">

						<div class="col s12 m4">
							<label><input type="checkbox" class="filled-in" name="a4"/><span>Pausa que agrega, mas não fundamental para o dia de trabalho</span></label>
						</div>

						<div class="col s12 m4">
							<label><input type="checkbox" class="filled-in" name="a5"/><span>Pausa indiferente para o dia trabalho</span></label>
						</div>

						<div class="col s12 m4">
							<label><input type="checkbox" class="filled-in" name="a6"/><span>Premiação merecida pelo bom serviço prestado</span></label>
						</div>

					</div>

					<div class="row">

						<div class="col s12 m4">
							<label><input type="checkbox" class="filled-in" name="a7"/><span>Relaxamento</span></label>
						</div>

						<div class="col s12 m8">
							<label><input type="checkbox" class="filled-in" name="a8"/><span>Tratamento de lesões musculares</span></label>
						</div>

					</div>




					<div class="row">
						<div class="input-field col s12 m12">
							<h5>Durante a sessão de Quick Massage, qual é o pré-requisito mais importante para que você tenha um relaxamento pleno? Marque até 2 alternativas.</h5>
						</div>
					</div>

					<div class="row">

						<div class="col s12 m3">
							<label><input type="checkbox" class="filled-in" name='gosta1'/><span>Luz do ambiente</span></label>
						</div>

						<div class="col s12 m3">
							<label><input type="checkbox" class="filled-in" name='gosta2'/><span>Empatia do profissional</span></label>
						</div>

						<div class="col s12 m3">
							<label><input type="checkbox" class="filled-in" name='gosta3'/><span>Toque inicial da sessão</span></label>
						</div>

						<div class="col s12 m3">
							<label><input type="checkbox" class="filled-in" name='gosta4'/><span>Música ambiente</span></label>
						</div>

					</div>

					<div class="row">

						<div class="col s12 m6">
							<label><input type="checkbox" class="filled-in" name='gosta5'/><span>Conforto no local que será realizada a Quick Massage (maca, cadeira)</span></label>
						</div>

						<div class="col s12 m6">
							<label><input type="checkbox" class="filled-in" name='gosta6'/><span>Finalização da sessão</span></label>
						</div>

					</div>





					<div class="row">
						<div class="input-field col s12 m12">
							<h5>Sobre o efeito das sessões de Quick Massage em relação ao seu trabalho, você:</h5>
						</div>
					</div>

					<div class="row">
						<div class="col s12 m6">Acredita que consegue melhorar o desempenho após a sessão</div>
						<div class="col s12 m6">
							<div class="switch">
								<label>Não<input type="checkbox" name="group1"><span class="lever"></span> Sim </label>
							</div>
						</div>
					</div>

					<div class="row">
						<div class="col s12 m6">Não sente diferença quanto a produtividade após a sessão</div>
						<div class="col s12 m6">
							<div class="switch">
								<label>Não<input type="checkbox" name="group2" ><span class="lever"></span> Sim </label>
							</div>
						</div>
					</div>

					<div class="row">
						<div class="input-field col s12 m6">
							<input id="Justifique" type="text" class="validate" name="justifique">
							<label for="Justifique">Justifique</label>
						</div>
					</div>





					<div class="row">
						<div class="input-field col s12 m12">
							<h5>Sobre o efeito das sessões de Quick Massage em relação ao seu corpo e mente, você: Marque até 2 alternativas.</h5>
						</div>
					</div>

					<div class="row">

						<div class="col s12 m6">
							<label><input type="checkbox" class="filled-in" name="b1"/><span>Acredita que as sessões estão sendo útil no melhor funcionamento fisiológico do corpo</span></label>
						</div>

						<div class="col s12 m6">
							<label><input type="checkbox" class="filled-in" name="b2"/><span>Considera importante para ter um momento sozinho, com objetivo de organizar melhor as estratégias de trabalho</span></label>
						</div>

					</div>

					<div class="row">

						<div class="col s12 m6">
							<label><input type="checkbox" class="filled-in" name="b3"/><span>Descobriu algum ponto doloroso incomum, que acabou por servir de alerta</span></label>
						</div>

						<div class="col s12 m6">
							<label><input type="checkbox" class="filled-in" name="b4"/><span>Percebeu que a tensão muscular diminuiu com a regularidade das sessões</span></label>
						</div>

					</div>

					<div class="row">

						<div class="col s12 m6">
							<label><input type="checkbox" class="filled-in" name="b5"/><span>Sente maior alívio mental / diminuição do estresse</span></label>
						</div>

					</div>





					<div class="row">
						<div class="input-field col s12 m12">
							<h5>Sobre o tempo das sessões de Quick Massage, considerando que se encontra em um ambiente de trabalho, você considera o tempo de Quick Massage:</h5>
						</div>
					</div>

					<div class="row">
						<div class="col s12 m4">
							<label><input type="radio" name="group3" value="1"/><span>Sim</span></label>
						</div>

						<div class="col s12 m4">
							<label><input type="radio" name="group3" value="2"/><span>Pouco Tempo</span></label>
						</div>

						<div class="col s12 m4">
							<label><input type="radio" name="group3" value="3"/><span>Muito Tempo</span></label>
						</div>
					</div>





					<div class="row">
						<div class="input-field col s12 m12">
							<h5>Em que região do corpo você sente maior alívio do estresse, quando massageado? Marque até 2 alternativas.</h5>
						</div>
					</div>

					<div class="row">

						<div class="col s12 m2">
							<label><input type="checkbox" class="filled-in" name="c1"/><span>Cabeça</span></label>
						</div>

						<div class="col s12 m2">
							<label><input type="checkbox" class="filled-in" name="c2"/><span>Ombros</span></label>
						</div>

						<div class="col s12 m2">
							<label><input type="checkbox" class="filled-in" name="c3"/><span>Mãos</span></label>
						</div>

						<div class="col s12 m2">
							<label><input type="checkbox" class="filled-in" name="c4"/><span>Costas</span></label>
						</div>

						<div class="col s12 m2">
							<label><input type="checkbox" class="filled-in" name="c5"/><span>Quadril</span></label>
						</div>

						<div class="col s12 m2">
							<label><input type="checkbox" class="filled-in" name="c6"/><span>Pés</span></label>
						</div>

					</div>





					<div class="row">
						<div class="input-field col s12 m12">
							<h5>Sobre o(s) profissional(ais) que executa(m) a Quick Massage:</h5>
						</div>
					</div>

					<div class="row">
						<div class="col s12 m4">
							<label><input type="radio" name="group4" value="1"/><span>Sim</span></label>
						</div>

						<div class="col s12 m4">
							<label><input type="radio" name="group4" value="2"/><span>Pouco Tempo</span></label>
						</div>

						<div class="col s12 m4">
							<label><input type="radio" name="group4" value="3"/><span>Depende de quem me atende</span></label>
						</div>
					</div>

					<div class="row">
						<div class="input-field col s12 m6">
							<input id="Justifique" type="text" class="validate" name="justifique2" value="">
							<label for="Justifique">Justifique</label>
						</div>
					</div>





					<div class="row">
						<div class="input-field col s12 m12">
							<h5>O seu terapeuta questiona, antes da sessão, a região de preferência da massagem naquele dia específico?</h5>
						</div>
					</div>

					<div class="row">
						<div class="col s12 m4">
							<label><input type="radio" name="group6" value="1"/><span>Sim</span></label>
						</div>

						<div class="col s12 m4">
							<label><input type="radio" name="group6" value="2"/><span>Pouco Tempo</span></label>
						</div>

						<div class="col s12 m4">
							<label><input type="radio" name="group6" value="3"/><span>Depende de quem me atende</span></label>
						</div>
					</div>

					<div class="row">
						<div class="input-field col s12 m6">
							<input id="Justifique" type="text" class="validate" name="justifique3" value="">
							<label for="Justifique">Justifique</label>
						</div>
					</div>





					<div class="row">
						<div class="input-field col s12 m12">
							<h5>O seu terapeuta questiona, antes da sessão, se você está com alguma dor/enfermidade?</h5>
						</div>
					</div>

					<div class="row">
						<div class="col s12 m4">
							<label><input type="radio" name="group7" value="1"/><span>Sim</span></label>
						</div>

						<div class="col s12 m4">
							<label><input type="radio" name="group7" value="2"/><span>Pouco Tempo</span></label>
						</div>

						<div class="col s12 m4">
							<label><input type="radio" name="group7" value="3"/><span>Depende de quem me atende</span></label>
						</div>
					</div>

					<div class="row">
						<div class="input-field col s12 m6">
							<input id="Justifique" type="text" class="validate" name="justifiqu4" value="">
							<label for="Justifique">Justifique</label>
						</div>
					</div>


					<div class="row">
						<div class="input-field col s12 m12">
							<h5>De 1 (muito negativo) a 5 (muito positivo), qual a nota para o profissional que lhe atende:</h5>
						</div>
					</div>

					<div class="row">
						<div class="input-field col s12 m8">
							<input id="Profissional1" type="text" class="validate" name="profissional" value="">
							<label for="Profissional1">Nome do Profissional</label>
						</div>

						<div class="input-field col s12 m4">
							<select name="nota">
								<option value="0" disabled selected>Selecione</option>
								<option value="1">1</option>
								<option value="2">2</option>
								<option value="3">3</option>
								<option value="4">4</option>
								<option value="5">5</option>

							</select>
							<label>Nota</label>
						</div>
					</div>


					<div class="row">
						<div class="input-field col s12 m12">
							<h5>Gostaria de fazer algum comentário sobre o(s) profissional(ais):</h5>
						</div>
					</div>

					<div class="row">
						<div class="input-field col s12">
							<textarea id="textarea1" class="materialize-textarea" name="comentario"></textarea>
							<label for="textarea1">Deixe aqui seu comentário</label>
						</div>
					</div>
					

					<div class="row">
						<div class="input-field col s12 m12">
							<h5>Área de Interesse (marque até 3):</h5>
						</div>
					</div>

					<div class="row">

						<div class="col s12 m3">
							<label><input type="checkbox" class="filled-in" name="d1"/><span>Nutrição/alimentação</span></label>
						</div>

						<div class="col s12 m3">
							<label><input type="checkbox" class="filled-in" name="d2"/><span>Gastronomia</span></label>
						</div>

						<div class="col s12 m3">
							<label><input type="checkbox" class="filled-in" name="d3"/><span>Finanças pessoais</span></label>
						</div>

						<div class="col s12 m3">
							<label><input type="checkbox" class="filled-in" name="d4"/><span>Cinema/leitura</span></label>
						</div>
					</div>

					<div class="row">
						<div class="col s12 m3">
							<label><input type="checkbox" class="filled-in" name="d5"/><span>Equilíbrio interior/meditação</span></label>
						</div>

						<div class="col s12 m3">
							<label><input type="checkbox" class="filled-in" name="d6"/><span>Distúrbio do sono</span></label>
						</div>

						<div class="col s12 m3">
							<label><input type="checkbox" class="filled-in" name="d7"/><span>Estética com saúde</span></label>
						</div>

						<div class="col s12 m3">
							<label><input type="checkbox" class="filled-in" name="d8"/><span>Terapias de relaxamento</span></label>
						</div>
					</div>

					<div class="row">
						<div class="col s12 m3">
							<label><input type="checkbox" class="filled-in" name="d9"/><span>Música/Dança</span></label>
						</div>
					</div>

					<div class="row">
						<div class="input-field col s12 m6">
							<input id="Outro" type="text" class="validate" name="doutro" value="">
							<label for="Outro">Outro</label>
						</div>
					</div>



					<div class="row">
						<div class="col s12 m12">
							<h5>Qual horário que você se sente menos produtivo:</h5>
						</div>
					</div>

					<div class="row">

						<div class="col s12 m5">
							<label><input type="checkbox" class="filled-in" name="e1"/><span>Manhã</span></label>
						</div>

						<div class="col s12 m5">
							<label><input type="checkbox" class="filled-in" name="e2"/><span>Tarde</span></label>
						</div>

						<div class="col s12 m2">
							<label><input type="checkbox" class="filled-in" name="e3"/><span>Tarde/Noite</span></label>
						</div>
					</div>





					<div class="row">
						<div class="input-field col s12 m12">
							<h5>De 1 (mínimo) a 5 ( máximo). Qual é o seu grau de estresse:</h5>
						</div>
					</div>

					<div class="row">
						<div class="col s12 m2">
							<label><input type="radio" name="group8" value="1"/><span>1</span></label>
						</div>

						<div class="col s12 m2">
							<label><input type="radio" name="group8" value="2"/><span>2</span></label>
						</div>

						<div class="col s12 m2">
							<label><input type="radio" name="group8" value="3"/><span>3</span></label>
						</div>

						<div class="col s12 m2">
							<label><input type="radio" name="group8" value="4"/><span>4</span></label>
						</div>

						<div class="col s12 m4">
							<label><input type="radio" name="group8" value="5"/><span>5</span></label>
						</div>
					</div>





					<div class="row">
						<div class="input-field col s12 m12">
							<h5>SINTOMAS - Você tem apresentado regularmente algum dos sintomas abaixo? (Caso a resposta seja negativa, não altere a opção)</h5>
						</div>
					</div>

					<div class="row">
						<div class="col s12 m6">Desconforto físico</div>
						<div class="col s12 m6">
							<div class="switch">
								<label>Não<input type="checkbox" name="f1"><span class="lever"></span> Sim </label>
							</div>
						</div>
					</div>

					<div class="row">
						<div class="col s12 m6">Indisposição para o trabalho</div>
						<div class="col s12 m6">
							<div class="switch">
								<label>Não<input type="checkbox" name="f2"><span class="lever"></span> Sim </label>
							</div>
						</div>
					</div>

					<div class="row">
						<div class="col s12 m6">Desanimo</div>
						<div class="col s12 m6">
							<div class="switch">
								<label>Não<input type="checkbox" name="f3"><span class="lever"></span> Sim </label>
							</div>
						</div>
					</div>

					<div class="row">
						<div class="col s12 m6">Dor de cabeça</div>
						<div class="col s12 m6">
							<div class="switch">
								<label>Não<input type="checkbox" name="f4"><span class="lever"></span> Sim </label>
							</div>
						</div>
					</div>

					<div class="row">
						<div class="col s12 m6">Ansiedade</div>
						<div class="col s12 m6">
							<div class="switch">
								<label>Não<input type="checkbox" name="f5"><span class="lever"></span> Sim </label>
							</div>
						</div>
					</div>

					<div class="row">
						<div class="col s12 m6">Irritabilidade</div>
						<div class="col s12 m6">
							<div class="switch">
								<label>Não<input type="checkbox" name="f6"><span class="lever"></span> Sim </label>
							</div>
						</div>
					</div>

					<div class="row">
						<div class="col s12 m6">Perda de memória</div>
						<div class="col s12 m6">
							<div class="switch">
								<label>Não<input type="checkbox" name="f7"><span class="lever"></span> Sim </label>
							</div>
						</div>
					</div>

					<div class="row">
						<div class="col s12 m6">Sente que tem trabalhado abaixo do seu potencial</div>
						<div class="col s12 m6">
							<div class="switch">
								<label>Não<input type="checkbox" name="f8"><span class="lever"></span> Sim </label>
							</div>
						</div>
					</div>

					<div class="row">
						<div class="col s12 m6">Sente vontade de ficar sozinho</div>
						<div class="col s12 m6">
							<div class="switch">
								<label>Não<input type="checkbox" name="f9"><span class="lever"></span> Sim </label>
							</div>
						</div>
					</div>





					<div class="row">
						<div class="input-field col s12 m12">
							<h5>Você buscou tratamento, nos últimos 6 meses para (Caso a resposta seja negativa, não altere a opção):</h5>
						</div>
					</div>

					<div class="row">
						<div class="col s12 m6">Dores nos membros superiores</div>
						<div class="col s12 m6">
							<div class="switch">
								<label>Não<input type="checkbox" name="f10"><span class="lever"></span> Sim </label>
							</div>
						</div>
					</div>

					<div class="row">
						<div class="col s12 m6">Dores nos membros inferiores</div>
						<div class="col s12 m6">
							<div class="switch">
								<label>Não<input type="checkbox" name="f11"><span class="lever"></span> Sim </label>
							</div>
						</div>
					</div>

					<div class="row">
						<div class="col s12 m6">Dores nas costas</div>
						<div class="col s12 m6">
							<div class="switch">
								<label>Não<input type="checkbox" name="f12"><span class="lever"></span> Sim </label>
							</div>
						</div>
					</div>

					<div class="row">
						<div class="col s12 m6">Sintomas de estresse</div>
						<div class="col s12 m6">
							<div class="switch">
								<label>Não<input type="checkbox" name="f13"><span class="lever"></span> Sim </label>
							</div>
						</div>
					</div>





					<div class="row">
						<div class="input-field col s12 m12">
							<h5>Você se ausentou do trabalho nos últimos 6 meses por causa de (Caso a resposta seja negativa, não altere a opção):</h5>
						</div>
					</div>

					<div class="row">
						<div class="col s12 m6">Dores nos membros superiores</div>
						<div class="col s12 m6">
							<div class="switch">
								<label>Não<input type="checkbox" name="f14"><span class="lever"></span> Sim </label>
							</div>
						</div>
					</div>

					<div class="row">
						<div class="col s12 m6">Dores nos membros inferiores</div>
						<div class="col s12 m6">
							<div class="switch">
								<label>Não<input type="checkbox" name="f15"><span class="lever"></span> Sim </label>
							</div>
						</div>
					</div>

					<div class="row">
						<div class="col s12 m6">Dores nas costas</div>
						<div class="col s12 m6">
							<div class="switch">
								<label>Não<input type="checkbox" name="f16"><span class="lever"></span> Sim </label>
							</div>
						</div>
					</div>

					<div class="row">
						<div class="col s12 m6">Sintomas de estresse</div>
						<div class="col s12 m6">
							<div class="switch">
								<label>Não<input type="checkbox" name="f17"><span class="lever"></span> Sim </label>
							</div>
						</div>
					</div>





					<div class="row">
						<div class="input-field col s12 m12">
							<h5>Perfil de Qualidade de Vida (Caso a resposta seja negativa, não altere a opção):</h5>
						</div>
					</div>

					<div class="row">
						<div class="col s12 m6">É fumante?</div>
						<div class="col s12 m6">
							<div class="switch">
								<label>Não<input type="checkbox" name="f18"><span class="lever"></span> Sim </label>
							</div>
						</div>
					</div>

					<div class="row">
						<div class="col s12 m6">Pratica atividade física?</div>
						<div class="col s12 m6">
							<div class="switch">
								<label>Não<input type="checkbox" name="f19"><span class="lever"></span> Sim </label>
							</div>
						</div>
					</div>

					<div class="row">
						<div class="col s12 m6">Estuda antes ou depois do trabalho?</div>
						<div class="col s12 m6">
							<div class="switch">
								<label>Não<input type="checkbox" name="f20"><span class="lever"></span> Sim </label>
							</div>
						</div>
					</div>

					<div class="row">
						<div class="col s12 m6">Tem outro trabalho?</div>
						<div class="col s12 m6">
							<div class="switch">
								<label>Não<input type="checkbox" name="f21"><span class="lever"></span> Sim </label>
							</div>
						</div>
					</div>

					<div class="row">
						<div class="col s12 m6">Toca instrumento musical?</div>
						<div class="col s12 m6">
							<div class="switch">
								<label>Não<input type="checkbox" name="f22"><span class="lever"></span> Sim </label>
							</div>
						</div>
					</div>

					<div class="row">
						<div class="col s12 m6">Tem algum problema de audição?</div>
						<div class="col s12 m6">
							<div class="switch">
								<label>Não<input type="checkbox" name="f23"><span class="lever"></span> Sim </label>
							</div>
						</div>
					</div>

					<div class="row">
						<div class="col s12 m6">Tem algum problema nas cordas vocais?</div>
						<div class="col s12 m6">
							<div class="switch">
								<label>Não<input type="checkbox" name="f24"><span class="lever"></span> Sim </label>
							</div>
						</div>
					</div>

					<div class="row">
						<div class="col s12 m6">Se alimenta de forma adequada?</div>
						<div class="col s12 m6">
							<div class="switch">
								<label>Não<input type="checkbox" name="f25"><span class="lever"></span> Sim </label>
							</div>
						</div>
					</div>

					<div class="row">
						<div class="col s12 m6">Apresenta pressão alta?</div>
						<div class="col s12 m6">
							<div class="switch">
								<label>Não<input type="checkbox" name="f26"><span class="lever"></span> Sim </label>
							</div>
						</div>
					</div>

					<div class="row">
						<div class="col s12 m6">Tem diabetes?</div>
						<div class="col s12 m6">
							<div class="switch">
								<label>Não<input type="checkbox" name="f27"><span class="lever"></span> Sim </label>
							</div>
						</div>
					</div>

					<div class="row">
						<div class="col s12 m6">Usa óculos ou lente corretora?</div>
						<div class="col s12 m6">
							<div class="switch">
								<label>Não<input type="checkbox" name="f28"><span class="lever"></span> Sim </label>
							</div>
						</div>
					</div>


					<br>
					<div class="row">
						<div class="col s12">
							<button class="btn2 waves-effect waves-light btn2" type="submit">Enviar</button>
						</div>
					</div>


				</form>
			</div>
		</div>

		<?php
	}

	if ( $_SESSION[ "acao" ][ 'quest' ] === "ergonomia" ) {
		$resposta=1;
		
		unset($_SESSION[ "acao" ]);

		$_SESSION[ "acao" ][ 'quest' ] = "ergonomia";
		$_SESSION[ "acao" ][ "acao" ] = 'inserir';
		?>

		<br>
		<div class="container">
			<div class="row">
				<form class="col s12" action="controller/questionario.php" method="post" enctype="multipart/form-data">
					<div class="row">
						<div class="col s12 m12 l12">
							<h2>Ergonomia</h2>
						</div>
					</div>

					<div class="row">

						<input class="hide" type="text" name="id" value="">

						<div class="input-field col s12 m2">
							<input id="Funcao" type="text" class="validate" name="funcao" value="">
							<label for="Funcao">Função</label>
						</div>

						<div class="input-field col s12 m3">
							<input id="Nivel" type="text" class="validate" name="nivel" value="">
							<label for="Nivel">Nível de Escolaridade</label>
						</div>

						<div class="input-field col s12 m1">
							<input id="Idade" type="text" class="validate" name="idade" value="">
							<label for="Idade">Idade</label>
						</div>

						<div class="input-field col s12 m3">
							<input id="PNE" type="text" class="validate" name="pne" value="">
							<label for="PNE">PNE</label>
						</div>

						<div class="input-field col s12 m3">
							<input id="Media" type="text" class="validate" name="media" value="">
							<label for="Media">Média de Horas Extras</label>
						</div>

					</div>





					<div class="row">
						<div class="col s12 m10">
							<h6>Você considera que a empresa dá condições de se conseguir os resultados que são cobrados?</h6>
						</div>

						<div class="input-field col s12 m2">
							<select name="resposta<?=$resposta++ ?>">
								<option value="0" disabled selected>Selecione</option>
								<option value="1">Sim</option>
								<option value="2">Não</option>
								<option value="3">N/A</option>

							</select>

						</div>
					</div>

					<div class="row">
						<div class="col s12 m10">
							<h6>Os sistemas são os adequados para se fazer a atividade que lhe é cobrada?</h6>
						</div>

						<div class="input-field col s12 m2">
							<select name="resposta<?=$resposta++ ?>">
								<option value="0" disabled selected>Selecione</option>
								<option value="1">Sim</option>
								<option value="2">Não</option>
								<option value="3">N/A</option>

							</select>

						</div>
					</div>

					<div class="row">
						<div class="col s12 m10">
							<h6>As máquinas são as adequadas para se fazer a atividade que lhe é cobrada?</h6>



						



						</div>

						<div class="input-field col s12 m2">
							<select name="resposta<?=$resposta++ ?>">
								<option value="0" disabled selected>Selecione</option>
								<option value="1">Sim</option>
								<option value="2">Não</option>
								<option value="3">N/A</option>

							</select>

						</div>
					</div>

					<div class="row">
						<div class="col s12 m10">
							<h6>Há material adequado para a realização do trabalho?</h6>
						</div>

						<div class="input-field col s12 m2">
							<select name="resposta<?=$resposta++ ?>">
								<option value="0" disabled selected>Selecione</option>
								<option value="1">Sim</option>
								<option value="2">Não</option>
								<option value="3">N/A</option>

							</select>

						</div>
					</div>

					<div class="row">
						<div class="col s12 m10">
							<h6>Você considera os métodos existentes na empresa para se conseguir os resultados adequados?</h6>
						</div>

						<div class="input-field col s12 m2">
							<select name="resposta<?=$resposta++ ?>">
								<option value="0" disabled selected>Selecione</option>
								<option value="1">Sim</option>
								<option value="2">Não</option>
								<option value="3">N/A</option>

							</select>

						</div>
					</div>

					<div class="row">
						<div class="col s12 m10">
							<h6>Você considera adequado os indicadores de desempenho existentes?</h6>
						</div>

						<div class="input-field col s12 m2">
							<select name="resposta<?=$resposta++ ?>">
								<option value="0" disabled selected>Selecione</option>
								<option value="1">Sim</option>
								<option value="2">Não</option>
								<option value="3">N/A</option>

							</select>

						</div>
					</div>

					<div class="row">
						<div class="col s12 m10">
							<h6>Existirem tempos pré- determinados para a execução da tarefa ou desempenho baseado em números é algo que lhe incomoda?</h6>
						</div>

						<div class="input-field col s12 m2">
							<select name="resposta<?=$resposta++ ?>">
								<option value="0" disabled selected>Selecione</option>
								<option value="1">Sim</option>
								<option value="2">Não</option>
								<option value="3">N/A</option>

							</select>

						</div>
					</div>

					<div class="row">
						<div class="col s12 m10">
							<h6>Você considera que, em geral, a empresa funciona bem?</h6>
						</div>

						<div class="input-field col s12 m2">
							<select name="resposta<?=$resposta++ ?>">
								<option value="0" disabled selected>Selecione</option>
								<option value="1">Sim</option>
								<option value="2">Não</option>
								<option value="3">N/A</option>

							</select>

						</div>
					</div>

					<div class="row">
						<div class="col s12 m10">
							<h6>Você considera que seu treinamento para a função foi o adequado?</h6>
						</div>

						<div class="input-field col s12 m2">
							<select name="resposta<?=$resposta++ ?>">
								<option value="0" disabled selected>Selecione</option>
								<option value="1">Sim</option>
								<option value="2">Não</option>
								<option value="3">N/A</option>

							</select>

						</div>
					</div>

					<div class="row">
						<div class="col s12 m10">
							<h6>Ocorrem erros por falta de comunicação?</h6>
						</div>

						<div class="input-field col s12 m2">
							<select name="resposta<?=$resposta++ ?>">
								<option value="0" disabled selected>Selecione</option>
								<option value="1">Sim</option>
								<option value="2">Não</option>
								<option value="3">N/A</option>

							</select>

						</div>
					</div>

					<div class="row">
						<div class="col s12 m10">
							<h6>Há pessoal em número suficiente para o trabalho a ser feito?</h6>
						</div>

						<div class="input-field col s12 m2">
							<select name="resposta<?=$resposta++ ?>">
								<option value="0" disabled selected>Selecione</option>
								<option value="1">Sim</option>
								<option value="2">Não</option>
								<option value="3">N/A</option>

							</select>

						</div>
					</div>

					<div class="row">
						<div class="col s12 m10">
							<h6>Em épocas de produção maior, há recursos humanos para executá-la?</h6>
						</div>

						<div class="input-field col s12 m2">
							<select name="resposta<?=$resposta++ ?>">
								<option value="0" disabled selected>Selecione</option>
								<option value="1">Sim</option>
								<option value="2">Não</option>
								<option value="3">N/A</option>

							</select>

						</div>
					</div>

					<div class="row">
						<div class="col s12 m10">
							<h6>Seus colegas consideram as chefias da empresa seguras e capazes?</h6>
						</div>

						<div class="input-field col s12 m2">
							<select name="resposta<?=$resposta++ ?>">
								<option value="0" disabled selected>Selecione</option>
								<option value="1">Sim</option>
								<option value="2">Não</option>
								<option value="3">N/A</option>

							</select>

						</div>
					</div>

					<div class="row">
						<div class="col s12 m10">
							<h6>Durante o seu trabalho você costuma ter um ritmo puxado do início ao fim?</h6>
						</div>

						<div class="input-field col s12 m2">
							<select name="resposta<?=$resposta++ ?>">
								<option value="0" disabled selected>Selecione</option>
								<option value="1">Sim</option>
								<option value="2">Não</option>
								<option value="3">N/A</option>

							</select>

						</div>
					</div>

					<div class="row">
						<div class="col s12 m10">
							<h6>Você participa da organização do trabalho?</h6>
						</div>

						<div class="input-field col s12 m2">
							<select name="resposta<?=$resposta++ ?>">
								<option value="0" disabled selected>Selecione</option>
								<option value="1">Sim</option>
								<option value="2">Não</option>
								<option value="3">N/A</option>

							</select>

						</div>
					</div>

					<div class="row">
						<div class="col s12 m10">
							<h6>Você se considera bem remunerado?</h6>
						</div>

						<div class="input-field col s12 m2">
							<select name="resposta<?=$resposta++ ?>">
								<option value="0" disabled selected>Selecione</option>
								<option value="1">Sim</option>
								<option value="2">Não</option>
								<option value="3">N/A</option>

							</select>

						</div>
					</div>

					<div class="row">
						<div class="col s12 m10">
							<h6>Você se identificou com o trabalho que faz nesta empresa?</h6>
						</div>

						<div class="input-field col s12 m2">
							<select name="resposta<?=$resposta++ ?>">
								<option value="0" disabled selected>Selecione</option>
								<option value="1">Sim</option>
								<option value="2">Não</option>
								<option value="3">N/A</option>

							</select>

						</div>
					</div>

					<div class="row">
						<div class="col s12 m10">
							<h6>Você se identificou com o trabalho que faz nesta empresa?</h6>
						</div>

						<div class="input-field col s12 m2">
							<select name="resposta<?=$resposta++ ?>">
								<option value="0" disabled selected>Selecione</option>
								<option value="1">Sim</option>
								<option value="2">Não</option>
								<option value="3">N/A</option>

							</select>

						</div>
					</div>

					<div class="row">
						<div class="col s12 m10">
							<h6>Você enxerga melhores perspectivas profissionais para a sua vida?</h6>
						</div>

						<div class="input-field col s12 m2">
							<select name="resposta<?=$resposta++ ?>">
								<option value="0" disabled selected>Selecione</option>
								<option value="1">Sim</option>
								<option value="2">Não</option>
								<option value="3">N/A</option>

							</select>

						</div>
					</div>

					<div class="row">
						<div class="col s12 m10">
							<h6>Você se sente integrado nessa empresa?</h6>
						</div>

						<div class="input-field col s12 m2">
							<select name="resposta<?=$resposta++ ?>">
								<option value="0" disabled selected>Selecione</option>
								<option value="1">Sim</option>
								<option value="2">Não</option>
								<option value="3">N/A</option>

							</select>

						</div>
					</div>

					<div class="row">
						<div class="col s12 m10">
							<h6>Houve alguma mudança que tenha determinado alguma alteração importante na sua visão de seu trabalho?</h6>
						</div>

						<div class="input-field col s12 m2">
							<select name="resposta<?=$resposta++ ?>">
								<option value="0" disabled selected>Selecione</option>
								<option value="1">Sim</option>
								<option value="2">Não</option>
								<option value="3">N/A</option>

							</select>

						</div>
					</div>

					<div class="row">
						<div class="col s12 m10">
							<h6>Você tem autonomia para executar o seu trabalho?</h6>
						</div>

						<div class="input-field col s12 m2">
							<select name="resposta<?=$resposta++ ?>">
								<option value="0" disabled selected>Selecione</option>
								<option value="1">Sim</option>
								<option value="2">Não</option>
								<option value="3">N/A</option>

							</select>

						</div>
					</div>

					<div class="row">
						<div class="col s12 m10">
							<h6>Você participa da elaboração e/ou modificação das regras em seu trabalho?</h6>
						</div>

						<div class="input-field col s12 m2">
							<select name="resposta<?=$resposta++ ?>">
								<option value="0" disabled selected>Selecione</option>
								<option value="1">Sim</option>
								<option value="2">Não</option>
								<option value="3">N/A</option>

							</select>

						</div>
					</div>

					<div class="row">
						<div class="col s12 m10">
							<h6>Você se relaciona bem com seus colegas de trabalho?</h6>
						</div>

						<div class="input-field col s12 m2">
							<select name="resposta<?=$resposta++ ?>">
								<option value="0" disabled selected>Selecione</option>
								<option value="1">Sim</option>
								<option value="2">Não</option>
								<option value="3">N/A</option>

							</select>

						</div>
					</div>

					<div class="row">
						<div class="col s12 m10">
							<h6>Você se relaciona bem com seus chefes?</h6>
						</div>

						<div class="input-field col s12 m2">
							<select name="resposta<?=$resposta++ ?>">
								<option value="0" disabled selected>Selecione</option>
								<option value="1">Sim</option>
								<option value="2">Não</option>
								<option value="3">N/A</option>

							</select>

						</div>
					</div>

					<div class="row">
						<div class="col s12 m10">
							<h6>Existem situações de desrespeito no seu ambiente de trabalho?</h6>
						</div>

						<div class="input-field col s12 m2">
							<select name="resposta<?=$resposta++ ?>">
								<option value="0" disabled selected>Selecione</option>
								<option value="1">Sim</option>
								<option value="2">Não</option>
								<option value="3">N/A</option>

							</select>

						</div>
					</div>

					<div class="row">
						<div class="col s12 m10">
							<h6>A condição material para realizar o trabalho é suficiente?</h6>
						</div>

						<div class="input-field col s12 m2">
							<select name="resposta<?=$resposta++ ?>">
								<option value="0" disabled selected>Selecione</option>
								<option value="1">Sim</option>
								<option value="2">Não</option>
								<option value="3">N/A</option>

							</select>

						</div>
					</div>

					<div class="row">
						<div class="col s12 m10">
							<h6>Ocorrem posições claramente distintas entre orientações das diversas chefias?</h6>
						</div>

						<div class="input-field col s12 m2">
							<select name="resposta<?=$resposta++ ?>">
								<option value="0" disabled selected>Selecione</option>
								<option value="1">Sim</option>
								<option value="2">Não</option>
								<option value="3">N/A</option>

							</select>

						</div>
					</div>

					<div class="row">
						<div class="col s12 m10">
							<h6>Quando você encontra alguma dificuldade, você obtém ajuda adequada? Como?</h6>
						</div>

						<div class="input-field col s12 m2">
							<select name="resposta<?=$resposta++ ?>">
								<option value="0" disabled selected>Selecione</option>
								<option value="1">Sim</option>
								<option value="2">Não</option>
								<option value="3">N/A</option>

							</select>

						</div>
					</div>

					<div class="row">
						<div class="col s12 m10">
							<h6>Você considera que tem possibilidade de expressar livremente suas dificuldades?</h6>
						</div>

						<div class="input-field col s12 m2">
							<select name="resposta<?=$resposta++ ?>">
								<option value="0" disabled selected>Selecione</option>
								<option value="1">Sim</option>
								<option value="2">Não</option>
								<option value="3">N/A</option>

							</select>

						</div>
					</div>

					<div class="row">
						<div class="col s12 m10">
							<h6>É possível fazer as pausas previstas pela empresa?</h6>
						</div>

						<div class="input-field col s12 m2">
							<select name="resposta<?=$resposta++ ?>">
								<option value="0" disabled selected>Selecione</option>
								<option value="1">Sim</option>
								<option value="2">Não</option>
								<option value="3">N/A</option>

							</select>

						</div>
					</div>

					<div class="row">
						<div class="col s12 m10">
							<h6>Você considera que, para fazer seu trabalho, você tem que estar tenso?</h6>
						</div>

						<div class="input-field col s12 m2">
							<select name="resposta<?=$resposta++ ?>">
								<option value="0" disabled selected>Selecione</option>
								<option value="1">Sim</option>
								<option value="2">Não</option>
								<option value="3">N/A</option>

							</select>

						</div>
					</div>





					<div class="row">
						<div class="input-field col s12">
							<textarea id="textarea1" class="materialize-textarea" name="textarea1"></textarea>
							<label for="textarea1">Faça aqui suas observações</label>
						</div>
					</div>




					<div class="row">
						<div class="col s12">
							<h5>Desconforto e/ou dores</h5>
						</div>
					</div>

					<div class="row">
						<div class="col s12">

							<div class="container-fluid">
								<div class="row">

									<div class="col-md-8 col-md-offset-2" style="min-height:495px;">

										<div id="#map" class="Corllet"></div>

									</div>
								</div>
							</div>
						</div>
					</div>

					<div class="row">
						<div class="col s12 m10">
							<h6>1 - Cabeça</h6>
						</div>

						<div class="col s12 m2">
							<select name="resposta<?=$resposta++ ?>">
								<option value="0" disabled selected>Selecione</option>
								<option value="1">Baixa</option>
								<option value="2">Média</option>
								<option value="3">Severa</option>

							</select>

						</div>
					</div>

					<div class="row">
						<div class="col s12 m10">
							<h6>2 - Cervical</h6>
						</div>

						<div class="col s12 m2">
							<select name="resposta<?=$resposta++ ?>">
								<option value="0" disabled selected>Selecione</option>
								<option value="1">Baixa</option>
								<option value="2">Média</option>
								<option value="3">Severa</option>

							</select>

						</div>
					</div>

					<div class="row">
						<div class="col s12 m10">
							<h6>3 - Torácica</h6>
						</div>

						<div class="col s12 m2">
							<select name="resposta<?=$resposta++ ?>">
								<option value="0" disabled selected>Selecione</option>
								<option value="1">Baixa</option>
								<option value="2">Média</option>
								<option value="3">Severa</option>

							</select>

						</div>
					</div>

					<div class="row">
						<div class="col s12 m10">
							<h6>4 - Lombar</h6>
						</div>

						<div class="col s12 m2">
							<select name="resposta<?=$resposta++ ?>">
								<option value="0" disabled selected>Selecione</option>
								<option value="1">Baixa</option>
								<option value="2">Média</option>
								<option value="3">Severa</option>

							</select>

						</div>
					</div>

					<div class="row">
						<div class="col s12 m10">
							<h6>5 - Quadril</h6>
						</div>

						<div class="col s12 m2">
							<select name="resposta<?=$resposta++ ?>">
								<option value="0" disabled selected>Selecione</option>
								<option value="1">Baixa</option>
								<option value="2">Média</option>
								<option value="3">Severa</option>

							</select>

						</div>
					</div>

					<div class="row">
						<div class="col s12 m10">
							<h6>6 - Ombro Direito</h6>
						</div>

						<div class="col s12 m2">
							<select name="resposta<?=$resposta++ ?>">
								<option value="0" disabled selected>Selecione</option>
								<option value="1">Baixa</option>
								<option value="2">Média</option>
								<option value="3">Severa</option>

							</select>

						</div>
					</div>

					<div class="row">
						<div class="col s12 m10">
							<h6>7 - Braço Direito</h6>
						</div>

						<div class="col s12 m2">
							<select name="resposta<?=$resposta++ ?>">
								<option value="0" disabled selected>Selecione</option>
								<option value="1">Baixa</option>
								<option value="2">Média</option>
								<option value="3">Severa</option>

							</select>

						</div>
					</div>

					<div class="row">
						<div class="col s12 m10">
							<h6>8 - Cotovelo Direito</h6>
						</div>

						<div class="col s12 m2">
							<select name="resposta<?=$resposta++ ?>">
								<option value="0" disabled selected>Selecione</option>
								<option value="1">Baixa</option>
								<option value="2">Média</option>
								<option value="3">Severa</option>

							</select>

						</div>
					</div>

					<div class="row">
						<div class="col s12 m10">
							<h6>9 - Antebraço direito</h6>
						</div>

						<div class="col s12 m2">
							<select name="resposta<?=$resposta++ ?>">
								<option value="0" disabled selected>Selecione</option>
								<option value="1">Baixa</option>
								<option value="2">Média</option>
								<option value="3">Severa</option>

							</select>

						</div>
					</div>

					<div class="row">
						<div class="col s12 m10">
							<h6>10 - Punho Direito</h6>
						</div>

						<div class="col s12 m2">
							<select name="resposta<?=$resposta++ ?>">
								<option value="0" disabled selected>Selecione</option>
								<option value="1">Baixa</option>
								<option value="2">Média</option>
								<option value="3">Severa</option>

							</select>

						</div>
					</div>

					<div class="row">
						<div class="col s12 m10">
							<h6>11 - Mão e Dedos Direitos</h6>
						</div>

						<div class="col s12 m2">
							<select name="resposta<?=$resposta++ ?>">
								<option value="0" disabled selected>Selecione</option>
								<option value="1">Baixa</option>
								<option value="2">Média</option>
								<option value="3">Severa</option>

							</select>

						</div>
					</div>

					<div class="row">
						<div class="col s12 m10">
							<h6>12 - Coxa Direita</h6>
						</div>

						<div class="col s12 m2">
							<select name="resposta<?=$resposta++ ?>">
								<option value="0" disabled selected>Selecione</option>
								<option value="1">Baixa</option>
								<option value="2">Média</option>
								<option value="3">Severa</option>

							</select>

						</div>
					</div>

					<div class="row">
						<div class="col s12 m10">
							<h6>13 - Joelho Direito</h6>
						</div>

						<div class="col s12 m2">
							<select name="resposta<?=$resposta++ ?>">
								<option value="0" disabled selected>Selecione</option>
								<option value="1">Baixa</option>
								<option value="2">Média</option>
								<option value="3">Severa</option>

							</select>

						</div>
					</div>

					<div class="row">
						<div class="col s12 m10">
							<h6>14 - Perna Direita</h6>
						</div>

						<div class="col s12 m2">
							<select name="resposta<?=$resposta++ ?>">
								<option value="0" disabled selected>Selecione</option>
								<option value="1">Baixa</option>
								<option value="2">Média</option>
								<option value="3">Severa</option>

							</select>

						</div>
					</div>

					<div class="row">
						<div class="col s12 m10">
							<h6>15 - Tornozelo Direito</h6>
						</div>

						<div class="col s12 m2">
							<select name="resposta<?=$resposta++ ?>">
								<option value="0" disabled selected>Selecione</option>
								<option value="1">Baixa</option>
								<option value="2">Média</option>
								<option value="3">Severa</option>

							</select>

						</div>
					</div>

					<div class="row">
						<div class="col s12 m10">
							<h6>16 - Pé e Dedos Direitos</h6>
						</div>

						<div class="col s12 m2">
							<select name="resposta<?=$resposta++ ?>">
								<option value="0" disabled selected>Selecione</option>
								<option value="1">Baixa</option>
								<option value="2">Média</option>
								<option value="3">Severa</option>

							</select>

						</div>
					</div>

					<div class="row">
						<div class="col s12 m10">
							<h6>6 - Ombro Esquerdo</h6>
						</div>

						<div class="col s12 m2">
							<select name="resposta<?=$resposta++ ?>">
								<option value="0" disabled selected>Selecione</option>
								<option value="1">Baixa</option>
								<option value="2">Média</option>
								<option value="3">Severa</option>

							</select>

						</div>
					</div>

					<div class="row">
						<div class="col s12 m10">
							<h6>7 - Braço Esquerdo</h6>
						</div>

						<div class="col s12 m2">
							<select name="resposta<?=$resposta++ ?>">
								<option value="0" disabled selected>Selecione</option>
								<option value="1">Baixa</option>
								<option value="2">Média</option>
								<option value="3">Severa</option>

							</select>

						</div>
					</div>

					<div class="row">
						<div class="col s12 m10">
							<h6>8 - Cotovelo Esquerdo</h6>
						</div>

						<div class="col s12 m2">
							<select name="resposta<?=$resposta++ ?>">
								<option value="0" disabled selected>Selecione</option>
								<option value="1">Baixa</option>
								<option value="2">Média</option>
								<option value="3">Severa</option>

							</select>

						</div>
					</div>

					<div class="row">
						<div class="col s12 m10">
							<h6>9 - Antebraço Esquerdo</h6>
						</div>

						<div class="col s12 m2">
							<select name="resposta<?=$resposta++ ?>">
								<option value="0" disabled selected>Selecione</option>
								<option value="1">Baixa</option>
								<option value="2">Média</option>
								<option value="3">Severa</option>

							</select>

						</div>
					</div>

					<div class="row">
						<div class="col s12 m10">
							<h6>10 - Punho Esquerdo</h6>
						</div>

						<div class="col s12 m2">
							<select name="resposta<?=$resposta++ ?>">
								<option value="0" disabled selected>Selecione</option>
								<option value="1">Baixa</option>
								<option value="2">Média</option>
								<option value="3">Severa</option>

							</select>

						</div>
					</div>

					<div class="row">
						<div class="col s12 m10">
							<h6>11 - Mão e Dedos Esquerdos</h6>
						</div>

						<div class="col s12 m2">
							<select name="resposta<?=$resposta++ ?>">
								<option value="0" disabled selected>Selecione</option>
								<option value="1">Baixa</option>
								<option value="2">Média</option>
								<option value="3">Severa</option>

							</select>

						</div>
					</div>

					<div class="row">
						<div class="col s12 m10">
							<h6>12 - Coxa Esquerda</h6>
						</div>

						<div class="col s12 m2">
							<select name="resposta<?=$resposta++ ?>">
								<option value="0" disabled selected>Selecione</option>
								<option value="1">Baixa</option>
								<option value="2">Média</option>
								<option value="3">Severa</option>

							</select>

						</div>
					</div>

					<div class="row">
						<div class="col s12 m10">
							<h6>13 - Joelho Esquerdo</h6>
						</div>

						<div class="col s12 m2">
							<select name="resposta<?=$resposta++ ?>">
								<option value="0" disabled selected>Selecione</option>
								<option value="1">Baixa</option>
								<option value="2">Média</option>
								<option value="3">Severa</option>

							</select>

						</div>
					</div>

					<div class="row">
						<div class="col s12 m10">
							<h6>14 - Perna Esquerda</h6>
						</div>

						<div class="col s12 m2">
							<select name="resposta<?=$resposta++ ?>">
								<option value="0" disabled selected>Selecione</option>
								<option value="1">Baixa</option>
								<option value="2">Média</option>
								<option value="3">Severa</option>

							</select>

						</div>
					</div>

					<div class="row">
						<div class="col s12 m10">
							<h6>15 - Tornozelo Esquerdo</h6>
						</div>

						<div class="col s12 m2">
							<select name="resposta<?=$resposta++ ?>">
								<option value="0" disabled selected>Selecione</option>
								<option value="1">Baixa</option>
								<option value="2">Média</option>
								<option value="3">Severa</option>

							</select>

						</div>
					</div>

					<div class="row">
						<div class="col s12 m10">
							<h6>16 - Pé e Dedos Esquerdos</h6>
						</div>

						<div class="col s12 m2">
							<select name="resposta<?=$resposta++ ?>">
								<option value="0" disabled selected>Selecione</option>
								<option value="1">Baixa</option>
								<option value="2">Média</option>
								<option value="3">Severa</option>

							</select>

						</div>
					</div>





					<div class="row">
						<div class="input-field col s12">
							<textarea id="textarea2" class="materialize-textarea" name="textarea2"></textarea>
							<label for="textarea2">Possui alguma doença Osteomuscular diagnosticada pelo médico?</label>
						</div>
					</div>


					<br>
					<div class="row">
						<div class="col s12">
							<button class="btn2 waves-effect waves-light btn2" type="submit">Enviar</button>
						</div>
					</div>


				</form>
			</div>
		</div>

		<?php
	}

	if ( $_SESSION[ "acao" ][ 'quest' ] === "qualidadevida" ) {		
		
		unset($_SESSION[ "acao" ]);

		$_SESSION[ "acao" ][ 'quest' ] = "qualidadevida";
		$_SESSION[ "acao" ][ "acao" ] = 'inserir';
		?>

		<br>
		<div class="container">
			<div class="row">
				<form class="col s12" action="controller/questionario.php" method="post" enctype="multipart/form-data">
					<div class="row">
						<div class="col s12 m12 l12">
							<h2>Qualidade de Vida</h2>
						</div>
					</div>					

					<div class="row">

						<input class="hide" type="text" name="id" value="">

						<div class="input-field col s12 m3">
							<select name="sexo">
								<option value="0" disabled selected>Selecione</option>
								<option value="1">Masculino</option>
								<option value="2">Feminino</option>
								<option value="3">Outro</option>

							</select>
							<label>Sexo:</label>
						</div>

						<div class="input-field col s12 m3">
							<input id="Idade" type="number" class="validate" name="idade" value="">
							<label for="Idade">Idade</label>
						</div>

						<div class="input-field col s12 m3">
							<input id="Peso" type="number" class="validate" name="peso" value="">
							<label for="Peso">Peso: Kg</label>
						</div>

						<div class="input-field col s12 m3">
							<input id="Altura" type="number" class="validate" name="altura" value="">
							<label for="Altura">Altura cm</label>
						</div>

					</div>

					<div class="row">

						<div class="input-field col s12 m4">
							<select name="lado">
								<option value="0" disabled selected>Selecione</option>
								<option value="1">Direito</option>
								<option value="2">Esquerdo</option>

							</select>
							<label>Lado dominante (que escreve):</label>
						</div>

						<div class="input-field col s12 m4">
							<select name="tempoempresa">
								<option value="0" disabled selected>Selecione</option>
								<option value="1">De 0 até 1 ano</option>
								<option value="2">De 1 até 2 anos</option>
								<option value="3">De 2 até 5 anos</option>
								<option value="3">Mais de 5 anos</option>

							</select>
							<label>Há quanto tempo trabalha na empresa?</label>
						</div>

						<div class="input-field col s12 m4">
							<input id="Departamento" type="text" class="validate" name="departamento" value="" placeholder="Unidade de Trabalho">
							<label for="Departamento">Departamento</label>
						</div>

					</div>

					<div class="row">
						<div class="col s12">
							<h5>Você sente frequentemente desconforto em alguma das regiões abaixo? Marque ao resposta do número referente a região de desconforto. Observe atentamente a indicação dos respostas no desenho.</h5>
						</div>
					</div>

					<div class="row">
						<div class="col s12">

							<div class="container-fluid">
								<div class="row">

									<div class="col-md-8 col-md-offset-2" style="min-height:495px;">

										<div id="#map" class="Corllet">

											<!-- 1 cabeça -->
											<label style="position:absolute; top: 0px; left: 179px;"><input type="checkbox" class="filled-in" name="body1"><span></span></label>
											<!-- 2 pescoço -->
											<label style="position:absolute; top:45px; left: 179px;"><input type="checkbox" class="filled-in" name="body2"><span></span></label>
											<!-- 3 cent -->
											<label style="position:absolute; top: 135px; left: 179px;"><input type="checkbox" class="filled-in" name="body3"><span></span></label>
											<!-- 4 cent -->
											<label style="position:absolute; top: 190px; left: 179px;"><input type="checkbox" class="filled-in" name="body4"><span></span></label>
											<!-- 5 cent -->
											<label style="position:absolute; top: 230px; left: 179px;"><input type="checkbox" class="filled-in" name="body5"><span></span></label>

											<!-- 6 dir -->
											<label style="position:absolute; top: 90px; left: 200px;"><input type="checkbox" class="filled-in" name="body6"><span></span></label>
											<!-- 7 dir -->
											<label style="position:absolute; top: 130px; left: 223px;"><input type="checkbox" class="filled-in" name="body7"><span></span></label>
											<!-- 8 dir -->
											<label style="position:absolute; top: 160px; left: 233px;"><input type="checkbox" class="filled-in" name="body8"><span></span></label>
											<!-- 9 dir -->
											<label style="position:absolute; top: 185px; left: 250px;"><input type="checkbox" class="filled-in" name="body9"><span></span></label>
											<!-- 10 dir -->
											<label style="position:absolute; top: 209px; left: 264px;"><input type="checkbox" class="filled-in" name="body10"><span></span></label>
											<!-- 11 dir -->
											<label style="position:absolute; top: 234px; left: 273px;"><input type="checkbox" class="filled-in" name="body11"><span></span></label>
											<!-- 12 dir -->
											<label style="position:absolute; top: 280px; left: 223px;"><input type="checkbox" class="filled-in" name="body12"><span></span></label>
											<!-- 13 dir -->
											<label style="position:absolute; top: 327px; left: 218px;"><input type="checkbox" class="filled-in" name="body13"><span></span></label>
											<!-- 14 dir -->
											<label style="position:absolute; top: 365px; left: 216px;"><input type="checkbox" class="filled-in" name="body14"><span></span></label>
											<!-- 15 dir -->
											<label style="position:absolute; top: 415px; left: 208px;"><input type="checkbox" class="filled-in" name="body15"><span></span></label>
											<!-- 16 dir -->
											<label style="position:absolute; top: 440px; left: 208px;"><input type="checkbox" class="filled-in" name="body16"><span></span></label>

											<!-- 6 esq -->
											<label style="position:absolute; top: 90px; left: 157px;"><input type="checkbox" class="filled-in" name="body17"><span></span></label>
											<!-- 7 esq -->
											<label style="position:absolute; top: 130px; left: 135px;"><input type="checkbox" class="filled-in" name="body18"><span></span></label>
											<!-- 8 esq -->
											<label style="position:absolute; top: 160px; left: 124px;"><input type="checkbox" class="filled-in" name="body19"><span></span></label>
											<!-- 9 esq -->
											<label style="position:absolute; top: 185px; left: 110px;"><input type="checkbox" class="filled-in" name="body20"><span></span></label>
											<!-- 10 esq -->
											<label style="position:absolute; top: 210px; left: 95px;"><input type="checkbox" class="filled-in" name="body21"><span></span></label>
											<!-- 11 esq -->
											<label style="position:absolute; top: 235px; left: 85px;"><input type="checkbox" class="filled-in" name="body22"><span></span></label>
											<!-- 12 esq -->
											<label style="position:absolute; top: 280px; left: 135px;"><input type="checkbox" class="filled-in" name="body23"><span></span></label>
											<!-- 13 esq -->
											<label style="position:absolute; top: 327px; left: 140px;"><input type="checkbox" class="filled-in" name="body24"><span></span></label>
											<!-- 14 esq -->
											<label style="position:absolute; top: 365px; left: 140px;"><input type="checkbox" class="filled-in" name="body25"><span></span></label>
											<!-- 15 esq -->
											<label style="position:absolute; top: 415px; left: 143px;"><input type="checkbox" class="filled-in" name="body26"><span></span></label>
											<!-- 16 esq -->
											<label style="position:absolute; top: 440px; left: 144px;"><input type="checkbox" class="filled-in" name="body27"><span></span></label>

										</div>

									</div>
								</div>
							</div>



						</div>
					</div>

					<div class="row">
						<div class="input-field col s12 m12">
							<h5>Área de Interesse (marque até 3):</h5>
						</div>
					</div>

					<div class="row">

						<div class="col s12 m3">
							<label><input type="checkbox" class="filled-in" name="a1"/><span>Nutrição/alimentação</span></label>
						</div>

						<div class="col s12 m3">
							<label><input type="checkbox" class="filled-in" name="a2"/><span>Gastronomia</span></label>
						</div>

						<div class="col s12 m3">
							<label><input type="checkbox" class="filled-in" name="a3"/><span>Finanças pessoais</span></label>
						</div>

						<div class="col s12 m3">
							<label><input type="checkbox" class="filled-in" name="a4"/><span>Cinema/leitura</span></label>
						</div>
					</div>

					<div class="row">
						<div class="col s12 m3">
							<label><input type="checkbox" class="filled-in" name="a5"/><span>Equilíbrio interior/meditação</span></label>
						</div>

						<div class="col s12 m3">
							<label><input type="checkbox" class="filled-in" name="a6"/><span>Distúrbio do sono</span></label>
						</div>

						<div class="col s12 m3">
							<label><input type="checkbox" class="filled-in" name="a7"/><span>Estética com saúde</span></label>
						</div>

						<div class="col s12 m3">
							<label><input type="checkbox" class="filled-in" name="a8"/><span>Terapias de relaxamento</span></label>
						</div>
					</div>

					<div class="row">
						<div class="col s12 m3">
							<label><input type="checkbox" class="filled-in" name="a9"/><span>Música/Dança</span></label>
						</div>
					</div>

					<div class="row">
						<div class="input-field col s12 m6">
							<input id="Outro" type="text" class="validate" name="outro">
							<label for="Outro">Outro</label>
						</div>
					</div>

					<div class="row">
						<div class="col s12 m12">
							<h5>Qual horário que você se sente menos produtivo:</h5>
						</div>
					</div>

					<div class="row">

						<div class="col s12 m5">
							<label><input type="checkbox" class="filled-in" name="b1"/><span>Manhã</span></label>
						</div>

						<div class="col s12 m5">
							<label><input type="checkbox" class="filled-in" name="b2"/><span>Tarde</span></label>
						</div>

						<div class="col s12 m2">
							<label><input type="checkbox" class="filled-in" name="b3"/><span>Tarde/Noite</span></label>
						</div>
					</div>


					<div class="row">
						<div class="input-field col s12 m12">
							<h5>De 1 (mínimo) a 5 ( máximo). Qual é o seu grau de estresse:</h5>
						</div>
					</div>

					<div class="row">
						<div class="col s12 m2">
							<label><input type="radio" name="group1" value="1"/><span>1</span></label>
						</div>

						<div class="col s12 m2">
							<label><input type="radio" name="group1" value="2"/><span>2</span></label>
						</div>

						<div class="col s12 m2">
							<label><input type="radio" name="group1" value="3"/><span>3</span></label>
						</div>

						<div class="col s12 m2">
							<label><input type="radio" name="group1" value="4"/><span>4</span></label>
						</div>

						<div class="col s12 m4">
							<label><input type="radio" name="group1" value="5"/><span>5</span></label>
						</div>
					</div>

					<div class="row">
						<div class="input-field col s12 m6">
							<input id="Justifique" type="text" class="validate" name="justifique" value="">
							<label for="Justifique">Justifique</label>
						</div>
					</div>





					<div class="row">
						<div class="input-field col s12 m12">
							<h5>SINTOMAS - Você tem apresentado regularmente algum dos sintomas abaixo? (Caso a resposta seja negativa, não altere a opção)</h5>
						</div>
					</div>

					<div class="row">
						<div class="col s12 m6">Desconforto físico</div>
						<div class="col s12 m6">
							<div class="switch">
								<label>Não<input type="checkbox" name="c1"><span class="lever"></span> Sim </label>
							</div>
						</div>
					</div>

					<div class="row">
						<div class="col s12 m6">Indisposição para o trabalho</div>
						<div class="col s12 m6">
							<div class="switch">
								<label>Não<input type="checkbox" name="c2"><span class="lever"></span> Sim </label>
							</div>
						</div>
					</div>

					<div class="row">
						<div class="col s12 m6">Desanimo</div>
						<div class="col s12 m6">
							<div class="switch">
								<label>Não<input type="checkbox" name="c3"><span class="lever"></span> Sim </label>
							</div>
						</div>
					</div>

					<div class="row">
						<div class="col s12 m6">Dor de cabeça</div>
						<div class="col s12 m6">
							<div class="switch">
								<label>Não<input type="checkbox" name="c4"><span class="lever"></span> Sim </label>
							</div>
						</div>
					</div>

					<div class="row">
						<div class="col s12 m6">Ansiedade</div>
						<div class="col s12 m6">
							<div class="switch">
								<label>Não<input type="checkbox" name="c5"><span class="lever"></span> Sim </label>
							</div>
						</div>
					</div>

					<div class="row">
						<div class="col s12 m6">Irritabilidade</div>
						<div class="col s12 m6">
							<div class="switch">
								<label>Não<input type="checkbox" name="c6"><span class="lever"></span> Sim </label>
							</div>
						</div>
					</div>

					<div class="row">
						<div class="col s12 m6">Perda de memória</div>
						<div class="col s12 m6">
							<div class="switch">
								<label>Não<input type="checkbox" name="c7"><span class="lever"></span> Sim </label>
							</div>
						</div>
					</div>

					<div class="row">
						<div class="col s12 m6">Sente que tem trabalhado abaixo do seu potencial</div>
						<div class="col s12 m6">
							<div class="switch">
								<label>Não<input type="checkbox" name="c8"><span class="lever"></span> Sim </label>
							</div>
						</div>
					</div>

					<div class="row">
						<div class="col s12 m6">Sente vontade de ficar sozinho</div>
						<div class="col s12 m6">
							<div class="switch">
								<label>Não<input type="checkbox" name="c9"><span class="lever"></span> Sim </label>
							</div>
						</div>
					</div>



					<div class="row">
						<div class="input-field col s12 m12">
							<h5>Você buscou tratamento, nos últimos 6 meses para (Caso a resposta seja negativa, não altere a opção):</h5>
						</div>
					</div>

					<div class="row">
						<div class="col s12 m6">Dores nos membros superiores</div>
						<div class="col s12 m6">
							<div class="switch">
								<label>Não<input type="checkbox" name="d1"><span class="lever"></span> Sim </label>
							</div>
						</div>
					</div>

					<div class="row">
						<div class="col s12 m6">Dores nos membros inferiores</div>
						<div class="col s12 m6">
							<div class="switch">
								<label>Não<input type="checkbox" name="d2"><span class="lever"></span> Sim </label>
							</div>
						</div>
					</div>

					<div class="row">
						<div class="col s12 m6">Dores nas costas</div>
						<div class="col s12 m6">
							<div class="switch">
								<label>Não<input type="checkbox" name="d3"><span class="lever"></span> Sim </label>
							</div>
						</div>
					</div>

					<div class="row">
						<div class="col s12 m6">Sintomas de estresse</div>
						<div class="col s12 m6">
							<div class="switch">
								<label>Não<input type="checkbox" name="d4"><span class="lever"></span> Sim </label>
							</div>
						</div>
					</div>





					<div class="row">
						<div class="input-field col s12 m12">
							<h5>Você se ausentou do trabalho nos últimos 6 meses por causa de (Caso a resposta seja negativa, não altere a opção):</h5>
						</div>
					</div>

					<div class="row">
						<div class="col s12 m6">Dores nos membros superiores</div>
						<div class="col s12 m6">
							<div class="switch">
								<label>Não<input type="checkbox" name="e1"><span class="lever"></span> Sim </label>
							</div>
						</div>
					</div>

					<div class="row">
						<div class="col s12 m6">Dores nos membros inferiores</div>
						<div class="col s12 m6">
							<div class="switch">
								<label>Não<input type="checkbox" name="e2"><span class="lever"></span> Sim </label>
							</div>
						</div>
					</div>

					<div class="row">
						<div class="col s12 m6">Dores nas costas</div>
						<div class="col s12 m6">
							<div class="switch">
								<label>Não<input type="checkbox" name="e3"><span class="lever"></span> Sim </label>
							</div>
						</div>
					</div>

					<div class="row">
						<div class="col s12 m6">Sintomas de estresse</div>
						<div class="col s12 m6">
							<div class="switch">
								<label>Não<input type="checkbox" name="e4"><span class="lever"></span> Sim </label>
							</div>
						</div>
					</div>





					<div class="row">
						<div class="input-field col s12 m12">
							<h5>Perfil de Qualidade de Vida (Caso a resposta seja negativa, não altere a opção):</h5>
						</div>
					</div>

					<div class="row">
						<div class="col s12 m6">É fumante?</div>
						<div class="col s12 m6">
							<div class="switch">
								<label>Não<input type="checkbox" name="f1"><span class="lever"></span> Sim </label>
							</div>
						</div>
					</div>

					<div class="row">
						<div class="col s12 m6">Pratica atividade física?</div>
						<div class="col s12 m6">
							<div class="switch">
								<label>Não<input type="checkbox" name="f2"><span class="lever"></span> Sim </label>
							</div>
						</div>
					</div>

					<div class="row">
						<div class="col s12 m6">Estuda antes ou depois do trabalho?</div>
						<div class="col s12 m6">
							<div class="switch">
								<label>Não<input type="checkbox" name="f3"><span class="lever"></span> Sim </label>
							</div>
						</div>
					</div>

					<div class="row">
						<div class="col s12 m6">Tem outro trabalho?</div>
						<div class="col s12 m6">
							<div class="switch">
								<label>Não<input type="checkbox" name="f4"><span class="lever"></span> Sim </label>
							</div>
						</div>
					</div>

					<div class="row">
						<div class="col s12 m6">Toca instrumento musical?</div>
						<div class="col s12 m6">
							<div class="switch">
								<label>Não<input type="checkbox" name="f5"><span class="lever"></span> Sim </label>
							</div>
						</div>
					</div>

					<div class="row">
						<div class="col s12 m6">Tem algum problema de audição?</div>
						<div class="col s12 m6">
							<div class="switch">
								<label>Não<input type="checkbox" name="f6"><span class="lever"></span> Sim </label>
							</div>
						</div>
					</div>

					<div class="row">
						<div class="col s12 m6">Tem algum problema nas cordas vocais?</div>
						<div class="col s12 m6">
							<div class="switch">
								<label>Não<input type="checkbox" name="f7"><span class="lever"></span> Sim </label>
							</div>
						</div>
					</div>

					<div class="row">
						<div class="col s12 m6">Se alimenta de forma adequada?</div>
						<div class="col s12 m6">
							<div class="switch">
								<label>Não<input type="checkbox" name="f8"><span class="lever"></span> Sim </label>
							</div>
						</div>
					</div>

					<div class="row">
						<div class="col s12 m6">Apresenta pressão alta?</div>
						<div class="col s12 m6">
							<div class="switch">
								<label>Não<input type="checkbox" name="f9"><span class="lever"></span> Sim </label>
							</div>
						</div>
					</div>

					<div class="row">
						<div class="col s12 m6">Tem diabetes?</div>
						<div class="col s12 m6">
							<div class="switch">
								<label>Não<input type="checkbox" name="f10"><span class="lever"></span> Sim </label>
							</div>
						</div>
					</div>

					<div class="row">
						<div class="col s12 m6">Usa óculos ou lente corretora?</div>
						<div class="col s12 m6">
							<div class="switch">
								<label>Não<input type="checkbox" name="f11"><span class="lever"></span> Sim </label>
							</div>
						</div>
					</div>

					
					<div class="row">
						<div class="col s12">
							<button class="btn2 waves-effect waves-light btn2" type="submit">Enviar</button>
						</div>
					</div>


				</form>
			</div>
		</div>

		<?php
	}
	
	if ( $_SESSION[ "acao" ][ 'quest' ] === "finalizar" ) {  ?>
	
	<br>
		<div class="container">
			<div class="row">				
				<div class="col s12">
					<h2> Agradecemos seu feedback </h2>
				</div>
			</div>
		</div>
	<?php }
	

} else {
	?>
	<br>
	<div class="container">
		<div class="row">
			<h3 align="center">Escolha um Questionário</h3>
		</div>
		<div class="row">
			<div class="col s12 m6">
				<div align="center"><a href="questionario.php?quest=ginlab" class="btn waves-effect waves-light" style="width: 200px;">Ginástica Laboral</a>
				</div>
			</div>
			<div class="col s12 m6">
				<div align="center"><a href="questionario.php?quest=shiatsu" class="btn waves-effect waves-light" style="width: 200px;">Shiatsu</a>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col s12 m6">
				<div align="center"><a href="questionario.php?quest=ergonomia" class="btn waves-effect waves-light" style="width: 200px;">Ergonomia</a>
				</div>
			</div>
			<div class="col s12 m6">
				<div align="center"><a href="questionario.php?quest=qualidadevida" class="btn waves-effect waves-light" style="width: 200px;">Qualida de Vida</a>
				</div>
			</div>
		</div>
	</div>

	<?php
}

if ( !empty( $_GET[ 'quest' ] ) ) {

	unset( $_SESSION[ "acao" ] );

	echo $_SESSION[ "acao" ][ 'quest' ] = $_GET[ 'quest' ];
	echo $_SESSION[ "acao" ][ 'id' ] = $_GET[ 'token' ];


	echo '<script>window.location.replace("questionario.php");</script>';
}

include_once( 'rodape.php' );
?>