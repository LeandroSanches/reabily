<?php
include_once( 'cabecalho.php' );
include_once( 'model/usuario.php' );
//include_once( 'model/empresa.php' );
include_once( 'model/telefone.php' );
include_once( 'model/endereco.php' );

error_reporting( E_ERROR | E_PARSE );

echo '<style>body{color: aliceblue;}</style>';

$usuario = $_SESSION['usuario'];

$nivel = listarTiposBD( $conexao );
$endereco = listarEndereco( $conexao, $usuario["id"], $usuario["id_tipo"] );
$telefone = listarTelefone( $conexao, $usuario["id"], $usuario["id_tipo"] );

$msgBotao  = "Alterar Meus Dados";

unset($_SESSION[ "acao" ][ 'acao' ]);
$_SESSION[ "acao" ][ 'acao' ] =  'alterarmeuperfil';

?>

<br>
<div class="container">
	<div class="row">
		<form class="col s12" action="controller/usuario.php" method="post" enctype="multipart/form-data">

		<h2>Meu Perfil</h2>



			<div class="row">
			
				<div class="input-field col s12">
							<h4>Acesso ao Sistema</h4>	
						</div>
				<?php if ( isset($usuario['id'] )) {
					echo '<input class="hide" type="text" name="id" value="'.$usuario["id"].'">';
				} ?>
			
				<div class="input-field col s12 m6">
					<i class="material-icons prefix">assignment_ind</i>
					<input id="Nome" type="text" class="validate txt" name="nome" value="<?= $usuario["nome"]?>" >
					<label for="Nome">Primeiro Nome</label>
				</div>

				<div class="input-field col s12 m6">
					<input id="Apelido" type="text" class="validate txt" name="apelido"  value="<?= $usuario["apelido"]?>">
					<label for="Apelido">Sobrenome</label>
				</div>

				<div class="input-field col s12 m6">
					<i class="material-icons prefix">email</i>
					<input id="email" type="email" class="validate" name="email" value="<?= $usuario["email"]?>">
					<label for="email">Email</label>
				</div>
				
				<div class="input-field col s12 m3">
					<input id="nascimento" type="date" class="validate" name="nascimento" value="<?= $usuario["nascimento"]?>">
					<label for="nascimento">Data de Nascimento:</label>
				</div>
				
				<div class="input-field col s12 m3">
				<?php
					switch ($usuario["sexo"]) {
						case 0:
							$sexo0 = 'selected';
							break;
						case 1:
							$sexo1 = 'selected';
							break;
						case 2:
							$sexo2 = 'selected';
							break;
						case 3:
							$sexo3 = 'selected';
							break;
					}

					?>
					<select name="sexo">
						<option value="0" disabled selected>Escolha uma opção</option>
						<option value="1" <?=$sexo1?> >Masculino</option>
						<option value="2" <?=$sexo2?> >Feminino</option>
						<option value="3" <?=$sexo3?> >Outro</option>
					</select>
					<label>Gênero:</label>
				</div>	

				<div class="input-field col s12 m3">
					<select name="nivel" disabled>
						<option value="" disabled selected>Escolha uma opção</option>
						<?php foreach ( $nivel as $nivel ){
					if ($usuario["id_tipo"] == $nivel["id"]){ $selected = 'selected';} else {$selected = '';}?>
						<option value="<?=$nivel["id"];?>" <?=$selected;?>><?=$nivel["nome"];?></option>
						<?php } ?>
					</select>
					<label>Nivel:</label>
				</div>
				
			</div>
			
			<div class="row">					
				<div class="input-field col s12">
					<h4>Telefones <small>(Não Obrigatório)</small></h4>	
				</div>

				<div class="clone">
					
					<?php if ( count($telefone) == 0 ){?>
					
					<div class="input-field col s12 m6">
						<input id="contato" type="text" class="validate" name="contato[]" value="<?=$telefone['contato']?>" >
						<label for="contato">Nome do Contato</label>
					</div>	

					<div class="input-field col s12 m3">							
						<input id="icon_telephone" type="tel" class="validate telefone sp_celphones" name="telefone[]" data-mask="(00) 000-000-000" maxlength="15">
						<label for="icon_telephone">Número</label>
					</div>

					<div class="input-field col s12 m3">
						 <input id="icon_telephone" type="tel" class="validate Ramal" name="ramal[]" data-mask="-0000" maxlength="15">
						 <label for="ramal">Ramal</label>
					</div>
					
					<?php  } 
					else {
						
					foreach($telefone as $telefone){?>

					<div class="input-field col s12 m6 hide">
						<input id="contato" type="text" class="validate" name="id[]" value="<?=$telefone['id']?>" >
						<label for="contato">ID</label>
					</div>	

					<div class="input-field col s12 m6">
						<input id="contato" type="text" class="validate" name="contato[]" value="<?=$telefone['contato']?>" >
						<label for="contato">Nome do Contato</label>
					</div>	

					<div class="input-field col s12 m3">							
						<input id="icon_telephone" type="tel" class="validate telefone sp_celphones" name="telefone[]" data-mask="(00) 000-000-000" maxlength="15" value="<?=$telefone['numero']?>" >
						<label for="icon_telephone">Número</label>
					</div>

					<div class="input-field col s12 m3">
						 <input id="icon_telephone" type="tel" class="validate Ramal" name="ramal[]" data-mask="-0000" maxlength="15" value="<?=$telefone['ramal']?>" >
						 <label for="ramal">Ramal</label>
					</div>
					
					<?php } } ?>

				</div>

				<div class="add"></div>						


				<p align="center" class="addNumero"> <i class="material-icons prefix">add</i> Adicionar Contato</p>

				<script>
					$(document).ready(function() {
						  $(".addNumero").click(function() {
						  var novoItem = $(".clone").clone().removeClass('clone'); 
						  $(".add").append(novoItem);
						});
					  });
				</script>
			</div>

			<div class="row">
					
				<div class="col s12">
					<h4>Endereço <small>(Não Obrigatório)</small></h4>	
				</div>
					<div class="col s12 m4">
						<label for="cep"> CEP </label>
						<input id="cep" type="text" class="validate" name="cep" data-mask="00000-000" value="<?= $endereco["cep"]?>" >								
					</div>

					<div class="col s12 m1">
						<label for="uf"> Estado </label>
						<input id="uf" type="text" class="validate blue-text text-lighten-3" name="estado" value="<?= $endereco["estado"]?>" maxlength="3" >

					</div>

					<div class="col s12 m3">
						<label for="cidade"> Cidade </label>
						<input id="cidade" type="text" class="validate blue-text text-lighten-3" name="cidade" value="<?= $endereco["cidade"]?>" >

					</div>	

					<div class="col s12 m4">
						<label for="bairro"> Bairro </label>
						<input id="bairro" type="text" class="validate blue-text text-lighten-3" name="bairro" value="<?= $endereco["bairro"]?>" >

					</div>			

					<div class="col m10">
						<label for="endereco" style="margin-bottom: 3px;"> Endereco </label>
						<input id="endereco" type="text" class="validate blue-text text-lighten-3" name="endereco" value="<?= $endereco["endereco"]?>" >

					</div>

					<div class="input-field col m2">
						<input id="numero" type="number" class="validate" name="numero" value="<?= $endereco["numero"]?>">
						<label for="numero"> Número </label>
					</div>

					<div class="input-field col m12">
						<input id="complemento" type="text" class="validate" name="complemento" value="<?= $endereco["complemento"]?>">
						<label for="complemento"> Complemento </label>
					</div>	
			</div>

			<small>* <strong>Atenção:</strong> A alteração só será visualizada após há proxima autenticação no sistema. </small>

			<div class="row"> <p align="center"><a class="Link modal-trigger" href="mudasenha.php">Mudar Minha Senha</a></p> </div>

			<br>
			<div class="row">
				<div class="col s6 m3 offset-m1" align="center">
						<a href="dashboard.php" class="btn waves-effect waves-light">Voltar</a> </div>
					<div class="col s6 m6">
					<button class="btn2 waves-effect waves-light right" type="submit"> <?= $msgBotao?> </button>
				</div>
			</div>
		</form>
	</div>
</div>

<?php include_once('rodape.php'); ?>
