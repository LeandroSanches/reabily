<?php
	include_once( 'cabecalho.php' );
	//include_once( 'model/usuario.php' );
	include_once( 'model/empresa.php' );


error_reporting( E_ERROR | E_PARSE );

echo '<style>body{color: aliceblue;}</style>';

	$empresa = listarSedeEmpresasBD( $conexao );

	session_start();

	unset($_SESSION[ "acao" ]);

	$_SESSION[ "acao" ][ 'acao' ] = 'cadastrar';
	$_SESSION[ "acao" ][ 'tipo' ] = 'cliente';
	$_SESSION[ "acao" ][ 'auto' ] = 'true';
	$btnvoltar = 'index.php';

	//Retornar MSG
	$_SESSION[ "log" ][ "on" ] = "true";	
	if($_SESSION[ "log" ][ "on" ] === "true" ){
		if( !empty($_SESSION[ "log" ][ "msg" ])){
			echo '<div class="msg">' . $_SESSION[ "log" ][ "msg" ] . '</div>';
		}			
		unset($_SESSION[ "log" ][ "msg" ]);
	}		
	$_SESSION[ "log" ][ "on" ] = "false";
	//Fim de Retornar MSG
?>

<div class="container">
	<div class="row">
		<form class="col s12" action="controller/usuario.php?acao=cadastrar" method="post" enctype="multipart/form-data">

			<div class="row">
				<div class="col s12 m12 l12">
					<h2 align="center">Cadastre-se</h2>
				</div>
			</div>
 
			<div class="row">
				<div class="input-field col s12">
							<h4>Acesso ao Sistema</h4>	
						</div>

				<div class="input-field col s12 m6">
					<i class="material-icons prefix">assignment_ind</i>
					<input id="Nome" type="text" class="validate txt" name="nome">
					<label for="Nome">Primeiro Nome</label>
				</div>

				<div class="input-field col s12 m6">
					<input id="Apelido" type="text" class="validate txt" name="apelido">
					<label for="Apelido">Sobrenome</label>
				</div>

				<div class="input-field col s12 m6">
					<i class="material-icons prefix">email</i>
					<input id="email" type="email" class="validate" name="email">
					<label for="email">Email</label>
				</div>

				<div class="input-field col s12 m3">
					<i class="material-icons prefix">fingerprint</i>
					<input id="password" type="password" class="validate" name="senha">
					<label for="password">Senha</label>
				</div>
				
				<div class="input-field col s12 m3">
					<input id="nascimento" type="date" class="validate" name="nascimento">
					<label for="nascimento">Data de Nascimento:</label>
				</div>

				<div class="input-field col s12 m3">
					<select name="sexo" >
						<option value="0" disabled selected>Escolha uma opção</option>
						<option value="1">Masculino</option>
						<option value="2">Feminino</option>
						<option value="3">Outro</option>

					</select>
					<label>Gênero:</label>
				</div>

				<div class="input-field col s12 m3 hide">
					<select name="nivel">
						<option value="4" disabled selected><span class="white-text">Cliente</span></option>
					</select>
					<label>Nivel:</label>
				</div>

				<div class="input-field col s12 m6">
							<select name="empresa">
								<option value="" disabled selected></option>
								<?php foreach ( $empresa as $empresa ){
								if ($usuario[0]["id_empresa"] === $empresa["id"]){ $selected = 'selected';} 
								else {$selected = '';}
								?>						
								<optgroup value="<?=$empresa["id"];?>" <?=$selected;?> label="<?=$empresa["sede"];?>" data-icon="<?php echo $empresa['logo'] = strstr($empresa['logo'],'usuarios/');?>">		
								<?php $filial = listarEmpresaSedeFilial( $conexao, $empresa["id"] );
									foreach ( $filial as $filial ){?>						
									<option value="<?=$filial["id"];?>" data-icon="<?php echo $filial['logo'] = strstr($filial['logo'],'usuarios/');?>"><?=$filial["sede"];?></option>
									<?php } ?>
								</optgroup>
								<?php } ?>
							</select>
							<label>Empresa:</label>
						</div>

			</div>	
			
			<div class="row">					
				<div class="input-field col s12">
					<h4>Telefones <small>(Não Obrigatório)</small></h4>	
				</div>

				<div class="clone">

				<div class="input-field col s12 m6">
					<input id="contato" type="text" class="validate" name="contato[]" >
					<label for="contato">Nome do Contato</label>
				</div>	

				<div class="input-field col s12 m3">							
					<input id="icon_telephone" type="tel" class="validate telefone sp_celphones" name="telefone[]" data-mask="(00) 000-000-000" maxlength="15">
					<label for="icon_telephone">Número</label>
				</div>

				<div class="input-field col s12 m3">
					 <input id="icon_telephone" type="tel" class="validate Ramal" name="ramal[]" data-mask="-0000" maxlength="15">
					 <label for="ramal">Ramal</label>
				</div>

				</div>

				<div class="add"></div>						


				<p align="center" class="addNumero"> <i class="material-icons prefix">add</i> Adicionar Contato</p>

				<script>
					$(document).ready(function() {
						  $(".addNumero").click(function() {
						  var novoItem = $(".clone").clone().removeClass('clone'); 
						  $(".add").append(novoItem);
						});
					  });
				</script>
			</div>
		
			<div class="row">
					
				<div class="col s12">
					<h4>Endereço <small>(Não Obrigatório)</small></h4>	
				</div>
					<div class="col s12 m4">
						<label for="cep"> CEP </label>
						<input id="cep" type="text" class="validate" name="cep" data-mask="00000-000" value="<?= $usuario[0]["cep"]?>" >								
					</div>

					<div class="col s12 m1">
						<label for="uf"> Estado </label>
						<input id="uf" type="text" class="validate blue-text text-lighten-3" name="estado" value="<?= $usuario[0]["estado"]?>" maxlength="3" >

					</div>

					<div class="col s12 m3">
						<label for="cidade"> Cidade </label>
						<input id="cidade" type="text" class="validate blue-text text-lighten-3" name="cidade" value="<?= $usuario[0]["cidade"]?>" >

					</div>	

					<div class="col s12 m4">
						<label for="bairro"> Bairro </label>
						<input id="bairro" type="text" class="validate blue-text text-lighten-3" name="bairro" value="<?= $usuario[0]["bairro"]?>" >

					</div>			

					<div class="col m10">
						<label for="endereco" style="margin-bottom: 3px;"> Endereco </label>
						<input id="endereco" type="text" class="validate blue-text text-lighten-3" name="endereco" value="<?= $usuario[0]["endereco"]?>" >

					</div>

					<div class="input-field col m2">
						<input id="numero" type="number" class="validate" name="numero" value="<?= $usuario[0]["numero"]?>">
						<label for="numero"> Número </label>
					</div>

					<div class="input-field col m12">
						<input id="complemento" type="text" class="validate" name="complemento" value="<?= $usuario[0]["complemento"]?>">
						<label for="complemento"> Complemento </label>
					</div>	
			</div>
			
			<div class="row">
				<div class="col s6 m3 offset-m1 ">
					<a href="<?= $btnvoltar?>" class="btn waves-effect waves-light">Voltar</a>
				</div>
				<div class="col s6 m6">
					<button class="btn2 waves-effect waves-light right" type="submit"> Cadastrar </button>
				</div>
			</div>

		</form>
	</div>
</div>

<?php include_once('rodape.php'); ?>
