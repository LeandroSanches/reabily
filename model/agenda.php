<?php  require_once("conecta.php");

function inseriragendaBD($conexao, $array){
	echo $query = "insert into agenda (nome, descricao, atividade, data, hora, frequencia, abertura, quantidades, empresa, funcionario, ativo, recadastrados)
        values ('$array[0]', '$array[1]', '$array[2]', '$array[3]', '$array[4]', '$array[5]', STR_TO_DATE('$array[6]','%Y%m%d'), '$array[7]', '$array[8]', '$array[9]', '$array[10]', '$array[11]')";
    return mysqli_query($conexao, $query);
}

function listarAgendaBD($conexao){
	$agenda = array();
	$query = "SELECT * FROM `agenda` where ativo = 0  LIMIT 50";
    $resultado = mysqli_query($conexao, $query);
	while($result = mysqli_fetch_assoc($resultado)) {
        array_push($agenda, $result);
    }
    return $agenda;
}

function listarAgendaComAtividadeBD( $conexao, $array ){
	$agenda = array();
	$query = "SELECT data, atividade, empresa, ativo
							FROM `agenda`
						    	WHERE ativo = '0'
						        	AND atividade = $array[0]
						        	AND empresa = $array[1]
						            AND data > now()
						            	ORDER BY `DATA` ASC";
  $resultado = mysqli_query($conexao, $query);
	while($result = mysqli_fetch_assoc($resultado)) {
        array_push($agenda, $result);
    }
    return $agenda;
}

function listarAgendaidBD($conexao, $id){
	$agenda = array();
	$query = "SELECT * FROM `agenda` where id = $id";
    $resultado = mysqli_query($conexao, $query);
	while($result = mysqli_fetch_assoc($resultado)) {
        array_push($agenda, $result);
    }
    return $agenda;
}

function alterarAgendaBD( $conexao, $array ){
	echo $query = "update agenda set nome = '$array[0]', descricao = '$array[1]', atividade = '$array[2]', data = '$array[3]', hora = '$array[4]', frequencia = '$array[5]', abertura = STR_TO_DATE('$array[6]','%Y%m%d'), quantidades = '$array[7]', empresa = '$array[8]', funcionario = '$array[9]',  ativo = '$array[10]',  recadastrados = '$array[12]'  where id = $array[11]";
    return mysqli_query($conexao, $query);
}

function alterarAgenda2BD( $conexao, $array ){
	$query = "update agenda set nome = '$array[0]', descricao = '$array[1]', atividade = '$array[2]', frequencia = '$array[3]', abertura = STR_TO_DATE('$array[4]','%Y%m%d'), quantidades = '$array[5]', empresa = '$array[6]', funcionario = '$array[7]',  ativo = '$array[8]',  recadastrados = '$array[10]' where id = $array[9]";
    return mysqli_query($conexao, $query);
}

function listarAgendaClienteBD($conexao, $id_empresa){
	$agenda = array();

	$query = "SELECT * , atividade.nome as nomeAtividade
				FROM atividade , agenda
					WHERE empresa = $id_empresa
						and atividade.id = agenda.atividade ";

    $resultado = mysqli_query($conexao, $query);
	while($result = mysqli_fetch_assoc($resultado)) {
        array_push($agenda, $result);
    }
    return $agenda;
}

function agendarHorarioBD($conexao, $array){
	$query = "insert into agendamentos ( id_usuario, id_agenda)
        values ('$array[0]', '$array[1]')";
    return mysqli_query($conexao, $query);
}

function listarVagas($conexao, $id_agenda){	
	$agenda = array();
	
	$query = "SELECT * FROM `agendamentos` WHERE id_agenda = '$id_agenda' AND presenca !=5 ORDER BY `agendamentos`.`id` ASC";
	
	//return mysqli_query($conexao, $query);
	
    $resultado = mysqli_query($conexao, $query);
	while($result = mysqli_fetch_assoc($resultado)) {
        array_push($agenda, $result);
    }
    return $agenda;
}

function listaragendadosClienteBD( $conexao, $id ){
	$agenda = array();

	//$query = "SELECT * FROM `agendamentos`WHERE id_usuario = $array[0]";
	$query = "SELECT *,
			atividade.nome as nomeAtividade,
			agendamentos.id as idAgendamentos
				FROM agendamentos, agenda, atividade
					WHERE id_usuario = $id
						and agendamentos.id_agenda = agenda.id
						and agenda.atividade = atividade.id
							ORDER BY `agenda`.`data` DESC";

    $resultado = mysqli_query($conexao, $query);
	while($result = mysqli_fetch_assoc($resultado)) {
        array_push($agenda, $result);
    }
    return $agenda;
}

function cancelarHorarioBD($conexao, $array){
	$query = "update agendamentos set presenca = 5 where id ='$array'";
    return mysqli_query($conexao, $query);
}

function blockMultiplosAgendamentosBD($conexao, $id_agenda, $id_usuario ){
	$agenda = array();

	$query = "SELECT * FROM `agendamentos`WHERE id_usuario = $id_usuario and id_agenda = $id_agenda and presenca != 5";

    $resultado = mysqli_query($conexao, $query);
	while($result = mysqli_fetch_assoc($resultado)) {
        array_push($agenda, $result);
    }
	return $agenda;
}

function listaragendadosFuncionarioBD( $conexao, $id ){
	$agenda = array();

	/*$query = "SELECT *,
			atividade.nome as nomeAtividade,
			agenda.id as idcod
				FROM  agendamentos, atividade, agenda
					WHERE agenda.funcionario = $id
						and agendamentos.id_agenda = agenda.id
						and agenda.atividade = atividade.id
							ORDER BY `agenda`.`data` ASC";*/
	/*$query = "SELECT *
				FROM  agendamentos
                	right join agenda
               			on agendamentos.id_agenda = agenda.id
							WHERE agenda.funcionario = $id ";*/
	$query = "SELECT *
				FROM  agenda
                	WHERE funcionario = $id
						ORDER BY `agenda`.`data` ASC";

    $resultado = mysqli_query($conexao, $query);
	while($result = mysqli_fetch_assoc($resultado)) {
        array_push($agenda, $result);
    }
    return $agenda;
}

function alterarPresencaBD( $conexao, $array ){
	$query = "update agendamentos set presenca = '$array[2]' where id = $array[0]";
    return mysqli_query($conexao, $query);

}

function alterarAdesaoBD( $conexao, $array ){
	$query = "update agendamentos set adesao = '$array[2]' where id = $array[0]";
    return mysqli_query($conexao, $query);

}

function excluirAgendaBD( $conexao, $id ) {
	$query = "update agenda set  ativo = '2' where id = '$id'";
	return mysqli_query( $conexao, $query );
}

function listarAgendadiaBD($conexao, $dia, $atividade, $empresa){
	$agenda = array();
	$query = "SELECT *
	    FROM agenda
        where agenda.ativo = 0
            and data = '$dia'
            and atividade = '$atividade'
						 and empresa = '$empresa'";
	 /*$query = "SELECT *, a.id as cod
								FROM agenda as A
								LEFT JOIN agendamentos as B
											on A.id = B.id_agenda
								WHERE B.id_agenda is null
								and ativo = 0
								and data = '$dia'
								and atividade = $atividade
								and empresa = $empresa";*/

    $resultado = mysqli_query($conexao, $query);
	while($result = mysqli_fetch_assoc($resultado)) {
        array_push($agenda, $result);
    }
    return $agenda;
}

function listarAgendamentosBD($conexao, $empresa, $parametro){
	$agenda = array();
	$query = "SELECT *
	    FROM agenda
        where ativo = 0
            and data $parametro
			 and empresa = '$empresa'
			  ORDER BY `agenda`.`data` DESC LIMIT 100"; 
    $resultado = mysqli_query($conexao, $query);
	while($result = mysqli_fetch_assoc($resultado)) {
        array_push($agenda, $result);
    }
    return $agenda;
}

function listaragendadosEmpresaBD( $conexao, $id ){
	$agenda = array();
	$query = "SELECT *
				FROM  agenda
                	WHERE funcionario = $id
						ORDER BY `agenda`.`data` ASC";

    $resultado = mysqli_query($conexao, $query);
	while($result = mysqli_fetch_assoc($resultado)) {
        array_push($agenda, $result);
    }
    return $agenda;
}

function listarAgendaEmpresaBD($conexao, $empresa){
	$agenda = array();
	$query = "SELECT *
				FROM agenda
					where agenda.ativo = 0
						and empresa = $empresa";
    $resultado = mysqli_query($conexao, $query);
	while($result = mysqli_fetch_assoc($resultado)) {
        array_push($agenda, $result);
    }
    return $agenda;
}

function listarAgendaEmpresaEntreDataBD($conexao, $empresa, $data1, $data2){
	$agenda = array();
	$query = "SELECT * 
				FROM agenda
					where data BETWEEN '$data1' AND '$data2'
					AND agenda.ativo = 0
					AND empresa = $empresa";
    $resultado = mysqli_query($conexao, $query);
	while($result = mysqli_fetch_assoc($resultado)) {
        array_push($agenda, $result);
    }
    return $agenda;
}

function listarAgendaCanceladoBD($conexao, $empresa){
	$agenda = array();
	$query = "SELECT *
				FROM agenda a
                        inner join agendamentos b
                        on a.id = b.id_agenda

					where a.ativo = 0
						and a.empresa = $empresa";
    $resultado = mysqli_query($conexao, $query);
	while($result = mysqli_fetch_assoc($resultado)) {
        array_push($agenda, $result);
    }
    return $agenda;
}

function listarAgendaCanceladoEntreDataBD($conexao, $empresa, $data1, $data2){
	$agenda = array();
	$query = "SELECT *
				FROM agenda a
                        inner join agendamentos b
                        on a.id = b.id_agenda
					where data BETWEEN '$data1' AND '$data2'
                    	and a.ativo = 0
						and a.empresa = $empresa";
    $resultado = mysqli_query($conexao, $query);
	while($result = mysqli_fetch_assoc($resultado)) {
        array_push($agenda, $result);
    }
    return $agenda;
}

function listarAgendasCorrespondentesBD( $conexao, $array){
	$agenda = array();
	$query = "SELECT *
				FROM `agenda`
				WHERE nome = '$array[1]'
				and descricao = '$array[2]'
				and atividade = $array[3]
				and empresa = $array[4]
				and funcionario = $array[5]";

    $resultado = mysqli_query($conexao, $query);
	while($result = mysqli_fetch_assoc($resultado)) {
        array_push($agenda, $result);
    }
    return $agenda;
}

function listarAgendaNoIntervaloBD( $conexao, $data1, $data2){
	$agenda = array();
	$query = "SELECT *
	FROM agendamentos
    where datadecriacao BETWEEN '$data2' AND '$data1'";

    $resultado = mysqli_query($conexao, $query);
	while($result = mysqli_fetch_assoc($resultado)) {
        array_push($agenda, $result);
    }
    return $agenda;
}

function filtarAgenda($conexao, $palavra){
	$agenda = array();
	$query = "SELECT *
				FROM `agenda`
					WHERE nome LIKE '%$palavra%'";
	$resultado = mysqli_query( $conexao, $query );
	while ( $result = mysqli_fetch_assoc( $resultado ) ) {
		array_push( $agenda, $result );
	}
	return $agenda;
}

function listarAdesao($conexao, $usuario, $data1 , $data2){	
	$setor = array();
	$query = "SELECT * FROM agenda, agendamentos 
						WHERE agenda.id = agendamentos.id_agenda 
						AND empresa = $usuario 
                        AND data >= '$data1'
                        AND data <= '$data2'";
	$resultado = mysqli_query($conexao, $query);
	while($result = mysqli_fetch_assoc($resultado)) {
		array_push($setor, $result);
	}
	return $setor;
}