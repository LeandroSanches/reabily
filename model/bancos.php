<?php require_once( "conecta.php" );

function listarBancosBD( $conexao ) {
	$bancos = array();
	$query = "SELECT * FROM  bancos";
	$resultado = mysqli_query( $conexao, $query );
	while ( $result = mysqli_fetch_assoc( $resultado ) ) {
		array_push( $bancos, $result );
	}
	return $bancos;
}