<?php  require_once("conecta.php");

function gravarContratoBD($conexao, $array){	
	$query = "insert into contrato (id_empresa, apelido, tipo, valor, exigencia, id_atividade, data_inicio, data_final, obs, id_renovacao)
        values ('$array[0]', '$array[1]', '$array[2]', '$array[3]', '$array[4]', '$array[5]', STR_TO_DATE('$array[6]','%Y%m%d'), STR_TO_DATE('$array[7]','%Y%m%d'), '$array[8]', '$array[9]')";
    return mysqli_query($conexao, $query);
}

function listarContratosBD($conexao){
	$contrato = array();
	$query = "SELECT * FROM `contrato` where ativo = 0 ORDER BY `contrato`.`data_final` ASC";
	//$query = "SELECT * FROM `contrato` ORDER BY `contrato`.`apelido` ASC";
    $resultado = mysqli_query($conexao, $query);
	while($result = mysqli_fetch_assoc($resultado)) {
        array_push($contrato, $result);
    }
    return $contrato;
}

function listarContratos2BD($conexao){
	$contrato = array();
	//$query = "SELECT * FROM `contrato` ORDER BY `contrato`.`data_final` ASC";
	$query = "SELECT * FROM `contrato`  where ativo = 0  ORDER BY `contrato`.`apelido` ASC";
    $resultado = mysqli_query($conexao, $query);
	while($result = mysqli_fetch_assoc($resultado)) {
        array_push($contrato, $result);
    }
    return $contrato;
}

function listarContratoBD($conexao, $id){
	$contrato = array();
	$query = "SELECT * FROM `contrato` where id = $id";
    $resultado = mysqli_query($conexao, $query);
	while($result = mysqli_fetch_assoc($resultado)) {
        array_push($contrato, $result);
    }
    return $contrato;
}

function alterarContratoBD( $conexao, $array ){
	$query = "update contrato set id_empresa = '$array[0]', apelido = '$array[1]', tipo = '$array[2]', valor = '$array[3]', exigencia = '$array[4]', id_atividade = '$array[5]', data_inicio = STR_TO_DATE('$array[6]','%Y%m%d'), data_final = STR_TO_DATE('$array[7]','%Y%m%d'), obs = '$array[8]' where id = $array[9]";
    return mysqli_query($conexao, $query);
}

function bloquearContratoBD( $conexao, $array ){
	$query = "update contrato set ativo = '$array[1]' where id = $array[0]";
    return mysqli_query($conexao, $query);
}

function excluirContratoBD( $conexao, $array ){
	$query = "update contrato set ativo = '$array[1]' where id = $array[0]";
    return mysqli_query($conexao, $query);
}