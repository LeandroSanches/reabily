-- phpMyAdmin SQL Dump
-- version 4.8.2
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: 26-Abr-2019 às 18:16
-- Versão do servidor: 5.7.22
-- PHP Version: 7.1.19

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `reabily`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `agenda`
--

CREATE TABLE `agenda` (
  `id` int(11) NOT NULL,
  `nome` varchar(255) NOT NULL,
  `descricao` text NOT NULL,
  `atividade` int(11) NOT NULL,
  `data` date NOT NULL,
  `hora` varchar(10) NOT NULL,
  `frequencia` int(11) NOT NULL,
  `abertura` date NOT NULL,
  `quantidades` int(11) NOT NULL,
  `empresa` int(11) NOT NULL,
  `funcionario` int(11) NOT NULL,
  `datadecriacao` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `ativo` int(1) NOT NULL,
  `recadastrados` int(2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estrutura da tabela `agendamentos`
--

CREATE TABLE `agendamentos` (
  `id` int(11) NOT NULL,
  `presenca` int(1) NOT NULL DEFAULT '0',
  `id_usuario` int(11) NOT NULL,
  `id_agenda` int(11) NOT NULL,
  `datadecriacao` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estrutura da tabela `atividade`
--

CREATE TABLE `atividade` (
  `id` int(11) NOT NULL,
  `nome` varchar(255) NOT NULL,
  `duracao` varchar(10) NOT NULL,
  `ativo` int(1) NOT NULL,
  `descricao` text NOT NULL,
  `datadecriacao` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estrutura da tabela `aula`
--

CREATE TABLE `aula` (
  `id` int(11) NOT NULL,
  `nome` varchar(255) NOT NULL,
  `objetivo` varchar(255) NOT NULL,
  `descricao` text NOT NULL,
  `id_tipo` int(11) NOT NULL,
  `id_material` int(11) NOT NULL,
  `datadecriacao` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estrutura da tabela `bancos`
--

CREATE TABLE `bancos` (
  `cod` int(11) NOT NULL,
  `nome` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estrutura da tabela `contrato`
--

CREATE TABLE `contrato` (
  `id` int(11) NOT NULL,
  `id_empresa` int(11) NOT NULL,
  `apelido` varchar(255) NOT NULL,
  `tipo` int(11) NOT NULL,
  `valor` int(11) NOT NULL,
  `exigencia` varchar(255) NOT NULL,
  `id_atividade` int(11) NOT NULL,
  `data_inicio` date NOT NULL,
  `data_final` date NOT NULL,
  `obs` text NOT NULL,
  `ativo` int(11) NOT NULL DEFAULT '0',
  `id_renovacao` int(11) DEFAULT NULL,
  `datadecriacao` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estrutura da tabela `curriculo`
--

CREATE TABLE `curriculo` (
  `id` int(11) NOT NULL,
  `nome` varchar(255) NOT NULL,
  `apelido` varchar(255) NOT NULL,
  `telefone` bigint(20) NOT NULL,
  `email` varchar(255) NOT NULL,
  `cep` int(11) NOT NULL,
  `estado` varchar(255) NOT NULL,
  `cidade` varchar(255) NOT NULL,
  `bairro` varchar(255) NOT NULL,
  `endereco` varchar(255) NOT NULL,
  `numero` int(11) NOT NULL,
  `complemento` varchar(255) NOT NULL,
  `formacao` text NOT NULL,
  `qualificacoes` text NOT NULL,
  `atividade` text NOT NULL,
  `datadeCriacao` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estrutura da tabela `empresa`
--

CREATE TABLE `empresa` (
  `id` int(11) NOT NULL,
  `id_empresa` int(11) NOT NULL,
  `sede` varchar(255) NOT NULL,
  `contato` varchar(255) NOT NULL,
  `municipal` bigint(20) NOT NULL,
  `estadual` bigint(20) NOT NULL,
  `setor` text NOT NULL,
  `nfe` text NOT NULL,
  `ramal` text NOT NULL,
  `obs` text NOT NULL,
  `datadecriacao` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estrutura da tabela `fornecedor`
--

CREATE TABLE `fornecedor` (
  `id` int(11) NOT NULL,
  `nome` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `telefone` bigint(20) NOT NULL,
  `material` varchar(255) NOT NULL,
  `valorunitario` int(11) NOT NULL,
  `quantidadepedido` int(11) NOT NULL,
  `descricao` text NOT NULL,
  `ativo` int(1) NOT NULL DEFAULT '0',
  `datadecriacao` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estrutura da tabela `frase`
--

CREATE TABLE `frase` (
  `id` int(11) NOT NULL,
  `frase` text NOT NULL,
  `ativo` int(1) NOT NULL,
  `datadecriacao` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estrutura da tabela `funcionario`
--

CREATE TABLE `funcionario` (
  `id` int(11) NOT NULL,
  `nomedoPai` varchar(255) DEFAULT NULL,
  `nomedaMae` varchar(255) DEFAULT NULL,
  `conjege` varchar(255) NOT NULL,
  `filiacao` varchar(255) NOT NULL,
  `nascimento` date DEFAULT NULL,
  `identidade` int(9) DEFAULT NULL,
  `identidadeOrgao` varchar(255) NOT NULL,
  `datadeExpedicao` date DEFAULT NULL,
  `naturalidade` varchar(255) DEFAULT NULL,
  `nacionalidade` varchar(255) DEFAULT NULL,
  `estadoCivil` int(2) DEFAULT NULL,
  `celular` bigint(20) NOT NULL,
  `trecado` int(11) NOT NULL,
  `nrecado` varchar(255) NOT NULL,
  `titulodeEleitor` bigint(12) DEFAULT NULL,
  `sessao` int(11) DEFAULT NULL,
  `zona` int(11) DEFAULT NULL,
  `passaporte` varchar(15) DEFAULT NULL,
  `reservista` bigint(20) NOT NULL,
  `registroprofissional` bigint(20) NOT NULL,
  `observacoesdocumento` text,
  `formacao` varchar(255) DEFAULT NULL,
  `qualificacao` text,
  `id_contratos` int(1) DEFAULT NULL,
  `funcao` varchar(255) DEFAULT NULL,
  `datadeDesligamento` date DEFAULT NULL,
  `motivo` text,
  `datadeAdmissão` date DEFAULT NULL,
  `pis` bigint(20) DEFAULT NULL,
  `ctps` int(11) DEFAULT NULL,
  `observacoes` text,
  `datadeCriacao` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `id_usuario` int(11) DEFAULT NULL,
  `salarioContratado` int(11) DEFAULT NULL,
  `cargaHoraria` int(11) DEFAULT NULL,
  `transporte` int(1) DEFAULT NULL,
  `alimentacao` int(1) DEFAULT NULL,
  `planodeSaude` int(1) DEFAULT NULL,
  `planoOdontologico` int(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estrutura da tabela `infobancario`
--

CREATE TABLE `infobancario` (
  `id` int(11) NOT NULL,
  `id_usuario` int(11) NOT NULL,
  `agencia` int(11) DEFAULT NULL,
  `conta` bigint(20) DEFAULT NULL,
  `tipodeconta` int(11) DEFAULT NULL,
  `banco` int(5) NOT NULL,
  `catadecriacao` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estrutura da tabela `material`
--

CREATE TABLE `material` (
  `id` int(11) NOT NULL,
  `material` varchar(255) NOT NULL,
  `descricao` text NOT NULL,
  `datadecriacao` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estrutura da tabela `questionarios`
--

CREATE TABLE `questionarios` (
  `id` int(11) NOT NULL,
  `id_usuario` int(11) NOT NULL,
  `datadecriacao` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estrutura da tabela `redessociais`
--

CREATE TABLE `redessociais` (
  `id` int(11) NOT NULL,
  `facebook` varchar(255) DEFAULT NULL,
  `linkedin` varchar(255) DEFAULT NULL,
  `instagram` varchar(255) DEFAULT NULL,
  `id_usuario` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estrutura da tabela `resposta`
--

CREATE TABLE `resposta` (
  `id` int(11) NOT NULL,
  `id_questionarios` int(11) NOT NULL,
  `resposta` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estrutura da tabela `sugestao`
--

CREATE TABLE `sugestao` (
  `id` int(11) NOT NULL,
  `email` varchar(255) NOT NULL,
  `sugestao` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estrutura da tabela `tipo`
--

CREATE TABLE `tipo` (
  `id` int(11) NOT NULL,
  `nome` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estrutura da tabela `usuario`
--

CREATE TABLE `usuario` (
  `id` int(11) NOT NULL,
  `cpf_cnpj` bigint(20) NOT NULL,
  `nome` varchar(60) NOT NULL,
  `apelido` varchar(30) NOT NULL,
  `email` varchar(255) NOT NULL,
  `telefone` bigint(20) NOT NULL,
  `nascimento` date DEFAULT NULL,
  `endereco` varchar(255) NOT NULL,
  `cep` int(11) NOT NULL,
  `estado` varchar(255) NOT NULL,
  `cidade` varchar(255) NOT NULL,
  `bairro` varchar(255) NOT NULL,
  `numero` int(11) NOT NULL,
  `complemento` varchar(255) NOT NULL,
  `sexo` int(1) DEFAULT NULL,
  `foto` varchar(255) NOT NULL,
  `senha` varchar(255) NOT NULL,
  `ativo` int(1) NOT NULL,
  `id_tipo` int(2) NOT NULL,
  `id_empresa` int(3) NOT NULL,
  `datadecricao` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `usuario`
--

INSERT INTO `usuario` (`id`, `cpf_cnpj`, `nome`, `apelido`, `email`, `telefone`, `nascimento`, `endereco`, `cep`, `estado`, `cidade`, `bairro`, `numero`, `complemento`, `sexo`, `foto`, `senha`, `ativo`, `id_tipo`, `id_empresa`, `datadecricao`) VALUES
(2, 0, 'Contato', 'leandrobaptistacombr', 'contato@leandrobaptista.com.br', 1111111111, NULL, '0', 0, '0', '0', '', 0, '0', 0, '', 'e10adc3949ba59abbe56e057f20f883e', 0, 1, 0, '2018-10-20 00:22:03'),
(3, 0, 'AZUL COMPANHIA DE SEGUROS GERAISSP', 'AZUL SEGUROS SP', 'barbara.martins@azulseguros.com.br', 2139062973, NULL, 'Av. Paulista', 0, 'São Paulo', 'Paraíso', '', 453, '18 andar', 0, '', 'd41d8cd98f00b204e9800998ecf8427e', 0, 3, 22, '2018-10-22 18:44:12'),
(4, 33448150000111, 'AZUL COMPANHIA DE SEGUROS GERAISRJ RIO BRANCO', 'AZUL SEGUROS RIO BRANCO', 'barbara.martins@azulseguros.com.br', 2139062973, NULL, 'Avenida Rio Branco - de 64 a 100 - lado par', 20040070, 'RIO DE JANEIRO', 'Rio de Janeiro', '', 80, '13°, 16° e 20° andar', 0, '', 'd41d8cd98f00b204e9800998ecf8427e', 0, 3, 22, '2018-10-22 18:51:27'),
(5, 33448150000111, 'AZUL COMPANHIA DE SEGUROS GERAISRJ RIO BRANCO', 'AZUL COMPANHIA DE SEGUROS GERA', 'barbara.martins@azulseguros.com.br', 2139062973, NULL, 'Av. Rio Branco', 20040070, 'Rio de Janeiro', 'Rio de Janeiro', '', 85, '13°, 16° e 20° andar', 3, 'img/usuarios/33448150000111.jpg', 'e10adc3949ba59abbe56e057f20f883e', 0, 3, 0, '2018-10-24 11:17:55'),
(7, 0, 'Empresa de Teste', 'Teste Empresa', 'empresa@empresa.com.br', 0, NULL, '', 0, '', '', '', 0, '', 0, '', 'd41d8cd98f00b204e9800998ecf8427e', 2, 3, 0, '2018-10-24 23:17:42'),
(8, 0, '', ' Empresa Filial', 'testefilial@empresa.com.br', 0, NULL, '', 0, '', '', '', 0, '', 0, '', 'd41d8cd98f00b204e9800998ecf8427e', 2, 3, 7, '2018-10-24 23:42:44'),
(9, 0, '', 'Empresa Filial Azul 2', 'email@email.com', 0, NULL, '', 0, '', '', '', 0, '', 3, 'img/usuarios/.jpg', '202cb962ac59075b964b07152d234b70', 2, 3, 0, '2018-10-24 23:53:14'),
(10, 0, 'AZUL COMPANHIA DE SEGUROS GERAISRJ ALFANDEGA', 'AZUL SEGUROS ALFANDEGA', 'barbara.martins@azulseguros.com.br', 2139062973, NULL, 'RUA DA ALFANDEGA', 20031151, 'RIO DE JANEIRO', 'RIO DE JANEIRO', '', 21, '', 0, '', 'd41d8cd98f00b204e9800998ecf8427e', 0, 3, 22, '2018-10-25 00:56:51'),
(11, 11902443000194, 'LIFERAY', 'LIFERAY', 'hr-br@liferay.com', 8130331405, NULL, 'Rua Jacó Velosino', 52061410, 'Pernambuco', 'Recife', '', 250, '10° andar- Empresarial Lucas Suassuna', 3, 'http://vlansolution.com.br/projetos/reabily/img/usuarios/11902443000194.jpg', 'e10adc3949ba59abbe56e057f20f883e', 0, 3, 0, '2018-10-25 01:07:26'),
(12, 0, 'TRANSMISSORA ALIANÇA DE ENERGIA ELETRICA SA', 'Taesa', 'daniel.lima@taesa.com.br', 2122126088, NULL, 'Praça Quinze de Novembro', 20010010, 'RJ', 'Rio de Janeiro', '', 20, '6° Andar salas 601 e 602', 0, '', 'd41d8cd98f00b204e9800998ecf8427e', 0, 3, 0, '2018-10-25 01:10:51'),
(13, 0, 'TRANSMISSORA ALIANÇA DE ENERGIA ELETRICA SA', 'Taesa Assis', 'joenio.ferraz@taesa.com.br', 1833242625, NULL, 'Rua Benjamin Constant', 19806130, 'São Paulo', 'Assis', '', 33, '3 andar', 0, '', 'd41d8cd98f00b204e9800998ecf8427e', 0, 3, 12, '2018-10-25 01:16:09'),
(14, 0, 'TRANSMISSORA ALIANÇA DE ENERGIA ELETRICA SA', 'Taesa Brasília', 'jose.aguirre@taesa.com.br', 6132142505, NULL, 'SHC-EQS 114/', 70377400, 'Distrito Federal', 'Brasilia', '', 115, 'Bloco A Sala 01 a 04, Ed. Casa Blanca-', 0, '', 'd41d8cd98f00b204e9800998ecf8427e', 0, 3, 0, '2018-10-25 01:22:13'),
(15, 0, 'TRANSMISSORA ALIANÇA DE ENERGIA ELETRICA SA', 'Taesa Gramame', 'cristiano.eustaquio@taesa.com.br', 8332335107, NULL, 'Médico Felipe Kumamoto', 58069259, 'Paraiba', 'João Pessoa ', '', 222, '', 3, 'img/usuarios/.jpg', 'e10adc3949ba59abbe56e057f20f883e', 0, 3, 12, '2018-10-25 01:30:20'),
(16, 7859971005361, 'TRANSMISSORA ALIANÇA DE ENERGIA ELETRICA SA', 'Taesa Santo Ângelo', 'rodolfo.holsbach@taesa.com.br', 55981340035, NULL, 'Travessa Fernando Ferrari', 98803370, 'Rio Grande do Sul', 'Santo Ângelo', '', 180, '', 3, 'img/usuarios/07859971005361.jpg', 'e10adc3949ba59abbe56e057f20f883e', 0, 3, 12, '2018-10-25 01:32:34'),
(17, 0, 'TRANSMISSORA ALIANÇA DE ENERGIA ELETRICA SA', 'Taesa Araguaína', 'rodrigo.faria@taesa.com.br', 6334121295, NULL, 'Avenida Rio Bandeira, Lt. 01, 02 e 03, Qd 14', 77813864, 'Tocantins', 'Araguaína', '', 0, '', 3, 'img/usuarios/.jpg', 'e10adc3949ba59abbe56e057f20f883e', 0, 3, 12, '2018-10-25 01:34:43'),
(18, 29339298000140, 'CGG do Brasil Participações ', 'CGG', 'maicon.fiuza@cgg.com', 2121267387, NULL, 'Avenida Presidente Wilson, 231', 20030905, 'RJ', 'Rio de Janeiro', '', 231, 'Salões 1501', 3, 'http://vlansolution.com.br/projetos/reabily/img/usuarios/29339298000140.jpg', 'e10adc3949ba59abbe56e057f20f883e', 0, 3, 0, '2018-10-25 01:37:39'),
(19, 33754482000124, 'Caixa de Previdência dos Funcionários do Banco do Brasil', 'Previ', 'claudiagomes@previ.com.br', 2138701613, NULL, 'Praia de Botafogo', 22250040, 'Rio de Janeiro', 'Rio de Janeiro', '', 501, '3º e 4º pavimento', 3, 'http://vlansolution.com.br/projetos/reabily/img/usuarios/33754482000124.jpg', 'e10adc3949ba59abbe56e057f20f883e', 0, 3, 0, '2018-10-25 01:47:41'),
(20, 0, 'REPSOL SINOPEC BRASIL SA', 'Repsol', 'dlemosca@repsolsinopec.com', 2125597034, NULL, 'Praia de Botafogo', 20031151, 'Rio de janeiro', 'Rio de janeiro', '', 300, '', 3, 'http://vlansolution.com.br/projetos/reabily/img/usuarios/.jpg', 'e10adc3949ba59abbe56e057f20f883e', 0, 3, 0, '2018-10-25 01:51:09'),
(21, 0, 'ONSRECIFE', 'ONS RECIFE', 'joaofsf@ons.org.br', 8132178802, NULL, 'Rua da Aurora', 0, 'Pernambuco', 'Recife', '', 1343, '', 0, '', 'd41d8cd98f00b204e9800998ecf8427e', 0, 3, 0, '2018-10-25 10:52:59'),
(22, 29309127012266, 'AMIL ASSISTENCIA MEDICA INTERNACIONAL SA', 'AMIL Barra da Tijuca', 'rcosta05@amil.com.br', 2138051267, NULL, 'Avenida das Américas - de 3979 a 5151 - lado ímpar', 22631004, 'RJ', 'Rio de Janeiro', '', 4200, 'bloco 3- Ed São Paulo', 3, 'http://vlansolution.com.br/projetos/reabily/img/usuarios/29309127012266.jpg', 'e10adc3949ba59abbe56e057f20f883e', 0, 3, 0, '2018-10-25 11:09:48'),
(23, 7794623001102, 'CEMED CARE  Empresa de Atendimento Clínico Geral Ltda', 'HOME CARE AMIL', 'pvasconcelos@cemedcare.com.br', 2126729134, NULL, 'Rua Almirante Greenfell', 25085135, 'Rio de Janeiro', 'Rio de Janeiro', '', 405, 'Bl. 01 Azul - 6º andar Vila São Luíz', 3, 'img/usuarios/07794623001102.jpg', 'e10adc3949ba59abbe56e057f20f883e', 0, 3, 22, '2018-10-25 11:18:58'),
(24, 7794623000980, 'Cemed Care  Empresa de Atendimento Clínico Geral  LTDA', 'Amil Total Care Botafogo', 'ebrito@amil.com.br', 2125791000, NULL, 'Rua Voluntários da Pátria', 0, 'Rio de Janeiro', 'Rio de Janeiro', '', 445, 'sobreloja 210 (Em frente à Cobal)', 3, 'img/usuarios/07794623000980.jpg', 'e10adc3949ba59abbe56e057f20f883e', 0, 3, 22, '2018-10-25 11:24:27'),
(25, 0, 'Amil Total Care  Barra', 'Amil Total Care Barra', 'lirodrigues@amil.com.br', 2131391000, NULL, 'Av das Américas', 22640100, 'Rio de Janeiro', 'Rio de Janeiro', '', 700, 'ljs 307-308 ( Shopping Cittá América )', 3, 'img/usuarios/.jpg', 'e10adc3949ba59abbe56e057f20f883e', 0, 3, 22, '2018-10-25 11:27:16'),
(26, 29309127011618, 'Amil Assistência Médica Internacional  SA', 'Amil Barueri SP', 'aamoura@amil.com.br', 2138051154, NULL, 'Av. Cauaxi', 6454020, 'São Paulo', 'Barueri ', '', 118, '', 3, 'img/usuarios/29309127011618.jpg', 'e10adc3949ba59abbe56e057f20f883e', 0, 3, 22, '2018-10-25 11:40:23'),
(27, 0, 'Amil Assistência Médica Internacional  SA', 'Amil DF', 'carolinasj@amil.com.br', 6130439439, NULL, 'SCS Qd.', 7030600, 'Distrito Federal', 'Brasilia', '', 6, 'Bl. A nº 157  - Ed. Bandeirantes ', 3, 'img/usuarios/.jpg', 'e10adc3949ba59abbe56e057f20f883e', 0, 3, 22, '2018-10-25 11:44:10'),
(28, 29309127007424, 'AMIL ASSISTÊNCIA MÉDICA INTERNACIONAL LTDA', 'AMIL Centro', 'diego_santos@amil.com.br', 2125069674, NULL, 'Rua Tenente Possolo', 20230160, 'Rio de Janeiro ', 'Rio de Janeiro ', '', 33, '6° e 4° andar', 3, 'img/usuarios/29309127007424.jpg', 'e10adc3949ba59abbe56e057f20f883e', 0, 3, 22, '2018-10-25 11:47:31'),
(29, 0, 'Christiane', 'do Eirado Silva', 'chris.eirado@gmail.com', 21969095553, NULL, 'Rua Belfort Roxo', 22020010, 'RJ', 'Rio de Janeiro', '', 296, '301', 0, '', 'd41d8cd98f00b204e9800998ecf8427e', 0, 2, 0, '2018-10-25 16:58:12'),
(30, 12121212121, 'Victor ', 'Hugo', 'brazlopes@gmail.com', 21212121212, NULL, 'Rua Rio Negro', 21760280, 'RJ', 'Rio de Janeiro', '', 0, '', 0, '', 'd41d8cd98f00b204e9800998ecf8427e', 0, 4, 0, '2018-10-25 17:08:12'),
(31, 0, 'Rafael', 'Mendes Malta', 'rafael@reabily.com.br', 0, NULL, '', 0, '', '', '', 0, '', 1, 'http://vlansolution.com.br/projetos/reabily/img/usuarios/.jpg', 'e10adc3949ba59abbe56e057f20f883e', 0, 2, 0, '2018-10-25 17:15:14'),
(34, 12345678901, 'Leandro', 'Baptista', 'email@email.com.br', 12121212121, NULL, '', 0, '', '', '', 0, '', 1, 'http://vlansolution.com.br/projetos/reabily/img/usuarios/12345678901.jpg', '202cb962ac59075b964b07152d234b70', 0, 4, 10, '2018-10-26 16:39:05'),
(35, 44444444444444, 'Razal', 'Nome', 'empresas@gmail.com.br', 33333333333, NULL, 'Avenida Oliveira Belo', 21221300, 'RJ', 'Rio de Janeiro', 'Vila da Penha', 0, '', 3, 'http://vlansolution.com.br/projetos/reabily/img/usuarios/44444444444444.jpg', 'e10adc3949ba59abbe56e057f20f883e', 0, 3, 0, '2018-10-26 23:07:41'),
(39, 0, 'Teste', 'teste', 'email@email.com', 0, NULL, '', 0, '', '', '', 0, '', 3, 'http://vlansolution.com.br/projetos/reabily/img/usuarios/.jpg', '202cb962ac59075b964b07152d234b70', 0, 3, 0, '2018-10-26 23:19:00'),
(40, 0, 'Leandro', 'Baptista', 'leandro.baptista@unigranrio.com.br', 11111111111, NULL, '', 0, '', '', '', 0, '', 0, '', 'd41d8cd98f00b204e9800998ecf8427e', 0, 1, 0, '2018-10-30 00:51:57'),
(41, 0, 'Empresa Teste', 'Empr', 'sede@email.com.br', 0, NULL, '', 0, '', '', '', 0, '', 0, '', 'e10adc3949ba59abbe56e057f20f883e', 0, 3, 0, '2018-10-30 23:32:42'),
(51, 0, 'TesteCliente', 'Cliente', 'TesteCliente@email.com', 0, NULL, '', 0, '', '', '', 0, '', 3, 'http://vlansolution.com.br/projetos/reabily/img/usuarios/.jpg', 'e10adc3949ba59abbe56e057f20f883e', 0, 4, 41, '2019-03-12 12:56:42'),
(52, 0, 'Fulano', 'Silva', 'fulano@email.com', 11111111111, '2019-04-04', '', 0, '', '', '', 0, '', 0, '', 'e10adc3949ba59abbe56e057f20f883e', 0, 4, 25, '2019-03-12 18:06:29'),
(54, 0, 'Empresa Teste Filial', 'Empresa Teste Filial', 'empresatestefilial@email.com', 0, NULL, '', 0, '', '', '', 0, '', 3, 'img/usuarios/.jpg', 'e10adc3949ba59abbe56e057f20f883e', 2, 3, 41, '2019-03-12 18:58:07'),
(66, 0, 'Nascimento', 'nASC', 'nascimento@emai.com', 0, '2019-04-02', '', 0, '', '', '', 0, '', 0, '', 'e10adc3949ba59abbe56e057f20f883e', 0, 4, 0, '2019-04-04 15:19:05'),
(68, 0, 'Nome', 'Sobre', 'email@email.com', 33333333333, '1111-11-11', '', 0, '', '', '', 0, '', 0, 'http://vlansolution.com.br/projetos/reabily/img/usuarios/.jpg', 'b0baee9d279d34fa1dfd71aadb908c3f', 0, 4, 41, '2019-04-04 15:32:51'),
(71, 11111111111111, 'Razao', 'Nome', 'razao@razao', 22222222222, '1111-11-11', '', 0, '', '', '', 0, '', 3, '/reabily/img/usuarios/11111111111111.jpg', 'e10adc3949ba59abbe56e057f20f883e', 1, 3, 0, '2019-04-09 20:40:11'),
(72, 11111111111111, 'Teste', 'Bome Teste', 'bometeste@email', 0, '1111-11-11', '', 0, '', '', '', 0, '', 3, 'img/usuarios/11111111111111.jpg', 'e10adc3949ba59abbe56e057f20f883e', 0, 3, 31, '2019-04-09 20:56:21');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `agenda`
--
ALTER TABLE `agenda`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `agendamentos`
--
ALTER TABLE `agendamentos`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `atividade`
--
ALTER TABLE `atividade`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `aula`
--
ALTER TABLE `aula`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `bancos`
--
ALTER TABLE `bancos`
  ADD PRIMARY KEY (`cod`);

--
-- Indexes for table `contrato`
--
ALTER TABLE `contrato`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `curriculo`
--
ALTER TABLE `curriculo`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `empresa`
--
ALTER TABLE `empresa`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `fornecedor`
--
ALTER TABLE `fornecedor`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `frase`
--
ALTER TABLE `frase`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `funcionario`
--
ALTER TABLE `funcionario`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `infobancario`
--
ALTER TABLE `infobancario`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `material`
--
ALTER TABLE `material`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `questionarios`
--
ALTER TABLE `questionarios`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `redessociais`
--
ALTER TABLE `redessociais`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `resposta`
--
ALTER TABLE `resposta`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sugestao`
--
ALTER TABLE `sugestao`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tipo`
--
ALTER TABLE `tipo`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `usuario`
--
ALTER TABLE `usuario`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `agenda`
--
ALTER TABLE `agenda`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `agendamentos`
--
ALTER TABLE `agendamentos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `atividade`
--
ALTER TABLE `atividade`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `aula`
--
ALTER TABLE `aula`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `contrato`
--
ALTER TABLE `contrato`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `curriculo`
--
ALTER TABLE `curriculo`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `empresa`
--
ALTER TABLE `empresa`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `fornecedor`
--
ALTER TABLE `fornecedor`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `frase`
--
ALTER TABLE `frase`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `funcionario`
--
ALTER TABLE `funcionario`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `infobancario`
--
ALTER TABLE `infobancario`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `material`
--
ALTER TABLE `material`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `questionarios`
--
ALTER TABLE `questionarios`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `redessociais`
--
ALTER TABLE `redessociais`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `resposta`
--
ALTER TABLE `resposta`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `sugestao`
--
ALTER TABLE `sugestao`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tipo`
--
ALTER TABLE `tipo`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `usuario`
--
ALTER TABLE `usuario`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=73;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
