<?php include_once('cabecalho.php');
?>
<br>
  <div class="row">
    <div class="col s12 m4 offset-m4">
      <div class="card ">
        <div class="card-content">
			
          <h2>Sugestões</h2>
			
          <form action="controller/sugestoes.php" method="post">
				<div class="row">
					<div class="input-field col s12 hide">
					  <input id="email" type="email" class="validate" name="email" value="<?=$_SESSION['usuario']['email'];?>">
					</div>				
					<div class="input-field col s12">
				   		<textarea id="textarea1" class="materialize-textarea" name="sugestao"></textarea>					  
					  <label for="textarea1">Sugestão</label>
					</div>								
					<div class="col s12">
						<button class="btn2 waves-effect waves-light right" type="submit">Enviar </button>
					</div>
				</div>
			</form>
        </div>
      </div>
    </div>
  </div>

<?php include_once('rodape.php'); ?>