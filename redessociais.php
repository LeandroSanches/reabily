<?php
/*error_reporting( E_ALL ^ E_NOTICE );
error_reporting( E_WARNING );
error_reporting( 0 );*/

session_start();

include_once( 'cabecalho.php' );
include_once( 'model/usuario.php' );

$id = $_SESSION[ "acao" ][ 'id' ];

$redessociais = array();

$usuario = listarUsuarioBD( $conexao, $id );	
$redessociais = listarRedesSociaisBD( $conexao, $id );
//$redesocial = listarRedeSocialBD( $conexao);

//var_dump($redessociais);

if(empty($redessociais) ){
	$msgFuncionario = 'Usuário '.$usuario[0]["apelido"].' não tem dados cadastrados';
	$msgBotao = 'Cadastrar';
	$_SESSION[ "acao" ][ 'acao' ] = 'inserirredessociais';
}
if(!empty($redessociais) ){
	$msgFuncionario = 'Usuário '.$usuario[0]["apelido"].' tem os dados cadastrados';
	$msgBotao = 'Atualizar';
	$_SESSION[ "acao" ][ 'acao' ] = 'alterarredessociais';
}

?>
<div class="container">
	<form action="controller/usuario.php" method="post" enctype="multipart/form-data">
		<div class="row">
			<h5 align="center"><?= $msgFuncionario?></h5>
		</div>
			<input type="text" class="hide" name="id" value="<?= $usuario[0]['id']?>">
			<input type="text" class="hide" name="id_redessociais" value="<?= $redessociais[0]['id']?>">
		
		<div class="row">	
		
			<div class="col s12 m6">					
				<div class="col s12 m3">
					<i class="fab fa-facebook" style="font-size: 4rem;"></i>							
				</div>
				<div class="input-field col s12 m9">
					<input id="facebook" type="text" class="validate" name="facebook" value="<?=$redessociais[0]['facebook']?>">
					<label for="facebook">Facebook</label>
				</div>												
			</div>
		
			<div class="col s12 m6">					
				<div class="col s12 m3">
					<i class="fab fa-linkedin" style="font-size: 4rem;"></i>							
				</div>
				<div class="input-field col s12 m9">
					<input id="linkedin" type="text" class="validate" name="linkedin" value="<?=$redessociais[0]['linkedin']?>">
					<label for="linkedin">Linkedin</label>
				</div>												
			</div>
		
			<div class="col s12 m6">					
				<div class="col s12 m3">
					<i class="fab fa-instagram" style="font-size: 4rem;"></i>							
				</div>
				<div class="input-field col s12 m9">
					<input id="instagram" type="text" class="validate" name="instagram" value="<?=$redessociais[0]['instagram']?>">
					<label for="instagram">Instagram</label>
				</div>												
			</div>
				
		</div>
		
			<div class="row">
				<div class="col s6 m3 offset-m1" align="center">
					<a href="usuario.php?acao=alterar&token=<?=$usuario[0]["id"]?>" class="btn waves-effect waves-light">Voltar</a> </div>
				<div class="col s6 m6">
					<button class="btn2 waves-effect waves-light right" type="submit">
						<?= $msgBotao?>
					</button>
				</div>
			</div>
		</form>
</div>

<?php

if ( !empty( $_GET['acao'] )){	
	
	echo $_SESSION[ "acao" ][ 'acao' ] = $_GET['acao'];
	echo $_SESSION[ "acao" ][ 'id' ] = $_GET['token'];
	echo $_SESSION[ "acao" ][ 'tipo' ] = $_GET['tipo'];
	
	$infAvan = "false";
	$infBancaria = "false";
	
	echo '<script>window.location.replace("usuario.php");</script>';
}

include_once('rodape.php'); ?>