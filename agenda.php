<?php
include_once( 'cabecalho.php' );
include_once( 'model/usuario.php' );
include_once( 'model/atividade.php' );
include_once( 'model/agenda.php' );
include_once( 'model/empresa.php' );


if ( isset( $_SESSION[ "acao" ][ 'acao' ] ) ) {
	
	if ( $_SESSION[ "acao" ][ 'acao' ] === 'escolhatipo' ){	
		?>

		<br>
		<div class="container">
				<div class="row">
					<div class="col s12 m12 l6">
						<h2>Tipos de Agenda </h2>
					</div>
				</div>
				<div class="row">
					<div class="col s12 m6">
						<p align="center">
							<a href="agenda.php?acao=listarMatriz" class="btn waves-effect waves-light  white-text" >Atividade</a>
						</p>
					</div>
					<div class="col s12 m6">
						<p align="center"><a href="agenda.php?acao=aula" class="btn waves-effect waves-light  white-text"> Aula </a>
						</p>
					</div>
				</div>
		</div>

	<?php  }	
	
	if ( $_SESSION[ "acao" ][ 'acao' ] === "listarMatriz" ) {
		$empresa = listarSedeEmpresasBD($conexao);
		
		if( empty($_POST['matriz'])){	
			$_SESSION[ "acao" ][ 'acao' ] = 'listarMatriz';	
		?>
		<br>		
			<div class="container"><h2>Agenda</h2>
				<form class="col s12" action="agenda.php" method="post" accept-charset="UTF-8">
				
					<div class="row">
						<div class="input-field col s12">
							<i class="material-icons prefix">domain</i>
							<select name="matriz">
								<option disabled selected></option>
								<?php foreach ( $empresa as $empresa ){
										if ($_POST['matriz'] == $empresa["id"]){ $selected = 'selected';} else {$selected = '';}?>
								<option value="<?=$empresa["id"];?>" label="<?=$empresa["sede"];?>" data-icon="<?php echo $empresa['logo'] = strstr($empresa['logo'],'usuarios/');?>" <?=$selected?> ><?=$empresa["sede"];?></option>
								<?php } ?>
							</select>
							<label for="empresa">Escolha uma Matriz:</label>
						</div>
					</div>				
					
					<div class="row">
						<div class="col s6 m3 offset-m1" align="center">
							<a href="dashboard.php" class="btn waves-effect waves-light">Voltar</a> </div>
						<div class="col s6 m6">
							<button class="btn2 waves-effect waves-light right" type="submit">Avançar</button>
						</div>
					</div>
					
				</form>
			</div>
		<?php include_once( 'rodape.php' ); 
		exit;	
									}
		
		if( !empty($_POST['matriz'])){	
			$_SESSION[ "acao" ][ 'acao' ] = 'data';	
			$filial =  listarEmpresaSedeFilial($conexao, $_POST['matriz']);
		?>
		<br>		
			<div class="container"><h2>Agenda</h2>
				<form class="col s12" action="agenda.php" method="post" accept-charset="UTF-8">
				
					<div class="row">
						<div class="input-field col s12">
							<i class="material-icons prefix">domain</i>
							<select name="matriz">
								<option disabled selected></option>
								<?php foreach ( $empresa as $empresa ){
										if ($_POST['matriz'] == $empresa["id"]){ $selected = 'selected';} else {$selected = '';}?>
								<option value="<?=$empresa["id"];?>" label="<?=$empresa["sede"];?>" data-icon="<?php echo $empresa['logo'] = strstr($empresa['logo'],'usuarios/');?>" <?=$selected?> ><?=$empresa["sede"];?></option>
								<?php } ?>
							</select>
							<label for="empresa">Escolha uma Matriz:</label>
						</div>
					</div>	
													
					<div class="row">
						<div class="input-field col s12">
							<i class="material-icons prefix">domain</i>
							<select name="empresa">
								<option disabled selected></option>
								<?php foreach ( $filial as $filial ){
										if ($agenda[0]["empresa"] == $filial["id"]){ $selected = 'selected';} else {$selected = '';}?>
								<option value="<?=$filial["id"];?>" data-icon="<?php echo $filial['logo'] = strstr($filial['logo'],'usuarios/');?>" <?=$selected;?> ><?=$filial["sede"];?></option>
								<?php } ?>
							</select>
							<label for="empresa">Escolha uma Filial:</label>
						</div>
					</div>				
					
					<div class="row">
						<div class="col s6 m3 offset-m1" align="center">
							<a href="dashboard.php" class="btn waves-effect waves-light">Voltar</a> </div>
						<div class="col s6 m6">
							<button class="btn2 waves-effect waves-light right" type="submit">Avançar</button>
						</div>
					</div>
					
				</form>
			</div>
		<?php include_once( 'rodape.php' ); 
		exit;	
									}

	}

	if ( $_SESSION[ "acao" ][ 'acao' ] === "data" ) { //Escolha o tempo
		
		if( !empty($_SESSION[ "acao" ][ 'id' ]) ){$_POST['empresa'] = $_SESSION[ "acao" ][ 'id' ];}
		
		?>
		<br>
		
		<div class="container">
			<h2>Agenda</h2>
			
			<p>Deseja listar qual agenda? </p>
			
			<div class="row">
			
				<div class="col s12 m4">
				  <div class="card center ">
					<div class="card-content white-text ">
					  <span class="card-title"> <i class="material-icons">arrow_back</i> Antigos</span>
					  <p>Lista dos Agendamentos Anteriores. </p>
					</div>
					<div class="card-action">
					  <a href="agenda.php?acao=agenda&token=<?= $_POST['empresa']?>&tipo=passado">Listar</a>
					</div>
				  </div>
				</div>
			
				<div class="col s12 m4">
				  <div class="card center">
					<div class="card-content white-text">
					  <span class="card-title"> <i class="material-icons">arrow_downward</i> Hoje</span>
					  <p>Lista dos Agendamentos de Hoje. </p>
					</div>
					<div class="card-action">
					  <a href="agenda.php?acao=agenda&token=<?= $_POST['empresa']?>&tipo=presente">Listar</a>
					</div>
				  </div>
				</div>
			
				<div class="col s12 m4">
				  <div class="card center">
					<div class="card-content white-text">
					  <span class="card-title"> <i class="material-icons">arrow_forward</i> Futuro</span>
					  <p>Lista dos Agendamentos Futuros. </p>
					</div>
					<div class="card-action">
					  <a href="agenda.php?acao=agenda&token=<?= $_POST['empresa']?>&tipo=futuro">Listar</a>
					</div>
				  </div>
				</div>

			</div>
						
			<div class="row">
				<div class="col s6 m3 offset-m1" align="center">
					<a href="agenda.php?acao=listarMatriz" class="btn waves-effect waves-light">Voltar</a> </div>					
			</div>
		</div>
<?php }
	
	if ( $_SESSION[ "acao" ][ 'acao' ] === "agenda" ) { 
		
		if ( $_SESSION[ "acao" ][ 'tipo' ] === "passado" ) {
			$parametro = " < '". date('Y-m-d')." ' ";
			$agenda = listarAgendamentosBD($conexao, $_SESSION[ "acao" ][ 'id' ], $parametro );
			$empresa =  listarSedeEmpresasIdBD( $conexao, $_SESSION[ "acao" ][ 'id' ] );
				
			
			if( !empty($_GET[ 'filtro' ]) ){	

				if( $_GET[ 'filtro' ] === 'hora' ){
					function OrdenarapelidoCrescente($a, $b) {	return $a['hora'] > $b['hora'];	}
					
					OrdenarapelidoCrescente($a, $b);
					usort($agenda, 'OrdenarapelidoCrescente');
				}
				
				if( $_GET[ 'filtro' ] === 'data' ){
					function OrdenarNomeCrescente($a, $b) {	return $a['data'] > $b['data'];	}

					OrdenarNomeCrescente($a, $b);
					usort($agenda, 'OrdenarNomeCrescente');
				}
			}
		?>
			<br>		
			<div class="container">
			
				<h2>Agendamentos Vencidos </h2>
				<p align="center"><strong>Empresa:</strong> <?php echo $empresa[0]['sede'];?></p>
				
				<?php if( empty($agenda)){ echo '<h3 align="center">Nenhum agendamento pra desta empresa</h3>';}
						else {?>
				
				<div class="row">
					<table>
					<thead>
						<tr>
						
						  	<th>Atividade</th>
							<th><a href="agenda.php?filtro=data" title="Ordenar por Nome" class=" white-text">Data</a> / <a href="agenda.php?filtro=hora" title="Ordenar por Nome" class=" white-text">Hora</a></th>
							<th><a href="agenda.php?filtro=abertura" title="Ordenar por Nome" class=" white-text">Abertura</a></th>
							<th></th>
						</tr>
					</thead>

					<tbody>
						<?php foreach ( $agenda as $agenda ) {?>
								<tr>
									<td>
										<?php $atividade = listarAtividadeBD($conexao, $agenda[ "atividade" ]);
											echo $atividade[0][ "nome" ]?>
									</td>
									<td>
										<?= date('d/m/Y', strtotime($agenda[ "data" ]))?> <small> (<?= mask($agenda[ "hora" ], '##:##')?>) </small>
									</td>
									<td>
										<?= date('d/m/Y', strtotime($agenda[ "data" ]))?>
									</td>
									<td><a href="agenda.php?acao=editar&token=<?=$agenda['id']?>" title="Editar"><i class="material-icons white-text">edit</i></a>
									<a href="agenda.php?acao=adicionar&token=<?=$agenda['id']?>" title="Adicionar Cliente"><i class="material-icons white-text">people</i></a>
									<a class="modal-trigger Link" href="#modal<?=$agenda['id']?>" title="Excluir"><i class="material-icons prefix amber-text text-darken-1 <?= $color?>">cancel</i></a>
									</td>
								</tr>

								<div id="modal<?=$agenda['id']?>" class="modal bottom-sheet">
									<div class="row">

										<div class="col s12 m6">

											<div class="modal-content">
											  <h4 class="grey-text text-darken-2">Deseja excluir <?=$agenda['nome']?>?</h4>
											  <p class="grey-text text-darken-2">Essa ação não poderá ser desfeita.</p>
											  <p class="grey-text text-darken-2"><small>Cod.: <?=$agenda['id']?></small></p>
											</div>

										</div>


										<div class="col s12 m6 Top">

											<div class="modal-footer">
												<a href="controller/agenda.php?acao=excluir&token=<?=$agenda['id']?>" class="modal-close btn2 waves-effect waves-light">Eu aceito</a>
											</div>

										</div>

									</div>

								  </div>
							<?php } ?>
					</tbody>

				</table>
				</div>
				<?php } ?>
				
				<div class="row">
					<div class="col s6 m3 offset-m1" align="center">
						<a href="agenda.php?acao=data&token=<?=$_SESSION[ "acao" ][ 'id' ]?>" class="btn waves-effect waves-light">Voltar</a> </div>					
				</div>
				
			</div>
		<?php }
		
		if ( $_SESSION[ "acao" ][ 'tipo' ] === "presente" ) {
			$parametro = " = '". date('Y-m-d')." ' ";
			$agenda = listarAgendamentosBD($conexao, $_SESSION[ "acao" ][ 'id' ], $parametro );
			$empresa =  listarSedeEmpresasIdBD( $conexao, $_SESSION[ "acao" ][ 'id' ] );
			$atividade = listarAtividadesBD($conexao);	
		
		if( !empty($_GET[ 'filtro' ]) ){	

			if( $_GET[ 'filtro' ] === 'hora' ){

				function OrdenarapelidoCrescente($a, $b) {
					return $a['hora'] > $b['hora'];
				}

				usort($agenda, 'OrdenarapelidoCrescente');

			}

			if( $_GET[ 'filtro' ] === 'data' ){

				switch ($ultimOrdenamento) {

					default:
						function OrdenarNomeCrescente($a, $b) {
							return $a['data'] > $b['data'];
						}

						OrdenarNomeCrescente($a, $b);

						usort($agenda, 'OrdenarNomeCrescente');

						$ultimOrdenamento = 'Crescente';
						break;

				}

			}
		}
		?>
			<br>		
			<div class="container">
			
				<h2>Agenda De Hoje <small>(<?=date('d/m/Y')?>)</small></h2>
				<p align="center"><strong>Empresa:</strong> <?php echo $empresa[0]['sede'];?></p>	
				
				<?php if( empty($agenda)){ echo '<h3 align="center">Nenhum agendamento pra hoje desta empresa</h3>';}
						else {?>
				
				<div class="row">
					<table>
					<thead>
						<tr>
							<th>Atividade</th>
							<th><a href="agenda.php?filtro=hora" title="Ordenar por Nome" class=" white-text">Hora</a></th>
							<th><a href="agenda.php?filtro=data" title="Ordenar por Nome" class=" white-text">Abertura</a></th>
							<th></th>
						</tr>
					</thead>

					<tbody>
						<?php foreach ( $agenda as $agenda ) {?>
								<tr>
									<td>
										<?php $atividade = listarAtividadeBD($conexao, $agenda[ "atividade" ]);
											echo $atividade[0][ "nome" ]?>
									</td>
									<td>
										<?= mask($agenda[ "hora" ], '##:##')?>
									</td>
									<td>
										<?= date('d/m/Y', strtotime($agenda[ "data" ]))?>
									</td>
									<td><a href="agenda.php?acao=editar&token=<?=$agenda['id']?>" title="Editar"><i class="material-icons white-text">edit</i></a>
									<a href="agenda.php?acao=adicionar&token=<?=$agenda['id']?>" title="Adicionar Cliente"><i class="material-icons white-text">people</i></a>
									<a class="modal-trigger Link" href="#modal<?=$agenda['id']?>" title="Excluir"><i class="material-icons prefix amber-text text-darken-1 <?= $color?>">cancel</i></a>
									</td>
								</tr>

								<div id="modal<?=$agenda['id']?>" class="modal bottom-sheet">
									<div class="row">

										<div class="col s12 m6">

											<div class="modal-content">
											  <h4 class="grey-text text-darken-2">Deseja excluir <?=$agenda['nome']?>?</h4>
											  <p class="grey-text text-darken-2">Essa ação não poderá ser desfeita.</p>
											  <p class="grey-text text-darken-2"><small>Cod.: <?=$agenda['id']?></small></p>
											</div>

										</div>


										<div class="col s12 m6 Top">

											<div class="modal-footer">
												<a href="controller/agenda.php?acao=excluir&token=<?=$agenda['id']?>" class="modal-close btn2 waves-effect waves-light">Eu aceito</a>
											</div>

										</div>

									</div>

								  </div>
							<?php } ?>
					</tbody>

				</table>
				</div>
				<?php } ?>
				
				<div class="row">
					<div class="col s6 m3 offset-m1" align="center">
						<a href="agenda.php?acao=data&token=<?=$_SESSION[ "acao" ][ 'id' ]?>" class="btn waves-effect waves-light">Voltar</a> </div>					
				</div>
				
			</div>
		<?php }
		
		if ( $_SESSION[ "acao" ][ 'tipo' ] === "futuro" ) {
			$parametro = " > '". date('Y-m-d')." ' ";
			$agenda = listarAgendamentosBD($conexao, $_SESSION[ "acao" ][ 'id' ], $parametro );
			$empresa =  listarSedeEmpresasIdBD( $conexao, $_SESSION[ "acao" ][ 'id' ] );
			$atividade = listarAtividadesBD($conexao);	
			
			$titulo = 'Agendamentos Futuros';
				
			
			if( !empty($_GET[ 'filtro' ]) ){	

				if( $_GET[ 'filtro' ] === 'hora' ){
					function OrdenarapelidoCrescente($a, $b) {	return $a['hora'] > $b['hora'];	}
					
					OrdenarapelidoCrescente($a, $b);
					usort($agenda, 'OrdenarapelidoCrescente');
				}
				
				if( $_GET[ 'filtro' ] === 'data' ){
					function OrdenarNomeCrescente($a, $b) {	return $a['data'] > $b['data'];	}

					OrdenarNomeCrescente($a, $b);
					usort($agenda, 'OrdenarNomeCrescente');
				}
				
				if( $_GET[ 'filtro' ] === 'date' ){	
					$agenda = listarAgendaEmpresaEntreDataBD($conexao, $_POST['empresa'], $_POST['data1'], $_POST['data2']);
					$titulo = 'Agendamento entre Datas';
				}
			}
		?>
			<br>		
			<div class="container">
			
				<h2><?=$titulo?></h2>
				
				
				<div class="row">
					<div class="col s6 m3 offset-m1" align="center">
						<a href="agenda.php?acao=data&token=<?=$_SESSION[ "acao" ][ 'id' ]?>" class="btn waves-effect waves-light">Voltar</a> </div>					
				</div>
				
				<div class="row">
				
					<div class="col s12 l6">
						<form method="post" action="agenda.php?filtro=date">							
							<div class="input-field col s12 hide">									
								<input type="number" class="validate" name="empresa" value="<?=$_SESSION[ "acao" ][ 'id' ]?>">	
							</div>
							<div class="input-field col s5">
								<div class="col s2">
								 <i class="material-icons">update</i> 									 
								</div>
								<div class="col s10">
								  <input type="date" class="validate" name="data1" value="<?=$_POST['data1']?>">								  
								</div>
							</div>
							<div class="input-field col s5">
								<div class="col s2">
								 <i class="material-icons">date_range</i> 									 
								</div>
								<div class="col s10">
								  <input type="date" class="validate" name="data2" value="<?=$_POST['data2']?>">								  
								</div>
							</div>
							<div class="input-field col s2">
								<button class="waves-effect waves-light white-text" type="submit" style="background-color: transparent; border: 0px"><i class="material-icons">search</i>  </button>
							</div>								
						</form>	
					</div>
				</div>
				
				<p align="center"><strong>Empresa:</strong> <?php echo $empresa[0]['sede'];?></p>
				
				<?php if( empty($agenda)){ echo '<h3 align="center">Nenhum agendamento pra desta empresa</h3>';}
						else {?>
				
				<div class="row">
					<table>
					<thead>
						<tr>
						
						  	<th>Atividade</th>
							<th><a href="agenda.php?filtro=data" title="Ordenar por Nome" class=" white-text">Data</a> / <a href="agenda.php?filtro=hora" title="Ordenar por Nome" class=" white-text">Hora</a></th>
							<th><a href="agenda.php?filtro=abertura" title="Ordenar por Nome" class=" white-text">Abertura</a></th>
							<th></th>
						</tr>
					</thead>

					<tbody>
						<?php foreach ( $agenda as $agenda ) {?>
								<tr>
									<td>
										<?php $atividade = listarAtividadeBD($conexao, $agenda[ "atividade" ]);
											echo $atividade[0][ "nome" ]?>
									</td>
									<td>
										<?= date('d/m/Y', strtotime($agenda[ "data" ]))?> <small> (<?= mask($agenda[ "hora" ], '##:##')?>) </small>
									</td>
									<td>
										<?= date('d/m/Y', strtotime($agenda[ "data" ]))?>
									</td>
									<td><a href="agenda.php?acao=editar&token=<?=$agenda['id']?>" title="Editar"><i class="material-icons white-text">edit</i></a>
									<a href="agenda.php?acao=adicionar&token=<?=$agenda['id']?>" title="Adicionar Cliente"><i class="material-icons white-text">people</i></a>
									<a class="modal-trigger Link" href="#modal<?=$agenda['id']?>" title="Excluir"><i class="material-icons prefix amber-text text-darken-1 <?= $color?>">cancel</i></a>
									</td>
								</tr>

								<div id="modal<?=$agenda['id']?>" class="modal bottom-sheet">
									<div class="row">

										<div class="col s12 m6">

											<div class="modal-content">
											  <h4 class="grey-text text-darken-2">Deseja excluir <?=$agenda['nome']?>?</h4>
											  <p class="grey-text text-darken-2">Essa ação não poderá ser desfeita.</p>
											  <p class="grey-text text-darken-2"><small>Cod.: <?=$agenda['id']?></small></p>
											</div>

										</div>


										<div class="col s12 m6 Top">

											<div class="modal-footer">
												<a href="controller/agenda.php?acao=excluir&token=<?=$agenda['id']?>" class="modal-close btn2 waves-effect waves-light">Eu aceito</a>
											</div>

										</div>

									</div>

								  </div>
							<?php } ?>
					</tbody>

				</table>
				</div>
				<?php } ?>
				
				<div class="row">
					<div class="col s6 m3 offset-m1" align="center">
						<a href="agenda.php?acao=data&token=<?=$_SESSION[ "acao" ][ 'id' ]?>" class="btn waves-effect waves-light">Voltar</a> </div>					
				</div>
				
			</div>
		<?php }
	}
	
	if ( $_SESSION[ "acao" ][ 'acao' ] === 'adicionar' ){

		$_SESSION[ "acao" ][ 'acao' ] = 'adicionar';
		

		$agenda = listarAgendaidBD( $conexao, $_SESSION[ "acao" ][ 'id' ] );
		$atividade = listarAtividadeBD($conexao, $agenda[0]['atividade']);
		$empresa = listarSedeEmpresasIdBD( $conexao, $agenda[0]['empresa']);
		$funcionario = listarUsuarioBD( $conexao, $agenda[0]['funcionario'] );
		$clientes = listarusuarioEmpresaBD($conexao, $agenda[0]['empresa']);	

		$_SESSION[ "acao" ][ "acao" ] = 'adicionar';
		$_SESSION[ "acao" ][ "id" ] = $agenda[ 0 ][ 'id' ];
		
		$i = 0;

		$msgBotao = 'Agendar';

	?>

	<br>
	<div class="container">
		<div class="row">
			<h2>Adicionar Cliente na Agenda:</h2>
			<div class="col s12 m3">
				<p>Cod.: <?= $agenda[0]['id']?></p>
			</div>
			<div class="col s12 m3">
				<p>Atividade:  <?= $atividade[0]['nome']?></p>
			</div>
			<div class="col s12 m6">
				<p>Empresa: <?= $empresa[0]['sede']?></p>
			</div>
			<div class="col s12 m3">
				<p>Funcionario: <?php if(!empty($funcionario)){ echo $funcionario[0]['apelido'];} else { echo 'Funcionário não encontrado';} ?></p>
			</div>
			<div class="col s12 m3">
				<p>Data: <?= date('d/m/Y', strtotime($agenda[0]['data']))?> </p>
			</div>
			<div class="col s12 m3">
				<p>Hora: <?= mask($agenda[0]['hora'], '##:##')?></p>
			</div>
		</div>

		<div class="row">
			<h2>Lista de Clientes:</h2>
			<form action="controller/agenda.php" method="post">

				<div class="row">
				<input name="id" value="<?= $agenda[0]['id']?>" class="hide">
				<input name="empresa" value="<?= $agenda[0]['empresa']?>" class="hide">				

				<?php if( empty($clientes)){ echo 'Nenhum Cliente Cadastrado.';}
						foreach ( $clientes as $clientes ) {?>
				<div class="col s12 m3">
					<div class="col s3">
						<p>
						  <label>
							<input type="checkbox" name="check<?=$i?>"/>
							<span>  </span>
						  </label>
						</p>
					</div>
					<div class="col s9">
						<p>Cod: <?= $clientes['id']?><br>
						Nome: <?= $clientes['nome']?></p>
					</div>
				</div>
				<?php $i++; } ?>
				</div>
				<div class="row">
					<div class="col s6 m3 offset-m1" align="center">
						<a href="agenda.php?acao=data&token=<?=$_SESSION[ "acao" ][ 'id' ]?>" class="btn waves-effect waves-light">Voltar</a> </div>
					<div class="col s6 m6">
						<button class="btn2 waves-effect waves-light right" type="submit">
							<?= $msgBotao?>
						</button>
					</div>
				</div>
			 </form>
		</div>
	</div>

	<?php  }		

	
	

	if ( $_SESSION[ "acao" ][ 'acao' ] === "listar" ) {
		
		$empresa = listarSedeEmpresasBD($conexao);		
		
		if( empty($_POST['matriz'])){	
			$_SESSION[ "acao" ][ 'acao' ] = 'listar';	
		?>
		<br>		
			<div class="container"><h2>Agenda</h2>
				<form class="col s12" action="agenda.php" method="post" accept-charset="UTF-8">
				
					<div class="row">
						<div class="input-field col s12">
							<i class="material-icons prefix">domain</i>
							<select name="matriz">
								<option disabled selected></option>
								<?php foreach ( $empresa as $empresa ){
										if ($_POST['matriz'] == $empresa["id"]){ $selected = 'selected';} else {$selected = '';}?>
								<option value="<?=$empresa["id"];?>" label="<?=$empresa["sede"];?>" data-icon="<?php echo $empresa['logo'] = strstr($empresa['logo'],'usuarios/');?>" <?=$selected?> ><?=$empresa["sede"];?></option>
								<?php } ?>
							</select>
							<label for="empresa">Escolha uma Matriz:</label>
						</div>
					</div>				
					
					<div class="row">
						<div class="col s6 m3 offset-m1" align="center">
							<a href="dashboard.php" class="btn waves-effect waves-light">Voltar</a> </div>
						<div class="col s6 m6">
							<button class="btn2 waves-effect waves-light right" type="submit">Avançar</button>
						</div>
					</div>
					
				</form>
			</div>
		<?php include_once( 'rodape.php' ); 
		exit;	
									}
		
		if( !empty($_POST['matriz'])){	
			$_SESSION[ "acao" ][ 'acao' ] = 'novo';	
			$filial =  listarEmpresaSedeFilial($conexao, $_POST['matriz']);
		?>
		<br>		
			<div class="container"><h2>Agenda</h2>
				<form class="col s12" action="agenda.php" method="post" accept-charset="UTF-8">
				
					<div class="row">
						<div class="input-field col s12">
							<i class="material-icons prefix">domain</i>
							<select name="matriz">
								<option disabled selected></option>
								<?php foreach ( $empresa as $empresa ){
										if ($_POST['matriz'] == $empresa["id"]){ $selected = 'selected';} else {$selected = '';}?>
								<option value="<?=$empresa["id"];?>" label="<?=$empresa["sede"];?>" data-icon="<?php echo $empresa['logo'] = strstr($empresa['logo'],'usuarios/');?>" <?=$selected?> ><?=$empresa["sede"];?></option>
								<?php } ?>
							</select>
							<label for="empresa">Escolha uma Matriz:</label>
						</div>
					</div>	
													
					<div class="row">
						<div class="input-field col s12">
							<i class="material-icons prefix">domain</i>
							<select name="empresa">
								<option disabled selected></option>
								<?php foreach ( $filial as $filial ){
										if ($agenda[0]["empresa"] == $filial["id"]){ $selected = 'selected';} else {$selected = '';}?>
								<option value="<?=$filial["id"];?>" data-icon="<?php echo $filial['logo'] = strstr($filial['logo'],'usuarios/');?>" <?=$selected;?> ><?=$filial["sede"];?></option>
								<?php } ?>
							</select>
							<label for="empresa">Escolha uma Filial:</label>
						</div>
					</div>				
					
					<div class="row">
						<div class="col s6 m3 offset-m1" align="center">
							<a href="dashboard.php" class="btn waves-effect waves-light">Voltar</a> </div>
						<div class="col s6 m6">
							<button class="btn2 waves-effect waves-light right" type="submit">Avançar</button>
						</div>
					</div>
					
				</form>
			</div>
		<?php include_once( 'rodape.php' ); 
		exit;	
									}
	}
	
	if ( $_SESSION[ "acao" ][ 'acao' ] === "novo" ) {
		
		$_SESSION[ "log" ][ "on" ] = "true";	
		if($_SESSION[ "log" ][ "on" ] === "true" ){
			if( !empty($_SESSION[ "log" ][ "msg" ])){
				echo '<div class="msg">' . $_SESSION[ "log" ][ "msg" ] . '</div>';
			}			
			unset($_SESSION[ "log" ][ "msg" ]);
		}		
		$_SESSION[ "log" ][ "on" ] = "false";

		$msgBotao = 'Agendar';

		$atividade = listarAtividadesBD( $conexao );		
		$funcionario = listarFuncionariosBD( $conexao );		
		$empresa = listarEmpresaFilial( $conexao, $_POST['empresa']);

		$_SESSION[ "acao" ][ "acao" ] = 'inserir';	
?>
		<br>
		<div class="container row">

			<h2>Novo Agendamento</h2>
			
			<div class="row">
				<p align="center">Filial: <?= $empresa[0]['social']?></p>
			</div>

			<div class="row">
				<form class="col s12" action="controller/agenda.php" method="post" accept-charset="UTF-8">
				
					<div class="row">
						<div class="input-field col s12 m6 xl4">
							<i class="material-icons prefix">directions_run</i>
							<select name="atividade">
								<option value="" disabled selected>Escolha uma Atividade</option>
								<?php foreach ( $atividade as $atividade ){
								if ($usuario[0]["id_atividade"] == $atividade["id"]){ $selected = 'selected';} else {$selected = '';}?>
								<option value="<?=$atividade["id"];?>" <?=$selected;?>> <?=$atividade["nome"];?> <small>(<?php
								$ativQTD = strlen($atividade["duracao"]);
									switch ($ativQTD) {
										case 1:
											echo mask($atividade["duracao"], '00:0#');
											break;
										case 2:
											echo mask($atividade["duracao"], '00:##');
											break;
										case 3:
											echo mask($atividade["duracao"], '0#:##');
											break;
										case 4:
											echo mask($atividade["duracao"], '##:##');
											break;
									}?>)</small> </option>
								<?php } ?>
							</select>
							<label for="atividade">Atividade</label>
						</div>
						<div class="input-field col s12 m6 xl4">
							<i class="material-icons prefix">account_circle</i>
							<input id="nome" type="text" class="validate" name="nome">
							<label for="nome">Nome </label>
						</div>
						<div class="input-field col s12 m12 xl4">
							<i class="material-icons prefix">description</i>
							<input id="descricao" type="text" class="validate" name="descricao">
							<label for="descricao">Descrição </label>
						</div>

					</div>

					<div class="row">


						<div class="input-field col s12 m6 xl3">
							<i class="material-icons prefix">date_range</i>
							<input id="data" type="date" class="validate" name="data">
							<label for="data">Data Inicio</label>
						</div>
						<div class="input-field col s12 m6 xl3">
							<i class="material-icons prefix">date_range_off</i>
							<input id="dataf" type="date" class="validate" name="dataf">
							<label for="dataf">Data Final</label>
						</div>
						<div class="input-field col s12 m6 xl3">
							<i class="material-icons prefix">timer</i>
							<input id="hora" type="text" class="validate time" name="hora" data-mask="00:00" min="0000" max="2359">
							<label for="hora">Hora Início</label>
						</div>
						<div class="input-field col s12 m6 xl3">
							<i class="material-icons prefix">timer_off</i>
							<input id="horaf" type="text" class="validate time" name="horaf" data-mask="00:00" min="0000" max="2359">
							<label for="horaf">Hora Final</label>
						</div>

					</div>

					<div class="row">
						<div class="input-field col s12 m6 xl3">
							<i class="material-icons prefix">event_available</i>
							<input id="abertura" type="date" class="validate" name="abertura">
							<label for="abertura">Abertura das vagas:</label>
						</div>
						<div class="input-field col s12 m6 xl3">
							<i class="material-icons prefix">add_circle_outline</i>
							<input id="quantidades" type="number" class="validate" name="quantidades">
							<label for="quantidades">Vagas: </label>
						</div>
						
						<div class="input-field col s12 m6 hide">
							<i class="material-icons prefix">domain</i>
							<select name="empresa">
								<option value="" disabled selected>Escolha uma empresa</option>
								<?php foreach ( $empresa as $empresa ){
								if ($_POST['empresa'] == $empresa["id"]){ $selected = 'selected';} else {$selected = '';}
								if (!empty($empresa["sede"])){ ?>
									<option value="<?=$empresa["id"];?>" <?=$selected;?>><?=$empresa["sede"];?></option>
								<?php }
								} ?>
							</select>
							<label for="empresa">Empresa <small>Filial</small></label>
						</div>
						<div class="input-field col s12 m6">
							<i class="material-icons prefix">assignment_ind</i>
							<select name="funcionario">
								<option value="" disabled selected>Escolha um Profissional</option>
								<?php foreach ( $funcionario as $funcionario ){
								if ($usuario[0]["id_funcionario"] == $funcionario["id"]){ $selected = 'selected';} else {$selected = '';}?>
								<option value="<?=$funcionario["id"];?>" <?=$selected;?>><?=$funcionario["nome"];?> <?=$funcionario["apelido"];?></option>
								<?php } ?>
							</select>
							<label for="funcionario">Profissional </label>
						</div>
					</div>
					
					<div class="row">
						<div class="input-field col s12 m6 xl3">
							<i class="material-icons prefix">block</i>
							<input id="recadastrados" type="number" class="validate" name="recadastrados" maxlength="20">
							<label for="recadastrados">Bloqueio (Recadastrados): </label>
						</div>
						<div class="input-field col s12 m6 xl3">
							<i class="material-icons prefix">timelapse</i>
							<select name="frequencia">
								<option value="" disabled selected>Escolha uma Frequência</option>
								<option value="1">Diaria</option>
								<option value="2">Semanal </option>
								<option value="3">Mensal</option>
								<option value="4">Anual</option>
							</select>
							<label for="frequencia">Frequência</label>
						</div>
					</div>

					<div class="row">
						<div class="input-field col s12">
						Repetir nos dias da semana:
						<p>
<div class="col s12 m3">
							  	<label>
									<input type="checkbox" name="dia0" />
									<span>Domingo</span>
								  </label>
</div>

<div class="col s12 m3">
						  <label>
							<input type="checkbox" name="dia1" />
							<span>Segunda</span>
						  </label>
</div>

<div class="col s12 m3">
						  <label>
							<input type="checkbox" name="dia2" />
							<span>Terça</span>
						  </label>
</div>

<div class="col s12 m3">

						  <label>
							<input type="checkbox" name="dia3" />
							<span>Quarta</span>
						  </label>
</div>

<div class="col s12 m3">

						  <label>
							<input type="checkbox" name="dia4" />
							<span>Quinta</span>
						  </label>
</div>

<div class="col s12 m3">

						  <label>
							<input type="checkbox" name="dia5" />
							<span>Sexta</span>
						  </label>
</div>

<div class="col s12 m3">

						  <label>
							<input type="checkbox" name="dia6" />
							<span>Sábado</span>
						  </label>
</div>
						</p>
						</div>
					</div>					
					
					<div class="row">
						<div class="input-field col s12 m3 offset-m4">
							<p align="center">
								<label>
								<input type="checkbox" name="ativo" checked class="white-text" />
								<span>Ativo</span>
								</label>
							</p>
						</div>
					</div>
					
					<div class="row">
						<div class="col s6 m3 offset-m1" align="center">
							<a href="dashboard.php" class="btn waves-effect waves-light">Voltar</a> </div>
						<div class="col s6 m6">
							<button class="btn2 waves-effect waves-light right" type="submit">
								<?= $msgBotao?>
							</button>
						</div>
					</div>
					
				</form>
			</div>
			
		</div>
		<?php 
	}
	
	
	

	if ( $_SESSION[ "acao" ][ 'acao' ] === "editar" ) {

		$msgBotao = 'alterar';

		$agenda = listarAgendaidBD( $conexao, $_SESSION[ "acao" ][ 'id' ] );
		$atividade = listarAtividadesBD( $conexao );
		$empresa = listarSedeEmpresasBD( $conexao );
		$funcionario = listarFuncionariosBD( $conexao );

		$_SESSION[ "acao" ][ "acao" ] = 'alterar';
		$_SESSION[ "acao" ][ "id" ] = $agenda[ 0 ][ 'id' ];

		?>
		<br>
		<div class="container row">

			<h2>Editar Agendamento</h2> 
			
			

			<div class="row">
				<div class="col s6 m3 offset-m1" align="center">
					<a href="agenda.php?acao=listar" class="btn waves-effect waves-light">Voltar</a> </div>				
			</div>

			<div class="row">	
				<form class="col s12" action="controller/agenda.php" method="post" accept-charset="UTF-8">
					<div class="row">
						<div class="input-field col s12 m6">
							<i class="material-icons prefix">account_circle</i>
							<input id="nome" type="text" class="validate" name="nome" value="<?= $agenda[0]['nome']?>">
							<label for="nome">Nome </label>
						</div>
						<div class="input-field col s12 m6">
							<i class="material-icons prefix">description</i>
							<input id="descricao" type="text" class="validate" name="descricao" value="<?= $agenda[0]['descricao']?>">
							<label for="descricao">Descrição </label>
						</div>
					</div>

					<div class="row">
						<div class="input-field col s12 m3">
							<i class="material-icons prefix">directions_run</i>
							<select name="atividade">
								<option value="" disabled selected>Escolha uma Atividade</option>
								<?php foreach ( $atividade as $atividade ){
										if ($agenda[0]["atividade"] === $atividade["id"]){ $selected = 'selected';} else {$selected = '';}?>
								<option value="<?=$atividade[" id "];?>" <?=$selected;?>><?=$atividade["nome"];?></option>
								<?php } ?>
							</select>
							<label for="atividade">Atividade</label>
						</div>
						<div class="input-field col s12 m3">
							<i class="material-icons prefix">date_range</i>
							<input id="data" type="date" class="validate" name="data" value="<?= $agenda[0]['data']?>">
							<label for="data">Data</label>
						</div>
						<div class="input-field col s12 m3">
							<i class="material-icons prefix">timer</i>
							<input id="hora" type="text" class="validate" name="hora" data-mask="00:00" value="<?= $agenda[0]['hora']?>">
							<label for="hora">Inicio</label>
						</div>
						<!--<div class="input-field col s12 m3">
							<i class="material-icons prefix">timer_off</i>
							<input id="duracao" type="text" class="validate" name="duracao" data-mask="00:00" value="<?= $agenda[0]['duracao']?>">
							<label for="duracao">Final</label>
						</div>-->
					</div>

					<div class="row">
						<div class="input-field col s12 m3">
							<i class="material-icons prefix">timelapse</i>
							<select name="frequencia">
								<?php
						switch ($agenda[0]['frequencia']) {
							case 1:
								$valor1 = 'selected';
								break;
							case 2:
								$valor2 = 'selected';
								break;
							case 3:
								$valor3 = 'selected';
								break;
							case 4:
								$valor4 = 'selected';
								break;
						}
						?>
								<option value="" disabled selected>Escolha uma Frequencia</option>
								<option value="1" <?=$valor1?> >Diaria</option>
								<option value="2" <?=$valor2?> >Semanal </option>
								<option value="3" <?=$valor3?> >Mensal</option>
								<option value="3" <?=$valor4?> >Anual</option>
							</select>
							<label for="frequencia">Frequencia</label>
						</div>
						<div class="input-field col s12 m3">
							<i class="material-icons prefix">event_available</i>
							<input id="abertura" type="date" class="validate" name="abertura" value="<?= $agenda[0]['abertura']?>">
							<label for="abertura">Abertura das vagas:</label>
						</div>
						<div class="input-field col s12 m3">
							<i class="material-icons prefix">add_circle_outline</i>
							<input id="quantidades" type="number" class="validate" name="quantidades" value="<?= $agenda[0]['quantidades']?>">
							<label for="quantidades">Quantidades de vagas: </label>
						</div>

						<div class="input-field col s12 m3">
							<i class="material-icons prefix">block</i>
							<input id="recadastrados" type="number" class="validate" name="recadastrados" value="<?= $agenda[0]['recadastrados']?>">
							<label for="recadastrados">Bloqueio: </label>
						</div>
					</div>

					<div class="row">
						<div class="input-field col s12 m6">
							<i class="material-icons prefix">domain</i>
							<select name="empresa">
								<option value="" disabled selected>Escolha uma empresa</option>
								<?php foreach ( $empresa as $empresa ){
										if ($agenda[0]["empresa"] == $empresa["id_empresa"]){ $selected = 'selected';} else {$selected = '';}?>
								<option value="<?=$empresa["id_empresa"];?>" <?=$selected;?>><?=$empresa["sede"];?></option>
								<?php } ?>
							</select>
							<label for="empresa">Empresa</label>
						</div>
						<div class="input-field col s12 m3">
							<i class="material-icons prefix">assignment_ind</i>
							<select name="funcionario">
								<option value="" disabled selected>Escolha um Funcionário</option>
								<?php foreach ( $funcionario as $funcionario ){
								$func[$funcionario["id"]] = $funcionario["nome"];
								if ($agenda[0]["funcionario"] == $funcionario["id"]){ $selected = 'selected';} else {$selected = '';}?>
								<option value="<?=$funcionario["id"];?>" <?=$selected;?>><?=$funcionario["nome"];?></option>
								<?php } ?>
							</select>
							<label for="funcionario">Profissional</label>
						</div>
					</div>
					
					<div class="row">
						<div class="input-field col s12 m3 offset-m4">
							<p align="center">
								<label>
								<input type="checkbox" name="ativo" checked class="white-text" />
								<span>Ativo</span>
								</label>
							</p>
						</div>
					</div>
					
					<?php
					$array = array( 	$agenda[0]['id'],
										$agenda[0]['nome'],
										$agenda[0]['descricao'],
										$agenda[0]["atividade"],
										$agenda[0]["empresa"],
										$agenda[0]["funcionario"],

									);

					$agendas = listarAgendasCorrespondentesBD( $conexao, $array);
					$agendas_qtds = count($agendas)	;
					$i = 0;
					?>
					
					<div class="row">
						<h2>Alterar Tambem </h2>
						<p><small> Agendamentos Parecidos <?= $agendas_qtds?></small> <label class="right"><input type="checkbox" name="marcarTodos" /> <span>Marcar Todos</span> </label> </p>				
						<div class="input-field col s12 hide">
							<i class="material-icons prefix">event_available</i>
							<input id="agendas_qtds" type="number" name="agendas_qtds" value="<?= $agendas_qtds?>">
							<label for="agendas_qtds">Abertura das vagas:</label>
						</div>
						
						<ul class="collapsible">
						<?php 
						/* Funciona
						foreach ( $agendas as $agendas ){ ?>
						
						<div class="col s6 hide">
						  <input id="outroid" type="text" name="outroid<?=$i?>" value="<?= $agendas['id']?>">
						  <label for="outroid">outroid</label>
						</div>
						<div class="col s6 ">
							<p>
							  <label>
								<input type="checkbox" name="outros<?=$i?>" />
								<span>Func. <?=  $func[$agendas['funcionario']]?><br>
								<small>(<?= date('d/m/Y', strtotime($agendas['data'])) ?> / <?= mask($agendas['hora'], '##:##')?>) - <?=  $agendas['id']?></small> </span>
							  </label>
							</p>
						</div>
						
						<?php $i++; }*/
						//Teste (Melhoria no Visual)
						$array = array( $agenda[0]['id'],
										$agenda[0]['nome'],
										$agenda[0]['descricao'],
										$agenda[0]["atividade"],
										$agenda[0]["empresa"],
										$agenda[0]["funcionario"]
									);

						$agendas = listarAgendasCorrespondentesBD( $conexao, $array);
						for($i = 0; $i < count($agendas); $i++){
							$datas[$i] = $agendas[$i]['data'];
						}
		
						count($datas);
		
						for($i = 0; $i < count($agendas); $i++){
							if($datas[--$i] != $datas[++$i]){?>
							<li>
							  <div class="collapsible-header" align="center"><?=date('d/m/Y', strtotime($datas[$i]))?></div>
							  <div class="collapsible-body">
								<?php unset($contadorDia);
									for($a = 0; $a < count($agendas); $a++){
									if($agendas[$a]['data'] === $datas[$i]){ 
										$contadorDia++; ?>								  		
									  <div>
										  <label>
											<input type="checkbox" name="outros<?=$a?>" />
											<span><?= mask($agendas[$a]['hora'], '##:##')?></span>
										  </label>
										</div>
									<?php }									
								} echo 'Registos do dia: '.$contadorDia;?>
								</div>
							</li>									
							<?php }							
						}?>
						</ul>
					</div>

					<div class="row">
						<div class="col s6 m3 offset-m1" align="center">
							<a href="agenda.php?acao=listar" class="btn waves-effect waves-light">Voltar</a> </div>
						<div class="col s6 m6">
							<button class="btn2 waves-effect waves-light right" type="submit">
								<?= $msgBotao?>
							</button>
						</div>
					</div>
				</form>
			</div>
		</div>


		<?php 
	}

	if ( $_SESSION[ "acao" ][ 'acao' ] === 'disponiveis' ) {

		unset( $_SESSION[ "acao" ] );

		//Lista a agenda para os clientes relacionado a empresa dele
		$agenda = listarAgendaClienteBD( $conexao, $_SESSION[ 'usuario' ][ 'id_empresa' ] );
		$dataHoje = date( 'Y/m/d' );

		?>
		<br>
		<div class="container row">
			<h2>Agendamento</h2>
			<table>
				<thead>
					<tr>
						<th>Atividade</th>
						<th>Empresa</th>
						<th>Data</th>
						<th>Hora</th>
						<th>Vagas</th>
						<th></th>
					</tr>
				</thead>

				<tbody>
			<?php foreach ( $agenda as $agenda ) {

					$dataAgendada = date('Y/m/d', strtotime($agenda[ "data" ]));

					if($dataHoje <= $dataAgendada){

						unset($block);
						$block = blockMultiplosAgendamentosBD($conexao, $agenda['id'], $_SESSION[ 'usuario' ][ 'id' ]);

						if (empty($block)) {
							$bloqueado = 'false';
							$color = 'white-text';;
						}

						if (!empty($block)) {
							foreach ( $block as $block ) {

								if($agenda["id"] == $block['id_agenda']){
									$bloqueado = 'true';
									$color = 'blue-text text-lighten-4';
								} else {
									$bloqueado = 'false';
									$color = 'white-text';
								}
							}
						}

					?>
					<tr class="<?=$color?>">
						<td>
							<?= $agenda[ "nomeAtividade" ]?>
						</td>
						<td>
							<?php 	$empresa = listarEmpresasBD( $conexao );
							$agenda[ "quantidades" ];

							if($empresa[0]["id"] == $agenda[ "empresa" ]) {
								echo $empresa[0]["nome"];
							}?>
						</td>
						<td data-mask="00/00/0000">
							<?= date('d/m/Y', strtotime($agenda[ "data" ]));?>
						</td>
						<td data-mask="00:00">
							<?= $agenda[ "hora" ]?>
						</td>
						<td>
							<?php if($bloqueado === 'true'){
									echo 'Já cadastrado';
									} else{
									$vagas = listarVagas($conexao, $agenda[ "id" ]);
										echo $disponivel = $agenda[ "quantidades" ] - $vagas->num_rows ;
							}?> </td>
						<td>
							<?php 	if($bloqueado === 'true'){
										echo '<i class="material-icons">close</i>';
									} else{
										if($disponivel > 0){
											echo '<a href="#modal'.$agenda[ "id" ].'" class="modal-trigger"><i class="material-icons white-text">event_available</i></a>';
										} else {
											echo '<a class="modal-trigger" title="Indisponivel"><i class="material-icons white-text">event_busy</i></a>';
										}
									} ?>
						</td>
					</tr>
					<!-- Modal Structure -->
					<div id="modal<?=$agenda[ "id" ]?>" class="modal grey-text text-darken-3">
						<div class="modal-content">

							<h4><?= $agenda[ "nome" ]?></h4>

							<p>Deseja agendar esse horário?</p>

						</div>

						<div class="modal-footer">
							<a href='agenda.php?acao=agendar&token=<?= $agenda[ "id" ]?>' class="modal-close waves-effect waves-green btn">Agendar</a>
						</div>

					</div>
					<?php } } ?>
				</tbody>
			</table>
		</div>
		<div class="row">
			<div class="col s6 m3 offset-m1" align="center">
				<a href="dashboard.php" class="btn waves-effect waves-light">Voltar</a> </div>
		</div>
		<?php 

	}

	if ( $_SESSION[ "acao" ][ 'acao' ] === 'agendar' ) {
		echo '<script>window.location.replace("controller/agenda.php");</script>'; 
	}

	if ( $_SESSION[ "acao" ][ 'acao' ] === 'cancelar' ) {
		echo '<script>window.location.replace("controller/agenda.php");</script>'; 
	}

	if ( $_SESSION[ "acao" ][ 'acao' ] === 'agendados' ) {

		//Funcionario
		if($_SESSION['usuario']['id_tipo'] === '2'){
			
			$dataHoje = date( 'Y/m/d' );?>
		<br>
		<div class="container row">

			<h2>Agenda Funcionário</h2>
			<p><small>* Recomendamos chegar 10 minutos antes do horario de inicio.</small></p>
			<p><small>** Cancelamentos somente 24H antes do horaio de inicio.</small></p>
			
			<h3>Agendamentos de Hoje</h3>
			
			<table>
				<thead>
					<tr>
						<th>Atividade</th>
						<th>Descrição</th>
						<th>Data</th>
						<th>Início</th>
					</tr>
				</thead>
				<tbody>
					<?php 
			$agenda = listaragendadosFuncionarioBD( $conexao, $_SESSION[ 'usuario' ][ 'id' ] );
			foreach ( $agenda as $agenda ) {
				$dataAgendada = date('Y/m/d', strtotime($agenda[ "data" ]));
					if($dataHoje === $dataAgendada){ ?>
						<tr>
							<td>
								<a href="#modal<?=$agenda['id']?>" class="modal-trigger white-text">
								<?php $atividade = listarAtividadeBD($conexao, $agenda["atividade"]); 
									echo $atividade[0]['nome'].'( <small>'.mask($atividade[0]['duracao'], '##:##').'</small> )' ;?>
							</a></td>
							<td>
								<a href="#modal<?=$agenda['id']?>" class="modal-trigger white-text">
								<?= $agenda[ "descricao" ] ?>
							</a></td>
							<td>
								<a href="#modal<?=$agenda['id']?>" class="modal-trigger white-text">
								<?= date('d/m/Y', strtotime($agenda[ "data" ]));?>
							</a></td>
							<td>
								<a href="#modal<?=$agenda['id']?>" class="modal-trigger white-text">
								<?= mask($agenda[ "hora" ], '##:##')?>
							</a></td>
						</tr>

						<!-- Modal Structure -->
						  <div id="modal<?=$agenda['id']?>" class="modal" style="background-color: #008aae;">
							<div class="modal-content">

							  <h4><?= $agenda[ "nome" ]?></h4>

							  <p><small>Cod.::  <?= $agenda[ "id" ] ?></small></p>

							  <p>Data: <?= date('d/m/Y', strtotime($agenda[ "data" ]));?> as <?= mask($agenda[ "hora" ], '##:##')?></p>
							  <p>Endereço: <?= $agenda[ "endereco" ] ?> </p>
							  <p>Descrição:  <?= $agenda[ "descricao" ] ?></p>
							  <p>Atividade:  <?php $atividade = listarAtividadeBD($conexao, $agenda[ "atividade" ]);
													echo $atividade[0]['nome'];?></p>
							  <p>Duracao:  <?= mask($atividade[0]['duracao'], '##:##') ?></p>
							  <p>Abertura das Vagas:  <?= date('d/m/Y', strtotime($agenda[ "abertura" ])) ?></p>
							  <p>Empresa:  <?php $empresa = listarUsuarioBD( $conexao, $agenda[ "empresa" ] );
													echo $empresa[0]['apelido']?></p>

							  <p>Quantidades de Vagas:  <?= $agenda[ "quantidades" ] ?></p>


								<?php $vagas = listarVagas($conexao, $agenda['id']);
									$disponiveis = $agenda[ "quantidades" ] - count($vagas);
								   if($disponiveis <= 0){
										echo "<p>Disponives: Lotado  <small>(Lista de Espera)</small></p>";
									}
									if($disponiveis > 0){
										echo '<p>Disponives: '. $disponiveis . '</p>';
								   	}
									?>

							  <p align="center">Lista de Aluno: </p>

									<?php $i = $agenda[ "quantidades" ];  
										
										foreach ( $vagas as $vagas ) {
											
											if($i > 0){
												$colorborder = 'style="border-left: 5px solid #00E762;"';
												$men = "Agendado";
											} else {
												unset($colorborder);
												$men = "Lista de Espera";
												
											}
										
											$i--;
										
											$PessoasAgendadas =  listarUsuarioBD( $conexao, $vagas['id_usuario'] );	?>
									   <div class="col s12 m6 ">
										   <blockquote <?=$colorborder?> >
										   	<p align="center"><?=$men?></p>
											<p>Aluno: <?= $PessoasAgendadas[0]['apelido']?> </p>
											<p>Status: <?php
													switch ($vagas['presenca']) {
														case 0:
															echo "Aguardando Registro";
															break;
														case 1:
															echo "Presente";
															break;
														case 2:
															echo "Faltou";
															break;
														case 5:
															echo "Cancelado";
															break;
													}
													?> </p>
											<p>Contatos: <?php $a = strlen($PessoasAgendadas[0]['telefone']);
															if( $a == 11){
																	echo mask($PessoasAgendadas[0]['telefone'], '(##) ###-###-###'). " / " .$PessoasAgendadas[0]['email'];
															} else { echo "Telefone não registrado / " .$PessoasAgendadas[0]['email'];}?>  
											</p>
											</blockquote>
							  			</div>
									<?php }	?>

							</div>
						  </div>
						<?php }
					} ?>
			</tbody>
			</table>
			
			<h3>Agendamentos Futuros</h3>
			<table>
				<thead>
					<tr>
						<th>Atividade</th>
						<th>Descrição</th>
						<th>Data</th>
						<th>Início</th>
					</tr>
				</thead>
				<tbody>
					<?php $agenda = listaragendadosFuncionarioBD( $conexao, $_SESSION[ 'usuario' ][ 'id' ] );
			foreach ( $agenda as $agenda ) {
				$dataAgendada = date('Y/m/d', strtotime($agenda[ "data" ]));

					if($dataHoje <= $dataAgendada){ ?>
						<tr>
							<td>
								<a href="#modal<?=$agenda['id']?>" class="modal-trigger white-text">
								<?php $atividade = listarAtividadeBD($conexao, $agenda["atividade"]); 
									echo $atividade[0]['nome'].'( <small>'.mask($atividade[0]['duracao'], '##:##').'</small> )' ;?>
							</a></td>
							<td>
								<a href="#modal<?=$agenda['id']?>" class="modal-trigger white-text">
								<?= $agenda[ "descricao" ] ?>
							</a></td>
							<td>
								<a href="#modal<?=$agenda['id']?>" class="modal-trigger white-text">
								<?= date('d/m/Y', strtotime($agenda[ "data" ]));?>
							</a></td>
							<td>
								<a href="#modal<?=$agenda['id']?>" class="modal-trigger white-text">
								<?= mask($agenda[ "hora" ], '##:##')?>
							</a></td>
						</tr>

						<!-- Modal Structure -->
						  <div id="modal<?=$agenda['id']?>" class="modal" style="background-color: #008aae;">
							<div class="modal-content">

							  <h4><?= $agenda[ "nome" ]?></h4>

							  <p><small>Cod.::  <?= $agenda[ "id" ] ?></small></p>

							  <p>Data: <?= date('d/m/Y', strtotime($agenda[ "data" ]));?> as <?= mask($agenda[ "hora" ], '##:##')?></p>
							  <p>Endereço: <?= $agenda[ "endereco" ] ?> </p>
							  <p>Descrição:  <?= $agenda[ "descricao" ] ?></p>
							  <p>Atividade:  <?php $atividade = listarAtividadeBD($conexao, $agenda[ "atividade" ]);
													echo $atividade[0]['nome'];?></p>
							  <p>Duração:  <?= mask($atividade[0]['duracao'], '##:##') ?></p>
							  <p>Abertura das Vagas:  <?= date('d/m/Y', strtotime($agenda[ "abertura" ])) ?></p>
							  <p>Empresa:  <?php $empresa = listarUsuarioBD( $conexao, $agenda[ "empresa" ] );
													echo $empresa[0]['apelido']?></p>

							  <p>Quantidades de Vagas:  <?= $agenda[ "quantidades" ] ?></p>


								<?php $vagas = listarVagas($conexao, $agenda['id']);
									$disponiveis = $agenda[ "quantidades" ] - count($vagas);
								   if($disponiveis <= 0){
										echo "<p>Disponives: Lotado  <small>(Lista de Espera)</small></p>";
									}
									if($disponiveis > 0){
										echo '<p>Disponives: '. $disponiveis . '</p>';
								   	}
									?>

							  <p align="center">Lista de Aluno: </p>

									<?php $i = $agenda[ "quantidades" ];  
										
										foreach ( $vagas as $vagas ) {
											
											if($i > 0){
												$colorborder = 'style="border-left: 5px solid #00E762;"';
												$men = "Agendado";
											} else {
												unset($colorborder);
												$men = "Lista de Espera";
												
											}
										
											$i--;
										
											$PessoasAgendadas =  listarUsuarioBD( $conexao, $vagas['id_usuario'] );	?>
									   <div class="col s12 m6 ">
										   <blockquote <?=$colorborder?> >
										   	<p align="center"><?=$men?></p>
											<p>Aluno: <?= $PessoasAgendadas[0]['apelido']?> </p>
											<p>Status: <?php
													switch ($vagas['presenca']) {
														case 0:
															echo "Aguardando Registro";
															break;
														case 1:
															echo "Presente";
															break;
														case 2:
															echo "Faltou";
															break;
														case 5:
															echo "Cancelado";
															break;
													}
													?> </p>
											<p>Contatos: <?php $a = strlen($PessoasAgendadas[0]['telefone']);
															if( $a == 11){
																	echo mask($PessoasAgendadas[0]['telefone'], '(##) ###-###-###'). " / " .$PessoasAgendadas[0]['email'];
															} else { echo "Telefone não registrado / " .$PessoasAgendadas[0]['email'];}?>  
											</p>
											</blockquote>
							  			</div>
									<?php }	?>

							</div>
						  </div>
						<?php }
					} ?>
			</tbody>
			</table>
		</div>


	<p align="center"><a href="#modal-1" class="Link modal-trigger">Histórico</a></p>
		<!-- Modal Structure -->
		  <div id="modal-1" class="modal">
			<div class="modal-content">
			  <h4>Histórico</h4>

				<table>
				<thead>
					<tr>
						<th>Atividade</th>
						<th>Endereço</th>
						<th>Data</th>
						<th>Início</th>
					</tr>
				</thead>

				<tbody>
					<?php
					$agenda = listaragendadosFuncionarioBD( $conexao, $_SESSION[ 'usuario' ][ 'id' ] );
					foreach ( $agenda as $agenda ) {
						$dataAgendada = date('Y/m/d', strtotime($agenda[ "data" ]));

							if($dataHoje > $dataAgendada || $agenda['presenca'] != 0 ){?>

								<tr>
									<td><a href="agenda.php?acao=visualizar&token=<?=$agenda['id']?>" class="modal-trigger">
										<?= $agenda[ "nome" ]?>
										</a>
									</td>
									<td><a href="agenda.php?acao=visualizar&token=<?=$agenda['id']?>" class="modal-trigger">
										<?= $agenda[ "endereco" ] ?>
										</a>
									</td><a href="agenda.php?acao=visualizar&token=<?=$agenda['id']?>" class="modal-trigger">
									<td data-mask="00/00/0000">
										<?= date('d/m/Y', strtotime($agenda[ "data" ]));?>
										</td></a>

									<td data-mask="00:00"><a href="agenda.php?acao=visualizar&token=<?=$agenda['id']?>" class="modal-trigger">
										<?= $agenda[ "hora" ]?>
										</a>
									</td>
								</tr>

								<?php } ?>
						<?php } ?>
				</tbody>
			</table>

			</div>
		  </div>


		<div class="row">
			<div class="col s6 m3 offset-m1" align="center">
				<a href="dashboard.php" class="btn2 waves-effect waves-light">Voltar</a> </div>
		</div>
		<?php }

		//Cliente
		if($_SESSION['usuario']['id_tipo'] === '4'){
			$agenda = listaragendadosClienteBD( $conexao, $_SESSION[ 'usuario' ][ 'id' ] );


		$dataHoje = date( 'Y/m/d' );

		?>
		<br>
		<div class="container row">

			<h2>Agenda</h2>
			<p>
				<small>* Recomendamos chegar 10 minutos antes do horario de inicio.</small><br>
				<small>** Cancelamentos somente 24H antes do horaio de inicio.</small>
			</p>
			<table>
				<thead>
					<tr>
						<th>Atividade</th>
						<th>Data</th>
						<th>Início</th>
						<th>Presença</th>
						<th></th>
					</tr>
				</thead>

				<tbody>
					<?php
		foreach ( $agenda as $agenda ) {
			$dataAgendada = date('Y/m/d', strtotime($agenda[ "data" ]));

				if($dataHoje <= $dataAgendada && $agenda['presenca'] === '0'){?>
					<tr>
						<td>
							<?= $agenda[ "nomeAtividade" ]?>
						</td>
						<td data-mask="00/00/0000">
							<?= date('d/m/Y', strtotime($agenda[ "data" ]));?>
						</td>
						<td data-mask="00:00">
							<?= mask($agenda[ "hora" ], '##:##')?>
						</td>
						<td>
							<?php 										  
								switch ($agenda[ "presenca" ]) {
									case 0:
										echo "Aguardando";
										break;
									case 1:
										echo "Presente";
										break;
									case 2:
										echo "Faltou";
										break;
								}
																			  
								//verificar lista de espera								  
								$listarVagas = listarVagas($conexao, $agenda[ "id_agenda" ]);
								$posicaoNaEspera = 0;
								foreach ( $listarVagas as $listarVagas ) {
									$posicaoNaEspera++;
									if ($_SESSION['usuario']['id'] === $listarVagas['id_usuario'] ){
										$v = $agenda['quantidades'] - $posicaoNaEspera;
										if($v <= 0){
											echo " <small>(Lista de Espera)</small>";
										}
									}
								}
							
							?>
						</td>
						<td><a href="#modal<?=$agenda[ 'id' ]?>" class="modal-trigger"><i class="material-icons amber-text text-darken-1" title="Cancelar">close</i></a></td>
					</tr>

					<!-- Modal Structure -->
					  <div id="modal<?=$agenda[ 'id' ]?>" class="modal">

						<div class="modal-content grey-text text-darken-3">

						  <h4>Cancelar</h4>

						  <p>Deseja realmente cancelar?</p>

						</div>

    					<div class="modal-footer">
						 	<a href="agenda.php?acao=cancelar&token=<?=$agenda[ 'idAgendamentos' ]?>" class="modal-close waves-effect waves-green btn right"> Sim</a>
					 	</div>

					 </div>

					<?php } ?>
				</tbody>
			<?php } ?>
			</table>
		</div>


		<p align="center"><a href="#modal-1" class="Link modal-trigger">Histórico</a></p>
		<!-- Modal Structure -->
	  	<div id="modal-1" class="modal">
			<div class="modal-content">
			  <h4>Histórico</h4>

				<table>
					<thead>
						<tr>
							<th>Atividade</th>
							<th>Endereço</th>
							<th>Data</th>
							<th>Início</th>
							<th>Presença</th>
						</tr>
					</thead>

					<tbody>
						<?php
						$agenda = listaragendadosClienteBD( $conexao, $_SESSION[ 'usuario' ][ 'id' ] );
						foreach ( $agenda as $agenda ) {
							$dataAgendada = date('Y/m/d', strtotime($agenda[ "data" ]));

							if($agenda['presenca'] != 0 ){?>
								<tr>
									<td>
										<?= $agenda[ "nome" ]?>
									</td>
									<td>
										<?= $agenda[ "endereco" ] ?>
									</td>
									<td data-mask="00/00/0000">
										<?= date('d/m/Y', strtotime($agenda[ "data" ]));?>
									</td>
									<td data-mask="00:00">
										<?= $agenda[ "hora" ]?>
									</td>
									<td>
										<?php
											switch ($agenda[ "presenca" ]) {
												case 0:
													echo "Não Registrada";
													break;
												case 1:
													echo "Presente";
													break;
												case 2:
													echo "Faltou";
													break;
												case 5:
													echo "Cancelado";
													break;
											}	?>
									</td>
								</tr>
							<?php } 
							if(empty($agenda['presenca']) ){ 
									echo '<tr>
											<td>
												Sem Histórico
											</td>						
										</tr>';
							}
						} ?>
					</tbody>					
				</table>

			</div>
		  </div>

		<div class="row">
			<div class="col s6 m3 offset-m1" align="center">
				<a href="dashboard.php" class="btn waves-effect waves-light">Voltar</a> </div>
		</div>
		<?php } 
	}

	if ( $_SESSION[ "acao" ][ 'acao' ] === 'visualizar' ){

	$_SESSION[ "acao" ][ 'acao' ] = 'presenca';

	$agenda = listarAgendaidBD($conexao, $_SESSION[ "acao" ][ 'id' ]);

	?>

	<br>
		<div class="container row">

		  <h4><?= $agenda[0][ "nome" ]?></h4>

		  <p><small>Cod.::  <?= $agenda[0][ "id" ] ?></small></p>

		  <p>Data: <?= date('d/m/Y', strtotime($agenda[0][ "data" ]));?> as <?= mask($agenda[0][ "hora" ], '##:##')?></p>
		  <p>Endereço: <?= $agenda[0][ "endereco" ] ?> </p>
		  <p>Descrição:  <?= $agenda[0][ "descricao" ] ?></p>
		  <p>Atividade:  <?php $atividade = listarAtividadeBD($conexao, $agenda[0][ "atividade" ]);
								echo $atividade[0]['nome'];?></p>
		  <p>Abertura das Vagas:  <?= date('d/m/Y', strtotime($agenda[0][ "abertura" ])) ?></p>
		  <p>Empresa:  <?php $empresa = listarUsuarioBD( $conexao, $agenda[0][ "empresa" ] );
								echo $empresa[0]['apelido']?></p>

		  <p>Quantidades de Vagas:  <?= $agenda[0][ "quantidades" ] ?></p>


			<?php $vagas = listarVagas($conexao, $agenda[0]['id']);
				$disponiveis = $agenda[0][ "quantidades" ] - count($vagas);
				echo '<p>Disponives: '. $disponiveis . '</p>';
				$qtd = $agenda[0][ "quantidades" ] - $disponiveis;
		
				if($disponiveis == $agenda[0][ "quantidades" ]){ echo '<p><strong>Não há alunos para lançar presença</strong></p>';}
				else { ?>
					<p align="center">Lista de Aluno: </p>
			 		<form class="col s12" action="controller/agenda.php" method="post" accept-charset="UTF-8">
						<?php foreach ( $vagas as $vagas ) {
							$PessoasAgendadas =  listarUsuarioBD( $conexao, $vagas['id_usuario'] );?>
						   	<div class="col s12 m6 ">

								<div class="input-field col s12">
								  <input id="id" type="text" class="hide" value="<?= $vagas['id']?>" name="id<?= $PessoasAgendadas[0]['id']?>">
								  <label for="id">Matricula: <?= $PessoasAgendadas[0]['id']?></label>
								</div>

								<div class="input-field col s12">
								  <input id="apelido" type="text" class="hide" value="<?= $PessoasAgendadas[0]['apelido']?>" name="apelido<?= $PessoasAgendadas[0]['id']?>">
								  <label for="apelido">Aluno: <?= $PessoasAgendadas[0]['apelido']?></label>
								</div>

								<?php switch ($vagas['presenca']) {
											case 0:
												$lista = 'selected';
												$presente = '';
												$faltou = '';
												break;
											case 1:
												$lista = '';
												$presente = 'selected';
												$faltou = '';
												break;
											case 2:
												$lista = '';
												$presente = '';
												$faltou = 'selected';
												break;
										}  ?>

								<div class="input-field col s12">
									<select name="presenca<?= $PessoasAgendadas[0]['id']?>">
									  <option value="0"  <?= $lista?> selected>Lista de Espera</option>
									  <option value="1" <?= $presente?>>Presente</option>
									  <option value="2" <?= $faltou?>>Faltou</option>
									</select>
									<label>Status:</label>
								</div>
							</div>
						<?php }	?>

				<div class="row">
						<div class="col s12">
							<button class="btn waves-effect waves-light" type="submit" style="width: 100%">Alterar Presença</button>
						</div>
					</div>
    		</form>
				<?php }	?>		  
		</div>
	<?php  }

} 

if ( !empty( $_GET[ 'acao' ] ) ) {

	unset( $_SESSION[ "acao" ] );

	echo $_SESSION[ "acao" ][ 'acao' ]	 	= $_GET[ 'acao' ];
	echo $_SESSION[ "acao" ][ 'id' ] 		= $_GET[ 'token' ];
	echo $_SESSION[ "acao" ][ 'filtro' ] 	= $_GET[ 'filtro' ];
	echo $_SESSION[ "acao" ][ 'tipo' ]		= $_GET[ 'tipo' ];
	
	echo $_SESSION[ "msg" ] 				= $_SESSION[ "msg" ];


	echo '<script>window.location.replace("agenda.php");</script>';	
} 

include_once( 'rodape.php' );
