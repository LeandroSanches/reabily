<?php
error_reporting( E_ALL ^ E_NOTICE );
error_reporting( E_WARNING );
error_reporting( 0 );

session_start();

include_once( 'cabecalho.php' );
include_once( 'model/usuario.php' );

//var_dump($_SESSION[ "acao" ]);
$tipoUsuario = $_SESSION[ "acao" ][ 'id_tipo' ];
$msgBotao = $_SESSION[ "acao" ][ 'msgBotao' ];
$id = $_SESSION[ "acao" ]['id'];

if ( $tipoUsuario == 1 || $tipoUsuario == 2 || $tipoUsuario == 3 ) {
	
	$usuario = listarUsuarioBD( $conexao, $id );	
	$funcionario = listarUsuarioAvancadoBD( $conexao, $id );
	
	switch ($usuario[0]['id_tipo']) {
    case 0:
        $chamar = "Error ";
		$passo2 = "false";
		$passo3 = "false";
        break;
    case 1:
        $chamar = "Administrador ";
		$passo2 = "true";
		$passo3 = "true";
        break;
    case 2:
        $chamar = "Funcionário ";
		$passo2 = "true";
		$passo3 = "true";
        break;
    case 3:
        $chamar = "Empresa ";
		$passo2 = "false";
		$passo3 = "true";
        break;
    case 4:
        $chamar = "Clientes ";
		$passo2 = "false";
		$passo3 = "false";
        break;
}
	
	//var_dump($usuario);
	//var_dump($funcionario);
	
	if(empty($funcionario) ){
		//usuario não tem ddos cadastrados vamos cadastrar
		$msgFuncionario = 'Usuario '.$usuario[0]["apelido"].' não tem dados cadastrados';
		$msgBotao = 'Cadastrar Informações';
		$_SESSION[ "acao" ][ 'acao' ] = 'inseriravan';
	}
	if(!empty($funcionario) ){
		$msgFuncionario = 'Usuario '.$usuario[0]["apelido"].' tem os dados cadastrados';
		$msgBotao = 'Atualizar Informações';
		$_SESSION[ "acao" ][ 'acao' ] = 'atualizaravan';
	}
	?>

	<div class="row container">
		<form action="controller/usuario.php" method="post" enctype="multipart/form-data">
			<div class="row">
				<div class="col s12 m12 l12">					
					<h5><?= $msgdadosbancarios?></h5>
				</div>
			</div>	
					
			
			<div class="row">
				<div class="col s12 m12 l12 center-align PassoAtiv">

					<span style="padding-right: 8px;" class="PassoDes"><i class="material-icons medium">looks_one</i></span> 
					<?php if($passo2 === 'true'){?>
					<span style="padding:0 8px 0 8px;"><i class="material-icons medium">looks_two</i></span> 
					<?php } ?>
					<?php if($passo3 === 'true'){?>
					<span style="padding-left: 8px;" class="PassoDes"><i class="material-icons medium">looks_3</i></span>
					<?php } ?>
				</div>					
			</div>
			
			<div class="row">

				<?php if ( isset($usuario[0]['id'] )) {
					echo '<input class="hide" type="text" name="id_usuario" value="'.$usuario[0]["id"].'">';
				} ?>

				<div class="input-field col s12 m3">
					<input id="salarioContratado" type="number" class="validate" name="salarioContratado" value="<?= $funcionario[0]['salarioContratado']?>">
					<label for="salarioContratado">Salário Contratado</label>
				</div>

				<div class="input-field col s12 m3">
					<input id="cargaHoraria" type="number" class="validate" name="cargaHoraria" value="<?= $funcionario[0]['cargaHoraria']?>">
					<label for="cargaHoraria">Carga Horária</label>
				</div>

				<div class="input-field col s12 m3">
					<input id="nascimeto" type="date" class="validate" name="nascimeto" value="<?= $funcionario[0]['nascimento']?>">
					<label for="nascimeto">Nascimento</label>
				</div>
				
				<div class="input-field col s12 m3 ">
					<input id="naturalidade" type="text" class="validate txt" name="naturalidade" value="<?= $funcionario[0]['naturalidade']?>">
					<label for="naturalidade">Naturalidade</label>
				</div>
				
				<div class="input-field col s12 m3 ">
					<input id="nacionalidade" type="text" class="validate txt" name="nacionalidade" value="<?= $funcionario[0]['nacionalidade']?>">
					<label for="nacionalidade">Nacionalidade</label>
				</div>
					<div class="input-field col s12 m3">
					<select name="estadocivil">
						<?php 
						switch ($funcionario[0]['estadoCivil']) {
							case 1:
								$valor1 = 'selected';
								break;
							case 2:
								$valor2 = 'selected';
								break;
							case 3:
								$valor3 = 'selected';
								break;
							case 4:
								$valor4 = 'selected';
								break;
							case 5:
								$valor5 = 'selected';
								break;
						}
						?>

						<option value="" disabled selected>Escolha um estado</option>										
						<option value="1" <?=$valor1?> > Solteiro(a) </option>
						<option value="2" <?=$valor2?> > Casado(a) </option>
						<option value="3" <?=$valor3?> > Divorciado(a) </option>
						<option value="4" <?=$valor4?> > Viúvo(a) </option>
						<option value="5" <?=$valor5?> > Separado(a) </option>
					</select>
					<label>Estado Civil:</label>
				</div>
			</div>
			
			<div class="row">
				<h5>Contato</h5>
				
				<div class="input-field col s12 m3 ">
					<input id="celular" type="text" class="validate" name="celular" data-mask="(00) 000-000-000" value="<?= $funcionario[0]['celular']?>">
					<label for="celular">Celular</label>
				</div>
				
				<div class="input-field col s12 m3 ">
					<input id="tRecado" type="text" class="validate" name="tRecado" data-mask="(00) 00000-0000" value="<?= $funcionario[0]['tRecado']?>">
					<label for="tRecado">Telefone de Recado</label>
				</div>
				
				<div class="input-field col s12 m3 ">
					<input id="nRecado" type="text" class="validate" name="nRecado" value="<?= $funcionario[0]['nRecado']?>">
					<label for="nRecado">Nome pra Recado</label>
				</div>
			</div>

			<div class="row">
				<h5>Parentesco</h5>
				<div class="input-field col s12 m6 ">
					<input id="pai" type="text" class="validate txt" name="pai" value="<?= $funcionario[0]['nomedoPai']?>">
					<label for="pai">Nome do Pai</label>
				</div>
				<div class="input-field col s12 m6 ">
					<input id="mae" type="text" class="validate txt" name="mae" value="<?= $funcionario[0]['nomedaMae']?>">
					<label for="mae">Nome da Mae</label>
				</div>
				<div class="input-field col s12 m6 ">
					<input id="conjege" type="text" class="validate txt" name="conjege" value="<?= $funcionario[0]['conjege']?>">
					<label for="conjege">Nome do Cônjuge</label>
				</div>
				<div class="input-field col s12 m6 ">
					<input id="filiacao" type="text" class="validate txt" name="filiacao" value="<?= $funcionario[0]['filiacao']?>">
					<label for="filiacao">Filiação</label>
				</div>
			</div>
			
			<div class="row">
				<h5>Documentação</h5>

				<div class="input-field col s12 m3 ">
					<input id="identidade" type="number" class="validate" name="identidade" maxlength="9" value="<?= $funcionario[0]['identidade']?>">
					<label for="identidade">Identidade</label>
				</div>
				<div class="input-field col s12 m3 ">
					<input id="identidadeOrgao" type="text" class="validate" name="identidadeOrgao" maxlength="9" value="<?= $funcionario[0]['identidadeOrgao']?>">
					<label for="identidadeOrgao">Orgão Emissor</label>
				</div>
				<div class="input-field col s12 m3 ">
					<input id="expedicao" type="date" class="validate" name="expedicao" value="<?= $funcionario[0]['datadeExpedicao']?>">
					<label for="expedicao">Data de Expedição</label>
				</div>
				<div class="input-field col s12 m3 ">
					<input id="pis" type="number" class="validate" name="pis" maxlength="11" value="<?= $funcionario[0]['pis']?>">
					<label for="pis">PIS</label>
				</div>
				<div class="input-field col s12 m3 ">
					<input id="ctps" type="number" class="validate" name="ctps" maxlength="8" value="<?= $funcionario[0]['ctps']?>">
					<label for="ctps">CTPS</label>
				</div>
				<div class="input-field col s12 m3 ">
					<input id="eleitor" type="number" class="validate" name="eleitor" maxlength="12" value="<?= $funcionario[0]['titulodeEleitor']?>">
					<label for="eleitor">Titulo de Eleitor</label>
				</div>
				<div class="input-field col s12 m3 ">
					<input id="sessao" type="number" class="validate" name="sessao" maxlength="3" value="<?= $funcionario[0]['sessao']?>">
					<label for="sessao">Sessão</label>
				</div>
				<div class="input-field col s12 m3 ">
					<input id="zona" type="number" class="validate" name="zona" maxlength="3" value="<?= $funcionario[0]['zona']?>">
					<label for="zona">Zona</label>
				</div>
				<div class="input-field col s12 m3 ">
					<input id="passaporte" type="number" class="validate" name="passaporte" maxlength="10" value="<?= $funcionario[0]['passaporte']?>">
					<label for="passaporte">Passaporte</label>
				</div>
				<div class="input-field col s12 m3 ">
					<input id="reservista" type="number" class="validate" name="reservista" maxlength="10" value="<?= $funcionario[0]['reservista']?>">
					<label for="reservista">Reservista</label>
				</div>
				<div class="input-field col s12 m3 ">
					<textarea id="registroprofissional" class="materialize-textarea" name="registroprofissional" maxlength="10"><?= $funcionario[0]['registroprofissional']?></textarea>
					<label for="registroprofissional">Registro Profissional</label>
				</div>
				<div class="input-field col s12 ">
					<textarea id="textarea1" class="materialize-textarea" name="observacoesdocumento"><?= $funcionario[0]['observacoesdocumento']?></textarea>
					<label for="textarea1">Observações</label>
				</div>
				

			</div>
			
			<div class="row">
				<h5>Profissional</h5>
				
				<div class="input-field col s12 m3 ">
					<select name="formacao">
					<?php 
						switch ($funcionario[0]['formacao']) {
							case 1:
								$valor1 = 'selected';
								break;
							case 2:
								$valor2 = 'selected';
								break;
							case 3:
								$valor3 = 'selected';
								break;
							case 4:
								$valor4 = 'selected';
								break;
							case 5:
								$valor5 = 'selected';
								break;
						}
						?>

						<option value="" disabled selected>Escolha uma opção</option>										
						<option value="1" <?=$valor1?> > Fundamental I </option>
						<option value="2" <?=$valor2?> > Fundamental II  </option>
						<option value="3" <?=$valor3?> > Ensino Medio - Incompleto </option>
						<option value="4" <?=$valor3?> > Ensino Medio - Completo </option>
						<option value="5" <?=$valor3?> > Graduação - Incompleto</option>
						<option value="6" <?=$valor3?> > Graduação - Completo </option>
						<option value="7" <?=$valor3?> > Graduação MBA	 </option>						
						<option value="8" <?=$valor3?> > Pós-graduação </option>
						<option value="9" <?=$valor4?> > Mestrado </option>
						<option value="10" <?=$valor5?> > Dotorado </option>
						<option value="11" <?=$valor6?> > PHD </option>
					</select>
					<label for="formacao">Formação</label>
				</div>
				<div class="input-field col s12 m6 ">
					<input id="qualificacao" type="text" class="validate" name="qualificacao" value="<?= $funcionario[0]['qualificacao']?>">
					<label for="qualificacao">Qualificação</label>
				</div>				
				<div class="input-field col s12 m3">
					<select name="contrato">
					<?php 	switch ($funcionario[0]['id_contratos']) {
								case 1:		
									$Tercerizado = " ";
									$Parceiro = " ";
									$Horista = " ";
									$Temporario = " ";
									$CLT = "selected ";
									break;
								case 2:									
									$CLT = " ";
									$Parceiro = " ";
									$Horista = " ";
									$Temporario = " ";
									$Tercerizado = "selected ";
									break;
								case 3:									
									$CLT = " ";
									$Tercerizado = " ";
									$Horista = " ";
									$Temporario = " ";
									$Parceiro = "selected ";
									break;
								case 4:									
									$CLT = " ";
									$Tercerizado = " ";
									$Parceiro = " ";
									$Temporario = " ";
									$Horista = "selected ";
									break;
								case 5:
									$CLT = " ";
									$Tercerizado = " ";
									$Parceiro = " ";
									$Horista = " ";
									$Temporario = "selected ";
									break;
							}
						?>
						<option value="0" disabled selected>Escolha uma opção</option>										
						<option value="1" <?=$CLT?> > CLT </option>
						<option value="2" <?=$Tercerizado?> > Tercerizado </option>
						<option value="3" <?=$Parceiro?> > Parceiro </option>
						<option value="4" <?=$Horista?> > Horista </option>
						<option value="5" <?=$Temporario?> > Temporario </option>
					</select>
					<label>Contrato</label>
				</div>
				<div class="input-field col s12 m6 ">
					<input id="funcao" type="text" class="validate" name="funcao" value="<?= $funcionario[0]['funcao']?>">
					<label for="funcao">Função</label>
				</div>
				<div class="input-field col s12 m6 ">
					<input id="admissao" type="date" class="validate" name="admissao" value="<?= $funcionario[0]['datadeAdmissão']?>">
					<label for="admissao">Admissão</label>
				</div>
				<div class="input-field col s12 m3 ">
					<input id="desligamento" type="date" class="validate" name="desligamento" value="<?= $funcionario[0]['datadeDesligamento']?>">
					<label for="desligamento">Desligamento</label>
				</div>
				<div class="input-field col s12 m9">
					<textarea id="textarea2" class="materialize-textarea" name="motivo"><?= $funcionario[0]['motivo']?></textarea>
					<label for="textarea2">Motivo do desligamento</label>
				</div>				
				
				<div class="input-field col s12 ">
					<textarea id="textarea3" class="materialize-textarea" name="observacoes_profissionais"><?= $funcionario[0]['observacoes']?></textarea>
					<label for="textarea3">Observações</label>
				</div>
				
			</div>
			
			<div class="row">
				<h5>Benefícios</h5>
				
				<div class="input-field col s12 m3 ">
					<select name="transporte">
					<?php 	if($funcionario[0]['transporte'] == '1'){$valorsim = 'selected';$valornao = '';}
							if($funcionario[0]['transporte'] == '2'){$valornao = 'selected';$valorsim = '';}
							if($funcionario[0]['transporte'] !== '1' && 
							   $funcionario[0]['transporte'] !== '2') {$valorsim = ''; $valornao = '';}?>
							   
						<option value="" disabled selected>Escolha uma opção</option>										
						<option value="1" <?=$valorsim?> > Sim </option>
						<option value="2" <?=$valornao?> > Não </option>
					</select>
					<label>Vale Transporte</label>
				</div>
				<div class="input-field col s12 m3 ">
					<select name="alimentacao">
					
					<?php 	if($funcionario[0]['alimentacao'] == '1'){$valorsim = 'selected';$valornao = '';}
							if($funcionario[0]['alimentacao'] == '2'){$valornao = 'selected';$valorsim = '';}
							if($funcionario[0]['alimentacao'] !== '1' && 
							   $funcionario[0]['alimentacao'] !== '2') {$valorsim = ''; $valornao = '';}?>
							   
						<option value="" disabled selected>Escolha uma opção</option>										
						<option value="1" <?=$valorsim?> > Sim </option>
						<option value="2" <?=$valornao?> > Não </option>
					</select>
					<label for="alimentacao">Vale Alimentação</label>
				</div>
				<div class="input-field col s12 m3 ">
					<select name="planodeSaude">
					<?php 
						switch ($funcionario[0]['planodeSaude']) {
							case 1:
								$valor1 = 'selected';
								break;
							case 2:
								$valor2 = 'selected';
								break;
							case 3:
								$valor3 = 'selected';
								break;
							case 4:
								$valor4 = 'selected';
								break;
							case 5:
								$valor5 = 'selected';
								break;
						}
						?>
						<option value="" disabled selected>Escolha uma opção</option>										
						<option value="1" <?=$valor1?> > Não </option>
						<option value="2" <?=$valor2?> > Plano I </option>
						<option value="3" <?=$valor3?> > Plano II </option>
						<option value="4" <?=$valor4?> > Plano II </option>
					</select>
					<label>Plano de Saúde</label>
				</div>
				<div class="input-field col s12 m3 ">
					<select name="planoOdontologico">					
					
					<?php 	if($funcionario[0]['planoOdontologico'] == '1'){$valorsim = 'selected';$valornao = '';}
							if($funcionario[0]['planoOdontologico'] == '2'){$valornao = 'selected';$valorsim = '';}
							if($funcionario[0]['planoOdontologico'] !== '1' && 
							   $funcionario[0]['planoOdontologico'] !== '2') {$valorsim = ''; $valornao = '';}?>
					
						<option value="" disabled selected>Escolha uma opção</option>										
						<option value="1" <?=$valorsim?> > Sim </option>
						<option value="2" <?=$valornao?> > Não </option>
					</select>
					<label for="planoOdontologico">Plano Odontologico</label>
				</div>
				
			</div>
				
			<div class="row">
				<div class="col s6 m3 offset-m1" align="center">
					<a href="usuario.php?acao=alterar&token=<?=$usuario[0]["id"]?>" class="btn waves-effect waves-light">Voltar</a> </div>
				<div class="col s6 m6">
					<button class="btn2 waves-effect waves-light right" type="submit">
						<?= $msgBotao?>
					</button>
				</div>
			</div>
		</form>
	</div>

	<?php
} else {
	echo "<h2>Não disponivel para o perfil de usuário</h2>";
}
include_once('rodape.php'); ?>